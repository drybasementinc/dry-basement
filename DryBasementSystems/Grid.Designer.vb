﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Grid
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.txtCol = New System.Windows.Forms.TextBox
    Me.txtRow = New System.Windows.Forms.TextBox
    Me.txtBottom_Row = New System.Windows.Forms.TextBox
    Me.txtTop_Row = New System.Windows.Forms.TextBox
    Me.txtRight_Col = New System.Windows.Forms.TextBox
    Me.txtLeft_Col = New System.Windows.Forms.TextBox
    Me.SuspendLayout()
    '
    'txtCol
    '
    Me.txtCol.Location = New System.Drawing.Point(3, 0)
    Me.txtCol.Name = "txtCol"
    Me.txtCol.Size = New System.Drawing.Size(100, 20)
    Me.txtCol.TabIndex = 0
    Me.txtCol.Tag = "SKIP"
    Me.txtCol.Visible = False
    '
    'txtRow
    '
    Me.txtRow.Location = New System.Drawing.Point(106, 0)
    Me.txtRow.Name = "txtRow"
    Me.txtRow.Size = New System.Drawing.Size(100, 20)
    Me.txtRow.TabIndex = 1
    Me.txtRow.Tag = "SKIP"
    Me.txtRow.Visible = False
    '
    'txtBottom_Row
    '
    Me.txtBottom_Row.Location = New System.Drawing.Point(106, 52)
    Me.txtBottom_Row.Name = "txtBottom_Row"
    Me.txtBottom_Row.Size = New System.Drawing.Size(100, 20)
    Me.txtBottom_Row.TabIndex = 3
    Me.txtBottom_Row.Tag = "SKIP"
    Me.txtBottom_Row.Visible = False
    '
    'txtTop_Row
    '
    Me.txtTop_Row.Location = New System.Drawing.Point(106, 26)
    Me.txtTop_Row.Name = "txtTop_Row"
    Me.txtTop_Row.Size = New System.Drawing.Size(100, 20)
    Me.txtTop_Row.TabIndex = 2
    Me.txtTop_Row.Tag = "SKIP"
    Me.txtTop_Row.Visible = False
    '
    'txtRight_Col
    '
    Me.txtRight_Col.Location = New System.Drawing.Point(3, 52)
    Me.txtRight_Col.Name = "txtRight_Col"
    Me.txtRight_Col.Size = New System.Drawing.Size(100, 20)
    Me.txtRight_Col.TabIndex = 5
    Me.txtRight_Col.Tag = "SKIP"
    Me.txtRight_Col.Visible = False
    '
    'txtLeft_Col
    '
    Me.txtLeft_Col.Location = New System.Drawing.Point(3, 26)
    Me.txtLeft_Col.Name = "txtLeft_Col"
    Me.txtLeft_Col.Size = New System.Drawing.Size(100, 20)
    Me.txtLeft_Col.TabIndex = 4
    Me.txtLeft_Col.Tag = "SKIP"
    Me.txtLeft_Col.Visible = False
    '
    'Grid
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoScroll = True
    Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
    Me.Controls.Add(Me.txtRight_Col)
    Me.Controls.Add(Me.txtLeft_Col)
    Me.Controls.Add(Me.txtBottom_Row)
    Me.Controls.Add(Me.txtTop_Row)
    Me.Controls.Add(Me.txtRow)
    Me.Controls.Add(Me.txtCol)
    Me.Name = "Grid"
    Me.Size = New System.Drawing.Size(811, 512)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents txtCol As System.Windows.Forms.TextBox
  Friend WithEvents txtRow As System.Windows.Forms.TextBox
  Friend WithEvents txtBottom_Row As System.Windows.Forms.TextBox
  Friend WithEvents txtTop_Row As System.Windows.Forms.TextBox
  Friend WithEvents txtRight_Col As System.Windows.Forms.TextBox
  Friend WithEvents txtLeft_Col As System.Windows.Forms.TextBox

End Class
