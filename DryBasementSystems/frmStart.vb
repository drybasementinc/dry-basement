﻿Public Class frmStart
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form Specific Declarations

  Private Sub cmdLead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLead.Click
    Try
      frmLead.Close()
    Catch ex As Exception
      'do nothing
    End Try
    frmLead.valNew = True
    frmLead.Show(Me)
  End Sub

  Private Sub frmStart_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    Close_DB()
  End Sub

  Private Sub frmStart_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    ' Handle Unhandled exceptions
    Dim currentDomain As AppDomain = AppDomain.CurrentDomain
    AddHandler currentDomain.UnhandledException, AddressOf HandleUnhandledException

    Open_DB()
    datDate = Now
    frmSchedule.Show()
    frmSchedule.Hide()
    frmSchedule.timRefresh.Enabled = False
    frmLogIn.ShowDialog()
    timAnnouce_Tick(Me, e)

  End Sub

  Private Sub cmdDispo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDispo.Click
    datDispo = Calendar_Date(Me, Now.Date)
    If datDispo = Nothing Then Exit Sub
    frmDispo.Show()
  End Sub

  Private Sub cmdRay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRay.Click
    ''Admin.Show()
  End Sub

  Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
    frmSearch.Show()
  End Sub

  Private Sub cmdReports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReports.Click
    rptSalesReport.Show()
  End Sub

  Private Sub cmdSchedules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSchedules.Click
    For Each Control In fraSelection.Controls
      Control.visible = False
    Next
    fraSchedules.Visible = True
  End Sub

  Private Sub cmdEstSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEstSchedule.Click
    datDate = Now
    frmSchedule.valSchedule = "estimators"
    frmSchedule.Schedule_Maker("estimators")
    frmSchedule.ShowInTaskbar = True
    frmSchedule.WindowState = FormWindowState.Maximized
    frmSchedule.Show()
  End Sub

  Private Sub cmdProduction_Schedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProduction_Schedule.Click
    frmProduction_Schedule.Tag = "DRY"
    frmProduction_Schedule.Show()
  End Sub

  Private Sub cmdMaintenance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMaintenance.Click
    Try
      frmUp_Keep.Show()
    Catch ex As Exception
    End Try
  End Sub

  Private Sub cmdPreJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPreJob.Click
    datDate = Now
    frmSchedule.valSchedule = "prejob"
    frmSchedule.Schedule_Maker("prejob")
    frmSchedule.ShowInTaskbar = True
    frmSchedule.WindowState = FormWindowState.Maximized
    frmSchedule.Show()
  End Sub

  Private Sub cmdAnnual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAnnual.Click
    datDate = Now
    frmSchedule.valSchedule = "annual"
    frmSchedule.Schedule_Maker("annual")
    frmSchedule.ShowInTaskbar = True
    frmSchedule.WindowState = FormWindowState.Maximized
    frmSchedule.Show()
  End Sub

  Private Sub cmdPlumbing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPlumbing.Click
    datDate = Now
    frmSchedule.valSchedule = "plumbing"
    frmSchedule.Schedule_Maker("plumbing")
    frmSchedule.ShowInTaskbar = True
    frmSchedule.WindowState = FormWindowState.Maximized
    frmSchedule.Show()
  End Sub

  Private Sub cmdCallList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCallList.Click
    frmCall_List.Show()
  End Sub

  Private Sub txtAnnoucements_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtAnnoucements.MouseDoubleClick
    strResponse = MsgBox("Would you like to add an announcement?", vbYesNo)
    If strResponse = vbNo Then Exit Sub
    dtpExpire.Value = Now.Date
    txtAnnoucement.Text = ""
    fraAnnouce.Visible = True
  End Sub

  Private Sub txtAnnoucements_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAnnoucements.TextChanged

  End Sub

  Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
    Manual_Update("Insert into", "Announcements", "(Expire, Announce, [Date]) Values( " & FilterByDate(dtpExpire.Value.Date) & ", '" & txtAnnoucement.Text & "', " & FilterByDate(Now.Date) & ")")
    fraAnnouce.Visible = False
  End Sub

  Private Sub timAnnouce_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timAnnouce.Tick
    Dim Annouce As New DataTable

    'Connect("Announcements", Announce, 0, "Expire >= " & FilterByDate(Now.Date), "ID DESC")

    txtAnnoucements.Text = ""
    For intCounter As Integer = 0 To Annouce.Rows.Count - 1
      txtAnnoucements.Text += Annouce.Rows(intCounter).Item("Date") & " " & Annouce.Rows(intCounter).Item("Announce") & Chr(13) + Chr(10) + Chr(13) + Chr(10)
    Next
  End Sub

  Private Sub cmdInventory_Click(sender As System.Object, e As System.EventArgs) Handles cmdInventory.Click
    frmInventory.Show()
  End Sub

  Private Sub cmdInventoryReport_Click(sender As Object, e As EventArgs) Handles cmdInventoryReport.Click
    frmInventoryReport.Show()
  End Sub

  Private Sub HandleUnhandledException(sender As Object, args As System.UnhandledExceptionEventArgs)

    Try
      Dim ex As Exception = DirectCast(args.ExceptionObject, Exception)

      LogException(ex)

    Catch ex2 As Exception
      'do nothing
    End Try

  End Sub


End Class
