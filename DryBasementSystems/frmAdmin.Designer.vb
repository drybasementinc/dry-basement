﻿''<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
''Partial Class Admin
''    Inherits System.Windows.Forms.Form

''    'Form overrides dispose to clean up the component list.
''    <System.Diagnostics.DebuggerNonUserCode()> _
''    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
''        Try
''            If disposing AndAlso components IsNot Nothing Then
''                components.Dispose()
''            End If
''        Finally
''            MyBase.Dispose(disposing)
''        End Try
''    End Sub

''    'Required by the Windows Form Designer
''    Private components As System.ComponentModel.IContainer

''    'NOTE: The following procedure is required by the Windows Form Designer
''    'It can be modified using the Windows Form Designer.  
''    'Do not modify it using the code editor.
''    <System.Diagnostics.DebuggerStepThrough()> _
''    Private Sub InitializeComponent()
''    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Admin))
''    Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("KCTV                         50,000")
''    Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("TIME WARNER         50,000")
''    Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("TV                                   100,000", New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2})
''    Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("KMBC                         50,000")
''    Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Radio                                50,000", New System.Windows.Forms.TreeNode() {TreeNode4})
''    Me.cmdImport = New System.Windows.Forms.Button()
''    Me.cmdFormat = New System.Windows.Forms.Button()
''    Me.Test = New System.Windows.Forms.TextBox()
''    Me.Button1 = New System.Windows.Forms.Button()
''    Me.cmdIce_Email = New System.Windows.Forms.Button()
''    Me.txtBody = New System.Windows.Forms.TextBox()
''    Me.TreeView1 = New System.Windows.Forms.TreeView()
''    Me.chtTest = New AxMSChart20Lib.AxMSChart()
''    Me.lblprogress = New System.Windows.Forms.Label()
''    CType(Me.chtTest, System.ComponentModel.ISupportInitialize).BeginInit()
''    Me.SuspendLayout()
''    '
''    'cmdImport
''    '
''    Me.cmdImport.Location = New System.Drawing.Point(10, 6)
''    Me.cmdImport.Name = "cmdImport"
''    Me.cmdImport.Size = New System.Drawing.Size(71, 24)
''    Me.cmdImport.TabIndex = 0
''    Me.cmdImport.Text = "Import"
''    Me.cmdImport.UseVisualStyleBackColor = True
''    '
''    'cmdFormat
''    '
''    Me.cmdFormat.Location = New System.Drawing.Point(10, 36)
''    Me.cmdFormat.Name = "cmdFormat"
''    Me.cmdFormat.Size = New System.Drawing.Size(71, 36)
''    Me.cmdFormat.TabIndex = 1
''    Me.cmdFormat.Text = "Data Format"
''    Me.cmdFormat.UseVisualStyleBackColor = True
''    '
''    'Test
''    '
''    Me.Test.Location = New System.Drawing.Point(200, 7)
''    Me.Test.Name = "Test"
''    Me.Test.Size = New System.Drawing.Size(70, 20)
''    Me.Test.TabIndex = 2
''    Me.Test.Visible = False
''    '
''    'Button1
''    '
''    Me.Button1.Location = New System.Drawing.Point(10, 78)
''    Me.Button1.Name = "Button1"
''    Me.Button1.Size = New System.Drawing.Size(71, 36)
''    Me.Button1.TabIndex = 3
''    Me.Button1.Text = "Dispo Format"
''    Me.Button1.UseVisualStyleBackColor = True
''    '
''    'cmdIce_Email
''    '
''    Me.cmdIce_Email.Location = New System.Drawing.Point(10, 120)
''    Me.cmdIce_Email.Name = "cmdIce_Email"
''    Me.cmdIce_Email.Size = New System.Drawing.Size(71, 36)
''    Me.cmdIce_Email.TabIndex = 4
''    Me.cmdIce_Email.Text = "Ice Email"
''    Me.cmdIce_Email.UseVisualStyleBackColor = True
''    '
''    'txtBody
''    '
''    Me.txtBody.Location = New System.Drawing.Point(105, 66)
''    Me.txtBody.Multiline = True
''    Me.txtBody.Name = "txtBody"
''    Me.txtBody.Size = New System.Drawing.Size(708, 364)
''    Me.txtBody.TabIndex = 5
''    Me.txtBody.Text = resources.GetString("txtBody.Text")
''    '
''    'TreeView1
''    '
''    Me.TreeView1.Location = New System.Drawing.Point(626, 12)
''    Me.TreeView1.Name = "TreeView1"
''    TreeNode1.Name = "Node1"
''    TreeNode1.Text = "KCTV                         50,000"
''    TreeNode2.Name = "Node4"
''    TreeNode2.Text = "TIME WARNER         50,000"
''    TreeNode3.Name = "nodTV"
''    TreeNode3.Text = "TV                                   100,000"
''    TreeNode4.Name = "Node6"
''    TreeNode4.Text = "KMBC                         50,000"
''    TreeNode5.Name = "Node5"
''    TreeNode5.Text = "Radio                                50,000"
''    Me.TreeView1.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode3, TreeNode5})
''    Me.TreeView1.Size = New System.Drawing.Size(225, 60)
''    Me.TreeView1.TabIndex = 6
''    '
''    'chtTest
''    '
''    Me.chtTest.DataSource = Nothing
''    Me.chtTest.Location = New System.Drawing.Point(87, 78)
''    Me.chtTest.Name = "chtTest"
''    Me.chtTest.OcxState = CType(resources.GetObject("chtTest.OcxState"), System.Windows.Forms.AxHost.State)
''    Me.chtTest.Size = New System.Drawing.Size(754, 396)
''    Me.chtTest.TabIndex = 7
''    '
''    'lblprogress
''    '
''    Me.lblprogress.AutoSize = True
''    Me.lblprogress.Location = New System.Drawing.Point(309, 22)
''    Me.lblprogress.Name = "lblprogress"
''    Me.lblprogress.Size = New System.Drawing.Size(39, 13)
''    Me.lblprogress.TabIndex = 8
''    Me.lblprogress.Text = "Label1"
''    '
''    'Admin
''    '
''    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
''    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
''    Me.ClientSize = New System.Drawing.Size(863, 486)
''    Me.Controls.Add(Me.lblprogress)
''    Me.Controls.Add(Me.txtBody)
''    Me.Controls.Add(Me.chtTest)
''    Me.Controls.Add(Me.TreeView1)
''    Me.Controls.Add(Me.cmdIce_Email)
''    Me.Controls.Add(Me.Button1)
''    Me.Controls.Add(Me.Test)
''    Me.Controls.Add(Me.cmdFormat)
''    Me.Controls.Add(Me.cmdImport)
''    Me.Name = "Admin"
''    Me.Tag = "test"
''    Me.Text = "Admin"
''    CType(Me.chtTest, System.ComponentModel.ISupportInitialize).EndInit()
''    Me.ResumeLayout(False)
''    Me.PerformLayout()

''  End Sub
''  Friend WithEvents cmdImport As System.Windows.Forms.Button
''  Friend WithEvents cmdFormat As System.Windows.Forms.Button
''  Friend WithEvents Test As System.Windows.Forms.TextBox
''  Friend WithEvents Button1 As System.Windows.Forms.Button
''  Friend WithEvents cmdIce_Email As System.Windows.Forms.Button
''  Friend WithEvents txtBody As System.Windows.Forms.TextBox
''  Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
''  Friend WithEvents chtTest As AxMSChart20Lib.AxMSChart
''  Friend WithEvents lblprogress As System.Windows.Forms.Label
''End Class
