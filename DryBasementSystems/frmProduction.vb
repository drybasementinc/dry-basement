﻿Public Class frmProduction
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  Dim Info As New DataTable
  Dim Inventory As New DataTable
  Dim drInventory As Double = 0

  Private Sub frmProduction_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    Control_Purge(tpgDry)
    Control_Purge(tpgMagic)
  End Sub

  Private Sub Control_Purge(ByVal owner As Object)
    For Each Control As Object In owner.Controls
      If Control.haschildren = True Then
        While Control.controls.count > 0
          Control.controls(0).dispose()
        End While
        Control.dispose()
        Control_Purge(owner)
        Exit Sub
      End If
    Next
  End Sub

  Private Sub Production_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Dim Dummy As New DataTable
    Dim drDummy As Double
    Dim intTop As Integer
    Dim intLeft As Integer
    Dim intFrameTop As Integer
    Dim intCompany As Integer
    Dim strCompany As String
    Dim Older As GroupBox

    Connect("Users", General, drGeneral, , "Name")
    drGeneral = 0
    While drGeneral < General.Rows.Count
      cboAnchorPerson.Items.Add(General.Rows(drGeneral).Item("Name"))
      cboCertPerson.Items.Add(General.Rows(drGeneral).Item("Name"))
      cboDigRitePerson.Items.Add(General.Rows(drGeneral).Item("Name"))
      cboFinancePerson.Items.Add(General.Rows(drGeneral).Item("Name"))
      cboHHPerson.Items.Add(General.Rows(drGeneral).Item("Name"))
      cboPermitsPerson.Items.Add(General.Rows(drGeneral).Item("Name"))
      cboPreJobPerson.Items.Add(General.Rows(drGeneral).Item("Name"))
      cboWaterPerson.Items.Add(General.Rows(drGeneral).Item("Name"))
      drGeneral = drGeneral + 1
    End While

    If strUserName = "Ray" Then fraValues.Visible = True
    Connect("Dispo_Categories", General, drGeneral, , "Company, Placement")
    drGeneral = 0

    intFrameTop = 0
    intCompany = 0
    strCompany = General.Rows(drGeneral).Item("Company")
    For drGeneral As Double = 0 To General.Rows.Count - 1
      'gets a count on how many products will be in the group box
      Connect("Products", Dummy, drDummy, "Category = '" & General.Rows(drGeneral).Item("Category") & "'", "Active,Placement,Product")
      If General.Rows(drGeneral).Item("Company") <> strCompany Then
        strCompany = General.Rows(drGeneral).Item("Company")
        intCompany = 0
        intFrameTop = 0
      End If

      Dim frame As New GroupBox
      frame.Name = Replace(General.Rows(drGeneral).Item("Category"), " ", "_")
      frame.Text = General.Rows(drGeneral).Item("Category")
      frame.Width = 140 + (Int(Int(Dummy.Rows.Count - 1) / 5) * 140)
      frame.Height = 150
      Older = Nothing
      If intCompany > 0 Then
        Older = tabProduction.Controls("tpg" & General.Rows(drGeneral).Item("Company")).controls(Replace(General.Rows(drGeneral - 1).Item("Category"), " ", "_"))
        If Older.Left + Older.Width + frame.Width > Older.Parent.Width - 10 Then
          intFrameTop = intFrameTop + 1
        End If
      End If
      frame.Top = 75 + (155 * intFrameTop)
      If intCompany > 0 Then
        If frame.Top = Older.Top Then
          frame.Left = Older.Left + Older.Width + 10
        Else
          frame.Left = 5
        End If
      Else
        frame.Left = 5
      End If
      tabProduction.Controls("tpg" & General.Rows(drGeneral).Item("Company")).controls.add(frame)
      intTop = 0
      intLeft = 0
      For drDummy = 0 To Dummy.Rows.Count - 1
        Dim text As New TextBox
        Dim label As New Label
        text.Name = "txt" & Replace(Dummy.Rows(drDummy).Item("Product_Field"), " ", "")
        text.Width = 30
        text.Top = text.Height + (intTop * text.Height) + 10
        text.Left = 100 + (intLeft * (text.Width + 100))
        text.Tag = Dummy.Rows(drDummy).Item("Company")
        text.ReadOnly = True
        text.BackColor = Color.White
        If Dummy.Rows(drDummy).Item("Active") = False Then
          text.Enabled = False
        End If
        frame.Controls.Add(text)
        text.Visible = True
        label.Name = "lbl" & Replace(Dummy.Rows(drDummy).Item("Product_Field"), " ", "")
        label.Text = Replace(Dummy.Rows(drDummy).Item("Product"), "_", " ")
        label.Tag = Dummy.Rows(drDummy).Item("Abbr")
        label.Top = label.Height + (intTop * label.Height) + 5
        label.Width = 90
        label.Left = 5 + (intLeft * (label.Width + text.Width)) + (10 * intLeft)
        frame.Controls.Add(label)
        label.Show()
        intTop = intTop + 1
        If intTop > 4 Then
          intTop = 0
          intLeft = intLeft + 1
        End If
        intCompany = intCompany + 1
      Next
      frame.Visible = True
    Next

    Connect("Dry_Production", DryProduction, drDryProduction, "Address_ID = " & dblAddressID, "Appt_Date DESC")
    drDryDispo = 0
    Connect("Magic_Production", MagicProduction, drMagicProduction, "Address_ID = " & dblAddressID, "Appt_Date DESC")
    drMagicDispo = 0
    'If DryDispo.Rows.Count = 0 Then flgDryUsed = True
    'If MagicDispo.Rows.Count = 0 Then flgMagicUsed = True

    grdProduction.Rows = DryProduction.Rows.Count + MagicProduction.Rows.Count + 1
    grdProduction.Cols = 4
    grdProduction.Create(400, 25, Me)
    grdProduction.ColWidth(0, 80)
    grdProduction.ColWidth(1, 110)
    grdProduction.Cell(0, 0).backcolor = Color.LightGray
    grdProduction.Cell(0, 0).text = "APPT DATE"
    grdProduction.Cell(0, 1).text = "ESTIMATOR"
    grdProduction.Cell(0, 2).text = "DRY"
    grdProduction.Cell(0, 3).text = "MAGIC"
    grdProduction.CellBorder(0, 0).topborder = True
    grdProduction.CellBorder(0, 0).bottomborder = True
    grdProduction.CellBorder(0, 0).leftborder = True
    grdProduction.CellBorder(0, 0).rightborder = True
    grdProduction.ApplyRowFormat(0, 0)
    For drDryproduction As Double = 0 To DryProduction.Rows.Count - 1
      Try
        grdProduction.Cell(drDryproduction + 1, 0).text = DryProduction.Rows(drDryproduction).Item("Appt_Date")
        grdProduction.Cell(drDryproduction + 1, 1).text = DryProduction.Rows(drDryproduction).Item("Estimator")
        grdProduction.Cell(drDryproduction + 1, 2).text = DryProduction.Rows(drDryproduction).Item("Print_Installed")
        grdProduction.Cell(drDryproduction + 1, 2).tag = DryProduction.Rows(drDryproduction).Item("ID")
        grdProduction.Cell(drDryproduction + 1, 3).tag = "0"
        grdProduction.Cell(drDryproduction + 1, 0).backcolor = Color.White
        grdProduction.ApplyRowFormat(drDryproduction + 1, 0)
      Catch ex As Exception
      End Try
    Next

    For drMagicproduction As Double = 0 To MagicProduction.Rows.Count - 1
      For r As Integer = 1 To grdProduction.Rows - 1
        If grdProduction.Cell(r, 0).text = "" Then
          grdProduction.Cell(r, 0).text = MagicProduction.Rows(drMagicproduction).Item("Appt_Date")
          grdProduction.Cell(r, 1).text = MagicProduction.Rows(drMagicproduction).Item("Estimator")
          grdProduction.Cell(r, 3).text = MagicProduction.Rows(drMagicproduction).Item("Print_Offer")
          grdProduction.Cell(r, 3).tag = MagicProduction.Rows(drMagicproduction).Item("ID")
          grdProduction.Cell(r, 2).tag = "0"
          grdProduction.Cell(r, 0).backcolor = Color.White
          grdProduction.ApplyRowFormat(drDryProduction + 1, 0)
          r = grdProduction.Rows
        ElseIf grdProduction.Cell(r, 0).text = MagicProduction.Rows(drMagicproduction).Item("Appt_Date") Then
          grdProduction.Cell(r, 3).text = MagicProduction.Rows(drMagicproduction).Item("Print_Installed") & ""
          r = grdProduction.Rows
        End If
      Next
    Next

    For r As Integer = 1 To grdProduction.Rows - 1
      If grdProduction.Cell(r, 0).text = "" Then
        For c As Integer = 0 To grdProduction.Cols - 1
          grdProduction.Cell(r, c).visible = False
        Next
      End If
    Next
  End Sub

  Private Sub Production_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    On Error Resume Next
    tabProduction.Width = Me.Width - 25
    tabProduction.Height = Me.Height - tabProduction.Top - 40
    grdProduction.Width = Me.Width - 25
    lblDryCover.Width = tpgDry.Width - 5
    lblDryCover.Height = tpgDry.Height - 5

  End Sub

  Private Sub grdProduction_CellDoubleClick(ByVal sender As Object, ByVal Owner As Object, ByVal CurrentRow As Double, ByVal CurrentCol As Double) Handles grdProduction.CellDoubleClick
    If grdProduction.Valid_Cell(sender) = False Then Exit Sub

    Production_Clear()
    Connect("Dry_Production", DryProduction, drDryProduction, "ID = " & grdProduction.Cell(CurrentRow, 2).tag & "")
    drDryProduction = 0
    Connect("Magic_Production", MagicProduction, drMagicProduction, "ID = " & grdProduction.Cell(CurrentRow, 3).tag & "")
    drMagicProduction = 0

    If DryProduction.Rows.Count > 0 Then
      Connect("Production_Info", Info, 0, "ID = " & DryProduction.Rows(drDryProduction).Item("Info_ID"))
    ElseIf MagicProduction.Rows.Count > 0 Then
      Connect("Production_Info", Info, 0, "ID = " & MagicProduction.Rows(drMagicProduction).Item("Info_ID"))
    End If

    If DryProduction.Rows.Count > 0 Then
      Dry_Load()
      lblDryCover.Visible = False
    Else
      lblDryCover.Visible = True
    End If

    If MagicProduction.Rows.Count > 0 Then
      Magic_Load()
      lblMagicCover.Visible = False
    Else
      lblMagicCover.Visible = True
    End If
  End Sub

  Private Sub grdProduction_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdProduction.Disposed

  End Sub

  Private Sub grdProduction_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdProduction.Load

  End Sub

  Private Sub Production_Clear()
    Dim int1 As Integer

    txtDryPrice.Text = ""
    For i As Integer = 0 To tpgDry.Controls.Count - 1
      If TypeOf (tpgDry.Controls(i)) Is TextBox Then
        tpgDry.Controls(i).Text = ""
      ElseIf tpgDry.Controls(i).HasChildren = True Then
        For int1 = 0 To tpgDry.Controls(i).Controls.Count - 1
          If TypeOf (tpgDry.Controls(i).Controls(int1)) Is TextBox Then
            tpgDry.Controls(i).Controls(int1).Text = ""
          End If
        Next
      End If
    Next

    txtMagicPrice.Text = ""
    For i As Integer = 0 To tpgMagic.Controls.Count - 1
      If TypeOf (tpgMagic.Controls(i)) Is TextBox Then
        tpgMagic.Controls(i).Text = ""
      ElseIf tpgMagic.Controls(i).HasChildren = True Then
        For int1 = 0 To tpgMagic.Controls(i).Controls.Count - 1
          If TypeOf (tpgMagic.Controls(i).Controls(int1)) Is TextBox Then
            tpgMagic.Controls(i).Controls(int1).Text = ""
          End If
        Next
      End If
    Next
    tpgDry.Tag = "0"
    tpgMagic.Tag = "0"
  End Sub

  Private Sub Dry_Load()
    Dim int1 As Integer
    Form_Loader(tpgDry, "DRY", DryProduction, drDryProduction)
    txtDryPrice.Text = DryProduction.Rows(drDryProduction).Item("Price")
    txtDry.Text = DryProduction.Rows(drDryProduction).Item("Print_Installed")
    If IsDBNull(DryProduction.Rows(drDryProduction).Item("Start_Date")) = False Then txtDryStart.Text = DryProduction.Rows(drDryProduction).Item("Start_Date")
    If IsDBNull(DryProduction.Rows(drDryProduction).Item("Complete_Date")) = False Then txtDryComplete.Text = DryProduction.Rows(drDryProduction).Item("Complete_Date")
    For i As Integer = 0 To tpgDry.Controls.Count - 1
      If TypeOf (tpgDry.Controls(i)) Is TextBox Then
        If tpgDry.Controls(i).Text = "0" Then tpgDry.Controls(i).Text = ""
      ElseIf tpgDry.Controls(i).HasChildren = True Then
        For int1 = 0 To tpgDry.Controls(i).Controls.Count - 1
          If TypeOf (tpgDry.Controls(i).Controls(int1)) Is TextBox Then
            If tpgDry.Controls(i).Controls(int1).Text = "0" Then tpgDry.Controls(i).Controls(int1).Text = ""
          End If
        Next
      End If
    Next
    tpgDry.Tag = DryProduction.Rows(drDryProduction).Item("ID")
    If Info.Rows.Count > 0 Then
      Form_Loader(Me, "INFO", Info, 0)
    End If
  End Sub

  Private Sub Magic_Load()
    Form_Loader(tpgMagic, "Magic", MagicProduction, drMagicProduction)
    txtMagicPrice.Text = MagicProduction.Rows(drMagicProduction).Item("Price")
    txtMagic.Text = MagicProduction.Rows(drMagicProduction).Item("Print_Offer")
    If IsDBNull(MagicProduction.Rows(drMagicProduction).Item("Start_Date")) = False Then cmdMagicStart.Text = MagicProduction.Rows(drMagicProduction).Item("Start_Date")
    If IsDBNull(MagicProduction.Rows(drMagicProduction).Item("Complete_Date")) = False Then cmdMagicComplete.Text = MagicProduction.Rows(drMagicProduction).Item("Complete_Date")
    For i As Integer = 0 To tpgMagic.Controls.Count - 1
      If TypeOf (tpgMagic.Controls(i)) Is TextBox Then
        If tpgMagic.Controls(i).Text = 0 Then tpgMagic.Controls(i).Text = ""
      ElseIf tpgMagic.Controls(i).HasChildren = True Then
        For int1 = 0 To tpgMagic.Controls(i).Controls.Count - 1
          If TypeOf (tpgMagic.Controls(i).Controls(int1)) Is TextBox Then
            If tpgMagic.Controls(i).Controls(int1).Text = "0" Then tpgMagic.Controls(i).Controls(int1).Text = ""
          End If
        Next
      End If
    Next
    tpgMagic.Tag = MagicProduction.Rows(drMagicProduction).Item("ID")
  End Sub

  Private Sub tpgMagic_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles tpgMagic.Resize
    lblMagicCover.Width = tpgMagic.Width - 5
    lblMagicCover.Height = tpgMagic.Height - 5
  End Sub

  Private Sub cmdDryStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDryStart.Click
    frmProduction_Schedule.Tag = "DRY"
    frmProduction_Schedule.ShowDialog(Me)

  End Sub

  Private Sub cmdMagicStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    frmProduction_Schedule.Tag = "MAGIC"
    frmProduction_Schedule.Show(Me)
  End Sub

  Private Sub cmdMagicComplete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    cmdMagicComplete.Text = Calendar_Date(Me, Now.Date)
  End Sub

  Private Sub cmdDryComplete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDryComplete.Click
    cmdDryComplete.Text = Calendar_Date(Me, Now.Date)
  End Sub

  Private Sub txtDryStart_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDryStart.TextChanged

  End Sub

  Private Sub cmdIssues_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

  End Sub

  Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
    Compiler("UPDATE", Me, "INFO")
    TableUpdate("UPDATE", "Production_Info", strSaveSQL1, strValues1, Me, "ID = " & Info.Rows(0).Item("ID"))
  End Sub

  Private Sub cmdDryCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDryCancel.Click
    strResponse = MsgBox("Are you sure you want to cancel this job?", vbYesNo)
    If strResponse = vbNo Then Exit Sub

    Manual_Update("Delete", "Dry_Production", "", "ID = " & DryProduction.Rows(drDryProduction).Item("ID"))
    Manual_Update("Delete", "Magic_Production", "", "ID = " & MagicProduction.Rows(drMagicProduction).Item("ID"))
  End Sub

  Private Sub cboForeman_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

  End Sub

  Private Sub cmdDrySave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDrySave.Click
    For intMain = 0 To tpgDry.Controls.Count - 1
      If TypeOf (tpgDry.Controls(intMain)) Is TextBox Then
        If tpgDry.Controls(intMain).Text = "" Then tpgDry.Controls(intMain).Text = "0"
      End If
      If tpgDry.Controls(intMain).HasChildren = True Then
        For intSub = 0 To tpgDry.Controls(intMain).Controls.Count - 1
          If TypeOf (tpgDry.Controls(intMain).Controls(intSub)) Is TextBox Then
            If tpgDry.Controls(intMain).Controls(intSub).Text = "" Then tpgDry.Controls(intMain).Controls(intSub).Text = "0"
          End If
        Next
      End If
    Next
    Compiler("Update", tpgDry, "DRY")
    TableUpdate("Update", "Dry_Production", strSaveSQL1, strValues1, tpgDry, "ID = " & DryProduction.Rows(drDryProduction).Item("ID"))
    If flgFailed = True Then
      MsgBox("Unable to update Dry Production information.")
      Exit Sub
    End If
    If IsDate(txtDryStart.Text) = True Then
      Manual_Update("Update", "Dry_Production", "Start_Date = " & FilterByDate(Date.Parse(txtDryStart.Text)), "ID = " & DryProduction.Rows(drDryProduction).Item("ID"))
    End If
    If IsDate(txtDryComplete.Text) = True Then
      Manual_Update("Update", "Dry_Production", "Complete_Date = " & FilterByDate(Date.Parse(txtDryComplete.Text)), "ID = " & DryProduction.Rows(drDryProduction).Item("ID"))
    End If
    MsgBox("Dry Production Saved. Updating Inventory")

    If DryProduction.Rows(drDryProduction).Item("Inventory_Complete") = False Then
      Dim exInventory As New excel
      Dim ProductCost As New DataTable
      Dim drProductCost As Double = 0
      Connect("Inventory", Inventory, drInventory)


      exInventory.Create_File("C:\Inventory\Cost_Accounting\" & DryProduction.Rows(drDryProduction).Item("ID"))
      Manual_Update("Update", "Dry_Production", "Inventory_Path = 'C:\Inventory\Cost_Accounting\" & DryProduction.Rows(drDryProduction).Item("ID") & "'")

      exInventory.Cell("A1").value = "Item ID"
      exInventory.Cell("B1").value = "Item Description"
      exInventory.Cell("C1").value = ""
      exInventory.Cell("D1").value = "Location"
      exInventory.Cell("E1").value = ""
      exInventory.Cell("F1").value = "Price"
      exInventory.Cell("G1").value = ""
      exInventory.Cell("H1").value = "Return"

      exInventory.Cell("B2").value = "hours"
      exInventory.Cell("G2").value = "=SUM(E2*F2)"

      exInventory.Cell("B3").value = "trip"
      exInventory.Cell("G3").value = "=SUM(E3*F3)"

      exInventory.Cell("B4").value = "a MISC"
      exInventory.Cell("B4").value = "misc"
      exInventory.Cell("F4").value = "1"
      exInventory.Cell("G4").value = "=SUM(E4*F4)"

      For intInv_Counter = 0 To Inventory.Rows.Count - 1
        exInventory.Cell(exInventory.Excel_Cell(Int(intInv_Counter + 5), 0)).value = Inventory.Rows(intInv_Counter).Item("Item_Code")
        exInventory.Cell(exInventory.Excel_Cell(Int(intInv_Counter + 5), 1)).value = Inventory.Rows(intInv_Counter).Item("Item_Description")
        exInventory.Cell(exInventory.Excel_Cell(Int(intInv_Counter + 5), 3)).value = Inventory.Rows(intInv_Counter).Item("Location")
        exInventory.Cell(exInventory.Excel_Cell(Int(intInv_Counter + 5), 5)).value = FormatCurrency(Val(Inventory.Rows(intInv_Counter).Item("Price") & ""), 2, , , TriState.True)
        exInventory.Cell(exInventory.Excel_Cell(Int(intInv_Counter + 5), 6)).value = "=" & exInventory.Excel_Cell(Int(intInv_Counter + 5), 4) & "*" & exInventory.Excel_Cell(Int(intInv_Counter + 5), 5)
        exInventory.Cell(exInventory.Excel_Cell(Int(intInv_Counter + 5), 6)).style = "Currency"
      Next
      Dim intRow As Integer = Inventory.Rows.Count - 1
      intRow += 6
      exInventory.Cell(exInventory.Excel_Cell(intRow, 3)).value = "TOTAL"
      exInventory.Cell(exInventory.Excel_Cell(intRow, 5)).value = "= sum(E5:E" & intRow - 1 & ")"

      For Each Control As Object In tpgDry.Controls
        If Control.haschildren = True Then
          For Each subControl As Object In Control.controls
            If Val(subControl.text) > 0 Then
              Connect("Product_Inventory_Cost", ProductCost, drProductCost, "Product_ID = '" & LT(subControl.name, 3) & "'")
              drProductCost = 0
              If ProductCost.Rows.Count > 0 Then
                While ProductCost.Rows(0).Item("Item" & drProductCost + 1) & "" <> ""
                  For intInv_Counter = 0 To Inventory.Rows.Count - 1
                    Debug.Print(exInventory.Cell(exInventory.Excel_Cell(Int(intInv_Counter + 5), 0)).value & " / " & ProductCost.Rows(0).Item("Item" & drProductCost + 1))
                    If exInventory.Cell(exInventory.Excel_Cell(Int(intInv_Counter + 5), 0)).value = ProductCost.Rows(0).Item("Item" & drProductCost + 1) Then
                      Dim result = New DataTable().Compute((Val(subControl.text) & ProductCost.Rows(0).Item("Item" & drProductCost + 1 & "_Measurement")) & " * " & ProductCost.Rows(0).Item("Item" & drProductCost + 1 & "_Quanity"), Nothing)
                      exInventory.Cell(exInventory.Excel_Cell(Int(intInv_Counter + 5), 4)).value = result
                    End If
                  Next
                  drProductCost += 1
                End While
              End If
            End If
          Next
        End If
      Next
      exInventory.Excel_Close_File(True)
    End If

  End Sub

  Private Function Get_Product(ByVal ID As Integer, Optional ByVal StartSpot As Integer = 0, Optional ByVal Cycles As Integer = 0) As String
    If Cycles > 0 Then
      Cycles = StartSpot + Cycles
      If Cycles > Inventory.Rows.Count - 1 Then Cycles = Inventory.Rows.Count - 1
    Else
      Cycles = Inventory.Rows.Count - 1
    End If

    For intCounter As Integer = StartSpot To Cycles
      If Inventory.Rows(intCounter).Item("ID") = ID Then
        Return Inventory.Rows(intCounter).Item("Item_Description")
      End If
    Next


    Return "no match"

  End Function

  Private Sub tpgDry_Click(sender As System.Object, e As System.EventArgs) Handles tpgDry.Click

  End Sub

  Private Sub cmdShowInv_Click(sender As System.Object, e As System.EventArgs) Handles cmdShowInv.Click
    Dim exInventory As New excel

    exInventory.Excel_Open_File("C:\Inventory\Cost_Accounting\" & DryProduction.Rows(drDryProduction).Item("ID"))
    exInventory.Excel_Show()
  End Sub
End Class