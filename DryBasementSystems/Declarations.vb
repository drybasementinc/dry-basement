﻿Module Declarations
  Public Data As New OleDb.OleDbDataAdapter

  'Public Address As New DataTable
  'Public Clients As New DataTable
  'Public General As New DataTable
  'Public DryDispo As New DataTable
  'Public MagicDispo As New DataTable
  'Public DryProduction As New DataTable
  'Public MagicProduction As New DataTable

  'Public drGeneral As Double
  'Public drAddress As Double
  'Public drClients As Double
  'Public drDryDispo As Double
  'Public drMagicDispo As Double
  'Public drDryProduction As Double
  'Public drMagicProduction As Double

  'Public i As Integer
  'Public r As Integer
  'Public c As Integer

  Public strPassword As String
  Public strAddressValues As String
  Public strAddressFields As String
  Public strClientsValues As String
  Public strClientsFields As String
  Public strGeneralValues As String
  Public strGeneralFields As String
  Public strDryDispoValues As String
  Public strDryDispoFields As String
  Public strMagicDispoValues As String
  Public strMagicDispoFields As String
  Public strDryProductionValues As String
  Public strDryProductionFields As String
  Public strMagicProductionValues As String
  Public strMagicProductionFields As String

  Public dblAddressID As Double
  Public dblClientID As Double

  Public strResponse As String
  Public strUserName As String
  Public strScheduleSender As String
  Public strWhere As String
  Public strSource As String

  Public flgMulti As Boolean
  Public flgLoading As Boolean
  Public flgFailed As Boolean
  Public flgDispo_Add As Boolean
  Public flgMatch As Boolean
  Public flgNewAddress As Boolean
  Public flgNewClient As Boolean

  Public datDispo As Date
  Public datDate As Date
  Public datCalendar As Date
End Module
