﻿''Public Class frmGraph

''  Private Sub chtGraph_ChartSelected(ByVal sender As System.Object, ByVal e As AxMSChart20Lib._DMSChartEvents_ChartSelectedEvent) Handles chtGraph.ChartSelected

''    Dim Sales(,) As Object = New Object(,) _
''      {{"Company", "Company A", "Company B"}, _
''      {"June", 20, 10}, _
''      {"July", 10, 5}, _
''      {"August", 30, 15}, _
''      {"September", 14, 7}}
''    chtGraph.ChartData = Sales

''    'Add a title and legend.
''    With Me.chtGraph
''      .Title.Text = "Sales"
''      .Legend.Location.LocationType = _
''         MSChart20Lib.VtChLocationType.VtChLocationTypeBottom
''      .Legend.Location.Visible = True
''    End With

''    'Add titles to the axes.
''    With Me.chtGraph.Plot
''      .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).AxisTitle.Text = "Year"
''      .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdY).AxisTitle.Text = "Millions of $"
''    End With

''    'Set custom colors for the bars.
''    With Me.chtGraph.Plot
''      'Yellow for Company A
''      ' -1 selects all the datapoints.
''      .SeriesCollection(1).DataPoints(-1).Brush.FillColor.Set(250, 250, 0)
''      'Purple for Company B
''      .SeriesCollection(2).DataPoints(-1).Brush.FillColor.Set(200, 50, 200)
''    End With
''  End Sub
''End Class