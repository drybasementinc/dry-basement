﻿Public Class frmCalendar

  Private Sub calendar2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    calEverything.Rows = 7
    calEverything.Cols = 7
    calEverything.Create(125, 75, Me)
    calEverything.RowHeight(0, 25)
    calEverything.Cell(0, 0).text = "MONDAY"
    calEverything.Cell(0, 1).text = "TUESDAY"
    calEverything.Cell(0, 2).text = "WEDNESDAY"
    calEverything.Cell(0, 3).text = "THURSDAY"
    calEverything.Cell(0, 4).text = "FRIDAY"
    calEverything.Cell(0, 5).text = "SATURDAY"
    calEverything.Cell(0, 6).text = "SUNDAY"
    One_Month(datCalendar)
  End Sub

  Private Sub calendar2_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    calEverything.Width = Me.Width - calEverything.Left
    calEverything.Height = Me.Height - calEverything.Top
  End Sub

  Private Sub calEverything_CellDoubleClick(ByVal sender As Object, ByVal Owner As Object, ByVal CurrentRow As Double, ByVal CurrentCol As Double) Handles calEverything.CellDoubleClick
    If calEverything.Valid_Cell(sender) = False Then Exit Sub
    datCalendar = DateValue(calEverything.Cell(CurrentRow, CurrentCol).tag).Date
    Close()
  End Sub

  Private Sub calEverything_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles calEverything.Load

  End Sub

  Private Sub cmdPrior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrior.Click
    datCalendar = DateAdd(DateInterval.Month, -1, datCalendar)
    One_Month(datCalendar)
  End Sub

  Private Sub One_Month(ByVal Main_Date As Date)
    Dim datCalStarter As Date
    Dim datTemp As Date
    Dim intRow As Integer
    Dim intCol As Integer
    Dim intDayValue As Integer

    datCalStarter = DateValue(Main_Date.Month & "/1/" & Main_Date.Year)
    With calEverything
      Select Case datCalStarter.DayOfWeek
        Case DayOfWeek.Monday
          intDayValue = 7
        Case DayOfWeek.Tuesday
          intDayValue = 1
        Case DayOfWeek.Wednesday
          intDayValue = 2
        Case DayOfWeek.Thursday
          intDayValue = 3
        Case DayOfWeek.Friday
          intDayValue = 4
        Case DayOfWeek.Saturday
          intDayValue = 5
        Case DayOfWeek.Sunday
          intDayValue = 6
      End Select
      For intRow = 1 To .Rows - 1
        For intCol = 0 To .Cols - 1
          datTemp = DateAdd(DateInterval.Day, (intCol + ((intRow - 1) * 7)) - intDayValue, datCalStarter)
          With calEverything.Cell(intRow, intCol)
            .text = datTemp
            .tag = datTemp
            .backcolor = Color.White
            calEverything.CellBorder(intRow, intCol).topborder = True
            calEverything.CellBorder(intRow, intCol).leftborder = True
            calEverything.CellBorder(intRow, intCol).rightborder = True
            calEverything.CellBorder(intRow, intCol).bottomborder = True
          End With
        Next
      Next
    End With
  End Sub

  Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
    datCalendar = DateAdd(DateInterval.Month, 1, datCalendar)
    One_Month(datCalendar)
  End Sub

  Private Sub cmdGo_To_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGo_To.Click
    If IsDate(dtpGoTo.Value) = True Then
      datCalendar = dtpGoTo.Value
      One_Month(datCalendar)
    Else
      MsgBox("You must choose a valid date in the selection box.")
    End If
  End Sub
End Class