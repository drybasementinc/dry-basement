﻿Public Class frmUp_Keep

  Private Sub cmdProductSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProductSave.Click
    Dim ProductCost As New DataTable

    If fraProduct.Tag & "" = "NEW" Then
      Compiler("Insert Into", fraProduct, "Product")
      TableUpdate("Insert Into", "Products", strSaveSQL1, strValues1, fraProduct)
      Manual_Update("Insert Into", "Product_Inventory_Cost", "(Product_ID) Values('" & txtProduct_Field.Text & "')")
    Else
      Compiler("Update", fraProduct, "Product")
      TableUpdate("Update", "Products", strSaveSQL1, strValues1, fraProduct, "ID = " & fraProduct.Tag)
      Connect("Product_Inventory_Cost", ProductCost, 0, "Product_ID = '" & txtProduct_Field.Text & "'")
      If ProductCost.Rows.Count <> 1 Then
        Manual_Update("Insert Into", "Product_Inventory_Cost", "(Product_ID) Values('" & txtProduct_Field.Text & "')")
      End If
    End If
    MsgBox("Saved")
    cmdProductExit_Click(Me, e)
  End Sub

  Private Sub grdProducts_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdProducts.CellDoubleClick
    Try
      fraProduct.Tag = grdProducts.Item(e.ColumnIndex, e.RowIndex).Tag
      grdProducts.Enabled = False
      fraProduct.Visible = True
      ProductLoad()
    Catch ex As Exception
      '
    End Try
  End Sub

  Private Sub cmdProductExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProductExit.Click
    grdProducts.Enabled = True
    For Each Control In fraProduct.Controls
      If TypeName(Control) = "ComboBox" Or TypeName(Control) = "TextBox" Then
        Control.text = ""
      End If
    Next
    chkActive.Checked = False
    fraProduct.Visible = False
    ProductRefresh()
  End Sub

  Private Sub ProductRefresh()
    Dim Products As New DataTable
    Grid_Lock(Me, grdProducts, True, True)

    Connect("Products", Products, 0)
    If Products.Rows.Count = 0 Then Exit Sub
    grdProducts.RowCount = Products.Rows.Count
    grdProducts.ColumnCount = 9
    grdProducts.Columns(0).HeaderText = "Product"
    grdProducts.Columns(1).HeaderText = "Product Field"
    grdProducts.Columns(2).HeaderText = "Company"
    grdProducts.Columns(3).HeaderText = "Abbr"
    grdProducts.Columns(4).HeaderText = "Measurement"
    grdProducts.Columns(5).HeaderText = "Category"
    grdProducts.Columns(6).HeaderText = "Active"
    grdProducts.Columns(7).HeaderText = "Placement"
    grdProducts.Columns(8).HeaderText = "Inventory ID"

    For intProducts As Integer = 0 To Products.Rows.Count - 1
      grdProducts.Item(0, intProducts).Value = Products.Rows(intProducts).Item("Product")
      grdProducts.Item(0, intProducts).Tag = Products.Rows(intProducts).Item("ID")
      grdProducts.Item(1, intProducts).Value = Products.Rows(intProducts).Item("Product_Field")
      grdProducts.Item(1, intProducts).Tag = Products.Rows(intProducts).Item("ID")
      grdProducts.Item(2, intProducts).Value = Products.Rows(intProducts).Item("Company")
      grdProducts.Item(2, intProducts).Tag = Products.Rows(intProducts).Item("ID")
      grdProducts.Item(3, intProducts).Value = Products.Rows(intProducts).Item("Abbr")
      grdProducts.Item(3, intProducts).Tag = Products.Rows(intProducts).Item("ID")
      grdProducts.Item(4, intProducts).Value = Products.Rows(intProducts).Item("Measurement")
      grdProducts.Item(4, intProducts).Tag = Products.Rows(intProducts).Item("ID")
      grdProducts.Item(5, intProducts).Value = Products.Rows(intProducts).Item("Category")
      grdProducts.Item(5, intProducts).Tag = Products.Rows(intProducts).Item("ID")
      grdProducts.Item(6, intProducts).Value = Products.Rows(intProducts).Item("Active")
      grdProducts.Item(6, intProducts).Tag = Products.Rows(intProducts).Item("ID")
      grdProducts.Item(7, intProducts).Value = Products.Rows(intProducts).Item("Placement")
      grdProducts.Item(7, intProducts).Tag = Products.Rows(intProducts).Item("ID")
      grdProducts.Item(8, intProducts).Value = Products.Rows(intProducts).Item("Inventory_ID")
      grdProducts.Item(8, intProducts).Tag = Products.Rows(intProducts).Item("ID")
    Next
    Grid_Lock(Me, grdProducts, True, True)
  End Sub

  Private Sub frmUpKeep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Dim Extra As New DataTable
    ProductRefresh()
    ProductRefresh()
    InventoryRefresh()
    InventoryRefresh()
    Source_Refresh()
    Source_Refresh()

    Connect("Dispo_Categories", Extra, 0)
    For intCounter As Integer = 0 To Extra.Rows.Count - 1
      cboCategory.Items.Add(Extra.Rows(intCounter).Item("Category"))
    Next

    Connect("Dispo_Measurements", Extra, 0)
    For intCounter As Integer = 0 To Extra.Rows.Count - 1
      cboMeasurement.Items.Add(Extra.Rows(intCounter).Item("Measurement"))
    Next

    Connect("Inventory", Extra, 0)
    For intCounter As Integer = 0 To Extra.Rows.Count - 1
      cboItem.Items.Add(Extra.Rows(intCounter).Item("Item_Code"))
    Next

  End Sub

  Private Sub cmdAddProduct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddProduct.Click
    fraProduct.Tag = "NEW"
    fraProduct.Visible = True
    grdProducts.Enabled = False
    ProductLoad()

  End Sub

  Private Sub fraProduct_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fraProduct.Enter

  End Sub

  Private Sub fraProduct_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fraProduct.VisibleChanged
  End Sub

  Private Sub ProductLoad()
    Dim Products As New DataTable
    If fraProduct.Tag.ToString <> "NEW" Then
      Connect("Products", Products, 0, "ID = " & fraProduct.Tag & "")
      If Products.Rows.Count = 0 Then
        MsgBox("Failure loading product.")
        fraProduct.Visible = False
        grdProducts.Enabled = True
        Exit Sub
      End If
      Form_Loader(fraProduct, "Product", Products, 0)
    End If
  End Sub

  Private Sub grdProducts_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdProducts.CellContentClick

  End Sub

  Private Sub InventoryRefresh()
    Dim Inventory As New DataTable
    Dim Items As New DataTable
    Grid_Lock(Me, grdInventory, True, True)

    Connect("Product_Inventory_Cost", Inventory, 0)
    If Inventory.Rows.Count = 0 Then Exit Sub
    grdInventory.RowCount = Inventory.Rows.Count
    grdInventory.ColumnCount = 122
    grdInventory.Columns(0).HeaderText = "ID"
    grdInventory.Columns(1).HeaderText = "Product"
    Dim intGroupItem As Integer = 1
    Dim intGroup As Integer = 1

    For intC As Integer = 2 To grdInventory.ColumnCount - 1
      Select Case intGroupItem
        Case 1
          grdInventory.Columns(intC).HeaderText = "Item" & intGroup
          grdInventory.Columns(intC).HeaderCell.Tag = "Item" & intGroup
          intGroupItem = intGroupItem + 1
        Case 2
          grdInventory.Columns(intC).HeaderText = "Item" & intGroup & " Quantity"
          grdInventory.Columns(intC).HeaderCell.Tag = "Item" & intGroup & "_Quanity"
          intGroupItem = intGroupItem + 1
        Case 3
          grdInventory.Columns(intC).HeaderText = "Item" & intGroup & " Formula"
          grdInventory.Columns(intC).HeaderCell.Tag = "Item" & intGroup & "_Measurement"
          intGroupItem = 1
          intGroup = intGroup + 1
      End Select
    Next

    For intInventory As Integer = 0 To Inventory.Rows.Count - 1
      grdInventory.Item(0, intInventory).Value = Inventory.Rows(intInventory).Item("ID")
      Try
        grdInventory.Item(1, intInventory).Value = Inventory.Rows(intInventory).Item("Product_ID")
      Catch ex As Exception
        grdInventory.Item(1, intInventory).Value = Inventory.Rows(intInventory).Item("Product_ID") & ""
      End Try
      For intC As Integer = 2 To grdInventory.Columns.Count - 1
        If Inventory.Rows(intInventory).Item(grdInventory.Columns(intC).HeaderCell.Tag).ToString.Contains("_") = False Then
          If Val(Inventory.Rows(intInventory).Item(grdInventory.Columns(intC).HeaderCell.Tag).ToString) > 0 Then
            grdInventory.Item(intC, intInventory).Value = Inventory.Rows(intInventory).Item(grdInventory.Columns(intC).HeaderCell.Tag)
          Else
            grdInventory.Item(intC, intInventory).Value = Inventory.Rows(intInventory).Item(grdInventory.Columns(intC).HeaderCell.Tag) & ""
          End If
        Else
          grdInventory.Item(intC, intInventory).Value = Inventory.Rows(intInventory).Item(grdInventory.Columns(intC).HeaderCell.Tag)
        End If
          grdInventory.Item(intC, intInventory).Tag = Inventory.Rows(intInventory).Item("ID")
      Next
    Next
    Grid_Lock(Me, grdInventory, True, True)
  End Sub

  Private Sub cmdInvSave_Click(sender As System.Object, e As System.EventArgs) Handles cmdInvSave.Click
    Dim Inventory As New DataTable

    Connect("Product_Inventory_Cost", Inventory, 0, "Product_ID = '" & txtInvProduct.Text & "'")
    If Inventory.Rows.Count > 0 And cboItemNumber.Text <> "" Then
      Manual_Update("UPDATE", "Product_Inventory_Cost", "Item" & cboItemNumber.Text & " = '" & cboItem.Text & "', Item" & cboItemNumber.Text & "_Quanity = " & Val(txtQuanity.Text) & ", Item" & cboItemNumber.Text & "_Measurement = '" & txtMeasurement.Text & "'")
    End If

    cmdInvExit_Click(Me, e)
  End Sub

  Private Sub grdInventory_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdInventory.CellDoubleClick
    txtInvProduct.Text = grdInventory.Item(1, e.RowIndex).Value

    For intCol As Integer = 2 To grdInventory.Columns.Count - 1 Step 3
      If grdInventory.Item(intCol, e.RowIndex).Value & "" = "" Then
        cboItemNumber.Text = Replace(LCase(grdInventory.Columns(intCol).HeaderText), "item", "")

      End If
    Next
  End Sub

  Private Sub fraInventory_Enter(sender As System.Object, e As System.EventArgs) Handles fraInventory.Enter

  End Sub

  Private Sub cboProduct_SelectedIndexChanged(sender As System.Object, e As System.EventArgs)

  End Sub

  Private Sub cboItemNumber_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboItemNumber.SelectedIndexChanged
    Dim Inventory As New DataTable

    Connect("Product_Inventory_Cost", Inventory, 0, "Product_ID = '" & txtInvProduct.Text & "'")
    If Inventory.Rows.Count > 0 And cboItemNumber.Text <> "" Then
      cboItem.Text = Inventory.Rows(0).Item("Item" & cboItemNumber.Text) & ""
      txtQuanity.Text = Inventory.Rows(0).Item("Item" & cboItemNumber.Text & "_Quanity")
      txtMeasurement.Text = Inventory.Rows(0).Item("Item" & cboItemNumber.Text & "_Measurement") & ""
    End If

    fraInventory.Visible = True
    grdInventory.Enabled = False
  End Sub

  Private Sub cmdInvExit_Click(sender As System.Object, e As System.EventArgs) Handles cmdInvExit.Click
    fraInventory.Visible = False
    grdInventory.Enabled = True
    InventoryRefresh()
    InventoryRefresh()
  End Sub

  Private Sub cmdSourceAdd_Click(sender As System.Object, e As System.EventArgs) Handles cmdSourceAdd.Click
    strResponse = InputBox("Please enter the new Source.", , "EXIT")
    If strResponse = "EXIT" Then Exit Sub
    Manual_Update("Insert Into", "Source", "([Source],[Company]) Values('" & strResponse & "','DRY')")
    If flgFailed = False Then MsgBox("Source Saved")
    Source_Refresh()
    Source_Refresh()
  End Sub

  Public Sub Source_Refresh()
    Dim Source As New DataTable

    Grid_Lock(Me, grdSource, True, True)

    Connect("Source", Source, 0)

    With grdSource
      .ColumnCount = 1
      .RowCount = Source.Rows.Count

      .Columns(0).HeaderText = "Source"

      For intCounter As Integer = 0 To Source.Rows.Count - 1
        .Item(0, intCounter).Value = Source.Rows(intCounter).Item("Source") & ""
        .Item(0, intCounter).Tag = Source.Rows(intCounter).Item("ID") & ""
      Next
    End With

  End Sub

  Private Sub grdSource_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSource.CellContentDoubleClick
    strResponse = MsgBox("Would you like to delete this Source?", vbYesNo)
    If strResponse = vbNo Then Exit Sub

    Manual_Update("Delete", "Source", "", "ID = " & grdSource.Item(e.ColumnIndex, e.RowIndex).Tag)

    Source_Refresh()
    Source_Refresh()
  End Sub
End Class