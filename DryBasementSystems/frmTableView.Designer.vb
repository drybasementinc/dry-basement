﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTableView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.grdMain = New System.Windows.Forms.DataGridView()
    Me.Dry_BasementDataSet1 = New DryBasementSystems.Dry_BasementDataSet1()
    Me.DryBasementDataSet1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
    Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
    Me.NameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
    Me.UserNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
    Me.PasswordDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
    Me.Button1 = New System.Windows.Forms.Button()
    CType(Me.grdMain, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Dry_BasementDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.DryBasementDataSet1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'grdMain
    '
    Me.grdMain.AutoGenerateColumns = False
    Me.grdMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdMain.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDDataGridViewTextBoxColumn, Me.NameDataGridViewTextBoxColumn, Me.UserNameDataGridViewTextBoxColumn, Me.PasswordDataGridViewTextBoxColumn})
    Me.grdMain.DataMember = "Operators"
    Me.grdMain.DataSource = Me.DryBasementDataSet1BindingSource
    Me.grdMain.Location = New System.Drawing.Point(12, 12)
    Me.grdMain.Name = "grdMain"
    Me.grdMain.Size = New System.Drawing.Size(730, 606)
    Me.grdMain.TabIndex = 0
    '
    'Dry_BasementDataSet1
    '
    Me.Dry_BasementDataSet1.DataSetName = "Dry_BasementDataSet1"
    Me.Dry_BasementDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
    '
    'DryBasementDataSet1BindingSource
    '
    Me.DryBasementDataSet1BindingSource.DataSource = Me.Dry_BasementDataSet1
    Me.DryBasementDataSet1BindingSource.Position = 0
    '
    'IDDataGridViewTextBoxColumn
    '
    Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
    Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
    Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
    '
    'NameDataGridViewTextBoxColumn
    '
    Me.NameDataGridViewTextBoxColumn.DataPropertyName = "Name"
    Me.NameDataGridViewTextBoxColumn.HeaderText = "Name"
    Me.NameDataGridViewTextBoxColumn.Name = "NameDataGridViewTextBoxColumn"
    '
    'UserNameDataGridViewTextBoxColumn
    '
    Me.UserNameDataGridViewTextBoxColumn.DataPropertyName = "UserName"
    Me.UserNameDataGridViewTextBoxColumn.HeaderText = "UserName"
    Me.UserNameDataGridViewTextBoxColumn.Name = "UserNameDataGridViewTextBoxColumn"
    '
    'PasswordDataGridViewTextBoxColumn
    '
    Me.PasswordDataGridViewTextBoxColumn.DataPropertyName = "Password"
    Me.PasswordDataGridViewTextBoxColumn.HeaderText = "Password"
    Me.PasswordDataGridViewTextBoxColumn.Name = "PasswordDataGridViewTextBoxColumn"
    '
    'Button1
    '
    Me.Button1.Location = New System.Drawing.Point(775, 35)
    Me.Button1.Name = "Button1"
    Me.Button1.Size = New System.Drawing.Size(72, 34)
    Me.Button1.TabIndex = 1
    Me.Button1.Text = "Button1"
    Me.Button1.UseVisualStyleBackColor = True
    '
    'frmTableView
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(864, 630)
    Me.Controls.Add(Me.Button1)
    Me.Controls.Add(Me.grdMain)
    Me.Name = "frmTableView"
    Me.Text = "frmTableView"
    CType(Me.grdMain, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Dry_BasementDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.DryBasementDataSet1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents grdMain As System.Windows.Forms.DataGridView
  Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
  Friend WithEvents NameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
  Friend WithEvents UserNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
  Friend WithEvents PasswordDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
  Friend WithEvents DryBasementDataSet1BindingSource As System.Windows.Forms.BindingSource
  Friend WithEvents Dry_BasementDataSet1 As DryBasementSystems.Dry_BasementDataSet1
  Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
