﻿Public Class frmSearch_Results
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form Specific Declarations
  Private Sub Search_Results_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    With grdResults
      Connect("Search", General, drGeneral, frmSearch.strWhere, "Zip")

      .AllowUserToAddRows = False
      .AllowUserToDeleteRows = False
      .AllowUserToOrderColumns = False
      .RowHeadersVisible = False
      .ReadOnly = True
      .AllowUserToOrderColumns = False

      .RowCount = General.Rows.Count
      .ColumnCount = 7

      .Columns(0).HeaderText = "First Name"
      .Columns(0).HeaderCell.Tag = "First_Name"
      .Columns(1).HeaderText = "Last Name"
      .Columns(1).HeaderCell.Tag = "Last_Name"
      .Columns(2).HeaderText = "Address"
      .Columns(2).HeaderCell.Tag = "Address"
      .Columns(3).HeaderText = "City"
      .Columns(3).HeaderCell.Tag = "City"
      .Columns(4).HeaderText = "State"
      .Columns(4).HeaderCell.Tag = "State"
      .Columns(4).Width = 35
      .Columns(5).HeaderText = "Zip"
      .Columns(5).HeaderCell.Tag = "Zip"
      .Columns(5).Width = 70
      .Columns(6).HeaderText = "Source"
      .Columns(6).HeaderCell.Tag = "Source"

      For drGeneral As Double = 0 To General.Rows.Count - 1
        For c As Integer = 0 To .ColumnCount - 1
          Dim cell As Integer
          cell = drGeneral
          If .Columns(c).HeaderCell.Tag = "Address" Then
            .Item(c, cell).Value = General.Rows(drGeneral).Item("Address_Numbers") & " " & General.Rows(drGeneral).Item("Address_Direction") & " " & General.Rows(drGeneral).Item("Address") & " " & General.Rows(drGeneral).Item("Address_Ending")
          Else
            Try
              .Item(c, cell).Value = General.Rows(drGeneral).Item(.Columns(c).HeaderCell.Tag)
            Catch ex As Exception
              .Item(c, cell).Value = General.Rows(drGeneral).Item("Addresses." & .Columns(c).HeaderCell.Tag)
            End Try
          End If
          .Item(c, cell).Tag = General.Rows(drGeneral).Item("AddressId")
          .Item(c, cell).Style.BackColor = Color.White
        Next
      Next
    End With

    Grid_Lock(Me, grdResults, True, True)

  End Sub

  Private Sub Search_Results_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    On Error Resume Next
    grdResults.Width = Me.Width - 30
    grdResults.Height = Me.Height - 40
  End Sub

  Private Sub grdResults_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdResults.CellContentDoubleClick

  End Sub

  Private Sub grdResults_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdResults.CellContentClick

  End Sub

  Private Sub grdResults_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdResults.CellDoubleClick
    Dim PickUp As New DataTable

    If Me.Tag = "PUP" Or Me.Tag = "RESET" Then
      Connect("Dry_Dispo", PickUp, 0, "Address_ID = " & grdResults.CurrentCell.Tag, "Appt_Date DESC")
      If PickUp.Rows.Count = 0 Then
        Connect("Magic_Dispo", PickUp, 0, "Address_ID = " & grdResults.CurrentCell.Tag, "Appt_Date DESC")
      End If
      If PickUp.Rows.Count = 0 Then
        MsgBox("Unable to locate a previous estimate. Please input as new appointment.")
        Connect("Addresses", Address, drAddress, "ID = " & grdResults.CurrentCell.Tag)
        drAddress = 0
        dblAddressID = Address.Rows(drAddress).Item("ID")
        Connect("Clients", Clients, drClients, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID"))
        drClients = 0
        dblClientID = Clients.Rows(drClients).Item("ID")
        Me.Close()
        frmSearch.Close()
        frmLead.ShowDialog(frmDispo)
        Exit Sub
      End If
      strResponse = MsgBox("Is the estimator still " & PickUp.Rows(0).Item("Estimator") & "?", vbYesNo)
      If strResponse = vbNo Then
        MsgBox("Please input as new appointment.")
        Connect("Addresses", Address, drAddress, "ID = " & grdResults.CurrentCell.Tag)
        drAddress = 0
        dblAddressID = Address.Rows(drAddress).Item("ID")
        Connect("Clients", Clients, drClients, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID"))
        drClients = 0
        dblClientID = Clients.Rows(drClients).Item("ID")
        Me.Close()
        frmSearch.Close()
        frmLead.ShowDialog(frmDispo)
        Exit Sub
      End If

      Manual_Update("Insert Into", "Schedule", "([Address_ID], [Person], [Date], [Estimator_Appt], [Appt_Type]) Values(" & grdResults.CurrentCell.Tag & ", '" & PickUp.Rows(0).Item("Estimator") & "', " & FilterByDate(datDispo) & ", True, '" & Me.Tag & "')")
      frmSearch.Close()
      Me.Close()
      frmDispo.BringToFront()
      Exit Sub
    End If
    Connect("Addresses", Address, drAddress, "ID = " & grdResults.CurrentCell.Tag)
    drAddress = 0
    dblAddressID = Address.Rows(drAddress).Item("ID")
    Connect("Clients", Clients, drClients, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID"))
    drClients = 0
    dblClientID = Clients.Rows(drClients).Item("ID")
    frmLead.Show()
  End Sub
End Class