﻿Imports System.Text
Imports System.IO

Public Class frmInventoryReport

  Private Sub frmInventoryReport_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    Dim lastMonth As Date = Now.AddMonths(-1)
    Dim daysInMonth As Int32 = DateTime.DaysInMonth(lastMonth.Year, lastMonth.Month)

    StartDatePicker.Text = lastMonth.Month & "/01/" & lastMonth.Year
    EndDatePicker.Text = lastMonth.Month & "/" & daysInMonth & "/" & lastMonth.Year

  End Sub

  Private Sub MagicDispoButton_Click(sender As Object, e As EventArgs) Handles MagicDispoButton.Click
    ExportReport("InventoryReportMagicDispo")
  End Sub

  Private Sub MagicProductionButton_Click(sender As Object, e As EventArgs) Handles MagicProductionButton.Click
    ExportReport("InventoryReportMagicProduction")
  End Sub

  Private Sub ExportReport(ByVal reportName As String)

    Try
      Dim result As New DataTable

      Dim filter As String

      filter = "Appt_Date >= " & FilterByDate(DateTime.Parse(StartDatePicker.Text)) & " AND Appt_Date <= " & FilterByDate(DateTime.Parse(EndDatePicker.Text))

      Connect(reportName, result, 0, filter, "Appt_Date")

      Dim csv As String = DataTableToCsv(result)

      Dim sfd As New SaveFileDialog()
      sfd.Filter = "csv files (*.csv)|"

      If sfd.ShowDialog() = DialogResult.OK Then
        File.WriteAllText(sfd.FileName, csv)

        MsgBox("Report Save Successfully!")
      End If

    Catch ex As Exception
      MsgBox("There was an issue while saving the report. " & ex.Message)
      LogException(ex)
    End Try

  End Sub

  Private Function DataTableToCsv(ByVal dataTable As DataTable)

    Dim sb As New StringBuilder

    For Each dcol As DataColumn In dataTable.Columns
      sb.Append(dcol.ColumnName & ",")
    Next

    'remove trailing comma
    sb.Remove(sb.Length - 1, 1)
    sb.AppendLine()

    For Each row As DataRow In dataTable.Rows
      For i As Integer = 0 To dataTable.Columns.Count - 1
        sb.Append(String.Format("""{0}"",", row.Item(i).ToString().Replace("""", "'")))
      Next

      'remove trailing comma
      sb.Remove(sb.Length - 1, 1)
      sb.AppendLine()
    Next

    Return sb.ToString()

  End Function

End Class