﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSchedule
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.cmdPrevious = New System.Windows.Forms.Button()
    Me.cmdNext = New System.Windows.Forms.Button()
    Me.lblBacklog = New System.Windows.Forms.Label()
    Me.lblTotals = New System.Windows.Forms.Label()
    Me.timRefresh = New System.Windows.Forms.Timer(Me.components)
    Me.fraSchedule = New System.Windows.Forms.GroupBox()
    Me.cboEnd_Time = New System.Windows.Forms.ComboBox()
    Me.lblEndTime = New System.Windows.Forms.Label()
    Me.txtEnd_Time = New System.Windows.Forms.TextBox()
    Me.cmdClose = New System.Windows.Forms.Button()
    Me.cmdSave = New System.Windows.Forms.Button()
    Me.lblDetails = New System.Windows.Forms.Label()
    Me.txtDetails = New System.Windows.Forms.TextBox()
    Me.cboApptType = New System.Windows.Forms.ComboBox()
    Me.lblApptType = New System.Windows.Forms.Label()
    Me.cboFontColor = New System.Windows.Forms.ComboBox()
    Me.lblForeColor = New System.Windows.Forms.Label()
    Me.cboBackColor = New System.Windows.Forms.ComboBox()
    Me.cboStart_Time = New System.Windows.Forms.ComboBox()
    Me.lblBackColor = New System.Windows.Forms.Label()
    Me.lblStartTime = New System.Windows.Forms.Label()
    Me.txtStart_Time = New System.Windows.Forms.TextBox()
    Me.fraSave_Items = New System.Windows.Forms.GroupBox()
    Me.valBonus_Sig = New System.Windows.Forms.TextBox()
    Me.valSet_Date = New System.Windows.Forms.TextBox()
    Me.valSource = New System.Windows.Forms.TextBox()
    Me.valAnnual_Appt = New System.Windows.Forms.CheckBox()
    Me.valAlways_Appt = New System.Windows.Forms.CheckBox()
    Me.valEnd_Time = New System.Windows.Forms.TextBox()
    Me.valEstimator_Appt = New System.Windows.Forms.CheckBox()
    Me.valExtra = New System.Windows.Forms.CheckBox()
    Me.valAppt_Type = New System.Windows.Forms.TextBox()
    Me.valExtra_Info = New System.Windows.Forms.TextBox()
    Me.valBackColor = New System.Windows.Forms.TextBox()
    Me.valFontColor = New System.Windows.Forms.TextBox()
    Me.valOperator = New System.Windows.Forms.TextBox()
    Me.valInfo = New System.Windows.Forms.TextBox()
    Me.valDate = New System.Windows.Forms.TextBox()
    Me.valPerson = New System.Windows.Forms.TextBox()
    Me.valAddress_ID = New System.Windows.Forms.TextBox()
    Me.valStart_Time = New System.Windows.Forms.TextBox()
    Me.fraEdit = New System.Windows.Forms.GroupBox()
    Me.cmdEditExit = New System.Windows.Forms.Button()
    Me.cmdDelete = New System.Windows.Forms.Button()
    Me.cmdOpen = New System.Windows.Forms.Button()
    Me.cmdColor = New System.Windows.Forms.Button()
    Me.lblBackColor2 = New System.Windows.Forms.Label()
    Me.lblFontColor2 = New System.Windows.Forms.Label()
    Me.cboBackColor2 = New System.Windows.Forms.ComboBox()
    Me.cboFontColor2 = New System.Windows.Forms.ComboBox()
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.mnuSchedules = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuEstimators = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuPreJob = New System.Windows.Forms.ToolStripMenuItem()
    Me.PlumbingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.AnnualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFormat = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuAutoInsert = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuAutoOff = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuAutoReset = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuAutoAvailable = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuDayOff = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuResetDay = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuAvailable = New System.Windows.Forms.ToolStripMenuItem()
    Me.grdSchedule = New DryBasementSystems.Grid()
    Me.fraSchedule.SuspendLayout()
    Me.fraSave_Items.SuspendLayout()
    Me.fraEdit.SuspendLayout()
    Me.MenuStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'cmdPrevious
    '
    Me.cmdPrevious.Location = New System.Drawing.Point(317, 12)
    Me.cmdPrevious.Name = "cmdPrevious"
    Me.cmdPrevious.Size = New System.Drawing.Size(75, 23)
    Me.cmdPrevious.TabIndex = 2
    Me.cmdPrevious.Text = "Previous"
    Me.cmdPrevious.UseVisualStyleBackColor = True
    '
    'cmdNext
    '
    Me.cmdNext.Location = New System.Drawing.Point(481, 12)
    Me.cmdNext.Name = "cmdNext"
    Me.cmdNext.Size = New System.Drawing.Size(75, 23)
    Me.cmdNext.TabIndex = 3
    Me.cmdNext.Text = "Next"
    Me.cmdNext.UseVisualStyleBackColor = True
    '
    'lblBacklog
    '
    Me.lblBacklog.AutoSize = True
    Me.lblBacklog.Location = New System.Drawing.Point(584, 14)
    Me.lblBacklog.Name = "lblBacklog"
    Me.lblBacklog.Size = New System.Drawing.Size(0, 13)
    Me.lblBacklog.TabIndex = 5
    '
    'lblTotals
    '
    Me.lblTotals.AutoSize = True
    Me.lblTotals.Location = New System.Drawing.Point(9, 11)
    Me.lblTotals.Name = "lblTotals"
    Me.lblTotals.Size = New System.Drawing.Size(0, 13)
    Me.lblTotals.TabIndex = 6
    '
    'timRefresh
    '
    Me.timRefresh.Enabled = True
    Me.timRefresh.Interval = 10000
    '
    'fraSchedule
    '
    Me.fraSchedule.Controls.Add(Me.cboEnd_Time)
    Me.fraSchedule.Controls.Add(Me.lblEndTime)
    Me.fraSchedule.Controls.Add(Me.txtEnd_Time)
    Me.fraSchedule.Controls.Add(Me.cmdClose)
    Me.fraSchedule.Controls.Add(Me.cmdSave)
    Me.fraSchedule.Controls.Add(Me.lblDetails)
    Me.fraSchedule.Controls.Add(Me.txtDetails)
    Me.fraSchedule.Controls.Add(Me.cboApptType)
    Me.fraSchedule.Controls.Add(Me.lblApptType)
    Me.fraSchedule.Controls.Add(Me.cboFontColor)
    Me.fraSchedule.Controls.Add(Me.lblForeColor)
    Me.fraSchedule.Controls.Add(Me.cboBackColor)
    Me.fraSchedule.Controls.Add(Me.cboStart_Time)
    Me.fraSchedule.Controls.Add(Me.lblBackColor)
    Me.fraSchedule.Controls.Add(Me.lblStartTime)
    Me.fraSchedule.Controls.Add(Me.txtStart_Time)
    Me.fraSchedule.Location = New System.Drawing.Point(261, 98)
    Me.fraSchedule.Name = "fraSchedule"
    Me.fraSchedule.Size = New System.Drawing.Size(408, 176)
    Me.fraSchedule.TabIndex = 7
    Me.fraSchedule.TabStop = False
    Me.fraSchedule.Text = "Schedule"
    Me.fraSchedule.Visible = False
    '
    'cboEnd_Time
    '
    Me.cboEnd_Time.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboEnd_Time.FormattingEnabled = True
    Me.cboEnd_Time.Items.AddRange(New Object() {"AM", "PM"})
    Me.cboEnd_Time.Location = New System.Drawing.Point(273, 16)
    Me.cboEnd_Time.Name = "cboEnd_Time"
    Me.cboEnd_Time.Size = New System.Drawing.Size(54, 21)
    Me.cboEnd_Time.TabIndex = 3
    '
    'lblEndTime
    '
    Me.lblEndTime.AutoSize = True
    Me.lblEndTime.Location = New System.Drawing.Point(170, 20)
    Me.lblEndTime.Name = "lblEndTime"
    Me.lblEndTime.Size = New System.Drawing.Size(30, 13)
    Me.lblEndTime.TabIndex = 19
    Me.lblEndTime.Text = "Time"
    '
    'txtEnd_Time
    '
    Me.txtEnd_Time.Location = New System.Drawing.Point(229, 16)
    Me.txtEnd_Time.Name = "txtEnd_Time"
    Me.txtEnd_Time.Size = New System.Drawing.Size(45, 20)
    Me.txtEnd_Time.TabIndex = 2
    '
    'cmdClose
    '
    Me.cmdClose.Location = New System.Drawing.Point(385, 0)
    Me.cmdClose.Name = "cmdClose"
    Me.cmdClose.Size = New System.Drawing.Size(18, 18)
    Me.cmdClose.TabIndex = 9
    Me.cmdClose.Text = "X"
    Me.cmdClose.UseVisualStyleBackColor = True
    '
    'cmdSave
    '
    Me.cmdSave.Location = New System.Drawing.Point(333, 15)
    Me.cmdSave.Name = "cmdSave"
    Me.cmdSave.Size = New System.Drawing.Size(48, 23)
    Me.cmdSave.TabIndex = 8
    Me.cmdSave.Text = "Save"
    Me.cmdSave.UseVisualStyleBackColor = True
    '
    'lblDetails
    '
    Me.lblDetails.AutoSize = True
    Me.lblDetails.Location = New System.Drawing.Point(6, 109)
    Me.lblDetails.Name = "lblDetails"
    Me.lblDetails.Size = New System.Drawing.Size(34, 13)
    Me.lblDetails.TabIndex = 14
    Me.lblDetails.Text = "Detail"
    '
    'txtDetails
    '
    Me.txtDetails.Location = New System.Drawing.Point(65, 105)
    Me.txtDetails.Multiline = True
    Me.txtDetails.Name = "txtDetails"
    Me.txtDetails.Size = New System.Drawing.Size(337, 62)
    Me.txtDetails.TabIndex = 7
    '
    'cboApptType
    '
    Me.cboApptType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboApptType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboApptType.FormattingEnabled = True
    Me.cboApptType.Location = New System.Drawing.Point(65, 44)
    Me.cboApptType.Name = "cboApptType"
    Me.cboApptType.Size = New System.Drawing.Size(262, 21)
    Me.cboApptType.TabIndex = 4
    '
    'lblApptType
    '
    Me.lblApptType.AutoSize = True
    Me.lblApptType.Location = New System.Drawing.Point(3, 47)
    Me.lblApptType.Name = "lblApptType"
    Me.lblApptType.Size = New System.Drawing.Size(56, 13)
    Me.lblApptType.TabIndex = 8
    Me.lblApptType.Text = "Appt Type"
    '
    'cboFontColor
    '
    Me.cboFontColor.FormattingEnabled = True
    Me.cboFontColor.Items.AddRange(New Object() {"BLACK", "BLUE", "GRAY", "GREEN", "RED", "WHITE", "YELLOW"})
    Me.cboFontColor.Location = New System.Drawing.Point(231, 70)
    Me.cboFontColor.Name = "cboFontColor"
    Me.cboFontColor.Size = New System.Drawing.Size(96, 21)
    Me.cboFontColor.TabIndex = 6
    '
    'lblForeColor
    '
    Me.lblForeColor.AutoSize = True
    Me.lblForeColor.Location = New System.Drawing.Point(170, 73)
    Me.lblForeColor.Name = "lblForeColor"
    Me.lblForeColor.Size = New System.Drawing.Size(55, 13)
    Me.lblForeColor.TabIndex = 6
    Me.lblForeColor.Text = "Font Color"
    '
    'cboBackColor
    '
    Me.cboBackColor.FormattingEnabled = True
    Me.cboBackColor.Items.AddRange(New Object() {"BLACK", "BLUE", "GRAY", "GREEN", "RED", "WHITE", "YELLOW"})
    Me.cboBackColor.Location = New System.Drawing.Point(65, 70)
    Me.cboBackColor.Name = "cboBackColor"
    Me.cboBackColor.Size = New System.Drawing.Size(98, 21)
    Me.cboBackColor.TabIndex = 5
    '
    'cboStart_Time
    '
    Me.cboStart_Time.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboStart_Time.FormattingEnabled = True
    Me.cboStart_Time.Items.AddRange(New Object() {"AM", "PM"})
    Me.cboStart_Time.Location = New System.Drawing.Point(109, 17)
    Me.cboStart_Time.Name = "cboStart_Time"
    Me.cboStart_Time.Size = New System.Drawing.Size(54, 21)
    Me.cboStart_Time.TabIndex = 1
    '
    'lblBackColor
    '
    Me.lblBackColor.AutoSize = True
    Me.lblBackColor.Location = New System.Drawing.Point(6, 73)
    Me.lblBackColor.Name = "lblBackColor"
    Me.lblBackColor.Size = New System.Drawing.Size(59, 13)
    Me.lblBackColor.TabIndex = 3
    Me.lblBackColor.Text = "Back Color"
    '
    'lblStartTime
    '
    Me.lblStartTime.AutoSize = True
    Me.lblStartTime.Location = New System.Drawing.Point(6, 21)
    Me.lblStartTime.Name = "lblStartTime"
    Me.lblStartTime.Size = New System.Drawing.Size(30, 13)
    Me.lblStartTime.TabIndex = 1
    Me.lblStartTime.Text = "Time"
    '
    'txtStart_Time
    '
    Me.txtStart_Time.Location = New System.Drawing.Point(65, 17)
    Me.txtStart_Time.Name = "txtStart_Time"
    Me.txtStart_Time.Size = New System.Drawing.Size(45, 20)
    Me.txtStart_Time.TabIndex = 0
    '
    'fraSave_Items
    '
    Me.fraSave_Items.Controls.Add(Me.valBonus_Sig)
    Me.fraSave_Items.Controls.Add(Me.valSet_Date)
    Me.fraSave_Items.Controls.Add(Me.valSource)
    Me.fraSave_Items.Controls.Add(Me.valAnnual_Appt)
    Me.fraSave_Items.Controls.Add(Me.valAlways_Appt)
    Me.fraSave_Items.Controls.Add(Me.valEnd_Time)
    Me.fraSave_Items.Controls.Add(Me.valEstimator_Appt)
    Me.fraSave_Items.Controls.Add(Me.valExtra)
    Me.fraSave_Items.Controls.Add(Me.valAppt_Type)
    Me.fraSave_Items.Controls.Add(Me.valExtra_Info)
    Me.fraSave_Items.Controls.Add(Me.valBackColor)
    Me.fraSave_Items.Controls.Add(Me.valFontColor)
    Me.fraSave_Items.Controls.Add(Me.valOperator)
    Me.fraSave_Items.Controls.Add(Me.valInfo)
    Me.fraSave_Items.Controls.Add(Me.valDate)
    Me.fraSave_Items.Controls.Add(Me.valPerson)
    Me.fraSave_Items.Controls.Add(Me.valAddress_ID)
    Me.fraSave_Items.Controls.Add(Me.valStart_Time)
    Me.fraSave_Items.Location = New System.Drawing.Point(12, 98)
    Me.fraSave_Items.Name = "fraSave_Items"
    Me.fraSave_Items.Size = New System.Drawing.Size(223, 366)
    Me.fraSave_Items.TabIndex = 8
    Me.fraSave_Items.TabStop = False
    Me.fraSave_Items.Text = "Save Items"
    Me.fraSave_Items.Visible = False
    '
    'valBonus_Sig
    '
    Me.valBonus_Sig.Location = New System.Drawing.Point(116, 129)
    Me.valBonus_Sig.Name = "valBonus_Sig"
    Me.valBonus_Sig.Size = New System.Drawing.Size(96, 20)
    Me.valBonus_Sig.TabIndex = 21
    Me.valBonus_Sig.Tag = "schedule"
    '
    'valSet_Date
    '
    Me.valSet_Date.Location = New System.Drawing.Point(116, 77)
    Me.valSet_Date.Name = "valSet_Date"
    Me.valSet_Date.Size = New System.Drawing.Size(96, 20)
    Me.valSet_Date.TabIndex = 20
    Me.valSet_Date.Tag = "schedule"
    '
    'valSource
    '
    Me.valSource.Location = New System.Drawing.Point(116, 51)
    Me.valSource.Name = "valSource"
    Me.valSource.Size = New System.Drawing.Size(96, 20)
    Me.valSource.TabIndex = 19
    Me.valSource.Tag = "schedule"
    '
    'valAnnual_Appt
    '
    Me.valAnnual_Appt.AutoSize = True
    Me.valAnnual_Appt.Location = New System.Drawing.Point(56, 207)
    Me.valAnnual_Appt.Name = "valAnnual_Appt"
    Me.valAnnual_Appt.Size = New System.Drawing.Size(15, 14)
    Me.valAnnual_Appt.TabIndex = 18
    Me.valAnnual_Appt.Tag = "schedule"
    Me.valAnnual_Appt.UseVisualStyleBackColor = True
    '
    'valAlways_Appt
    '
    Me.valAlways_Appt.AutoSize = True
    Me.valAlways_Appt.Location = New System.Drawing.Point(35, 207)
    Me.valAlways_Appt.Name = "valAlways_Appt"
    Me.valAlways_Appt.Size = New System.Drawing.Size(15, 14)
    Me.valAlways_Appt.TabIndex = 17
    Me.valAlways_Appt.Tag = "schedule"
    Me.valAlways_Appt.UseVisualStyleBackColor = True
    '
    'valEnd_Time
    '
    Me.valEnd_Time.Location = New System.Drawing.Point(116, 25)
    Me.valEnd_Time.Name = "valEnd_Time"
    Me.valEnd_Time.Size = New System.Drawing.Size(96, 20)
    Me.valEnd_Time.TabIndex = 16
    Me.valEnd_Time.Tag = "schedule"
    Me.valEnd_Time.Visible = False
    '
    'valEstimator_Appt
    '
    Me.valEstimator_Appt.AutoSize = True
    Me.valEstimator_Appt.Location = New System.Drawing.Point(14, 207)
    Me.valEstimator_Appt.Name = "valEstimator_Appt"
    Me.valEstimator_Appt.Size = New System.Drawing.Size(15, 14)
    Me.valEstimator_Appt.TabIndex = 14
    Me.valEstimator_Appt.Tag = "schedule"
    Me.valEstimator_Appt.UseVisualStyleBackColor = True
    '
    'valExtra
    '
    Me.valExtra.AutoSize = True
    Me.valExtra.Location = New System.Drawing.Point(14, 285)
    Me.valExtra.Name = "valExtra"
    Me.valExtra.Size = New System.Drawing.Size(15, 14)
    Me.valExtra.TabIndex = 13
    Me.valExtra.Tag = "schedule"
    Me.valExtra.UseVisualStyleBackColor = True
    '
    'valAppt_Type
    '
    Me.valAppt_Type.Location = New System.Drawing.Point(14, 337)
    Me.valAppt_Type.Name = "valAppt_Type"
    Me.valAppt_Type.Size = New System.Drawing.Size(96, 20)
    Me.valAppt_Type.TabIndex = 12
    Me.valAppt_Type.Tag = "schedule"
    '
    'valExtra_Info
    '
    Me.valExtra_Info.Location = New System.Drawing.Point(14, 311)
    Me.valExtra_Info.Name = "valExtra_Info"
    Me.valExtra_Info.Size = New System.Drawing.Size(96, 20)
    Me.valExtra_Info.TabIndex = 11
    Me.valExtra_Info.Tag = "schedule"
    '
    'valBackColor
    '
    Me.valBackColor.Location = New System.Drawing.Point(14, 259)
    Me.valBackColor.Name = "valBackColor"
    Me.valBackColor.Size = New System.Drawing.Size(96, 20)
    Me.valBackColor.TabIndex = 9
    Me.valBackColor.Tag = "schedule"
    '
    'valFontColor
    '
    Me.valFontColor.Location = New System.Drawing.Point(14, 233)
    Me.valFontColor.Name = "valFontColor"
    Me.valFontColor.Size = New System.Drawing.Size(96, 20)
    Me.valFontColor.TabIndex = 8
    Me.valFontColor.Tag = "schedule"
    '
    'valOperator
    '
    Me.valOperator.Location = New System.Drawing.Point(14, 181)
    Me.valOperator.Name = "valOperator"
    Me.valOperator.Size = New System.Drawing.Size(96, 20)
    Me.valOperator.TabIndex = 6
    Me.valOperator.Tag = "schedule"
    '
    'valInfo
    '
    Me.valInfo.Location = New System.Drawing.Point(14, 155)
    Me.valInfo.Name = "valInfo"
    Me.valInfo.Size = New System.Drawing.Size(96, 20)
    Me.valInfo.TabIndex = 5
    Me.valInfo.Tag = "schedule"
    '
    'valDate
    '
    Me.valDate.Location = New System.Drawing.Point(14, 129)
    Me.valDate.Name = "valDate"
    Me.valDate.Size = New System.Drawing.Size(96, 20)
    Me.valDate.TabIndex = 4
    Me.valDate.Tag = "schedule"
    '
    'valPerson
    '
    Me.valPerson.Location = New System.Drawing.Point(14, 77)
    Me.valPerson.Name = "valPerson"
    Me.valPerson.Size = New System.Drawing.Size(96, 20)
    Me.valPerson.TabIndex = 2
    Me.valPerson.Tag = "schedule"
    '
    'valAddress_ID
    '
    Me.valAddress_ID.Location = New System.Drawing.Point(14, 51)
    Me.valAddress_ID.Name = "valAddress_ID"
    Me.valAddress_ID.Size = New System.Drawing.Size(96, 20)
    Me.valAddress_ID.TabIndex = 1
    Me.valAddress_ID.Tag = "schedule"
    '
    'valStart_Time
    '
    Me.valStart_Time.Location = New System.Drawing.Point(14, 25)
    Me.valStart_Time.Name = "valStart_Time"
    Me.valStart_Time.Size = New System.Drawing.Size(96, 20)
    Me.valStart_Time.TabIndex = 0
    Me.valStart_Time.Tag = "schedule"
    '
    'fraEdit
    '
    Me.fraEdit.Controls.Add(Me.cmdEditExit)
    Me.fraEdit.Controls.Add(Me.cmdDelete)
    Me.fraEdit.Controls.Add(Me.cmdOpen)
    Me.fraEdit.Controls.Add(Me.cmdColor)
    Me.fraEdit.Controls.Add(Me.lblBackColor2)
    Me.fraEdit.Controls.Add(Me.lblFontColor2)
    Me.fraEdit.Controls.Add(Me.cboBackColor2)
    Me.fraEdit.Controls.Add(Me.cboFontColor2)
    Me.fraEdit.Location = New System.Drawing.Point(261, 280)
    Me.fraEdit.Name = "fraEdit"
    Me.fraEdit.Size = New System.Drawing.Size(327, 97)
    Me.fraEdit.TabIndex = 9
    Me.fraEdit.TabStop = False
    Me.fraEdit.Text = "Edit"
    Me.fraEdit.Visible = False
    '
    'cmdEditExit
    '
    Me.cmdEditExit.Location = New System.Drawing.Point(309, 1)
    Me.cmdEditExit.Name = "cmdEditExit"
    Me.cmdEditExit.Size = New System.Drawing.Size(18, 18)
    Me.cmdEditExit.TabIndex = 17
    Me.cmdEditExit.Text = "X"
    Me.cmdEditExit.UseVisualStyleBackColor = True
    '
    'cmdDelete
    '
    Me.cmdDelete.Location = New System.Drawing.Point(223, 71)
    Me.cmdDelete.Name = "cmdDelete"
    Me.cmdDelete.Size = New System.Drawing.Size(98, 20)
    Me.cmdDelete.TabIndex = 6
    Me.cmdDelete.Text = "Cancel Appt"
    Me.cmdDelete.UseVisualStyleBackColor = True
    '
    'cmdOpen
    '
    Me.cmdOpen.Location = New System.Drawing.Point(223, 45)
    Me.cmdOpen.Name = "cmdOpen"
    Me.cmdOpen.Size = New System.Drawing.Size(98, 20)
    Me.cmdOpen.TabIndex = 0
    Me.cmdOpen.Text = "Open Lead"
    Me.cmdOpen.UseVisualStyleBackColor = True
    '
    'cmdColor
    '
    Me.cmdColor.Location = New System.Drawing.Point(223, 19)
    Me.cmdColor.Name = "cmdColor"
    Me.cmdColor.Size = New System.Drawing.Size(98, 20)
    Me.cmdColor.TabIndex = 4
    Me.cmdColor.Text = "Color Update"
    Me.cmdColor.UseVisualStyleBackColor = True
    '
    'lblBackColor2
    '
    Me.lblBackColor2.AutoSize = True
    Me.lblBackColor2.Location = New System.Drawing.Point(6, 49)
    Me.lblBackColor2.Name = "lblBackColor2"
    Me.lblBackColor2.Size = New System.Drawing.Size(59, 13)
    Me.lblBackColor2.TabIndex = 3
    Me.lblBackColor2.Text = "Back Color"
    '
    'lblFontColor2
    '
    Me.lblFontColor2.AutoSize = True
    Me.lblFontColor2.Location = New System.Drawing.Point(6, 22)
    Me.lblFontColor2.Name = "lblFontColor2"
    Me.lblFontColor2.Size = New System.Drawing.Size(55, 13)
    Me.lblFontColor2.TabIndex = 2
    Me.lblFontColor2.Text = "Font Color"
    '
    'cboBackColor2
    '
    Me.cboBackColor2.FormattingEnabled = True
    Me.cboBackColor2.Items.AddRange(New Object() {"BLACK", "BLUE", "GRAY", "GREEN", "RED", "WHITE", "YELLOW"})
    Me.cboBackColor2.Location = New System.Drawing.Point(65, 46)
    Me.cboBackColor2.Name = "cboBackColor2"
    Me.cboBackColor2.Size = New System.Drawing.Size(98, 21)
    Me.cboBackColor2.TabIndex = 1
    '
    'cboFontColor2
    '
    Me.cboFontColor2.FormattingEnabled = True
    Me.cboFontColor2.Items.AddRange(New Object() {"BLACK", "BLUE", "GRAY", "GREEN", "RED", "WHITE", "YELLOW"})
    Me.cboFontColor2.Location = New System.Drawing.Point(65, 19)
    Me.cboFontColor2.Name = "cboFontColor2"
    Me.cboFontColor2.Size = New System.Drawing.Size(98, 21)
    Me.cboFontColor2.TabIndex = 0
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSchedules, Me.mnuFormat})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Size = New System.Drawing.Size(864, 24)
    Me.MenuStrip1.TabIndex = 10
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'mnuSchedules
    '
    Me.mnuSchedules.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEstimators, Me.mnuPreJob, Me.PlumbingToolStripMenuItem, Me.AnnualToolStripMenuItem})
    Me.mnuSchedules.Name = "mnuSchedules"
    Me.mnuSchedules.Size = New System.Drawing.Size(72, 20)
    Me.mnuSchedules.Text = "Schedules"
    '
    'mnuEstimators
    '
    Me.mnuEstimators.Name = "mnuEstimators"
    Me.mnuEstimators.Size = New System.Drawing.Size(129, 22)
    Me.mnuEstimators.Text = "Estimators"
    '
    'mnuPreJob
    '
    Me.mnuPreJob.Name = "mnuPreJob"
    Me.mnuPreJob.Size = New System.Drawing.Size(129, 22)
    Me.mnuPreJob.Text = "PreJob"
    '
    'PlumbingToolStripMenuItem
    '
    Me.PlumbingToolStripMenuItem.Name = "PlumbingToolStripMenuItem"
    Me.PlumbingToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
    Me.PlumbingToolStripMenuItem.Text = "Plumbing"
    '
    'AnnualToolStripMenuItem
    '
    Me.AnnualToolStripMenuItem.Name = "AnnualToolStripMenuItem"
    Me.AnnualToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
    Me.AnnualToolStripMenuItem.Text = "Annual"
    '
    'mnuFormat
    '
    Me.mnuFormat.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAutoInsert, Me.mnuDayOff, Me.mnuResetDay, Me.mnuAvailable})
    Me.mnuFormat.Name = "mnuFormat"
    Me.mnuFormat.Size = New System.Drawing.Size(57, 20)
    Me.mnuFormat.Text = "Format"
    '
    'mnuAutoInsert
    '
    Me.mnuAutoInsert.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAutoOff, Me.mnuAutoReset, Me.mnuAutoAvailable})
    Me.mnuAutoInsert.Name = "mnuAutoInsert"
    Me.mnuAutoInsert.Size = New System.Drawing.Size(152, 22)
    Me.mnuAutoInsert.Text = "Auto Insert"
    '
    'mnuAutoOff
    '
    Me.mnuAutoOff.Name = "mnuAutoOff"
    Me.mnuAutoOff.Size = New System.Drawing.Size(130, 22)
    Me.mnuAutoOff.Text = "Days Off"
    '
    'mnuAutoReset
    '
    Me.mnuAutoReset.Name = "mnuAutoReset"
    Me.mnuAutoReset.Size = New System.Drawing.Size(130, 22)
    Me.mnuAutoReset.Text = "Reset Days"
    '
    'mnuAutoAvailable
    '
    Me.mnuAutoAvailable.Name = "mnuAutoAvailable"
    Me.mnuAutoAvailable.Size = New System.Drawing.Size(130, 22)
    Me.mnuAutoAvailable.Text = "Available"
    '
    'mnuDayOff
    '
    Me.mnuDayOff.CheckOnClick = True
    Me.mnuDayOff.Name = "mnuDayOff"
    Me.mnuDayOff.Size = New System.Drawing.Size(152, 22)
    Me.mnuDayOff.Text = "Day Off"
    '
    'mnuResetDay
    '
    Me.mnuResetDay.CheckOnClick = True
    Me.mnuResetDay.Name = "mnuResetDay"
    Me.mnuResetDay.Size = New System.Drawing.Size(152, 22)
    Me.mnuResetDay.Text = "Reset Day"
    '
    'mnuAvailable
    '
    Me.mnuAvailable.CheckOnClick = True
    Me.mnuAvailable.Name = "mnuAvailable"
    Me.mnuAvailable.Size = New System.Drawing.Size(152, 22)
    Me.mnuAvailable.Text = "Available"
    '
    'grdSchedule
    '
    Me.grdSchedule.AutoScroll = True
    Me.grdSchedule.BackColor = System.Drawing.SystemColors.ControlDarkDark
    Me.grdSchedule.Bottom_Row = ""
    Me.grdSchedule.Cols = ""
    Me.grdSchedule.Left_Col = ""
    Me.grdSchedule.Location = New System.Drawing.Point(1, 40)
    Me.grdSchedule.Name = "grdSchedule"
    Me.grdSchedule.Right_Col = ""
    Me.grdSchedule.Rows = ""
    Me.grdSchedule.Size = New System.Drawing.Size(121, 52)
    Me.grdSchedule.TabIndex = 4
    Me.grdSchedule.Top_Row = ""
    '
    'frmSchedule
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(864, 556)
    Me.Controls.Add(Me.fraEdit)
    Me.Controls.Add(Me.fraSave_Items)
    Me.Controls.Add(Me.fraSchedule)
    Me.Controls.Add(Me.lblTotals)
    Me.Controls.Add(Me.lblBacklog)
    Me.Controls.Add(Me.grdSchedule)
    Me.Controls.Add(Me.cmdNext)
    Me.Controls.Add(Me.cmdPrevious)
    Me.Controls.Add(Me.MenuStrip1)
    Me.MainMenuStrip = Me.MenuStrip1
    Me.Name = "frmSchedule"
    Me.ShowInTaskbar = False
    Me.Text = "Schedule"
    Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
    Me.fraSchedule.ResumeLayout(False)
    Me.fraSchedule.PerformLayout()
    Me.fraSave_Items.ResumeLayout(False)
    Me.fraSave_Items.PerformLayout()
    Me.fraEdit.ResumeLayout(False)
    Me.fraEdit.PerformLayout()
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
    Friend WithEvents cmdPrevious As System.Windows.Forms.Button
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents grdSchedule As DryBasementSystems.Grid
    Friend WithEvents lblBacklog As System.Windows.Forms.Label
    Friend WithEvents lblTotals As System.Windows.Forms.Label
    Friend WithEvents timRefresh As System.Windows.Forms.Timer
    Friend WithEvents fraSchedule As System.Windows.Forms.GroupBox
    Friend WithEvents lblBackColor As System.Windows.Forms.Label
    Friend WithEvents lblStartTime As System.Windows.Forms.Label
    Friend WithEvents txtStart_Time As System.Windows.Forms.TextBox
    Friend WithEvents cboBackColor As System.Windows.Forms.ComboBox
    Friend WithEvents cboStart_Time As System.Windows.Forms.ComboBox
    Friend WithEvents cboApptType As System.Windows.Forms.ComboBox
    Friend WithEvents lblApptType As System.Windows.Forms.Label
    Friend WithEvents cboFontColor As System.Windows.Forms.ComboBox
    Friend WithEvents lblForeColor As System.Windows.Forms.Label
    Friend WithEvents lblDetails As System.Windows.Forms.Label
    Friend WithEvents txtDetails As System.Windows.Forms.TextBox
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents fraSave_Items As System.Windows.Forms.GroupBox
    Friend WithEvents valBackColor As System.Windows.Forms.TextBox
    Friend WithEvents valFontColor As System.Windows.Forms.TextBox
    Friend WithEvents valOperator As System.Windows.Forms.TextBox
    Friend WithEvents valInfo As System.Windows.Forms.TextBox
    Friend WithEvents valDate As System.Windows.Forms.TextBox
    Friend WithEvents valPerson As System.Windows.Forms.TextBox
    Friend WithEvents valAddress_ID As System.Windows.Forms.TextBox
    Friend WithEvents valStart_Time As System.Windows.Forms.TextBox
    Friend WithEvents valExtra_Info As System.Windows.Forms.TextBox
    Friend WithEvents valAppt_Type As System.Windows.Forms.TextBox
    Friend WithEvents valExtra As System.Windows.Forms.CheckBox
    Friend WithEvents valEstimator_Appt As System.Windows.Forms.CheckBox
    Friend WithEvents fraEdit As System.Windows.Forms.GroupBox
    Friend WithEvents lblBackColor2 As System.Windows.Forms.Label
    Friend WithEvents lblFontColor2 As System.Windows.Forms.Label
    Friend WithEvents cboBackColor2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboFontColor2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdOpen As System.Windows.Forms.Button
    Friend WithEvents cmdColor As System.Windows.Forms.Button
    Friend WithEvents cmdEditExit As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuSchedules As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEstimators As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPreJob As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboEnd_Time As System.Windows.Forms.ComboBox
    Friend WithEvents lblEndTime As System.Windows.Forms.Label
    Friend WithEvents txtEnd_Time As System.Windows.Forms.TextBox
    Friend WithEvents valEnd_Time As System.Windows.Forms.TextBox
    Friend WithEvents PlumbingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnnualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents valAnnual_Appt As System.Windows.Forms.CheckBox
    Friend WithEvents valAlways_Appt As System.Windows.Forms.CheckBox
    Friend WithEvents valSet_Date As System.Windows.Forms.TextBox
    Friend WithEvents valSource As System.Windows.Forms.TextBox
    Friend WithEvents valBonus_Sig As System.Windows.Forms.TextBox
    Friend WithEvents mnuFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAutoInsert As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAutoOff As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAutoReset As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAutoAvailable As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDayOff As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuResetDay As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAvailable As System.Windows.Forms.ToolStripMenuItem
End Class
