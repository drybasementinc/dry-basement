﻿Public Class frmProduction_Schedule
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form Specific Declarations
  Dim datProduction As Date

  Private Sub Production_Schedule_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Dim intRowValue As Integer
    With grdSchedule
      .AllowUserToAddRows = False
      .AllowUserToDeleteRows = False
      .AllowUserToOrderColumns = False
      .RowHeadersVisible = False
      .ReadOnly = True
      .AllowUserToOrderColumns = False
      .RowHeadersVisible = False
      .ColumnHeadersVisible = False
      .AllowUserToResizeColumns = False
      .AllowUserToResizeRows = False

      .RowCount = 68
      .ColumnCount = 7
      .Item(0, 0).Style.BackColor = Color.LightGray
      .Columns(0).DividerWidth = 2
      .Item(1, 0).Value = "MONDAY"
      .Item(1, 0).Style.BackColor = Color.LightGray
      .Columns(1).DividerWidth = 2
      .Columns(1).Width = 100
      .Item(2, 0).Value = "TUESDAY"
      .Item(2, 0).Style.BackColor = Color.LightGray
      .Columns(2).DividerWidth = 2
      .Columns(2).Width = 100
      .Item(3, 0).Value = "WEDNESDAY"
      .Item(3, 0).Style.BackColor = Color.LightGray
      .Columns(3).DividerWidth = 2
      .Columns(3).Width = 100
      .Item(4, 0).Value = "THURDAY"
      .Item(4, 0).Style.BackColor = Color.LightGray
      .Columns(4).DividerWidth = 2
      .Columns(4).Width = 100
      .Item(5, 0).Value = "FRIDAY"
      .Item(5, 0).Style.BackColor = Color.LightGray
      .Columns(5).DividerWidth = 2
      .Columns(5).Width = 100
      .Item(6, 0).Style.BackColor = Color.LightGray
      intRowValue = 1
      For r As Integer = 1 To .RowCount - 1
        .Item(0, r).Style.BackColor = Color.LightGray
        .Rows(r).Height = 15
        Select Case intRowValue
          Case 1
            .Item(0, r).Value = "NAME"
          Case 2
            .Item(0, r).Value = "LOCATION"
          Case 3
            .Item(0, r).Value = "INSTALL"
          Case 4
            .Item(0, r).Value = "FOUNDATION"
          Case 5
            .Item(0, r).Value = "DETAIL"
          Case 6
            .Item(0, r).Value = "DEPOSIT"
          Case 7
            .Item(0, r).Value = "COLLECT"
          Case 8
            .Item(0, r).Value = "TOTAL"
          Case 9
            .Item(0, r).Value = "PAY TYPE"
            .Rows(r).DividerHeight = 2
        End Select
        intRowValue = intRowValue + 1
        If intRowValue = 10 Then intRowValue = 1
      Next

      For r As Integer = 0 To .RowCount - 1
        For c As Integer = 0 To .ColumnCount - 1
          .Item(c, r).Style.Font = Font_Change("ARIAL", 6, FontStyle.Regular)
        Next
      Next

    End With
    datProduction = Now.Date
    If Me.Tag = "DRY" Then
      Dry_Load()
    ElseIf Me.Tag = "MAGIC" Then

    End If
  End Sub

  Private Sub Production_Schedule_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    On Error Resume Next
    grdSchedule.Width = Me.Width - grdSchedule.Left - 10
    grdSchedule.Height = Me.Height - grdSchedule.Top - 10
  End Sub

  Private Sub Dry_Load()
    Dim datKeeper As Date
    With grdSchedule
      For r As Integer = 1 To .RowCount - 1
        For c As Integer = 1 To .ColumnCount - 1
          .Item(c, r).Value = ""
          .Item(c, r).Tag = ""
        Next
      Next
      Select Case datProduction.DayOfWeek
        Case DayOfWeek.Monday
          'do nothing
        Case DayOfWeek.Tuesday
          datProduction = DateAdd(DateInterval.Day, -1, datProduction)
        Case DayOfWeek.Wednesday
          datProduction = DateAdd(DateInterval.Day, -2, datProduction)
        Case DayOfWeek.Thursday
          datProduction = DateAdd(DateInterval.Day, -3, datProduction)
        Case DayOfWeek.Friday
          datProduction = DateAdd(DateInterval.Day, -4, datProduction)
        Case DayOfWeek.Saturday
          datProduction = DateAdd(DateInterval.Day, 2, datProduction)
        Case DayOfWeek.Sunday
          datProduction = DateAdd(DateInterval.Day, 1, datProduction)
      End Select
      .Item(1, 0).Value = "MONDAY " & datProduction
      .Item(2, 0).Value = "TUESDAY " & DateAdd(DateInterval.Day, 1, datProduction)
      .Item(3, 0).Value = "WEDNESDAY " & DateAdd(DateInterval.Day, 2, datProduction)
      .Item(4, 0).Value = "THURSDAY " & DateAdd(DateInterval.Day, 3, datProduction)
      .Item(5, 0).Value = "FRIDAY " & DateAdd(DateInterval.Day, 4, datProduction)

      .Item(1, 0).Tag = datProduction
      .Item(2, 0).Tag = DateAdd(DateInterval.Day, 1, datProduction)
      .Item(3, 0).Tag = DateAdd(DateInterval.Day, 2, datProduction)
      .Item(4, 0).Tag = DateAdd(DateInterval.Day, 3, datProduction)
      .Item(5, 0).Tag = DateAdd(DateInterval.Day, 4, datProduction)
      datKeeper = DateAdd(DateInterval.Day, 5, datProduction)

      For c As Integer = 1 To grdSchedule.ColumnCount - 2

        Connect("Dry_Production", DryProduction, drDryProduction, "Start_Date = " & FilterByDate(datProduction), "Address_ID")

        For r As Integer = 0 To DryProduction.Rows.Count - 1
          Connect("Addresses", Address, drAddress, "ID = " & DryProduction.Rows(r).Item("Address_ID") & "")
          Connect("Clients", Clients, drClients, "ID = " & Address.Rows(0).Item("Current_Client_ID") & "")
          .Item(c, (1) + (9 * r)).Value = Clients.Rows(0).Item("Last_Name")
          .Item(c, (2) + (9 * r)).Value = Address.Rows(0).Item("City") & " " & Address.Rows(0).Item("Zip")
          .Item(c, (3) + (9 * r)).Value = DryProduction.Rows(r).Item("Print_Installed")
          .Item(c, (4) + (9 * r)).Value = Address.Rows(0).Item("Foundation")
          .Item(c, (5) + (9 * r)).Value = DryProduction.Rows(r).Item("Estimator") & " " & DryProduction.Rows(r).Item("Source")
          .Item(c, (6) + (9 * r)).Value = "Down"
          .Item(c, (7) + (9 * r)).Value = "Owed"
          .Item(c, (8) + (9 * r)).Value = "Total"
          .Item(c, (9) + (9 * r)).Value = "Pay Type"

          .Item(c, (1) + (9 * r)).Tag = Address.Rows(0).Item("ID")
          .Item(c, (2) + (9 * r)).Tag = Address.Rows(0).Item("ID")
          .Item(c, (3) + (9 * r)).Tag = Address.Rows(0).Item("ID")
          .Item(c, (4) + (9 * r)).Tag = Address.Rows(0).Item("ID")
          .Item(c, (5) + (9 * r)).Tag = Address.Rows(0).Item("ID")
          .Item(c, (6) + (9 * r)).Tag = Address.Rows(0).Item("ID")
          .Item(c, (7) + (9 * r)).Tag = Address.Rows(0).Item("ID")
          .Item(c, (8) + (9 * r)).Tag = Address.Rows(0).Item("ID")
          .Item(c, (9) + (9 * r)).Tag = Address.Rows(0).Item("ID")
        Next
        datProduction = DateAdd(DateInterval.Day, 1, datProduction)
      Next

      datProduction = DateAdd(DateInterval.Day, -5, datProduction)
    End With

  End Sub

  Private Sub cmdPrior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrior.Click
    datProduction = DateAdd(DateInterval.Day, -7, datProduction)
    If Me.Tag = "DRY" Then
      Dry_Load()
    ElseIf Me.Tag = "MAGIC" Then

    End If
  End Sub

  Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
    datProduction = DateAdd(DateInterval.Day, 7, datProduction)
    If Me.Tag = "DRY" Then
      Dry_Load()
    ElseIf Me.Tag = "MAGIC" Then

    End If

  End Sub

  Private Sub cmdGo_To_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGo_To.Click
    datProduction = dtpGoTo.Value.Date
    If Me.Tag = "DRY" Then
      Dry_Load()
    ElseIf Me.Tag = "MAGIC" Then

    End If
  End Sub

  Private Sub grdSchedule_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSchedule.CellContentClick

  End Sub

  Private Sub grdSchedule_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSchedule.CellDoubleClick
    If Me.Owner IsNot Nothing Then
      If Me.Owner.Name = "frmProduction" Then
        If grdSchedule.CurrentCell.Value = "" Then
          frmProduction.txtDryStart.Text = grdSchedule.Item(e.ColumnIndex, 0).Tag

          Manual_Update("UPDATE", "Dry_Production", "Start_Date = " & FilterByDate(Date.Parse(frmProduction.txtDryStart.Text)), "ID = " & frmProduction.tpgDry.Tag)

          Close()
        Else
          MsgBox("Please choose an empty spot.")
          Exit Sub
        End If
      Else
        dblAddressID = grdSchedule.CurrentCell.Tag
        frmLead.Show()
        Close()
      End If
    Else
      dblAddressID = grdSchedule.CurrentCell.Tag
      frmLead.Show()
      Close()
    End If
  End Sub
End Class