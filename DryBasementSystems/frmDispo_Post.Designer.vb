﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDispo_Post
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.fraValues = New System.Windows.Forms.GroupBox()
    Me.valDry_Dispo_ID = New System.Windows.Forms.TextBox()
    Me.valMagic_Dispo_ID = New System.Windows.Forms.TextBox()
    Me.valCombined = New System.Windows.Forms.CheckBox()
    Me.valAppt_Date = New System.Windows.Forms.TextBox()
    Me.valEstimator = New System.Windows.Forms.TextBox()
    Me.valAddress_ID = New System.Windows.Forms.TextBox()
    Me.txtMagic = New System.Windows.Forms.TextBox()
    Me.lblMagicPrice = New System.Windows.Forms.Label()
    Me.txtDry = New System.Windows.Forms.TextBox()
    Me.txtMagicPrice = New System.Windows.Forms.TextBox()
    Me.fraMagicStatus = New System.Windows.Forms.GroupBox()
    Me.optMagicDNS = New System.Windows.Forms.RadioButton()
    Me.optMagicSold = New System.Windows.Forms.RadioButton()
    Me.lblEstimator = New System.Windows.Forms.Label()
    Me.tabDispo = New System.Windows.Forms.TabControl()
    Me.tpgDry = New System.Windows.Forms.TabPage()
    Me.lblDryPrice = New System.Windows.Forms.Label()
    Me.txtDryPrice = New System.Windows.Forms.TextBox()
    Me.fraDryStatus = New System.Windows.Forms.GroupBox()
    Me.optDryDNS = New System.Windows.Forms.RadioButton()
    Me.optDrySold = New System.Windows.Forms.RadioButton()
    Me.tpgMagic = New System.Windows.Forms.TabPage()
    Me.txtEstimator = New System.Windows.Forms.TextBox()
    Me.grdDispo = New DryBasementSystems.Grid()
    Me.cmdDelete = New System.Windows.Forms.Button()
    Me.fraValues.SuspendLayout()
    Me.fraMagicStatus.SuspendLayout()
    Me.tabDispo.SuspendLayout()
    Me.tpgDry.SuspendLayout()
    Me.fraDryStatus.SuspendLayout()
    Me.tpgMagic.SuspendLayout()
    Me.SuspendLayout()
    '
    'fraValues
    '
    Me.fraValues.Controls.Add(Me.valDry_Dispo_ID)
    Me.fraValues.Controls.Add(Me.valMagic_Dispo_ID)
    Me.fraValues.Controls.Add(Me.valCombined)
    Me.fraValues.Controls.Add(Me.valAppt_Date)
    Me.fraValues.Controls.Add(Me.valEstimator)
    Me.fraValues.Controls.Add(Me.valAddress_ID)
    Me.fraValues.Location = New System.Drawing.Point(801, 7)
    Me.fraValues.Name = "fraValues"
    Me.fraValues.Size = New System.Drawing.Size(73, 165)
    Me.fraValues.TabIndex = 17
    Me.fraValues.TabStop = False
    Me.fraValues.Tag = "SKIP"
    Me.fraValues.Text = "Invisible"
    Me.fraValues.Visible = False
    '
    'valDry_Dispo_ID
    '
    Me.valDry_Dispo_ID.Location = New System.Drawing.Point(10, 139)
    Me.valDry_Dispo_ID.Name = "valDry_Dispo_ID"
    Me.valDry_Dispo_ID.Size = New System.Drawing.Size(54, 20)
    Me.valDry_Dispo_ID.TabIndex = 9
    Me.valDry_Dispo_ID.Tag = "MAGIC"
    '
    'valMagic_Dispo_ID
    '
    Me.valMagic_Dispo_ID.Location = New System.Drawing.Point(10, 113)
    Me.valMagic_Dispo_ID.Name = "valMagic_Dispo_ID"
    Me.valMagic_Dispo_ID.Size = New System.Drawing.Size(54, 20)
    Me.valMagic_Dispo_ID.TabIndex = 8
    Me.valMagic_Dispo_ID.Tag = "DRY"
    '
    'valCombined
    '
    Me.valCombined.AutoSize = True
    Me.valCombined.Location = New System.Drawing.Point(10, 93)
    Me.valCombined.Name = "valCombined"
    Me.valCombined.Size = New System.Drawing.Size(15, 14)
    Me.valCombined.TabIndex = 7
    Me.valCombined.UseVisualStyleBackColor = True
    '
    'valAppt_Date
    '
    Me.valAppt_Date.Location = New System.Drawing.Point(10, 67)
    Me.valAppt_Date.Name = "valAppt_Date"
    Me.valAppt_Date.Size = New System.Drawing.Size(54, 20)
    Me.valAppt_Date.TabIndex = 6
    Me.valAppt_Date.Tag = "BOTH"
    '
    'valEstimator
    '
    Me.valEstimator.Location = New System.Drawing.Point(10, 41)
    Me.valEstimator.Name = "valEstimator"
    Me.valEstimator.Size = New System.Drawing.Size(54, 20)
    Me.valEstimator.TabIndex = 5
    Me.valEstimator.Tag = "BOTH"
    '
    'valAddress_ID
    '
    Me.valAddress_ID.Location = New System.Drawing.Point(10, 15)
    Me.valAddress_ID.Name = "valAddress_ID"
    Me.valAddress_ID.Size = New System.Drawing.Size(54, 20)
    Me.valAddress_ID.TabIndex = 4
    Me.valAddress_ID.Tag = "BOTH"
    '
    'txtMagic
    '
    Me.txtMagic.Location = New System.Drawing.Point(14, 178)
    Me.txtMagic.Name = "txtMagic"
    Me.txtMagic.ReadOnly = True
    Me.txtMagic.Size = New System.Drawing.Size(860, 20)
    Me.txtMagic.TabIndex = 16
    '
    'lblMagicPrice
    '
    Me.lblMagicPrice.AutoSize = True
    Me.lblMagicPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMagicPrice.Location = New System.Drawing.Point(156, 29)
    Me.lblMagicPrice.Name = "lblMagicPrice"
    Me.lblMagicPrice.Size = New System.Drawing.Size(40, 17)
    Me.lblMagicPrice.TabIndex = 5
    Me.lblMagicPrice.Tag = "SKIP"
    Me.lblMagicPrice.Text = "Price"
    '
    'txtDry
    '
    Me.txtDry.Location = New System.Drawing.Point(14, 152)
    Me.txtDry.Name = "txtDry"
    Me.txtDry.ReadOnly = True
    Me.txtDry.Size = New System.Drawing.Size(860, 20)
    Me.txtDry.TabIndex = 15
    '
    'txtMagicPrice
    '
    Me.txtMagicPrice.Location = New System.Drawing.Point(214, 29)
    Me.txtMagicPrice.Name = "txtMagicPrice"
    Me.txtMagicPrice.Size = New System.Drawing.Size(85, 20)
    Me.txtMagicPrice.TabIndex = 4
    Me.txtMagicPrice.Tag = ""
    '
    'fraMagicStatus
    '
    Me.fraMagicStatus.Controls.Add(Me.optMagicDNS)
    Me.fraMagicStatus.Controls.Add(Me.optMagicSold)
    Me.fraMagicStatus.Location = New System.Drawing.Point(6, 6)
    Me.fraMagicStatus.Name = "fraMagicStatus"
    Me.fraMagicStatus.Size = New System.Drawing.Size(118, 58)
    Me.fraMagicStatus.TabIndex = 3
    Me.fraMagicStatus.TabStop = False
    Me.fraMagicStatus.Text = "Status"
    '
    'optMagicDNS
    '
    Me.optMagicDNS.AutoSize = True
    Me.optMagicDNS.Location = New System.Drawing.Point(58, 24)
    Me.optMagicDNS.Name = "optMagicDNS"
    Me.optMagicDNS.Size = New System.Drawing.Size(48, 17)
    Me.optMagicDNS.TabIndex = 1
    Me.optMagicDNS.TabStop = True
    Me.optMagicDNS.Tag = ""
    Me.optMagicDNS.Text = "DNS"
    Me.optMagicDNS.UseVisualStyleBackColor = True
    '
    'optMagicSold
    '
    Me.optMagicSold.AutoSize = True
    Me.optMagicSold.Location = New System.Drawing.Point(6, 24)
    Me.optMagicSold.Name = "optMagicSold"
    Me.optMagicSold.Size = New System.Drawing.Size(54, 17)
    Me.optMagicSold.TabIndex = 0
    Me.optMagicSold.TabStop = True
    Me.optMagicSold.Tag = ""
    Me.optMagicSold.Text = "SOLD"
    Me.optMagicSold.UseVisualStyleBackColor = True
    '
    'lblEstimator
    '
    Me.lblEstimator.AutoSize = True
    Me.lblEstimator.Location = New System.Drawing.Point(8, 19)
    Me.lblEstimator.Name = "lblEstimator"
    Me.lblEstimator.Size = New System.Drawing.Size(50, 13)
    Me.lblEstimator.TabIndex = 11
    Me.lblEstimator.Text = "Estimator"
    '
    'tabDispo
    '
    Me.tabDispo.Controls.Add(Me.tpgDry)
    Me.tabDispo.Controls.Add(Me.tpgMagic)
    Me.tabDispo.Location = New System.Drawing.Point(11, 225)
    Me.tabDispo.Name = "tabDispo"
    Me.tabDispo.SelectedIndex = 0
    Me.tabDispo.Size = New System.Drawing.Size(868, 319)
    Me.tabDispo.TabIndex = 9
    '
    'tpgDry
    '
    Me.tpgDry.AutoScroll = True
    Me.tpgDry.BackColor = System.Drawing.SystemColors.Control
    Me.tpgDry.Controls.Add(Me.lblDryPrice)
    Me.tpgDry.Controls.Add(Me.txtDryPrice)
    Me.tpgDry.Controls.Add(Me.fraDryStatus)
    Me.tpgDry.Location = New System.Drawing.Point(4, 22)
    Me.tpgDry.Name = "tpgDry"
    Me.tpgDry.Padding = New System.Windows.Forms.Padding(3)
    Me.tpgDry.Size = New System.Drawing.Size(860, 293)
    Me.tpgDry.TabIndex = 0
    Me.tpgDry.Text = "Dry Basement"
    '
    'lblDryPrice
    '
    Me.lblDryPrice.AutoSize = True
    Me.lblDryPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblDryPrice.Location = New System.Drawing.Point(155, 29)
    Me.lblDryPrice.Name = "lblDryPrice"
    Me.lblDryPrice.Size = New System.Drawing.Size(40, 17)
    Me.lblDryPrice.TabIndex = 3
    Me.lblDryPrice.Tag = "SKIP"
    Me.lblDryPrice.Text = "Price"
    '
    'txtDryPrice
    '
    Me.txtDryPrice.Location = New System.Drawing.Point(213, 29)
    Me.txtDryPrice.Name = "txtDryPrice"
    Me.txtDryPrice.Size = New System.Drawing.Size(85, 20)
    Me.txtDryPrice.TabIndex = 2
    Me.txtDryPrice.Tag = ""
    '
    'fraDryStatus
    '
    Me.fraDryStatus.Controls.Add(Me.optDryDNS)
    Me.fraDryStatus.Controls.Add(Me.optDrySold)
    Me.fraDryStatus.Location = New System.Drawing.Point(6, 6)
    Me.fraDryStatus.Name = "fraDryStatus"
    Me.fraDryStatus.Size = New System.Drawing.Size(118, 58)
    Me.fraDryStatus.TabIndex = 1
    Me.fraDryStatus.TabStop = False
    Me.fraDryStatus.Text = "Status"
    '
    'optDryDNS
    '
    Me.optDryDNS.AutoSize = True
    Me.optDryDNS.Location = New System.Drawing.Point(58, 24)
    Me.optDryDNS.Name = "optDryDNS"
    Me.optDryDNS.Size = New System.Drawing.Size(48, 17)
    Me.optDryDNS.TabIndex = 1
    Me.optDryDNS.TabStop = True
    Me.optDryDNS.Tag = ""
    Me.optDryDNS.Text = "DNS"
    Me.optDryDNS.UseVisualStyleBackColor = True
    '
    'optDrySold
    '
    Me.optDrySold.AutoSize = True
    Me.optDrySold.Location = New System.Drawing.Point(6, 24)
    Me.optDrySold.Name = "optDrySold"
    Me.optDrySold.Size = New System.Drawing.Size(54, 17)
    Me.optDrySold.TabIndex = 0
    Me.optDrySold.TabStop = True
    Me.optDrySold.Tag = ""
    Me.optDrySold.Text = "SOLD"
    Me.optDrySold.UseVisualStyleBackColor = True
    '
    'tpgMagic
    '
    Me.tpgMagic.BackColor = System.Drawing.SystemColors.Control
    Me.tpgMagic.Controls.Add(Me.lblMagicPrice)
    Me.tpgMagic.Controls.Add(Me.txtMagicPrice)
    Me.tpgMagic.Controls.Add(Me.fraMagicStatus)
    Me.tpgMagic.Location = New System.Drawing.Point(4, 22)
    Me.tpgMagic.Name = "tpgMagic"
    Me.tpgMagic.Padding = New System.Windows.Forms.Padding(3)
    Me.tpgMagic.Size = New System.Drawing.Size(860, 293)
    Me.tpgMagic.TabIndex = 1
    Me.tpgMagic.Text = "Basement Magic"
    '
    'txtEstimator
    '
    Me.txtEstimator.BackColor = System.Drawing.SystemColors.Window
    Me.txtEstimator.Location = New System.Drawing.Point(63, 14)
    Me.txtEstimator.Name = "txtEstimator"
    Me.txtEstimator.ReadOnly = True
    Me.txtEstimator.Size = New System.Drawing.Size(146, 20)
    Me.txtEstimator.TabIndex = 20
    '
    'grdDispo
    '
    Me.grdDispo.AutoScroll = True
    Me.grdDispo.BackColor = System.Drawing.SystemColors.ControlDarkDark
    Me.grdDispo.Bottom_Row = ""
    Me.grdDispo.Cols = ""
    Me.grdDispo.Left_Col = ""
    Me.grdDispo.Location = New System.Drawing.Point(11, 48)
    Me.grdDispo.Name = "grdDispo"
    Me.grdDispo.Right_Col = ""
    Me.grdDispo.Rows = ""
    Me.grdDispo.Size = New System.Drawing.Size(864, 98)
    Me.grdDispo.TabIndex = 18
    Me.grdDispo.Top_Row = ""
    '
    'cmdDelete
    '
    Me.cmdDelete.Location = New System.Drawing.Point(762, 204)
    Me.cmdDelete.Name = "cmdDelete"
    Me.cmdDelete.Size = New System.Drawing.Size(94, 37)
    Me.cmdDelete.TabIndex = 4
    Me.cmdDelete.Text = "Delete Current Dispo"
    Me.cmdDelete.UseVisualStyleBackColor = True
    '
    'frmDispo_Post
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSize = True
    Me.ClientSize = New System.Drawing.Size(895, 584)
    Me.Controls.Add(Me.cmdDelete)
    Me.Controls.Add(Me.fraValues)
    Me.Controls.Add(Me.txtEstimator)
    Me.Controls.Add(Me.grdDispo)
    Me.Controls.Add(Me.txtMagic)
    Me.Controls.Add(Me.txtDry)
    Me.Controls.Add(Me.lblEstimator)
    Me.Controls.Add(Me.tabDispo)
    Me.Name = "frmDispo_Post"
    Me.Text = "Dispo_Post"
    Me.fraValues.ResumeLayout(False)
    Me.fraValues.PerformLayout()
    Me.fraMagicStatus.ResumeLayout(False)
    Me.fraMagicStatus.PerformLayout()
    Me.tabDispo.ResumeLayout(False)
    Me.tpgDry.ResumeLayout(False)
    Me.tpgDry.PerformLayout()
    Me.fraDryStatus.ResumeLayout(False)
    Me.fraDryStatus.PerformLayout()
    Me.tpgMagic.ResumeLayout(False)
    Me.tpgMagic.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents fraValues As System.Windows.Forms.GroupBox
  Friend WithEvents valDry_Dispo_ID As System.Windows.Forms.TextBox
  Friend WithEvents valMagic_Dispo_ID As System.Windows.Forms.TextBox
  Friend WithEvents valCombined As System.Windows.Forms.CheckBox
  Friend WithEvents valAppt_Date As System.Windows.Forms.TextBox
  Friend WithEvents valEstimator As System.Windows.Forms.TextBox
  Friend WithEvents valAddress_ID As System.Windows.Forms.TextBox
  Friend WithEvents txtMagic As System.Windows.Forms.TextBox
  Friend WithEvents lblMagicPrice As System.Windows.Forms.Label
  Friend WithEvents txtDry As System.Windows.Forms.TextBox
  Friend WithEvents txtMagicPrice As System.Windows.Forms.TextBox
  Friend WithEvents fraMagicStatus As System.Windows.Forms.GroupBox
  Friend WithEvents optMagicDNS As System.Windows.Forms.RadioButton
  Friend WithEvents optMagicSold As System.Windows.Forms.RadioButton
  Friend WithEvents lblEstimator As System.Windows.Forms.Label
  Friend WithEvents tabDispo As System.Windows.Forms.TabControl
  Friend WithEvents tpgDry As System.Windows.Forms.TabPage
  Friend WithEvents lblDryPrice As System.Windows.Forms.Label
  Friend WithEvents txtDryPrice As System.Windows.Forms.TextBox
  Friend WithEvents fraDryStatus As System.Windows.Forms.GroupBox
  Friend WithEvents optDryDNS As System.Windows.Forms.RadioButton
  Friend WithEvents optDrySold As System.Windows.Forms.RadioButton
  Friend WithEvents tpgMagic As System.Windows.Forms.TabPage
  Friend WithEvents grdDispo As DryBasementSystems.Grid
  Friend WithEvents txtEstimator As System.Windows.Forms.TextBox
  Friend WithEvents cmdDelete As System.Windows.Forms.Button
End Class
