﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStart
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStart))
    Me.cmdLead = New System.Windows.Forms.Button()
    Me.cmdDispo = New System.Windows.Forms.Button()
    Me.cmdRay = New System.Windows.Forms.Button()
    Me.cmdSearch = New System.Windows.Forms.Button()
    Me.cmdReports = New System.Windows.Forms.Button()
    Me.fraSelection = New System.Windows.Forms.GroupBox()
    Me.fraSchedules = New System.Windows.Forms.GroupBox()
    Me.fraInvento = New System.Windows.Forms.GroupBox()
    Me.Button1 = New System.Windows.Forms.Button()
    Me.Button2 = New System.Windows.Forms.Button()
    Me.Button3 = New System.Windows.Forms.Button()
    Me.Button4 = New System.Windows.Forms.Button()
    Me.Button5 = New System.Windows.Forms.Button()
    Me.cmdPlumbing = New System.Windows.Forms.Button()
    Me.cmdAnnual = New System.Windows.Forms.Button()
    Me.cmdPreJob = New System.Windows.Forms.Button()
    Me.cmdEstSchedule = New System.Windows.Forms.Button()
    Me.cmdProduction_Schedule = New System.Windows.Forms.Button()
    Me.txtAnnoucements = New System.Windows.Forms.TextBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.cmdSchedules = New System.Windows.Forms.Button()
    Me.cmdMaintenance = New System.Windows.Forms.Button()
    Me.cmdCallList = New System.Windows.Forms.Button()
    Me.timAnnouce = New System.Windows.Forms.Timer(Me.components)
    Me.fraAnnouce = New System.Windows.Forms.GroupBox()
    Me.cmdSave = New System.Windows.Forms.Button()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.txtAnnoucement = New System.Windows.Forms.TextBox()
    Me.lblExpire = New System.Windows.Forms.Label()
    Me.dtpExpire = New System.Windows.Forms.DateTimePicker()
    Me.cmdInventory = New System.Windows.Forms.Button()
    Me.cmdInventoryReport = New System.Windows.Forms.Button()
    Me.fraSelection.SuspendLayout()
    Me.fraSchedules.SuspendLayout()
    Me.fraInvento.SuspendLayout()
    Me.fraAnnouce.SuspendLayout()
    Me.SuspendLayout()
    '
    'cmdLead
    '
    Me.cmdLead.Location = New System.Drawing.Point(12, 12)
    Me.cmdLead.Name = "cmdLead"
    Me.cmdLead.Size = New System.Drawing.Size(83, 34)
    Me.cmdLead.TabIndex = 0
    Me.cmdLead.Text = "Take Lead"
    Me.cmdLead.UseVisualStyleBackColor = True
    '
    'cmdDispo
    '
    Me.cmdDispo.Location = New System.Drawing.Point(12, 92)
    Me.cmdDispo.Name = "cmdDispo"
    Me.cmdDispo.Size = New System.Drawing.Size(83, 34)
    Me.cmdDispo.TabIndex = 5
    Me.cmdDispo.Text = "Dispo"
    Me.cmdDispo.UseVisualStyleBackColor = True
    '
    'cmdRay
    '
    Me.cmdRay.Location = New System.Drawing.Point(12, 442)
    Me.cmdRay.Name = "cmdRay"
    Me.cmdRay.Size = New System.Drawing.Size(83, 34)
    Me.cmdRay.TabIndex = 6
    Me.cmdRay.Text = "Ray Form"
    Me.cmdRay.UseVisualStyleBackColor = True
    Me.cmdRay.Visible = False
    '
    'cmdSearch
    '
    Me.cmdSearch.Location = New System.Drawing.Point(12, 52)
    Me.cmdSearch.Name = "cmdSearch"
    Me.cmdSearch.Size = New System.Drawing.Size(83, 34)
    Me.cmdSearch.TabIndex = 7
    Me.cmdSearch.Text = "Search"
    Me.cmdSearch.UseVisualStyleBackColor = True
    '
    'cmdReports
    '
    Me.cmdReports.Location = New System.Drawing.Point(12, 132)
    Me.cmdReports.Name = "cmdReports"
    Me.cmdReports.Size = New System.Drawing.Size(83, 34)
    Me.cmdReports.TabIndex = 9
    Me.cmdReports.Text = "Reports"
    Me.cmdReports.UseVisualStyleBackColor = True
    '
    'fraSelection
    '
    Me.fraSelection.Controls.Add(Me.fraSchedules)
    Me.fraSelection.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.fraSelection.Location = New System.Drawing.Point(186, 212)
    Me.fraSelection.Name = "fraSelection"
    Me.fraSelection.Size = New System.Drawing.Size(678, 332)
    Me.fraSelection.TabIndex = 10
    Me.fraSelection.TabStop = False
    Me.fraSelection.Text = "Selection"
    '
    'fraSchedules
    '
    Me.fraSchedules.Controls.Add(Me.fraInvento)
    Me.fraSchedules.Controls.Add(Me.cmdPlumbing)
    Me.fraSchedules.Controls.Add(Me.cmdAnnual)
    Me.fraSchedules.Controls.Add(Me.cmdPreJob)
    Me.fraSchedules.Controls.Add(Me.cmdEstSchedule)
    Me.fraSchedules.Controls.Add(Me.cmdProduction_Schedule)
    Me.fraSchedules.Location = New System.Drawing.Point(6, 25)
    Me.fraSchedules.Name = "fraSchedules"
    Me.fraSchedules.Size = New System.Drawing.Size(666, 301)
    Me.fraSchedules.TabIndex = 9
    Me.fraSchedules.TabStop = False
    Me.fraSchedules.Text = "Schedules"
    Me.fraSchedules.Visible = False
    '
    'fraInvento
    '
    Me.fraInvento.Controls.Add(Me.Button1)
    Me.fraInvento.Controls.Add(Me.Button2)
    Me.fraInvento.Controls.Add(Me.Button3)
    Me.fraInvento.Controls.Add(Me.Button4)
    Me.fraInvento.Controls.Add(Me.Button5)
    Me.fraInvento.Location = New System.Drawing.Point(0, 0)
    Me.fraInvento.Name = "fraInvento"
    Me.fraInvento.Size = New System.Drawing.Size(666, 301)
    Me.fraInvento.TabIndex = 14
    Me.fraInvento.TabStop = False
    Me.fraInvento.Text = "Schedules"
    Me.fraInvento.Visible = False
    '
    'Button1
    '
    Me.Button1.Location = New System.Drawing.Point(357, 25)
    Me.Button1.Name = "Button1"
    Me.Button1.Size = New System.Drawing.Size(111, 59)
    Me.Button1.TabIndex = 13
    Me.Button1.Text = "Plumbing Schedule"
    Me.Button1.UseVisualStyleBackColor = True
    '
    'Button2
    '
    Me.Button2.Location = New System.Drawing.Point(240, 25)
    Me.Button2.Name = "Button2"
    Me.Button2.Size = New System.Drawing.Size(111, 59)
    Me.Button2.TabIndex = 12
    Me.Button2.Text = "Annual Schedule"
    Me.Button2.UseVisualStyleBackColor = True
    '
    'Button3
    '
    Me.Button3.Location = New System.Drawing.Point(123, 25)
    Me.Button3.Name = "Button3"
    Me.Button3.Size = New System.Drawing.Size(111, 59)
    Me.Button3.TabIndex = 11
    Me.Button3.Text = "PreJob Schedule"
    Me.Button3.UseVisualStyleBackColor = True
    '
    'Button4
    '
    Me.Button4.Location = New System.Drawing.Point(6, 25)
    Me.Button4.Name = "Button4"
    Me.Button4.Size = New System.Drawing.Size(111, 59)
    Me.Button4.TabIndex = 9
    Me.Button4.Text = "Estimator's Schedule"
    Me.Button4.UseVisualStyleBackColor = True
    '
    'Button5
    '
    Me.Button5.Location = New System.Drawing.Point(474, 25)
    Me.Button5.Name = "Button5"
    Me.Button5.Size = New System.Drawing.Size(111, 59)
    Me.Button5.TabIndex = 10
    Me.Button5.Text = "Production Schedule"
    Me.Button5.UseVisualStyleBackColor = True
    '
    'cmdPlumbing
    '
    Me.cmdPlumbing.Location = New System.Drawing.Point(357, 25)
    Me.cmdPlumbing.Name = "cmdPlumbing"
    Me.cmdPlumbing.Size = New System.Drawing.Size(111, 59)
    Me.cmdPlumbing.TabIndex = 13
    Me.cmdPlumbing.Text = "Plumbing Schedule"
    Me.cmdPlumbing.UseVisualStyleBackColor = True
    '
    'cmdAnnual
    '
    Me.cmdAnnual.Location = New System.Drawing.Point(240, 25)
    Me.cmdAnnual.Name = "cmdAnnual"
    Me.cmdAnnual.Size = New System.Drawing.Size(111, 59)
    Me.cmdAnnual.TabIndex = 12
    Me.cmdAnnual.Text = "Annual Schedule"
    Me.cmdAnnual.UseVisualStyleBackColor = True
    '
    'cmdPreJob
    '
    Me.cmdPreJob.Location = New System.Drawing.Point(123, 25)
    Me.cmdPreJob.Name = "cmdPreJob"
    Me.cmdPreJob.Size = New System.Drawing.Size(111, 59)
    Me.cmdPreJob.TabIndex = 11
    Me.cmdPreJob.Text = "PreJob Schedule"
    Me.cmdPreJob.UseVisualStyleBackColor = True
    '
    'cmdEstSchedule
    '
    Me.cmdEstSchedule.Location = New System.Drawing.Point(6, 25)
    Me.cmdEstSchedule.Name = "cmdEstSchedule"
    Me.cmdEstSchedule.Size = New System.Drawing.Size(111, 59)
    Me.cmdEstSchedule.TabIndex = 9
    Me.cmdEstSchedule.Text = "Estimator's Schedule"
    Me.cmdEstSchedule.UseVisualStyleBackColor = True
    '
    'cmdProduction_Schedule
    '
    Me.cmdProduction_Schedule.Location = New System.Drawing.Point(474, 25)
    Me.cmdProduction_Schedule.Name = "cmdProduction_Schedule"
    Me.cmdProduction_Schedule.Size = New System.Drawing.Size(111, 59)
    Me.cmdProduction_Schedule.TabIndex = 10
    Me.cmdProduction_Schedule.Text = "Production Schedule"
    Me.cmdProduction_Schedule.UseVisualStyleBackColor = True
    '
    'txtAnnoucements
    '
    Me.txtAnnoucements.Location = New System.Drawing.Point(186, 40)
    Me.txtAnnoucements.Multiline = True
    Me.txtAnnoucements.Name = "txtAnnoucements"
    Me.txtAnnoucements.ReadOnly = True
    Me.txtAnnoucements.Size = New System.Drawing.Size(678, 154)
    Me.txtAnnoucements.TabIndex = 11
    '
    'Label1
    '
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(181, 12)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(683, 27)
    Me.Label1.TabIndex = 12
    Me.Label1.Text = "Announcements"
    Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'cmdSchedules
    '
    Me.cmdSchedules.Location = New System.Drawing.Point(12, 172)
    Me.cmdSchedules.Name = "cmdSchedules"
    Me.cmdSchedules.Size = New System.Drawing.Size(83, 34)
    Me.cmdSchedules.TabIndex = 13
    Me.cmdSchedules.Text = "Schedules"
    Me.cmdSchedules.UseVisualStyleBackColor = True
    '
    'cmdMaintenance
    '
    Me.cmdMaintenance.Location = New System.Drawing.Point(12, 292)
    Me.cmdMaintenance.Name = "cmdMaintenance"
    Me.cmdMaintenance.Size = New System.Drawing.Size(83, 34)
    Me.cmdMaintenance.TabIndex = 14
    Me.cmdMaintenance.Text = "Up Keep"
    Me.cmdMaintenance.UseVisualStyleBackColor = True
    '
    'cmdCallList
    '
    Me.cmdCallList.Location = New System.Drawing.Point(12, 212)
    Me.cmdCallList.Name = "cmdCallList"
    Me.cmdCallList.Size = New System.Drawing.Size(83, 34)
    Me.cmdCallList.TabIndex = 15
    Me.cmdCallList.Text = "Call Lists"
    Me.cmdCallList.UseVisualStyleBackColor = True
    '
    'timAnnouce
    '
    Me.timAnnouce.Enabled = True
    Me.timAnnouce.Interval = 300000
    '
    'fraAnnouce
    '
    Me.fraAnnouce.Controls.Add(Me.cmdSave)
    Me.fraAnnouce.Controls.Add(Me.Label2)
    Me.fraAnnouce.Controls.Add(Me.txtAnnoucement)
    Me.fraAnnouce.Controls.Add(Me.lblExpire)
    Me.fraAnnouce.Controls.Add(Me.dtpExpire)
    Me.fraAnnouce.Location = New System.Drawing.Point(226, 42)
    Me.fraAnnouce.Name = "fraAnnouce"
    Me.fraAnnouce.Size = New System.Drawing.Size(598, 151)
    Me.fraAnnouce.TabIndex = 16
    Me.fraAnnouce.TabStop = False
    Me.fraAnnouce.Text = "Annoucement"
    Me.fraAnnouce.Visible = False
    '
    'cmdSave
    '
    Me.cmdSave.Location = New System.Drawing.Point(503, 46)
    Me.cmdSave.Name = "cmdSave"
    Me.cmdSave.Size = New System.Drawing.Size(79, 21)
    Me.cmdSave.TabIndex = 4
    Me.cmdSave.Text = "Save"
    Me.cmdSave.UseVisualStyleBackColor = True
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(59, 16)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(474, 13)
    Me.Label2.TabIndex = 3
    Me.Label2.Text = "Annoucement must be less than 255 characters and will delete automatically on the" & _
    " expiration date."
    '
    'txtAnnoucement
    '
    Me.txtAnnoucement.Location = New System.Drawing.Point(6, 75)
    Me.txtAnnoucement.MaxLength = 255
    Me.txtAnnoucement.Multiline = True
    Me.txtAnnoucement.Name = "txtAnnoucement"
    Me.txtAnnoucement.Size = New System.Drawing.Size(586, 70)
    Me.txtAnnoucement.TabIndex = 2
    '
    'lblExpire
    '
    Me.lblExpire.AutoSize = True
    Me.lblExpire.Location = New System.Drawing.Point(10, 55)
    Me.lblExpire.Name = "lblExpire"
    Me.lblExpire.Size = New System.Drawing.Size(36, 13)
    Me.lblExpire.TabIndex = 1
    Me.lblExpire.Text = "Expire"
    '
    'dtpExpire
    '
    Me.dtpExpire.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpExpire.Location = New System.Drawing.Point(52, 49)
    Me.dtpExpire.Name = "dtpExpire"
    Me.dtpExpire.Size = New System.Drawing.Size(164, 20)
    Me.dtpExpire.TabIndex = 0
    '
    'cmdInventory
    '
    Me.cmdInventory.Location = New System.Drawing.Point(12, 252)
    Me.cmdInventory.Name = "cmdInventory"
    Me.cmdInventory.Size = New System.Drawing.Size(83, 34)
    Me.cmdInventory.TabIndex = 17
    Me.cmdInventory.Text = "Inventory"
    Me.cmdInventory.UseVisualStyleBackColor = True
    '
    'cmdInventoryReport
    '
    Me.cmdInventoryReport.Location = New System.Drawing.Point(12, 332)
    Me.cmdInventoryReport.Name = "cmdInventoryReport"
    Me.cmdInventoryReport.Size = New System.Drawing.Size(83, 34)
    Me.cmdInventoryReport.TabIndex = 18
    Me.cmdInventoryReport.Text = "Inventory Report"
    Me.cmdInventoryReport.UseVisualStyleBackColor = True
    '
    'frmStart
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(876, 569)
    Me.Controls.Add(Me.cmdInventoryReport)
    Me.Controls.Add(Me.cmdInventory)
    Me.Controls.Add(Me.fraAnnouce)
    Me.Controls.Add(Me.cmdCallList)
    Me.Controls.Add(Me.cmdMaintenance)
    Me.Controls.Add(Me.cmdSchedules)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.cmdRay)
    Me.Controls.Add(Me.txtAnnoucements)
    Me.Controls.Add(Me.fraSelection)
    Me.Controls.Add(Me.cmdReports)
    Me.Controls.Add(Me.cmdSearch)
    Me.Controls.Add(Me.cmdDispo)
    Me.Controls.Add(Me.cmdLead)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frmStart"
    Me.Text = "Dry Basement Enterprises"
    Me.fraSelection.ResumeLayout(False)
    Me.fraSchedules.ResumeLayout(False)
    Me.fraInvento.ResumeLayout(False)
    Me.fraAnnouce.ResumeLayout(False)
    Me.fraAnnouce.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents cmdLead As System.Windows.Forms.Button
  Friend WithEvents cmdDispo As System.Windows.Forms.Button
  Friend WithEvents cmdRay As System.Windows.Forms.Button
  Friend WithEvents cmdSearch As System.Windows.Forms.Button
  Friend WithEvents cmdReports As System.Windows.Forms.Button
  Friend WithEvents fraSelection As System.Windows.Forms.GroupBox
  Friend WithEvents txtAnnoucements As System.Windows.Forms.TextBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents fraSchedules As System.Windows.Forms.GroupBox
  Friend WithEvents cmdEstSchedule As System.Windows.Forms.Button
  Friend WithEvents cmdProduction_Schedule As System.Windows.Forms.Button
  Friend WithEvents cmdSchedules As System.Windows.Forms.Button
  Friend WithEvents cmdMaintenance As System.Windows.Forms.Button
  Friend WithEvents cmdPlumbing As System.Windows.Forms.Button
  Friend WithEvents cmdAnnual As System.Windows.Forms.Button
  Friend WithEvents cmdPreJob As System.Windows.Forms.Button
  Friend WithEvents cmdCallList As System.Windows.Forms.Button
  Friend WithEvents timAnnouce As System.Windows.Forms.Timer
  Friend WithEvents fraAnnouce As System.Windows.Forms.GroupBox
  Friend WithEvents cmdSave As System.Windows.Forms.Button
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents txtAnnoucement As System.Windows.Forms.TextBox
  Friend WithEvents lblExpire As System.Windows.Forms.Label
  Friend WithEvents dtpExpire As System.Windows.Forms.DateTimePicker
  Friend WithEvents fraInvento As System.Windows.Forms.GroupBox
  Friend WithEvents Button1 As System.Windows.Forms.Button
  Friend WithEvents Button2 As System.Windows.Forms.Button
  Friend WithEvents Button3 As System.Windows.Forms.Button
  Friend WithEvents Button4 As System.Windows.Forms.Button
  Friend WithEvents Button5 As System.Windows.Forms.Button
  Friend WithEvents cmdInventory As System.Windows.Forms.Button
  Friend WithEvents cmdInventoryReport As System.Windows.Forms.Button


End Class
