﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCall_List
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.grdCallList = New System.Windows.Forms.DataGridView()
    Me.cboCallList = New System.Windows.Forms.ComboBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.cmdGo = New System.Windows.Forms.Button()
    CType(Me.grdCallList, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'grdCallList
    '
    Me.grdCallList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdCallList.Location = New System.Drawing.Point(12, 100)
    Me.grdCallList.Name = "grdCallList"
    Me.grdCallList.Size = New System.Drawing.Size(810, 164)
    Me.grdCallList.TabIndex = 0
    '
    'cboCallList
    '
    Me.cboCallList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboCallList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboCallList.FormattingEnabled = True
    Me.cboCallList.Location = New System.Drawing.Point(83, 12)
    Me.cboCallList.Name = "cboCallList"
    Me.cboCallList.Size = New System.Drawing.Size(221, 21)
    Me.cboCallList.TabIndex = 1
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(9, 15)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(48, 13)
    Me.Label1.TabIndex = 2
    Me.Label1.Text = "Call Lists"
    '
    'cmdGo
    '
    Me.cmdGo.Location = New System.Drawing.Point(320, 12)
    Me.cmdGo.Name = "cmdGo"
    Me.cmdGo.Size = New System.Drawing.Size(37, 20)
    Me.cmdGo.TabIndex = 3
    Me.cmdGo.Text = "Go"
    Me.cmdGo.UseVisualStyleBackColor = True
    '
    'frmCall_List
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(834, 415)
    Me.Controls.Add(Me.cmdGo)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.cboCallList)
    Me.Controls.Add(Me.grdCallList)
    Me.Name = "frmCall_List"
    Me.Text = "Call List"
    CType(Me.grdCallList, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents grdCallList As System.Windows.Forms.DataGridView
  Friend WithEvents cboCallList As System.Windows.Forms.ComboBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents cmdGo As System.Windows.Forms.Button
End Class
