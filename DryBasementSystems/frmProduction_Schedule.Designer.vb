﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProduction_Schedule
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.grdSchedule = New System.Windows.Forms.DataGridView
    Me.cmdGo_To = New System.Windows.Forms.Button
    Me.dtpGoTo = New System.Windows.Forms.DateTimePicker
    Me.cmdNext = New System.Windows.Forms.Button
    Me.cmdPrior = New System.Windows.Forms.Button
    CType(Me.grdSchedule, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'grdSchedule
    '
    Me.grdSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdSchedule.Location = New System.Drawing.Point(12, 40)
    Me.grdSchedule.Name = "grdSchedule"
    Me.grdSchedule.Size = New System.Drawing.Size(954, 491)
    Me.grdSchedule.TabIndex = 0
    '
    'cmdGo_To
    '
    Me.cmdGo_To.Location = New System.Drawing.Point(466, 11)
    Me.cmdGo_To.Name = "cmdGo_To"
    Me.cmdGo_To.Size = New System.Drawing.Size(78, 20)
    Me.cmdGo_To.TabIndex = 14
    Me.cmdGo_To.Text = "Go To Date"
    Me.cmdGo_To.UseVisualStyleBackColor = True
    '
    'dtpGoTo
    '
    Me.dtpGoTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpGoTo.Location = New System.Drawing.Point(310, 14)
    Me.dtpGoTo.Name = "dtpGoTo"
    Me.dtpGoTo.Size = New System.Drawing.Size(136, 20)
    Me.dtpGoTo.TabIndex = 13
    '
    'cmdNext
    '
    Me.cmdNext.Location = New System.Drawing.Point(124, 8)
    Me.cmdNext.Name = "cmdNext"
    Me.cmdNext.Size = New System.Drawing.Size(97, 19)
    Me.cmdNext.TabIndex = 12
    Me.cmdNext.Text = "Next Week"
    Me.cmdNext.UseVisualStyleBackColor = True
    '
    'cmdPrior
    '
    Me.cmdPrior.Location = New System.Drawing.Point(14, 8)
    Me.cmdPrior.Name = "cmdPrior"
    Me.cmdPrior.Size = New System.Drawing.Size(87, 20)
    Me.cmdPrior.TabIndex = 11
    Me.cmdPrior.Text = "Prior Week"
    Me.cmdPrior.UseVisualStyleBackColor = True
    '
    'Production_Schedule
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1008, 561)
    Me.Controls.Add(Me.cmdGo_To)
    Me.Controls.Add(Me.dtpGoTo)
    Me.Controls.Add(Me.cmdNext)
    Me.Controls.Add(Me.cmdPrior)
    Me.Controls.Add(Me.grdSchedule)
    Me.Name = "Production_Schedule"
    Me.Text = "Production_Schedule"
    CType(Me.grdSchedule, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents grdSchedule As System.Windows.Forms.DataGridView
  Friend WithEvents cmdGo_To As System.Windows.Forms.Button
  Friend WithEvents dtpGoTo As System.Windows.Forms.DateTimePicker
  Friend WithEvents cmdNext As System.Windows.Forms.Button
  Friend WithEvents cmdPrior As System.Windows.Forms.Button
End Class
