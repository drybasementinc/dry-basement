﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCalendar
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.cmdGo_To = New System.Windows.Forms.Button()
    Me.dtpGoTo = New System.Windows.Forms.DateTimePicker()
    Me.cmdNext = New System.Windows.Forms.Button()
    Me.cmdPrior = New System.Windows.Forms.Button()
    Me.calEverything = New DryBasementSystems.Grid()
    Me.SuspendLayout()
    '
    'cmdGo_To
    '
    Me.cmdGo_To.Location = New System.Drawing.Point(464, 14)
    Me.cmdGo_To.Name = "cmdGo_To"
    Me.cmdGo_To.Size = New System.Drawing.Size(78, 20)
    Me.cmdGo_To.TabIndex = 10
    Me.cmdGo_To.Text = "Go To Date"
    Me.cmdGo_To.UseVisualStyleBackColor = True
    '
    'dtpGoTo
    '
    Me.dtpGoTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpGoTo.Location = New System.Drawing.Point(308, 17)
    Me.dtpGoTo.Name = "dtpGoTo"
    Me.dtpGoTo.Size = New System.Drawing.Size(136, 20)
    Me.dtpGoTo.TabIndex = 9
    '
    'cmdNext
    '
    Me.cmdNext.Location = New System.Drawing.Point(122, 11)
    Me.cmdNext.Name = "cmdNext"
    Me.cmdNext.Size = New System.Drawing.Size(97, 19)
    Me.cmdNext.TabIndex = 8
    Me.cmdNext.Text = "Next Month"
    Me.cmdNext.UseVisualStyleBackColor = True
    '
    'cmdPrior
    '
    Me.cmdPrior.Location = New System.Drawing.Point(12, 11)
    Me.cmdPrior.Name = "cmdPrior"
    Me.cmdPrior.Size = New System.Drawing.Size(87, 20)
    Me.cmdPrior.TabIndex = 7
    Me.cmdPrior.Text = "Prior Month"
    Me.cmdPrior.UseVisualStyleBackColor = True
    '
    'calEverything
    '
    Me.calEverything.AutoScroll = True
    Me.calEverything.BackColor = System.Drawing.SystemColors.ControlDarkDark
    Me.calEverything.Bottom_Row = ""
    Me.calEverything.Cols = ""
    Me.calEverything.Left_Col = ""
    Me.calEverything.Location = New System.Drawing.Point(12, 45)
    Me.calEverything.Name = "calEverything"
    Me.calEverything.Right_Col = ""
    Me.calEverything.Rows = ""
    Me.calEverything.Size = New System.Drawing.Size(965, 513)
    Me.calEverything.TabIndex = 6
    Me.calEverything.Top_Row = ""
    '
    'frmCalendar
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(987, 570)
    Me.Controls.Add(Me.cmdGo_To)
    Me.Controls.Add(Me.dtpGoTo)
    Me.Controls.Add(Me.cmdNext)
    Me.Controls.Add(Me.cmdPrior)
    Me.Controls.Add(Me.calEverything)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frmCalendar"
    Me.ShowInTaskbar = False
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
    Me.Text = "Calendar"
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cmdGo_To As System.Windows.Forms.Button
  Friend WithEvents dtpGoTo As System.Windows.Forms.DateTimePicker
  Friend WithEvents cmdNext As System.Windows.Forms.Button
  Friend WithEvents cmdPrior As System.Windows.Forms.Button
  Friend WithEvents calEverything As DryBasementSystems.Grid

End Class
