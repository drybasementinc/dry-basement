﻿Public Class frmSchedule
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form Specific Declarations
  Dim strCurrentCell As String
  Dim dblCurrentRow As Double
  Dim dblCurrentCol As Double
  Dim Schedule As New DataTable
  Dim drSchedule As Double
  Dim strScheduleFields As String
  Dim strScheduleValues As String
  Dim Extra As New DataTable
  Dim drExtra As Double
  Public valSchedule As String
  Dim flgTopDate As Boolean = False

  Private Sub Schedule_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    'e.Cancel = True
    'datDate = Now
    'Schedule_Load()
    'grdSchedule.Tag = ""
    fraSchedule.Tag = ""
    For i As Integer = 0 To fraSchedule.Controls.Count - 1
      If TypeOf fraSchedule.Controls(i) Is Label = False And TypeOf fraSchedule.Controls(i) Is Button = False Then
        fraSchedule.Controls(i).Text = ""
      End If
    Next
    For i As Integer = 0 To fraSave_Items.Controls.Count - 1
      If TypeOf fraSave_Items.Controls(i) Is Label = False And TypeOf fraSave_Items.Controls(i) Is Button = False Then
        If TypeOf (fraSave_Items.Controls(i)) Is CheckBox Then
          Dim chk As New CheckBox
          chk = fraSave_Items.Controls(i)
          chk.Checked = False
        End If
        fraSave_Items.Controls(i).Text = ""
      End If
    Next
    fraSchedule.Visible = False
    fraEdit.Visible = False
    'Me.WindowState = FormWindowState.Minimized
    'Me.Hide()
    'timRefresh.Enabled = False
  End Sub

  Private Sub frmSchedule_HandleDestroyed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.HandleDestroyed

  End Sub

  Private Sub frmSchedule1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

  End Sub

  Private Sub Schedule_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Connect("Appt_Types", Extra, drExtra, , "Appt_Type")
    drExtra = 0
    cboApptType.Items.Clear()
    For drExtra As Double = 0 To Extra.Rows.Count - 1
      cboApptType.Items.Add(Extra.Rows(drExtra).Item("Appt_Type"))
    Next
    Add_Combo_Check(Me)
  End Sub

  Private Sub frmSchedule1_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseLeave

  End Sub

  Private Sub frmSchedule_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    On Error Resume Next
    timRefresh.Enabled = True
    grdSchedule.Width = Me.Width - 25
    grdSchedule.Height = Me.Height - 100
    'schedule_Resize()
  End Sub

  Private Sub cmdPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrevious.Click
    datDate = DateAdd(DateInterval.Day, -7, datDate)
    Schedule_Load()
  End Sub

  Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
    datDate = DateAdd(DateInterval.Day, 7, datDate)
    Schedule_Load()
  End Sub

  Public Sub grdSchedule_CellDoubleClick(ByVal sender As Object, ByVal Owner As Object, ByVal CurrentRow As Double, ByVal CurrentCol As Double) Handles grdSchedule.CellDoubleClick
    If grdSchedule.Valid_Cell(sender) = False Then Exit Sub
    With grdSchedule
      'If .Cell(CurrentRow, CurrentCol) Is Nothing = False Then
      strCurrentCell = CurrentRow & "," & CurrentCol
      dblCurrentCol = CurrentCol
      dblCurrentRow = CurrentRow
      valDate.Text = .Cell(dblCurrentRow, 0).tag
      valAddress_ID.Text = .Cell(dblCurrentRow, dblCurrentCol).tag
      valPerson.Text = .Cell(0, dblCurrentCol).text
      'End If

      If mnuDayOff.Checked = True Or mnuResetDay.Checked = True Or mnuAvailable.Checked = True Then
        If mnuAvailable.Checked = True Then
          Connect("Schedule", Schedule, drSchedule = 0, "Estimator_Appt = 1 And Date = " & FilterByDate(DateValue(valDate.Text)) & " And Person = '" & valPerson.Text & "'")
          If Schedule.Rows.Count < 5 Then
            Manual_Update("Insert Into", "Schedule", "(Estimator_Appt, Person, [Date], Info, BackColor, FontColor) Values(True, '" & valPerson.Text & "', " & FilterByDate(DateValue(valDate.Text)) & ", 'AVAILABLE', 'BLUE', 'BLACK')")
          End If
        Else
          Dim strBack As String
          If mnuResetDay.Checked = True Then
            strResponse = "RESET"
            strBack = "GREEN"
          Else
            strResponse = "OFF"
            strBack = "GREY"
          End If
          Connect("Schedule", Schedule, drSchedule = 0, "Estimator_Appt = 1 And Date = " & FilterByDate(DateValue(valDate.Text)) & " And Person = '" & valPerson.Text & "'")
          While Schedule.Rows.Count < 6
            Manual_Update("Insert Into", "Schedule", "(Estimator_Appt, Person, [Date], Info, Address_ID, BackColor, FontColor) Values(" & True & ", '" & valPerson.Text & "', " & FilterByDate(DateValue(valDate.Text)) & ", '" & strResponse & "', 0, '" & strBack & "', 'BLACK')")
            Connect("Schedule", Schedule, drSchedule = 0, "Estimator_Appt = 1 And Date = " & FilterByDate(DateValue(valDate.Text)) & " And Person = '" & valPerson.Text & "'")
            If Schedule.Rows.Count = 5 Then Exit While
          End While
        End If
        Schedule_Load()
        Exit Sub
      End If
      If .Tag = "LEAD" Then
        If .Cell(CurrentRow, CurrentCol).text = "AVAILABLE" Or .Cell(CurrentRow, CurrentCol).text = "OFF" Or .Cell(CurrentRow, CurrentCol).text = "RESET" Then
          fraSchedule.Tag = "OVERWRITE"
        ElseIf .Cell(CurrentRow, CurrentCol).text <> "" Then
          MsgBox("Please choose an empty cell.")
          Exit Sub
        End If
        lblApptType.Visible = True
        cboApptType.Visible = True
        lblDetails.Visible = False
        txtDetails.Visible = False
        fraSchedule.Visible = True
      Else
        If .Cell(CurrentRow, CurrentCol).text = "AVAILABLE" Or .Cell(CurrentRow, CurrentCol).text = "OFF" Or .Cell(CurrentRow, CurrentCol).text = "RESET DAY" Then
          strResponse = MsgBox("Would you like to remove this block?", MsgBoxStyle.YesNo)
          If strResponse = vbNo Then Exit Sub
          TableUpdate("DELETE", "schedule", "", "", Me, "ID = " & .Cell(CurrentRow, CurrentCol).tag)
          Schedule_Load()
        ElseIf .Cell(CurrentRow, CurrentCol).text <> "" Then
          fraEdit.Visible = True
          cboBackColor2.Text = Color_to_String(grdSchedule.Cell(dblCurrentRow, dblCurrentCol).backcolor)
          cboFontColor2.Text = Color_to_String(grdSchedule.Cell(dblCurrentRow, dblCurrentCol).forecolor)

        Else
          fraSchedule.Visible = True
        End If
      End If
    End With
  End Sub

  Private Sub grdSchedule_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdSchedule.Load

  End Sub

  Private Sub timRefresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timRefresh.Tick
    'Estimator_Load()
    Schedule_Load()
  End Sub

  Private Sub txtTime_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStart_Time.LostFocus
    If txtStart_Time.Text = "" Then Exit Sub
    Select Case Len(txtStart_Time.Text)
      Case 1
        txtStart_Time.Text = txtStart_Time.Text & ":00"
      Case 2
        txtStart_Time.Text = txtStart_Time.Text & ":00"
      Case 3
        txtStart_Time.SelectionStart = 0
        txtStart_Time.SelectionLength = 1
        strResponse = txtStart_Time.SelectedText & ":"
        txtStart_Time.Text = strResponse & LT(txtStart_Time.Text, 1)
      Case 4
        txtStart_Time.SelectionStart = 0
        txtStart_Time.SelectionLength = 2
        strResponse = txtStart_Time.SelectedText & ":"
        txtStart_Time.Text = strResponse & LT(txtStart_Time.Text, 2)
      Case 5
        'correct time
      Case Else
        MsgBox("Cannot convert. Please try again.")
        txtStart_Time.Text = ""
        txtStart_Time.Focus()
    End Select
    Try
      txtStart_Time.Text = Format(TimeValue(txtStart_Time.Text), "H:mm")
    Catch ex As Exception
      MsgBox("Could not convert. Please try again.")
      txtStart_Time.Text = ""
      txtStart_Time.Focus()
    End Try
  End Sub

  Private Sub txtTime_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtStart_Time.TextChanged

  End Sub

  Private Sub cboApptType_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApptType.LostFocus
    If cboApptType.Text = "" Then Exit Sub
    If cboApptType.Items.Contains(cboApptType.Text) = False Then
      cboApptType.Text = ""
      MsgBox("Please choose from the given selection.")
      cboApptType.Focus()
    End If
  End Sub

  Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
    fraSchedule.Visible = False
    txtStart_Time.Text = ""
    cboApptType.Text = ""
    cboStart_Time.Text = ""
  End Sub

  Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
    Dim flgFailed As Boolean
    Dim Rules As New DataTable
    Dim drRules As Double = 0
    With grdSchedule

      'check that all information is present
      If txtStart_Time.Text = "" Or cboStart_Time.Text = "" Then
        MsgBox("You must provide a time.")
        txtStart_Time.Focus()
        Exit Sub
      ElseIf cboApptType.Text = "" Then
        MsgBox("You must provide the appointment type.")
        cboApptType.Focus()
        Exit Sub
      End If

      If frmLead.Visible = True Then
        Connect("Schedule", Schedule, drSchedule, "Date >= " & FilterByDate(Now) & " and Address_ID = " & dblAddressID & " And Not ID = Address_ID", "[Date], [Start_Time]")
        drSchedule = 0
        If Schedule.Rows.Count > 0 Then
          strResponse = MsgBox("There are " & Schedule.Rows.Count & " appointment scheduled from today on. Would you like to add another?", vbYesNo)
          If strResponse = vbNo Then Exit Sub
          strResponse = MsgBox("Would you like to delete any of the current appointments?", vbYesNo)
          If strResponse = vbYes Then
            While drSchedule < Schedule.Rows.Count
                If Schedule.Rows(drSchedule).Item("Start_Time") > Format(Now, "short time") Then
                strResponse = MsgBox("There is an appointment set for this address on " & Schedule.Rows(drSchedule).Item("Date") & " at " & Schedule.Rows(drSchedule).Item("Start_Time") & ". Would you like to delete it?", MsgBoxStyle.YesNo)
                  If strResponse = vbYes Then
                    TableUpdate("Delete", "schedule", "", "", Me, "ID = " & Schedule.Rows(drSchedule).Item("ID"))
                  End If
                End If
              drSchedule += 1
            End While
          End If
        End If

        Debug.Print(.Cell(dblCurrentRow, 0).text)
        If flgTopDate = False Then
          Connect("schedule", Schedule, drSchedule, "Date = " & FilterByDate(Date.Parse(grdSchedule.Cell(dblCurrentRow, 0).tag)) & " And Person = '" & grdSchedule.Cell(0, dblCurrentCol).text & "'", "[Date], [Start_Time]")
        Else
          Connect("schedule", Schedule, drSchedule, "Date = " & FilterByDate(Date.Parse(grdSchedule.Cell(0, dblCurrentCol).tag)) & " And Person = '" & grdSchedule.Cell(dblCurrentRow, 0).text & "'", "[Date], [Start_Time]")
        End If

        drSchedule = 0

        Dim datStartTime As Date = DateValue("12/30/1899")
        Dim datEndTime As Date = DateValue("12/30/1899")


        datStartTime = datStartTime.AddHours(Val(txtStart_Time.Text))
        If cboStart_Time.Text = "PM" Then datStartTime = datStartTime.AddHours(12)
        datStartTime = datStartTime.AddMinutes(LT(txtStart_Time.Text, Len(txtStart_Time.Text) - 2))

        datEndTime = datEndTime.AddHours(Val(txtEnd_Time.Text))
        If cboEnd_Time.Text = "PM" Then datEndTime = datEndTime.AddHours(12)
        datEndTime = datEndTime.AddMinutes(LT(txtEnd_Time.Text, Len(txtEnd_Time.Text) - 2))

        If Schedule.Rows.Count > 0 Then
          For drSchedule As Double = 0 To Schedule.Rows.Count - 1
            If IsDBNull(Schedule.Rows(drSchedule).Item("Start_Time")) = False And IsDBNull(Schedule.Rows(drSchedule).Item("End_Time")) = False Then

              If DateDiff(DateInterval.Minute, datStartTime, Schedule.Rows(drSchedule).Item("Start_Time")) <= 0 And DateDiff(DateInterval.Minute, datStartTime, Schedule.Rows(drSchedule).Item("End_Time")) >= 0 Or _
                DateDiff(DateInterval.Minute, datEndTime, Schedule.Rows(drSchedule).Item("Start_Time")) <= 0 And DateDiff(DateInterval.Minute, datEndTime, Schedule.Rows(drSchedule).Item("End_Time")) >= 0 Or _
                DateDiff(DateInterval.Minute, datStartTime, Schedule.Rows(drSchedule).Item("Start_Time")) >= 0 And DateDiff(DateInterval.Minute, datEndTime, Schedule.Rows(drSchedule).Item("End_Time")) <= 0 Then

                'If DateDiff(DateInterval.Minute, DateValue(txtStart_Time.Text & " " & cboStart_Time.Text).TimeOfDay, Schedule.Rows(drSchedule).Item("Start_Time").timeofday) - 60 And DateDiff(DateInterval.Minute, DateValue(txtStart_Time.Text & " " & cboStart_Time.Text), Schedule.Rows(drSchedule).Item("Start_Time")) < 60 Then
                'If DateDiff(DateInterval.Minute, DateValue(txtTime.Text & " " & cboTime.Text), Schedule.Rows(drSchedule).Item("Time")) > -60 And DateDiff(DateInterval.Minute, DateValue(txtTime.Text & " " & cboTime.Text), Schedule.Rows(drSchedule).Item("Time")) < 60 Then
                MsgBox("There is already an appointment scheduled within that time. Please choose another time.")
                txtStart_Time.Focus()
                Exit Sub
              End If
            End If
          Next
        End If

        flgFailed = True

        'Connect("Schedule_Rules", Rules, drRules)
        'drRules = 0
        'If Rules.Rows.Count > 0 Then
        '  For drRules = 0 To Rules.Rows.Count - 1
        '    For i As Integer = 0 To Rules.Columns.Count - 1
        '      If flgFailed = False Then
        '        'do nothing
        '      Else
        '        If IsDBNull(Rules.Rows(drRules).Item(i)) = False Then
        '          If InStr(Rules.Columns(i).Caption, "Operator", CompareMethod.Text) > 0 Then
        '            If Rules.Rows(drRules).Item(i) <> "" Then
        '              Select Case Rules.Rows(drRules).Item(i)
        '                Case ">"
        '                  If InStr(Rules.Columns(i).Caption, "ate", CompareMethod.Text) > 0 Then
        '                    If DateDiff(DateInterval.Day, Rules.Rows(drRules).Item(Replace(Rules.Columns(i).Caption, "_Operator", "")), DateValue(Format(DateValue(.Cell(dblCurrentRow, 0).tag), "short date"))) > 0 Then
        '                      'do nothing
        '                    Else
        '                      flgFailed = False
        '                    End If
        '                  ElseIf InStr(Rules.Columns(i).Caption, "ime", CompareMethod.Text) > 0 Then
        '                    If DateDiff(DateInterval.Minute, DateValue(Rules.Rows(drRules).Item(Replace(Rules.Columns(i).Caption, "_Operator", ""))), TimeValue(txtStart_Time.Text & " " & cboStart_Time.Text)) > 0 Then
        '                      'do nothing
        '                    Else
        '                      flgFailed = False
        '                    End If
        '                  End If

        '                Case "="
        '                  If InStr(Rules.Columns(i).Caption, "ate", CompareMethod.Text) > 0 Then
        '                    If DateDiff(DateInterval.Day, Rules.Rows(drRules).Item(Replace(Rules.Columns(i).Caption, "_Operator", "")), DateValue(Format(DateValue(.Cell(dblCurrentRow, 0).tag), "short date"))) = 0 Then
        '                      'do nothing
        '                    Else
        '                      flgFailed = False
        '                    End If
        '                  ElseIf InStr(Rules.Columns(i).Caption, "ime", CompareMethod.Text) > 0 Then
        '                    If DateDiff(DateInterval.Minute, DateValue(Rules.Rows(drRules).Item(Replace(Rules.Columns(i).Caption, "_Operator", ""))), TimeValue(txtStart_Time.Text & " " & cboStart_Time.Text)) = 0 Then
        '                      'do nothing
        '                    Else
        '                      flgFailed = False
        '                    End If
        '                  ElseIf InStr(Rules.Columns(i).Caption, "ay", CompareMethod.Text) > 0 Then
        '                    If Rules.Rows(drRules).Item(Replace(Rules.Columns(i).Caption, "_Operator", "")) = Format(DateValue(.Cell(dblCurrentRow, 0).tag), "dddd") Then
        '                      'do nothing
        '                    Else
        '                      flgFailed = False
        '                    End If
        '                  Else
        '                    If Rules.Rows(drRules).Item(i) = .Cell(0, dblCurrentCol) Then
        '                      'do nothing
        '                    Else
        '                      flgFailed = False
        '                    End If

        '                  End If

        '                Case "<"
        '                  If InStr(Rules.Columns(i).Caption, "ate", CompareMethod.Text) > 0 Then
        '                    If DateDiff(DateInterval.Day, Rules.Rows(drRules).Item(Replace(Rules.Columns(i).Caption, "_Operator", "")), DateValue(Format(DateValue(.Cell(dblCurrentRow, 0).tag), "short date"))) < 0 Then
        '                      'do nothing
        '                    Else
        '                      flgFailed = False
        '                    End If
        '                  ElseIf InStr(Rules.Columns(i).Caption, "ime", CompareMethod.Text) > 0 Then
        '                    If DateDiff(DateInterval.Minute, DateValue(Rules.Rows(drRules).Item(Replace(Rules.Columns(i).Caption, "_Operator", ""))), TimeValue(txtStart_Time.Text & " " & cboStart_Time.Text)) < 0 Then
        '                      'do nothing
        '                    Else
        '                      flgFailed = False
        '                    End If
        '                  End If
        '              End Select
        '            End If
        '          End If
        '        End If
        '      End If
        '    Next
        '    If flgFailed = True Then
        '      MsgBox(Rules.Rows(drRules).Item("Message"))
        '      If strUserName = "Ray" Or strUserName = "Jenny" Then
        '        strResponse = "none"
        '        While strResponse <> "Drybase"
        '          strResponse = InputBox("Override:")
        '          If strResponse = "" Then Exit Sub
        '        End While
        '      Else
        '        Exit Sub
        '      End If
        '    End If
        '  Next
        'End If
        'finally save the damn appointment
        If fraSchedule.Tag = "OVERWRITE" Then
          fraSchedule.Tag = ""
          TableUpdate("DELETE", "schedule", "", "", Me, "ID = " & .Cell(dblCurrentRow, dblCurrentCol).tag)
        End If
        valStart_Time.Text = txtStart_Time.Text & " " & cboStart_Time.Text
        valEnd_Time.Text = txtEnd_Time.Text & " " & cboEnd_Time.Text
        valAddress_ID.Text = dblAddressID
        If flgTopDate = False Then
          valPerson.Text = .Cell(0, dblCurrentCol).text
          valDate.Text = .Cell(dblCurrentRow, 0).tag
        Else
          If valSchedule = "prejob" Then
            valPerson.Text = "TIM"
          Else
            valPerson.Text = .Cell(dblCurrentRow, 0).text
          End If
          valDate.Text = .Cell(0, dblCurrentCol).tag

        End If
        valAppt_Type.Text = cboApptType.Text

        valOperator.Text = strUserName
        valBonus_Sig.Text = strUserName
        valEstimator_Appt.Checked = False
        valAlways_Appt.Checked = False
        valAnnual_Appt.Checked = False
        Select Case LCase(valSchedule)
          Case "estimators"
            valEstimator_Appt.Checked = True
          Case "plumbing"
            valAlways_Appt.Checked = True
          Case "annual"
            valAnnual_Appt.Checked = True

        End Select


        valFontColor.Text = cboFontColor.Text
        valBackColor.Text = cboBackColor.Text
        valExtra_Info.Text = txtDetails.Text
        valSet_Date.Text = Now.Date
        valSource.Text = strSource

        Try
          valInfo.Text = frmLead.txtZip.Text & " " & frmLead.cboCity.Text
        Catch ex As Exception
          'do nothing
        End Try

        Compiler("Insert into", Me, "schedule")
        Try
          fraSave_Items.Controls.Add(valEnd_Time)
          TableUpdate("Insert into", "schedule", strSaveSQL1, strValues1, Me)
          If flgFailed = False Then MsgBox("Appointment Saved.")
          strSource = ""
          Me.Close()
        Catch ex As Exception
          MsgBox("Save Failed")
          Exit Sub
        End Try
      End If
    End With
  End Sub

  Private Sub cmdEditExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditExit.Click
    fraEdit.Visible = False
  End Sub

  Private Sub cmdOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOpen.Click
    fraEdit.Visible = False
    dblAddressID = grdSchedule.Cell(dblCurrentRow, dblCurrentCol).tag
    frmLead.Show()
  End Sub

  Private Sub cmdColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdColor.Click
    fraEdit.Visible = False

    Manual_Update("UPDATE", "schedule", "BackColor = '" & cboBackColor2.Text & "', FontColor = '" & cboFontColor2.Text & "'", "Address_ID = " & grdSchedule.Cell(dblCurrentRow, dblCurrentCol).tag & " And Date = " & FilterByDate(Date.Parse(grdSchedule.Cell(dblCurrentRow, 0).tag)))

    Schedule_Load()
  End Sub

  Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
    fraEdit.Visible = False
    strResponse = ""
    strResponse = MsgBox("Do you want to cancel this appointment?", MsgBoxStyle.YesNo)
    If strResponse = "" Or strResponse = vbNo Then Exit Sub
    If flgTopDate = True Then
      TableUpdate("DELETE", "schedule", "", "", Me, "Address_ID = " & grdSchedule.Cell(dblCurrentRow, dblCurrentCol).tag & " And Date = " & FilterByDate(Date.Parse(grdSchedule.Cell(0, dblCurrentCol).tag)) & " AND Person = '" & grdSchedule.Cell(dblCurrentRow, 0).text & "'")
    Else
      TableUpdate("DELETE", "schedule", "", "", Me, "Address_ID = " & grdSchedule.Cell(dblCurrentRow, dblCurrentCol).tag & " And Date = " & FilterByDate(Date.Parse(grdSchedule.Cell(dblCurrentRow, 0).tag)) & " AND Person = '" & grdSchedule.Cell(0, dblCurrentCol).text & "'")
    End If
    Schedule_Load()
  End Sub

  Public Sub Schedule_Maker(ByVal Schedule_Type As String)
    grdSchedule.Visible = False
    Select Case LCase(Schedule_Type)
      Case "estimators"
        flgTopDate = False
        valSchedule = LCase(Schedule_Type)
        grdSchedule.Rows = 36

        Connect("Employees", Extra, drExtra, "Group_Name = 'Estimators' and Active = 1", "[Position]")
        drExtra = 0

        grdSchedule.Cols = Extra.Rows.Count + 1
        grdSchedule.Create(Val(Screen.PrimaryScreen.Bounds.Size.Width.ToString) / grdSchedule.Cols, 26.75, Me)

        grdSchedule.Cell(0, 0).backcolor = Color.LightGray
        grdSchedule.Cell(0, 0).TextAlign = HorizontalAlignment.Left
        grdSchedule.CellBorder(0, 0).topborder = True
        grdSchedule.CellBorder(0, 0).bottomborder = True
        grdSchedule.CellBorder(0, 0).rightborder = True
        grdSchedule.CellBorder(0, 0).leftborder = True
        grdSchedule.ApplyRowFormat(0, 0)
        grdSchedule.ApplyColFormat(0, 0)


        grdSchedule.ColWidth(0, 50)

        grdSchedule.Merge("1,0", "5,0")
        grdSchedule.Merge("6,0", "10,0")
        grdSchedule.Merge("11,0", "15,0")
        grdSchedule.Merge("16,0", "20,0")
        grdSchedule.Merge("21,0", "25,0")
        grdSchedule.Merge("26,0", "30,0")
        grdSchedule.Merge("31,0", "35,0")

      Case "prejob"
        flgTopDate = True
        valSchedule = LCase(Schedule_Type)
        grdSchedule.Rows = 36
        grdSchedule.Cols = 10
        grdSchedule.Create(50, 20, Me)

        c = 1
        While c < grdSchedule.Cols
          grdSchedule.ColWidth(c, 200)
          c += 2
        End While


        c = 0
        While c < grdSchedule.Cols
          grdSchedule.CellBorder(0, c).topborder = True
          grdSchedule.CellBorder(0, c).bottomborder = True
          grdSchedule.CellBorder(0, c).leftborder = True
          grdSchedule.CellBorder(0, c).rightborder = True

          grdSchedule.CellBorder(0, c + 1).topborder = True
          grdSchedule.CellBorder(0, c + 1).bottomborder = True
          grdSchedule.CellBorder(0, c + 1).leftborder = True
          grdSchedule.CellBorder(0, c + 1).rightborder = True

          grdSchedule.Merge("1," & c, "5," & c)
          grdSchedule.CellBorder(1, c).topborder = True
          grdSchedule.CellBorder(1, c).bottomborder = True
          grdSchedule.CellBorder(1, c).leftborder = True
          grdSchedule.CellBorder(1, c).rightborder = True

          grdSchedule.Merge("6," & c, "10," & c)
          grdSchedule.CellBorder(6, c).topborder = True
          grdSchedule.CellBorder(6, c).bottomborder = True
          grdSchedule.CellBorder(6, c).leftborder = True
          grdSchedule.CellBorder(6, c).rightborder = True

          grdSchedule.Merge("11," & c, "15," & c)
          grdSchedule.CellBorder(11, c).topborder = True
          grdSchedule.CellBorder(11, c).bottomborder = True
          grdSchedule.CellBorder(11, c).leftborder = True
          grdSchedule.CellBorder(11, c).rightborder = True

          grdSchedule.Merge("16," & c, "20," & c)
          grdSchedule.CellBorder(16, c).topborder = True
          grdSchedule.CellBorder(16, c).bottomborder = True
          grdSchedule.CellBorder(16, c).leftborder = True
          grdSchedule.CellBorder(16, c).rightborder = True

          grdSchedule.Merge("21," & c, "25," & c)
          grdSchedule.CellBorder(21, c).topborder = True
          grdSchedule.CellBorder(21, c).bottomborder = True
          grdSchedule.CellBorder(21, c).leftborder = True
          grdSchedule.CellBorder(21, c).rightborder = True

          grdSchedule.Merge("26," & c, "30," & c)
          grdSchedule.CellBorder(26, c).topborder = True
          grdSchedule.CellBorder(26, c).bottomborder = True
          grdSchedule.CellBorder(26, c).leftborder = True
          grdSchedule.CellBorder(26, c).rightborder = True

          grdSchedule.Merge("31," & c, "35," & c)
          grdSchedule.CellBorder(31, c).topborder = True
          grdSchedule.CellBorder(31, c).bottomborder = True
          grdSchedule.CellBorder(31, c).leftborder = True
          grdSchedule.CellBorder(31, c).rightborder = True
          c += 2
        End While

        c = 1
        While c < grdSchedule.Cols
          r = 0
          While r < grdSchedule.Rows - 1
            grdSchedule.CellBorder(r + 1, c).topborder = True
            grdSchedule.CellBorder(r + 5, c).bottomborder = True
            grdSchedule.CellBorder(r + 1, c).leftborder = True
            grdSchedule.CellBorder(r + 1, c).rightborder = True
            grdSchedule.CellBorder(r + 2, c).leftborder = True
            grdSchedule.CellBorder(r + 2, c).rightborder = True
            grdSchedule.CellBorder(r + 3, c).leftborder = True
            grdSchedule.CellBorder(r + 3, c).rightborder = True
            grdSchedule.CellBorder(r + 4, c).leftborder = True
            grdSchedule.CellBorder(r + 4, c).rightborder = True
            grdSchedule.CellBorder(r + 5, c).leftborder = True
            grdSchedule.CellBorder(r + 5, c).rightborder = True
            r += 5
          End While
          c += 2
        End While

      Case "plumbing"
        flgTopDate = True
        valSchedule = LCase(Schedule_Type)
        Connect("Employees", Extra, drExtra, "Group_Name = 'Plumbing' and Active = 1", "[Position]")
        drExtra = 0
        grdSchedule.Rows = 8 * Extra.Rows.Count + 1
        grdSchedule.Cols = 13
        grdSchedule.Create(50, 30, Me)

        grdSchedule.ColWidth(0, 100)
        c = 2
        While c < grdSchedule.Cols
          grdSchedule.ColWidth(c, 200)
          c += 2
        End While

        Dim introw As Integer
        For intCounter = 0 To Extra.Rows.Count - 1
          introw = 8 * intCounter + 1
          grdSchedule.Merge(introw & ",0", introw + 7 & ",0")
          grdSchedule.CellBorder(introw, 0).topborder = True
          grdSchedule.CellBorder(introw, 0).bottomborder = True
          grdSchedule.CellBorder(introw, 0).leftborder = True
          grdSchedule.CellBorder(introw, 0).rightborder = True
        Next

        c = 1
        r = 1
        For r As Integer = 1 To grdSchedule.Rows.Count - 1
          For c As Integer = 1 To grdSchedule.Cols.Count - 1
            grdSchedule.CellBorder(r, c).topborder = True
            grdSchedule.CellBorder(r, c).bottomborder = True
            grdSchedule.CellBorder(r, c).leftborder = True
            grdSchedule.CellBorder(r, c).rightborder = True
          Next
        Next

      Case "annual"
        flgTopDate = True
        valSchedule = LCase(Schedule_Type)
        Connect("Employees", Extra, drExtra, "Group_Name = 'Annual' and Active = 1", "[Position]")
        drExtra = 0
        grdSchedule.Rows = 8 * Extra.Rows.Count + 1
        grdSchedule.Cols = 11
        grdSchedule.Create(50, 30, Me)

        grdSchedule.ColWidth(0, 100)
        c = 2
        While c < grdSchedule.Cols
          grdSchedule.ColWidth(c, 200)
          c += 2
        End While

        Dim introw As Integer
        For intCounter = 0 To Extra.Rows.Count - 1
          introw = 8 * intCounter + 1
          grdSchedule.Merge(introw & ",0", introw + 7 & ",0")
          grdSchedule.CellBorder(introw, 0).topborder = True
          grdSchedule.CellBorder(introw, 0).bottomborder = True
          grdSchedule.CellBorder(introw, 0).leftborder = True
          grdSchedule.CellBorder(introw, 0).rightborder = True
        Next

        c = 1
        r = 1
        For r As Integer = 1 To grdSchedule.Rows.Count - 1
          For c As Integer = 1 To grdSchedule.Cols.Count - 1
            grdSchedule.CellBorder(r, c).topborder = True
            grdSchedule.CellBorder(r, c).bottomborder = True
            grdSchedule.CellBorder(r, c).leftborder = True
            grdSchedule.CellBorder(r, c).rightborder = True
          Next
        Next

    End Select
    Schedule_Load()
    grdSchedule.Visible = True
  End Sub

  Public Sub Schedule_Load()

    timRefresh.Enabled = False
    Select Case LCase(valSchedule)
      Case "estimators"

        ''original
        grdSchedule.Clear(1, 1)

        Connect("Employees", Extra, drExtra, "Group_Name = 'Estimators' and Active = 1", "[Position]")
        drExtra = 0

        datDate = Format(datDate, "M/d/yyyy")

        While Not datDate.DayOfWeek = DayOfWeek.Monday
          datDate = DateAdd(DateInterval.Day, -1, datDate)
        End While

        grdSchedule.Cell(1, 0).text = "MON" & (Chr(13) + Chr(10)) & Format(DateAdd("d", 0, datDate), "M/d/yy")
        grdSchedule.Cell(6, 0).text = "TUES" & (Chr(13) + Chr(10)) & Format(DateAdd("d", 1, datDate), "M/d/yy")
        grdSchedule.Cell(11, 0).text = "WED" & (Chr(13) + Chr(10)) & Format(DateAdd("d", 2, datDate), "M/d/yy")
        grdSchedule.Cell(16, 0).text = "THURS" & (Chr(13) + Chr(10)) & Format(DateAdd("d", 3, datDate), "M/d/yy")
        grdSchedule.Cell(21, 0).text = "FRI" & (Chr(13) + Chr(10)) & Format(DateAdd("d", 4, datDate), "M/d/yy")
        grdSchedule.Cell(26, 0).text = "SAT" & (Chr(13) + Chr(10)) & Format(DateAdd("d", 5, datDate), "M/d/yy")
        grdSchedule.Cell(31, 0).text = "SUN" & (Chr(13) + Chr(10)) & Format(DateAdd("d", 6, datDate), "M/d/yy")

        grdSchedule.Cell(1, 0).tag = Format(DateAdd("d", 0, datDate), "M/d/yy")
        grdSchedule.Cell(6, 0).tag = Format(DateAdd("d", 1, datDate), "M/d/yy")
        grdSchedule.Cell(11, 0).tag = Format(DateAdd("d", 2, datDate), "M/d/yy")
        grdSchedule.Cell(16, 0).tag = Format(DateAdd("d", 3, datDate), "M/d/yy")
        grdSchedule.Cell(21, 0).tag = Format(DateAdd("d", 4, datDate), "M/d/yy")
        grdSchedule.Cell(26, 0).tag = Format(DateAdd("d", 5, datDate), "M/d/yy")
        grdSchedule.Cell(31, 0).tag = Format(DateAdd("d", 6, datDate), "M/d/yy")

        Connect("Employees", Extra, drExtra, "Group_Name = 'Estimators' And Active = 1", "[Position]")
        drExtra = 0

        For c As Integer = 1 To grdSchedule.Cols - 1
          grdSchedule.Cell(0, c).text = UCase(Extra.Rows(drExtra).Item("Display_Name"))
          grdSchedule.Cell(0, c).TextAlign = HorizontalAlignment.Center

          Connect("schedule", Schedule, drSchedule, "Person = '" & Extra.Rows(drExtra).Item("Display_Name") & "' and Date >= " & FilterByDate(datDate) & " and Date <= " & FilterByDate(DateAdd(DateInterval.Day, 6, datDate)) & " And Estimator_Appt = 1", "Date,Start_Time")

          drSchedule = 0

          Dim intRow As Integer = 1
          For drSchedule As Double = 0 To Schedule.Rows.Count - 1
            If Schedule.Rows(drSchedule).Item("Date") = DateValue(grdSchedule.Cell(intRow, 0).tag) Then
              If IsDBNull(Schedule.Rows(drSchedule).Item("Start_Time")) = False Then
                grdSchedule.Cell(intRow, c).text = Format(TimeValue(Schedule.Rows(drSchedule).Item("Start_Time")), "h:mm tt") & " " & Schedule.Rows(drSchedule).Item("Appt_Type") & " " & Schedule.Rows(drSchedule).Item("Info") & " " & Schedule.Rows(drSchedule).Item("Detail")
              Else
                grdSchedule.Cell(intRow, c).text = Schedule.Rows(drSchedule).Item("Appt_Type") & " " & Schedule.Rows(drSchedule).Item("Info") & " " & Schedule.Rows(drSchedule).Item("Detail")
              End If
              grdSchedule.Cell(intRow, c).text = grdSchedule.Cell(intRow, c).text.replace("NORMAL", "EST") & ""
              grdSchedule.Cell(intRow, c).tag = Schedule.Rows(drSchedule).Item("Address_ID")
              grdSchedule.Cell(intRow, c).backcolor = String_to_Color(Schedule.Rows(drSchedule).Item("BackColor") & "")
              grdSchedule.Cell(intRow, c).forecolor = String_to_Color(Schedule.Rows(drSchedule).Item("FontColor") & "")
              If Schedule.Rows(drSchedule).Item("Bold") = True Then
                grdSchedule.Cell(intRow, c).font = Font_Change("Sax Mono", 6, FontStyle.Bold)
              Else
                grdSchedule.Cell(intRow, c).font = Font_Change("Sax Mono", 6, FontStyle.Regular)
              End If
              intRow = intRow + 1
            Else
              Select Case DateValue(Schedule.Rows(drSchedule).Item("Date")).DayOfWeek
                Case DayOfWeek.Monday
                  intRow = 1
                Case DayOfWeek.Tuesday
                  intRow = 6
                Case DayOfWeek.Wednesday
                  intRow = 11
                Case DayOfWeek.Thursday
                  intRow = 16
                Case DayOfWeek.Friday
                  intRow = 21
                Case DayOfWeek.Saturday
                  intRow = 26
                Case DayOfWeek.Sunday
                  intRow = 31
              End Select
              drSchedule = drSchedule - 1
            End If
          Next


          drExtra = drExtra + 1
        Next


      Case "prejob"
        grdSchedule.Clear(0, 0)

        For r As Integer = 0 To grdSchedule.Rows - 1
          For c As Integer = 0 To grdSchedule.Cols - 1
            grdSchedule.Cell(r, c).font = Font_Change("Sax Mono", 7, FontStyle.Regular)
            grdSchedule.Cell(r, c).TextAlign = HorizontalAlignment.Center
          Next
        Next
        While Not datDate.DayOfWeek = DayOfWeek.Monday
          datDate = DateAdd(DateInterval.Day, -1, datDate)
        End While

        grdSchedule.Cell(0, 1).text = "MON " & Format(DateAdd("d", 0, datDate), "M/d/yy")
        grdSchedule.Cell(0, 3).text = "TUES " & Format(DateAdd("d", 1, datDate), "M/d/yy")
        grdSchedule.Cell(0, 5).text = "WED " & Format(DateAdd("d", 2, datDate), "M/d/yy")
        grdSchedule.Cell(0, 7).text = "THURS " & Format(DateAdd("d", 3, datDate), "M/d/yy")
        grdSchedule.Cell(0, 9).text = "FRI " & Format(DateAdd("d", 4, datDate), "M/d/yy")

        grdSchedule.Cell(0, 1).tag = Format(DateAdd("d", 0, datDate), "M/d/yy")
        grdSchedule.Cell(0, 3).tag = Format(DateAdd("d", 1, datDate), "M/d/yy")
        grdSchedule.Cell(0, 5).tag = Format(DateAdd("d", 2, datDate), "M/d/yy")
        grdSchedule.Cell(0, 7).tag = Format(DateAdd("d", 3, datDate), "M/d/yy")
        grdSchedule.Cell(0, 9).tag = Format(DateAdd("d", 4, datDate), "M/d/yy")

        Connect("Employees", Extra, drExtra, "Group_Name = 'PreJob' and Active = 1", "[Position]")
        drExtra = 0

        While Not datDate.DayOfWeek = DayOfWeek.Monday
          datDate = DateAdd(DateInterval.Day, -1, datDate)
        End While
        c = 0
        While c < grdSchedule.Cols
          grdSchedule.Cell(0, c).text = datDate.Date
          datDate = DateAdd(DateInterval.Day, 1, datDate)
          c += 2
        End While

        For c As Integer = 1 To grdSchedule.Cols Step 2
          r = 1
          Try

            Connect("Schedule", Schedule, drSchedule, "Person = '" & Extra.Rows(drExtra).Item("Display_Name") & "' AND Date = " & FilterByDate(Date.Parse(grdSchedule.Cell(0, c).tag)), "Start_Time")

            drSchedule = 0
          Catch ex As Exception
            MsgBox("No schedule information matching the criteria was found.")
            Exit Sub
          End Try
          For drSchedule As Integer = 0 To Schedule.Rows.Count - 1
            Connect("Addresses", Address, drAddress, "ID = " & Schedule.Rows(drSchedule).Item("Address_ID"))
            drAddress = 0
            Connect("Clients", Clients, drClients, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID"))
            drClients = 0
            grdSchedule.Cell(r, c).text = Clients.Rows(drClients).Item("Last_Name")
            grdSchedule.Cell(r, c).tag = Schedule.Rows(drSchedule).Item("Address_ID")
            grdSchedule.Cell(r + 1, c).text = Address.Rows(drAddress).Item("City") & " " & Address.Rows(drAddress).Item("State")
            grdSchedule.Cell(r + 1, c).tag = Schedule.Rows(drSchedule).Item("Address_ID")
            grdSchedule.Cell(r + 2, c).text = Address.Rows(drAddress).Item("Zip") & " " & Schedule.Rows(drSchedule).Item("Bonus_Sig")
            grdSchedule.Cell(r + 2, c).tag = Schedule.Rows(drSchedule).Item("Address_ID")
            grdSchedule.Cell(r + 3, c).text = Schedule.Rows(drSchedule).Item("Appt_Type")
            grdSchedule.Cell(r + 3, c).tag = Schedule.Rows(drSchedule).Item("Address_ID")
            grdSchedule.Cell(r + 4, c).text = Schedule.Rows(drSchedule).Item("Info")
            grdSchedule.Cell(r + 4, c).tag = Schedule.Rows(drSchedule).Item("Address_ID")

            grdSchedule.Cell(r, c - 1).text = Format(Schedule.Rows(drSchedule).Item("Start_Time"), "short time") & Chr(13) + Chr(10) & " TO " & Chr(13) + Chr(10) & Format(Schedule.Rows(drSchedule).Item("End_Time"), "short time")
            grdSchedule.Cell(r, c - 1).multiline = True
            grdSchedule.Cell(r, c - 1).tag = Schedule.Rows(drSchedule).Item("Address_ID")

            r += 5
          Next
        Next

      Case "plumbing"
        grdSchedule.Clear(0, 0)

        For r As Integer = 0 To grdSchedule.Rows - 1
          For c As Integer = 0 To grdSchedule.Cols - 1
            grdSchedule.Cell(r, c).font = Font_Change("Sax Mono", 7, FontStyle.Regular)
            grdSchedule.Cell(r, c).TextAlign = HorizontalAlignment.Center
          Next
        Next
        While Not datDate.DayOfWeek = DayOfWeek.Monday
          datDate = DateAdd(DateInterval.Day, -1, datDate)
        End While

        grdSchedule.Cell(0, 2).text = "MON " & Format(DateAdd("d", 0, datDate), "M/d/yy")
        grdSchedule.Cell(0, 4).text = "TUES " & Format(DateAdd("d", 1, datDate), "M/d/yy")
        grdSchedule.Cell(0, 6).text = "WED " & Format(DateAdd("d", 2, datDate), "M/d/yy")
        grdSchedule.Cell(0, 8).text = "THURS " & Format(DateAdd("d", 3, datDate), "M/d/yy")
        grdSchedule.Cell(0, 10).text = "FRI " & Format(DateAdd("d", 4, datDate), "M/d/yy")
        grdSchedule.Cell(0, 12).text = "SAT " & Format(DateAdd("d", 5, datDate), "M/d/yy")

        grdSchedule.Cell(0, 2).tag = Format(DateAdd("d", 0, datDate), "M/d/yy")
        grdSchedule.Cell(0, 4).tag = Format(DateAdd("d", 1, datDate), "M/d/yy")
        grdSchedule.Cell(0, 6).tag = Format(DateAdd("d", 2, datDate), "M/d/yy")
        grdSchedule.Cell(0, 8).tag = Format(DateAdd("d", 3, datDate), "M/d/yy")
        grdSchedule.Cell(0, 10).tag = Format(DateAdd("d", 4, datDate), "M/d/yy")
        grdSchedule.Cell(0, 12).tag = Format(DateAdd("d", 5, datDate), "M/d/yy")

        Connect("Employees", Extra, drExtra, "Group_Name = 'Plumbing' and Active = 1", "[Position]")
        drExtra = 0
        r = 1
        While r < grdSchedule.Rows - 1
          grdSchedule.Cell(r, 0).text = Extra.Rows(drExtra).Item("Display_Name")
          drExtra += 1
          r += 8
        End While

        While Not datDate.DayOfWeek = DayOfWeek.Monday
          datDate = DateAdd(DateInterval.Day, -1, datDate)
        End While

        For intCounter As Integer = 1 To grdSchedule.Rows - 1 Step 8
          For c As Integer = 2 To grdSchedule.Cols - 1 Step 2

            Connect("Schedule", Schedule, 0, "Person = '" & grdSchedule.Cell(intCounter, 0).text & "' And Date = " & FilterByDate(Date.Parse(grdSchedule.Cell(0, c).tag)), "Start_Time")

            For drSchedule As Integer = 0 To Schedule.Rows.Count - 1
              grdSchedule.Cell(intCounter + drSchedule, c - 1).multiline = True
              grdSchedule.Cell(intCounter + drSchedule, c - 1).tag = Schedule.Rows(drSchedule).Item("Address_ID")
              grdSchedule.Cell(intCounter + drSchedule, c - 1).text = Schedule.Rows(drSchedule).Item("Start_Time") & Chr(13) + Chr(10) & " TO " & Chr(13) + Chr(10) & Schedule.Rows(drSchedule).Item("End_Time")
              grdSchedule.Cell(intCounter + drSchedule, c).tag = Schedule.Rows(drSchedule).Item("Address_ID")
              grdSchedule.Cell(intCounter + drSchedule, c).text = Schedule.Rows(drSchedule).Item("Info")
            Next
          Next
        Next

      Case "annual"
        grdSchedule.Clear(0, 0)

        For r As Integer = 0 To grdSchedule.Rows - 1
          For c As Integer = 0 To grdSchedule.Cols - 1
            grdSchedule.Cell(r, c).font = Font_Change("Sax Mono", 7, FontStyle.Regular)
            grdSchedule.Cell(r, c).TextAlign = HorizontalAlignment.Center
          Next
        Next
        While Not datDate.DayOfWeek = DayOfWeek.Monday
          datDate = DateAdd(DateInterval.Day, -1, datDate)
        End While

        grdSchedule.Cell(0, 2).text = "MON " & Format(DateAdd("d", 0, datDate), "M/d/yy")
        grdSchedule.Cell(0, 4).text = "TUES " & Format(DateAdd("d", 1, datDate), "M/d/yy")
        grdSchedule.Cell(0, 6).text = "WED " & Format(DateAdd("d", 2, datDate), "M/d/yy")
        grdSchedule.Cell(0, 8).text = "THURS " & Format(DateAdd("d", 3, datDate), "M/d/yy")
        grdSchedule.Cell(0, 10).text = "FRI " & Format(DateAdd("d", 4, datDate), "M/d/yy")


        grdSchedule.Cell(0, 2).tag = Format(DateAdd("d", 0, datDate), "M/d/yy")
        grdSchedule.Cell(0, 4).tag = Format(DateAdd("d", 1, datDate), "M/d/yy")
        grdSchedule.Cell(0, 6).tag = Format(DateAdd("d", 2, datDate), "M/d/yy")
        grdSchedule.Cell(0, 8).tag = Format(DateAdd("d", 3, datDate), "M/d/yy")
        grdSchedule.Cell(0, 10).tag = Format(DateAdd("d", 4, datDate), "M/d/yy")


        Connect("Employees", Extra, drExtra, "Group_Name = 'Annual' and Active = 1", "[Position]")
        drExtra = 0
        r = 1
        While r < grdSchedule.Rows - 1
          grdSchedule.Cell(r, 0).text = Extra.Rows(drExtra).Item("Display_Name")
          drExtra += 1
          r += 8
        End While

        While Not datDate.DayOfWeek = DayOfWeek.Monday
          datDate = DateAdd(DateInterval.Day, -1, datDate)
        End While

        For intCounter As Integer = 1 To grdSchedule.Rows - 1 Step 8
          For c As Integer = 2 To grdSchedule.Cols - 1 Step 2

            Connect("Schedule", Schedule, 0, "Person = '" & grdSchedule.Cell(intCounter, 0).text & "' And Date = " & FilterByDate(Date.Parse(grdSchedule.Cell(0, c).tag)), "Start_Time")

            For drSchedule As Integer = 0 To Schedule.Rows.Count - 1
              grdSchedule.Cell(intCounter + drSchedule, c - 1).multiline = True
              grdSchedule.Cell(intCounter + drSchedule, c - 1).tag = Schedule.Rows(drSchedule).Item("Address_ID")
              grdSchedule.Cell(intCounter + drSchedule, c - 1).text = Schedule.Rows(drSchedule).Item("Start_Time") & Chr(13) + Chr(10) & " TO " & Chr(13) + Chr(10) & Schedule.Rows(drSchedule).Item("End_Time")
              grdSchedule.Cell(intCounter + drSchedule, c).tag = Schedule.Rows(drSchedule).Item("Address_ID")
              grdSchedule.Cell(intCounter + drSchedule, c).text = Schedule.Rows(drSchedule).Item("Info")
            Next
          Next
        Next
    End Select
  End Sub

  Public Sub Schedule_Resize()
    Select Case valSchedule
      Case "estimator"
        For c As Integer = 1 To grdSchedule.Cols - 1
          grdSchedule.ColWidth(c, (grdSchedule.Width - 50) / grdSchedule.Cols - 1)
        Next
    End Select
  End Sub

  Private Sub PreJobToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreJob.Click
    Schedule_Maker("prejob")
    flgTopDate = True
  End Sub

  Private Sub mnuEstimators_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEstimators.Click
    Schedule_Maker("estimators")
    flgTopDate = False
  End Sub

  Private Sub txtEnd_Time_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEnd_Time.LostFocus
    If txtEnd_Time.Text = "" Then Exit Sub
    Select Case Len(txtEnd_Time.Text)
      Case 1
        txtEnd_Time.Text = txtEnd_Time.Text & ":00"
      Case 2
        txtEnd_Time.Text = txtEnd_Time.Text & ":00"
      Case 3
        txtEnd_Time.SelectionStart = 0
        txtEnd_Time.SelectionLength = 1
        strResponse = txtEnd_Time.SelectedText & ":"
        txtEnd_Time.Text = strResponse & LT(txtEnd_Time.Text, 1)
      Case 4
        txtEnd_Time.SelectionStart = 0
        txtEnd_Time.SelectionLength = 2
        strResponse = txtEnd_Time.SelectedText & ":"
        txtEnd_Time.Text = strResponse & LT(txtEnd_Time.Text, 2)
      Case 5
        'correct time
      Case Else
        MsgBox("Cannot convert. Please try again.")
        txtEnd_Time.Text = ""
        txtEnd_Time.Focus()
    End Select
    Try
      txtEnd_Time.Text = Format(TimeValue(txtEnd_Time.Text), "H:mm")
    Catch ex As Exception
      MsgBox("Could not convert. Please try again.")
      txtEnd_Time.Text = ""
      txtEnd_Time.Focus()
    End Try
  End Sub

  Private Sub cboBackColor_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBackColor.LostFocus

  End Sub

  Private Sub cboBackColor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBackColor.SelectedIndexChanged

  End Sub

  Private Sub PlumbingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PlumbingToolStripMenuItem.Click
    Schedule_Maker("plumbing")
    flgTopDate = True
  End Sub

  Private Sub AnnualToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnnualToolStripMenuItem.Click
    Schedule_Maker("annual")

  End Sub

    Private Sub DaysOffToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles mnuAutoOff.Click

    End Sub

    Private Sub mnuDayOff_Click(sender As System.Object, e As System.EventArgs) Handles mnuDayOff.Click
    mnuAvailable.Checked = False
    mnuResetDay.Checked = False
    End Sub

  Private Sub mnuFormat_Click(sender As System.Object, e As System.EventArgs) Handles mnuFormat.Click

  End Sub

  Private Sub mnuResetDay_Click(sender As System.Object, e As System.EventArgs) Handles mnuResetDay.Click
    mnuAvailable.Checked = False
    mnuDayOff.Checked = False
  End Sub

  Private Sub mnuAvailable_Click(sender As System.Object, e As System.EventArgs) Handles mnuAvailable.Click
    mnuResetDay.Checked = False
    mnuDayOff.Checked = False
  End Sub
End Class