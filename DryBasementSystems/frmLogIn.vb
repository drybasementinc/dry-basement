﻿Public Class frmLogIn
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form Specific Declarations

  Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
    Connect("Users", General, drGeneral, "UserName = '" & UCase(Trim(txtUsername.Text)) & "'")
    If General.Rows.Count = 0 Then
      MsgBox("Invalid username.")
      Exit Sub
    End If
    If General.Rows(0).Item("Password") = Trim(txtPassword.Text) Then
      strUserName = General.Rows(0).Item("UserName")
      Me.Close()
    Else
      MsgBox("Invalid Username/Password. Please enter your username and password.")
    End If
  End Sub

  Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
    Me.Close()
  End Sub

  Private Sub LogoPictureBox_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogoPictureBox.DoubleClick
    frmTableView.Show()
  End Sub

  Private Sub frmLogIn_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Connect("Admin_Password", General, 0)
    Try
      strPassword = General.Rows(0).Item("Password")
    Catch ex As Exception
      MsgBox("The password was not found. This could lead to open access for all users. Please notify a supervisor.")
    End Try
  End Sub
End Class
