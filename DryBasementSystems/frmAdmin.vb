﻿''Public Class Admin
''  'General Declarations
''  Dim Address As New DataTable
''  Dim Clients As New DataTable
''  Dim General As New DataTable
''  Dim DryDispo As New DataTable
''  Dim MagicDispo As New DataTable
''  Dim DryProduction As New DataTable
''  Dim MagicProduction As New DataTable
''  Dim drGeneral As Double
''  Dim drAddress As Double
''  Dim drClients As Double
''  Dim drDryDispo As Double
''  Dim drMagicDispo As Double
''  Dim drDryProduction As Double
''  Dim drMagicProduction As Double
''  Dim i As Integer
''  Dim r As Integer
''  Dim c As Integer

''  'Form Specific Declarations
''  Private Sub cmdFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFormat.Click
''    'On Error Resume Next
''    Dim Est_Info As New DataTable

''    Test.Text = ""

''    Manual_Update("UPDATE", "Clients", "Company_Name = ''", "Company_Name IS Null")

''    Connect("Addresses", Address, drAddress)
''    For intCounter As Integer = 0 To Address.Rows.Count - 1
''      With Address.Rows(intCounter)
''        Manual_Update("Insert into", "Est_Info", "(Address_ID) Values(" & Address.Rows(intCounter).Item("ID") & ")")
''        Connect("Est_Info", Est_Info, 0, "Address_ID = " & Address.Rows(intCounter).Item("ID"))
''        'est info convert
''        Manual_Update("Update", "Est_Info", "[Source] = '" & .Item("Source") & "', [Source2] = '" & .Item("Source2") & "', [Cracks] = " & .Item("Prob_Cracks") _
''          & ", [Water] = " & .Item("Prob_Water") & ", [Bowed_Walls] = " & .Item("Prob_Bowed") & ", [Settling] = " & .Item("Prob_Bowed") _
''          & ", [Crawl] = " & .Item("Prob_Crawl") & ", [Other] = " & .Item("Prob_Other") _
''          & ", [Other_Text] = '" & Replace(.Item("Prob_Other_Text"), "'", "''") & "', [Zone] = '" & .Item("Zone") & "', [Full_Source] = '" & .Item("Full_Source") & "'", "ID = " & Est_Info.Rows(0).Item("ID"))

''        'annual convert
''        If .Item("Annual") = True Then
''          If IsDate(.Item("Annual_Date")) = True And IsDate(.Item("Next_Annual_Date")) = True Then
''            Manual_Update("Update", "Annual_Info", "Pump_Count = " & Val(.Item("Number_Pumps")) & ", Last_Annual = #" & .Item("Annual_Date") & "#," _
''              & "Next_Annual = #" & .Item("Next_Annual_Date") & "#, Price = 0", "Address_ID = " & .Item("ID"))
''          ElseIf IsDate(.Item("Annual_Date")) = True Then
''            Manual_Update("Update", "Annual_Info", "Pump_Count = " & .Item("Number_Pumps") & ", Last_Annual = #" & .Item("Annual_Date") & "#," _
''              & "Next_Annual = " & vbNull & ", Price = 0", "Address_ID = " & .Item("ID"))
''          ElseIf IsDate(.Item("Next_Annual_Date")) = True Then
''            Manual_Update("Update", "Annual_Info", "Pump_Count = " & Val(.Item("Number_Pumps")) & ", Last_Annual = " & vbNull & "," _
''              & "Next_Annual = #" & .Item("Next_Annual_Date") & "#, Price = 0", "Address_ID = " & .Item("ID"))
''          Else
''            Manual_Update("Update", "Annual_Info", "Pump_Count = " & .Item("Number_Pumps") & ", Last_Annual = " & vbNull & "," _
''              & "Next_Annual = " & vbNull & ", Price = 0", "Address_ID = " & .Item("ID"))
''          End If
''        End If

''        Manual_Update("Insert into", "Always_Info", "(Address_ID) Values(" & Address.Rows(intCounter).Item("ID") & ")")
''        Manual_Update("Insert into", "Annual_Info", "(Address_ID) Values(" & Address.Rows(intCounter).Item("ID") & ")")
''      End With
''      'lblprogress.Text = intCounter & " of " & Address.Rows.Count
''      'Me.Refresh()
''    Next

''    Connect("Estimators_Schedule", Address, 0)

''    For intCounter As Integer = 0 To Address.Rows.Count - 1
''      With Address.Rows(intCounter)
''        If IsDBNull(.Item("Time")) = True Then
''          Manual_Update("Insert into", "Schedule", "([Address_ID], [Date], [Person], [Info], [Operator], [BackColor], [FontColor], [Bold], [Appt_Type], [Detail], [Estimator_Appt], [Dispo_Done])" _
''            & " Values(" & .Item("Address_ID") & ", #" & .Item("Date") & "#, '" & .Item("Person") & "', '" & .Item("Info") & "', '" & .Item("Operator") _
''            & "', '" & .Item("BackColor") & "', '" & .Item("FontColor") & "', " & .Item("Bold") & ", '" & .Item("Appt_Type") & "', '" & .Item("Detail") & "', " & True & ", " & True & ")")
''        Else
''          Manual_Update("Insert into", "Schedule", "([Address_ID], [Date], [Person], [Info], [Operator], [BackColor], [FontColor], [Bold], [Appt_Type], [Detail], [Estimator_Appt], [Start_Time], [Dispo_Done])" _
''           & " Values(" & .Item("Address_ID") & ", #" & .Item("Date") & "#, '" & .Item("Person") & "', '" & .Item("Info") & "', '" & .Item("Operator") _
''           & "', '" & .Item("BackColor") & "', '" & .Item("FontColor") & "', " & .Item("Bold") & ", '" & .Item("Appt_Type") & "', '" & Replace(.Item("Detail"), "'", "''") & "', " & vbTrue & ", #" & .Item("Time") & "#, " & True & ")")
''        End If
''      End With
''      lblprogress.Text = intCounter & " of " & Address.Rows.Count
''    Next


''    Button1_Click(Me, e)

''    Exit Sub

''    Connect("Clients", Clients, drClients)
''    Connect("Addresses", Address, drAddress)

''    For c As Integer = 0 To Clients.Columns.Count - 1
''      Debug.Print(Clients.Columns(c).DataType.ToString)
''      If Clients.Columns(c).DataType.ToString <> "System.DateTime" Then
''        Manual_Update("Update", "Clients", Clients.Columns(c).Caption & " = " & Test.Text, Clients.Columns(c).Caption & " Is NULL")
''        'TableUpdate("UPDATE", "Clients", Clients.Columns(c).Caption & ", ", "@Test, ", Me, Clients.Columns(c).Caption & " Is NULL")
''      End If
''    Next

''    For c As Integer = 0 To Address.Columns.Count - 1
''      If Address.Columns(c).DataType.ToString <> "System.DateTime" Then
''        Manual_Update("UPDATE", "Addresses", Address.Columns(c).Caption & " = " & Test.Text, Address.Columns(c).Caption & " Is NULL")
''        'TableUpdate("UPDATE", "Addresses", Address.Columns(c).Caption & ", ", "@Test, ", Me, Address.Columns(c).Caption & " Is NULL")
''      End If
''    Next
''  End Sub

''  Private Sub cmdImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImport.Click

''    Manual_Update("UPDATE", "Schedule", "Dispo_Done = False", "Date >= #3/30/2012# AND Estimator_Appt = 1")

''    Exit Sub



''    For intCounter As Integer = 0 To Address.Rows.Count - 1
''      Address.Rows(intCounter).Item("Dispo_Needed") = True
''    Next

''    Connect("Addresses", Address, drAddress = 0)
''    For intcounter As Integer = 0 To Address.Rows.Count - 1
''      If Address.Rows(intcounter).Item("Dry_Reset") = True Or Address.Rows(intcounter).Item("Magic_Reset") = True Then
''        Manual_Update("Insert Into", "Call_List_Info", "(Address_ID, Call_List, Active_Date, Last_Contact_Name) Values(" & Address.Rows(intcounter).Item("ID") & ", 'RESET', " & FilterByDate(Now.Date) & ", '')")
''      End If
''      If Address.Rows(intcounter).Item("Reval") = True Then
''        If IsDBNull(Address.Rows(intcounter).Item("Reval_Last_Call_Date")) = False Then
''          Manual_Update("Insert Into", "Call_List_Info", "(Address_ID, Call_List, Active_Date, Last_Contact_Name, Last_Contact_Date) Values(" & Address.Rows(intcounter).Item("ID") & ", 'Reval', " & FilterByDate(Now.Date) & ", '', #" & Address.Rows(intcounter).Item("Reval_Last_Call_Date") & "#)")
''        Else
''          Manual_Update("Insert Into", "Call_List_Info", "(Address_ID, Call_List, Active_Date, Last_Contact_Name) Values(" & Address.Rows(intcounter).Item("ID") & ", 'Reval', " & FilterByDate(Now.Date) & ", '')")
''        End If
''      End If
''    Next
''    MsgBox("DONE")
''  End Sub

''  Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
''    'Connect("Estimators_Schedule", General, drGeneral)
''    'drGeneral = 0
''    'Manual_Update("UPDATE", "Estimators_Schedule", "Show_Appt = True, Active = True", "Person <> 'FLANAGAN' And Person <> 'SCHMIDT'")
''    'Exit Sub

''    Connect("Dry_Dispo", DryDispo, drDryDispo, "Source is NULL")
''    drDryDispo = 0

''    For drDryDispo As Double = DryDispo.Rows.Count - 1 To 0 Step -1
''      Connect("Addresses", Address, drAddress, "ID = " & DryDispo.Rows(drDryDispo).Item("Address_ID") & "")
''      drAddress = 0
''      If drAddress < Address.Rows.Count Then
''        If IsDBNull(Address.Rows(drAddress).Item("Source")) = True Then
''          Manual_Update("UPDATE", "Addresses", "[Source] = 'REF'", "ID = " & Address.Rows(drAddress).Item("ID"), Address)
''        End If
''        Manual_Update("UPDATE", "Dry_Dispo", "[Zip] = " & Address.Rows(drAddress).Item("Zip") & ", [Source] = '" & Replace(Address.Rows(drAddress).Item("Source"), " ", " ") & "'", "ID = " & DryDispo.Rows(drDryDispo).Item("ID") & "")
''      End If
''      lblprogress.Text = drDryDispo & " of " & DryDispo.Rows.Count
''    Next

''    Connect("Dry_Production", DryProduction, drDryProduction, "Source is NULL")
''    drDryProduction = 0

''    For drDryProduction As Double = DryProduction.Rows.Count - 1 To 0 Step -1
''      Connect("Addresses", Address, drAddress, "ID = " & DryProduction.Rows(drDryProduction).Item("Address_ID") & "")
''      drAddress = 0
''      Manual_Update("INSERT INTO", "Production_Info", "([Production_ID]) Values(" & DryProduction.Rows(drDryProduction).Item("ID") & ")")

''      If drAddress < Address.Rows.Count Then
''        If IsDBNull(Address.Rows(drAddress).Item("Source")) = True Then
''          Manual_Update("UPDATE", "Addresses", "[Source] = 'REF'", "ID = " & Address.Rows(drAddress).Item("ID"), Address)
''        End If
''        Manual_Update("UPDATE", "Dry_Production", "Zip = " & Address.Rows(drAddress).Item("Zip") & ", Source = '" & Replace(Address.Rows(drAddress).Item("Source"), " ", "\") & "'", "ID = " & DryProduction.Rows(drDryProduction).Item("ID") & "")
''        'Manual_Update("UPDATE", "Dry_Production", "[Zip], [Source], ", "@" & Address.Rows(drAddress).Item("Zip") & ", @" & Replace(Address.Rows(drAddress).Item("Source"), " ", "\") & ", ", "ID = " & DryProduction.Rows(drDryProduction).Item("ID") & "")
''      End If
''      lblprogress.Text = drDryProduction & " of " & DryProduction.Rows.Count
''    Next

''    'Connect("Dry_Production", DryProduction, drDryProduction)
''    'drDryProduction = 0

''    'For drDryProduction As Double = DryProduction.Rows.Count - 1 To 0 Step -1
''    '  Connect("Production_Info", Address, drAddress, "Production_ID = " & DryProduction.Rows(drDryProduction).Item("ID") & "")
''    '  drAddress = 0
''    '  If Address.Rows.Count = 0 Then
''    '    Manual_Update("INSERT_INTO", "Production_Info", "([Production_ID])  Values(" & Address.Rows(0).Item("ID") & ")")
''    '    'Manual_Update("UPDATE", "Dry_Production", "[Zip], [Source], ", "@" & Address.Rows(drAddress).Item("Zip") & ", @" & Replace(Address.Rows(drAddress).Item("Source"), " ", "\") & ", ", "ID = " & DryProduction.Rows(drDryProduction).Item("ID") & "")
''    '  End If
''    'Next
''    MsgBox("DONE")
''    Exit Sub

''    Connect("Magic_Dispo", MagicDispo, drMagicDispo, "Source is NULL")
''    drMagicDispo = 0

''    For drmagicDispo As Double = 0 To MagicDispo.Rows.Count - 1
''      Connect("Addresses", Address, drAddress, "ID = " & MagicDispo.Rows(drmagicDispo).Item("Address_ID") & "")
''      drAddress = 0
''      Manual_Update("UPDATE", "Magic_Dispo", "[Zip], [Source], ", "@" & Address.Rows(drAddress).Item("Zip") & ", @" & Replace(Address.Rows(drAddress).Item("Source"), " ", "\"), "ID = " & MagicDispo.Rows(drmagicDispo).Item("ID") & "")
''    Next
''  End Sub

''  Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdIce_Email.Click
''    'Connect_Special("ice_guard", General, drGeneral, , "Email")
''    'drGeneral = 0
''    'For drGeneral As Integer = 0 To General.Rows.Count - 1
''    '  SendMail("Service@DryBasementSystems.com", General.Rows(drGeneral).Item("Email"), "", "Freezing Alert From Dry Basement", txtBody.Text, "")
''    'Next
''    'MsgBox("DONE")
''    SendMail("ray.mayberry@gmail.com", "ray_mayberry@gmail.com", "", "Freezing Alert From Dry Basement", txtBody.Text, "")
''  End Sub

''  Public Sub Connect_Special(ByVal Table As String, ByVal RST As DataTable, ByVal ROW As Double, Optional ByVal Filter As String = "1=1", Optional ByVal Order As String = "ID")
''    '//using declaration for OLE DB
''    '//specify the ConnectionString property
''    conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=G:\Ray_Mayberry\Dry_Basement\Dry_Basement.mdb"
''    '//Initializes a new instance of the OleDbConnection
''    con = New System.Data.OleDb.OleDbConnection(conString)
''    '// open the database connection with the property settings
''    '// specified by the ConnectionString "conString"
''    If con.State = ConnectionState.Open Then con.Close()
''    Try
''      con.Open()
''    Catch ex As Exception
''      conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\G-\Dry_Basement_App\Dry_BasementTEST.mdb"
''    End Try
''    dadapter = New System.Data.OleDb.OleDbDataAdapter(Trim("Select * From " & Table & " Where " & Filter & " Order By " & Order), con)
''    If IsDBNull(dadapter) = True Then
''      MsgBox("Failed to read: " & Table)
''      con.Close()
''      Exit Sub
''    End If
''    RST.Clear()
''    Try
''      dadapter.Fill(RST)
''    Catch ex As Exception
''      MsgBox("Unable to fill data table.")
''    End Try
''    ROW = 0
''    con.Close()
''  End Sub

''  Private Sub Admin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
''    Dim row As Integer
''    Dim col As Integer
''    Dim flgswitch As Boolean
''    flgswitch = False
''    row = 1
''    While row <= chtTest.RowCount
''      col = 1
''      chtTest.RowLabel = "Graph " & row
''      While col <= chtTest.ColumnCount
''        chtTest.Row = row
''        chtTest.Column = col
''        If flgswitch = False Then
''          chtTest.ColumnLabel = "Ray Pay"
''          chtTest.Data = Rnd(3)
''          flgswitch = True
''        Else
''          chtTest.ColumnLabel = "Curtis Pay"
''          chtTest.Data = Rnd(9)
''          flgswitch = False
''        End If
''        col = col + 1
''      End While
''      row = row + 1
''    End While


''  End Sub

''  Private Sub chtTest_ChartSelected(ByVal sender As System.Object, ByVal e As AxMSChart20Lib._DMSChartEvents_ChartSelectedEvent) Handles chtTest.ChartSelected

''    Dim Sales(,) As Object = New Object(,) _
''     {{"Company", "Company A", "Company B"}, _
''     {"June", 20, 10}, _
''     {"July", 10, 5}, _
''     {"August", 30, 15}, _
''     {"September", 14, 7}}
''    chtTest.ChartData = Sales

''    'Add a title and legend.
''    With Me.chtTest
''      .Title.Text = "Sales"
''      .Legend.Location.LocationType = MSChart20Lib.VtChLocationType.VtChLocationTypeBottom
''      .Legend.Location.Visible = True
''    End With

''    'Add titles to the axes.
''    With Me.chtTest.Plot
''      .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).AxisTitle.Text = "Year"
''      .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdY).AxisTitle.Text = "Millions of $"
''    End With

''    'Set custom colors for the bars.
''    With Me.chtTest.Plot
''      'Yellow for Company A
''      ' -1 selects all the datapoints.
''      .SeriesCollection(1).DataPoints(-1).Brush.FillColor.Set(250, 250, 0)
''      'Purple for Company B
''      .SeriesCollection(2).DataPoints(-1).Brush.FillColor.Set(200, 50, 200)
''    End With


''    If chtTest.chartType = MSChart20Lib.VtChChartType.VtChChartType2dPie Then
''      chtTest.chartType = MSChart20Lib.VtChChartType.VtChChartType2dBar
''    ElseIf chtTest.chartType = MSChart20Lib.VtChChartType.VtChChartType2dBar Then
''      chtTest.chartType = MSChart20Lib.VtChChartType.VtChChartType2dLine
''    Else
''      chtTest.chartType = MSChart20Lib.VtChChartType.VtChChartType2dPie
''    End If

''  End Sub
''End Class