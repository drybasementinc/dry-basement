﻿Public Class frmSearch
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form Specific Declarations
  Public strWhere As String

  Private Sub frmSearch_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

  End Sub

  Private Sub frmSearch_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    If e.KeyCode = Keys.Enter Then
      cmdSearch_Click(Me, e)
    End If
  End Sub

  Private Sub Search_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

  End Sub

  Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
    strWhere = ""
    Search_Finder(Me, "SEARCH")
    strWhere = " And First_Name Like '%" & txtFirst_Name.Text & "%' And Last_Name Like '%" & txtLast_Name.Text & "%'" & strWhere & " Or First_Name2 Like '%" & txtFirst_Name.Text & "%' And Last_Name2 Like '%" & txtLast_Name.Text & "%'" & strWhere
    If strWhere <> "" Then
      strWhere = LT(strWhere, 5)
      'strWhere = RT(strWhere, 1)
      Connect("Search", General, drGeneral, strWhere, "Zip")
      If General.Rows.Count > 0 Then
        frmSearch_Results.Tag = Me.Tag
        frmSearch_Results.Show()
      Else
        MsgBox("No Results Found.")
      End If
    Else
      MsgBox("This will return every record. Please narrow the search.")
      Exit Sub
    End If
  End Sub
  Public Sub Search_Finder(ByVal Source As System.Object, ByVal tag1 As String)
    Dim int1 As Integer

    Try
      For int1 = 0 To Source.Controls.Count - 1
        If Source.Controls(int1).Tag = tag1 Then
          If TypeOf (Source.Controls(int1)) Is TextBox Or TypeOf (Source.Controls(int1)) Is ComboBox Then
            If Source.controls(int1).text <> "" Then
              strWhere = strWhere & " And " & LT(Source.Controls(int1).Name, 3) & " Like '" & Source.controls(int1).text & "%'"
            End If
          End If
        ElseIf Source.Controls(int1).HasChildren = True Then
          Search_Finder(Source.controls(int1), tag1)
        End If
      Next

    Catch ext As Exception
      MsgBox("Could not compile information. Please make sure the correct information was provided.")
    End Try
  End Sub

  Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClear.Click
    Me.Close()
    frmStart.cmdSearch.PerformClick()
  End Sub
End Class