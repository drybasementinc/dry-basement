﻿Public Class remDataKeeper
  Dim recordID As Double = 0
  Dim valEOF As Boolean = False
  Dim valBOF As Boolean = False
  Dim myDataTable As New DataTable
  Dim valRowCount As Double
  Dim valTable As String
  Dim valFilter As String
  Dim valOrder As String

  Sub MoveForward(Optional Number As Integer = 1)
    If Number < 1 Then
      MsgBox("An invalid record change move was attempted. Please use only positive integers that are greater than 0.")
    End If
    recordID += Number
    valBOF = False
    If recordID >= myDataTable.Rows.Count - 1 Then
      recordID = myDataTable.Rows.Count - 1
      valEOF = True
    End If
  End Sub

  Sub MoveBackward(Optional Number As Integer = 1)
    If Number < 1 Then
      MsgBox("An invalid record change move was attempted. Please use only positive integers that are greater than 0.")
      Exit Sub
    End If
    recordID -= Number
    valEOF = False
    If recordID <= 0 Then
      recordID = 0
      valBOF = True
    End If
  End Sub

  Sub Update()
    Dim strValues As String = ""
    Dim strSQLPart As String = ""

    For intCol As Integer = 0 To myDataTable.Columns.Count - 1
      strSQLPart = strSQLPart & ", " & myDataTable.Columns(intCol).Caption & " = @" & intCol
      strValues = strValues & "@" & intCol
    Next

    flgFailed = False
    '//using declaration for OLE DB
    '//specify the ConnectionString property
    conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\DBI\DBI.mdb"
    '//Initializes a new instance of the OleDbConnection
    con = New System.Data.OleDb.OleDbConnection(conString)
    '// open the database connection with the property settings
    '// specified by the ConnectionString "conString"
    If con.State = ConnectionState.Open Then con.Close()

    Dim cmd As OleDb.OleDbCommand

    cmd = New OleDb.OleDbCommand("UPDATE " & valTable & "Set " & strSQLPart & " WHERE ID = " & Me.Field("ID"), con)

    For intcol = 0 To strValues.Split("@").Count - 1
      cmd.Parameters.AddWithValue("@" & intcol, Trim(UCase(Replace(strValues(intcol), "@", ""))) & "")
    Next

    
    cmd.Connection.Open()
    Try
      cmd.ExecuteNonQuery()
    Catch ex As Exception
      flgFailed = MsgBox("Save Failed.")
      con.Close()
      Exit Sub
    End Try
    cmd.Connection.Close()
    con.Close()
  End Sub

  Sub Load(ByVal Table As String, Optional ByVal Filter As String = "1=1", Optional ByVal Order As String = "ID")
    '//specify the ConnectionString property

    conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\DBI\DBI.mdb"

    '//Initializes a new instance of the OleDbConnection
    con = New System.Data.OleDb.OleDbConnection(conString)
    '// open the database connection with the property settings
    '// specified by the ConnectionString "conString"
    If con.State = ConnectionState.Open Then con.Close()
    Try
      con.Open()
    Catch ex As Exception
      conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\DBI\DBI.mdb"
    End Try
    dadapter = New System.Data.OleDb.OleDbDataAdapter(Trim("Select * From " & Table & " Where " & Filter & " Order By " & Order), con)
    If IsDBNull(dadapter) = True Then
      MsgBox("Failed to read: " & Table)
      con.Close()
      Exit Sub
    End If
    myDataTable.Clear()
    recordID = 0
    Try
      dadapter.Fill(myDataTable)
    Catch ex As Exception
      MsgBox("Unable to fill data table.")
    End Try
    con.Close()
    Me.MoveFirst()
    valTable = Table
    valFilter = Filter
    valOrder = Order
  End Sub

  Function RowCount()
    Return myDataTable.Rows.Count
  End Function

  Property Field(ByVal RequestedField As String, Optional ByVal Offset As Integer = 0)
    Get
      Return myDataTable.Rows(recordID + Offset).Item(RequestedField)
    End Get
    Set(value)
      myDataTable.Rows(recordID + Offset).Item(RequestedField) = value
    End Set
  End Property

  Property Field(ByVal RequestedField As Integer, Optional ByVal Offset As Integer = 0)
    Get
      Return myDataTable.Rows(recordID + Offset).Item(myDataTable.Columns(RequestedField))
    End Get
    Set(value)
      myDataTable.Rows(recordID + Offset).Item(myDataTable.Columns(RequestedField)) = value
    End Set
  End Property

  ReadOnly Property EoF As Boolean
    Get
      Return valEOF
    End Get
  End Property

  ReadOnly Property BoF As Boolean
    Get
      Return valBOF
    End Get

  End Property

  Sub Refresh()
    Me.Load(valTable, valFilter, valOrder)
  End Sub

  Function RowID()
    Return recordID
  End Function

  Sub MoveLast()
    Me.MoveForward(Me.RowCount + 1)
  End Sub

  Sub MoveFirst()
    Me.MoveBackward(Me.RowCount + 1)
  End Sub
End Class
