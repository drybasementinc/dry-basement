﻿Public Class frmDispo
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim Schedule As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim drSchedule As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form specific Declarations
  Dim dblCombinedID As Double
  Dim Numbers As New DataTable
  Dim drNumbers As Double
  Dim Source As New DataTable
  Dim drSource As Double
  Dim Zips As New DataTable
  Dim drZips As Double
  Dim flgDryDone As Boolean
  Dim flgMagicDone As Boolean

  Private Sub Control_Purge(ByVal owner As Object)
    For Each Control As Object In owner.Controls
      If Control.haschildren = True Then
        While Control.controls.count > 0
          Control.controls(0).dispose()
        End While
        Control.dispose()
        Control_Purge(owner)
        Exit Sub
      End If
    Next
  End Sub

  Private Sub frmDispo_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    If Me.Owner IsNot Nothing Then
      If Me.Owner.Name = "Dispo_Post" Then
        If valDry_Dispo_ID.Text = "" And valMagic_Dispo_ID.Text = "" Then
          strResponse = MsgBox("By exiting now you will delete this dispo entirely. Do you want to continue?", MsgBoxStyle.YesNo)
          If strResponse = vbNo Then
            e.Cancel = True
          Else
            Control_Purge(tpgDry)
            Control_Purge(tpgMagic)
          End If
        End If
      End If
    End If
  End Sub

  Private Sub frmDispo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Dim Dummy As New DataTable
    Dim drDummy As Double
    Dim intTop As Integer
    Dim intLeft As Integer
    Dim intFrameTop As Integer
    Dim intCompany As Integer
    Dim strCompany As String
    Dim Older As GroupBox

    tpgDry.Controls.Remove(fraReset)
    Me.Controls.Add(fraReset)
    fraReset.BringToFront()
    fraReset.Left = Me.Width / 2 - fraReset.Width / 2
    fraReset.Top = Me.Height / 2 - fraReset.Height / 2
    Connect("Dispo_Categories", General, drGeneral, , "Company, Placement")
    drGeneral = 0

    Me.Text = "DISPOS FOR " & datDispo
    intFrameTop = 0
    intCompany = 0
    strCompany = General.Rows(drGeneral).Item("Company")
    For drGeneral As Double = 0 To General.Rows.Count - 1
      'gets a count on how many products will be in the group box
      Connect("Products", Dummy, drDummy, "Category = '" & General.Rows(drGeneral).Item("Category") & "' And Company = '" & strCompany & "' And Active = 1", "Active,Placement,Product")
      If General.Rows(drGeneral).Item("Company") <> strCompany Then
        strCompany = General.Rows(drGeneral).Item("Company")
        intCompany = 0
        intFrameTop = 0
      End If

      Dim frame As New GroupBox
      frame.Name = Replace(General.Rows(drGeneral).Item("Category"), " ", "_")
      frame.Text = General.Rows(drGeneral).Item("Category")
      frame.Width = 140 + (Int(Int(Dummy.Rows.Count - 1) / 5) * 140)
      frame.Height = 150
      Older = Nothing
      If intCompany > 0 Then
        Older = tabDispo.Controls("tpg" & General.Rows(drGeneral).Item("Company")).controls(Replace(General.Rows(drGeneral - 1).Item("Category"), " ", "_"))
        If Older.Left + Older.Width + frame.Width > Older.Parent.Width - 10 Then
          intFrameTop = intFrameTop + 1
        End If
      End If
      frame.Top = 75 + (155 * intFrameTop)
      If intCompany > 0 Then
        If frame.Top = Older.Top Then
          frame.Left = Older.Left + Older.Width + 10
        Else
          frame.Left = 5
        End If
      Else
        frame.Left = 5
      End If
      tabDispo.Controls("tpg" & General.Rows(drGeneral).Item("Company")).controls.add(frame)
      intTop = 0
      intLeft = 0
      For drDummy = 0 To Dummy.Rows.Count - 1
        Dim text As New TextBox
        Dim label As New Label
        text.Name = "txt" & Replace(Dummy.Rows(drDummy).Item("Product_Field"), " ", "")
        text.Width = 30
        text.Top = text.Height + (intTop * text.Height) + 10
        text.Left = 100 + (intLeft * (text.Width + 100))
        text.Tag = Dummy.Rows(drDummy).Item("Company")
        If Dummy.Rows(drDummy).Item("Active") = False Then
          text.Enabled = False
        End If
        frame.Controls.Add(text)
        AddHandler text.TextChanged, AddressOf Text_Check
        AddHandler text.GotFocus, AddressOf Highlight
        text.Visible = True
        label.Name = "lbl" & Replace(Dummy.Rows(drDummy).Item("Product_Field"), " ", "")
        label.Text = Replace(Dummy.Rows(drDummy).Item("Product"), "_", " ")
        label.AutoSize = True
        label.Tag = Dummy.Rows(drDummy).Item("Abbr")
        label.Top = label.Height + (intTop * label.Height) + 5
        label.Width = 90
        label.Left = 5 + (intLeft * (label.Width + text.Width)) + (10 * intLeft)
        frame.Controls.Add(label)
        AddHandler label.DoubleClick, AddressOf Label_Add
        label.Show()
        intTop = intTop + 1
        If intTop > 4 Then
          intTop = 0
          intLeft = intLeft + 1
        End If
        intCompany = intCompany + 1
      Next
      frame.Visible = True
    Next

    Connect("Employees", General, drGeneral, "[Active] = 1 AND Group_Name = 'Estimators'", "[Position]")
    drGeneral = 0
    For drGeneral As Double = 0 To General.Rows.Count - 1
      cboEstimator.Items.Add(General.Rows(drGeneral).Item("Display_Name"))
      Estimator_Remove(General.Rows(drGeneral).Item("Display_Name"))
    Next

    valAppt_Date.Text = datDispo
  End Sub

  Private Sub frmDispo_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    tabDispo.Width = Me.Width - 25
    tabDispo.Height = Me.Height - tabDispo.Top - 40
  End Sub

  Public Sub Text_Check(ByVal sender As Object, ByVal e As System.EventArgs)
    If IsNumeric(sender.Text) = False Then
      If sender.text <> "" Then
        MsgBox("Only numbers are valid.")
        sender.Text = RT(sender.text, 1)
      End If
    Else
      With Me.Controls("txt" & sender.tag)
        .text = Write_Me(sender.parent.parent)
        If UCase(sender.tag) = "DRY" Then
          If optDrySold.Checked = True Then
            .text = "SOLD " & txtDryPrice.Text & .text
          ElseIf optDryDNS.Checked = True Then
            .text = "DNS " & txtDryPrice.Text & .text
          End If
        ElseIf UCase(sender.tag) = "MAGIC" Then
          If optMagicSold.Checked = True Then
            .text = "SOLD " & txtMagicPrice.Text & .text
          ElseIf optMagicDNS.Checked = True Then
            .text = "DNS " & txtMagicPrice.Text & .text
          End If
        End If
      End With
    End If
  End Sub

  Public Sub Highlight(ByVal sender As Object, ByVal e As System.EventArgs)
    sender.selectall()
  End Sub

  Public Sub Label_Add(ByVal sender As Object, ByVal e As System.EventArgs)
    sender.parent.controls("txt" & LT(sender.name, 3)).text = Val(sender.parent.controls("txt" & LT(sender.name, 3)).text) + 1
  End Sub

  Private Sub cboEstimator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstimator.SelectedIndexChanged
    Me.Cursor = Cursors.WaitCursor
    Appt_Load()
    Me.Cursor = Cursors.Default
    valEstimator.Text = cboEstimator.Text
  End Sub

  Public Sub Appointment_Change(ByVal sender As Object, ByVal e As System.EventArgs)
    Connect("Addresses", Address, drAddress, "ID = " & sender.tag & "")
    Connect("Est_Info", General, drGeneral, "Address_ID = " & Address.Rows(0).Item("ID"))
    drAddress = 0
    dblAddressID = (Address.Rows(drAddress).Item("ID"))
    valAddress_ID.Text = dblAddressID
    valZip.Text = Address.Rows(0).Item("Zip")
    valDry_New.Checked = Address.Rows(drAddress).Item("Dry_New")
    valMagic_New.Checked = Address.Rows(drAddress).Item("Magic_New")
    Try
      valSource.Text = General.Rows(0).Item("Source")
    Catch ex As Exception
      valSource.Text = ""
    End Try
    cmdExit_Click(Me, e)
    flgDryDone = False
    flgMagicDone = False
  End Sub

  Public Sub Estimator_Remove(ByVal estimator As String)
    Dim Dummy As New DataTable
    Dim drDummy As Double

    If estimator = "" Then Exit Sub

    Connect("Schedule", Dummy, drDummy, "Date = " & FilterByDate(datDispo) & " And Person = '" & estimator & "' And Dispo_Done = 0 AND Appt_Type <> '' And Not Appt_Type Is Null")

    If Dummy.Rows.Count = 0 Then
      cboEstimator.Items.Remove(estimator)
    End If
  End Sub

  Public Sub Appt_Load()
    Dim control As Object
    With fraAppointments
      If Address.Rows.Count > 0 Then
        If flgDryDone <> flgMagicDone Then
          MsgBox("Please finish the current dispo before changing estimators or appointments.")
          cboEstimator.Text = Address.Rows(drAddress).Item("Estimator")
          Exit Sub
        End If
      End If
      valCombined.Checked = False
      valMagic_Dispo_ID.Text = "0"
      valDry_Dispo_ID.Text = "0"

      While .Controls.Count > 0
        control = .Controls(0)
        .Controls.Remove(.Controls(0))
        control.Dispose()
      End While

      Connect("Schedule", Schedule, drSchedule, "Date = " & FilterByDate(datDispo) & " and Person = '" & cboEstimator.Text & "' and Dispo_Done = 0 And Estimator_Appt = 1 AND Appt_Type <> '' And Not Appt_Type Is Null", "Start_Time")

      drSchedule = 0
      
      If Schedule.Rows.Count > 0 Then
        For drSchedule As Double = 0 To (Schedule.Rows.Count - 1)
          Dim opt As New RadioButton
          If drSchedule < Schedule.Rows.Count Then
            Try
              Connect("Addresses", Address, drAddress, "ID = " & Schedule.Rows(drSchedule).Item("Address_ID") & "")
              drAddress = 0
              Connect("Clients", Clients, drClients, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID"))
              drClients = 0
            Catch ex As Exception
              MsgBox("Unable to load the customer information properly.")
              Exit Sub
            End Try
            opt.Name = "opt" & drSchedule
            opt.Tag = Address.Rows(drAddress).Item("ID")
            opt.Text = Clients.Rows(drClients).Item("First_Name") & " " & Clients.Rows(drClients).Item("Last_Name") & " " & Address.Rows(drAddress).Item("Address_Numbers") & " " & Address.Rows(drAddress).Item("Address_Direction") & " " & Address.Rows(drAddress).Item("Address") & " " & Address.Rows(drAddress).Item("Address_Ending") & "     " & Address.Rows(drAddress).Item("ID")
            opt.Left = 3
            opt.Top = 17 + (20 * drSchedule)
            opt.Width = fraAppointments.Width - 10
            AddHandler opt.CheckedChanged, AddressOf Appointment_Change
            fraAppointments.Controls.Add(opt)
            opt.Visible = True
            If drSchedule = 0 Then opt.Checked = True
          End If
        Next
        drAddress = 0
      Else
        Estimator_Remove(cboEstimator.Text)
      End If
    End With
  End Sub

  Private Sub cmdDryNA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDryNA.Click
    flgDryDone = True
    If flgDryDone = True And flgMagicDone = True Then
      Manual_Update("UPDATE", "Schedule", "Dispo_Done = 1", "Date = " & FilterByDate(datDispo) & " AND Person = '" & valEstimator.Text & "' AND Address_ID = " & valAddress_ID.Text, Address)
      Appt_Load()
    End If
  End Sub

  Private Sub cmdMagicNA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMagicNA.Click
    'Manual_Update("UPDATE", "Addresses", "Magic_Dispo_Done = True", "ID = " & dblAddressID, Address)
    flgMagicDone = True
    If flgDryDone = True And flgMagicDone = True Then
      Manual_Update("UPDATE", "Schedule", "Dispo_Done = 1", "Date = " & FilterByDate(datDispo) & " AND Person = '" & valEstimator.Text & "' AND Address_ID = " & valAddress_ID.Text, Address)
      Appt_Load()
    End If
  End Sub

  Public Function Write_Me(ByVal source As Object)
    Dim intPosition As Integer
    Dim strReturn As String

    strReturn = ""
    If source.tag = "SKIP" Then
      'do nothing
    Else
      For intPosition = 0 To source.controls.count - 1
        If TypeOf (source.controls(intPosition)) Is TextBox Then
          If source.controls(intPosition).text <> "" And source.controls(intPosition).text <> "0" Then
            If IsDBNull(source.Controls("lbl" & LT(source.controls(intPosition).name, 3)).tag) = True Then
              source.Controls("lbl" & LT(source.controls(intPosition).name, 3)).tag = "SKIP"
            End If
            If source.Controls("lbl" & LT(source.controls(intPosition).name, 3)).tag <> "SKIP" Then
              strReturn = strReturn & " | " & source.controls(intPosition).text & " " & source.Controls("lbl" & LT(source.controls(intPosition).name, 3)).tag
            End If
          End If
        ElseIf source.controls(intPosition).haschildren = True Then
          strReturn = strReturn & Write_Me(source.controls(intPosition))
        End If
      Next
    End If
    Return strReturn
  End Function

  Private Sub cmdDrySave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDrySave.Click
    Dim intMain As Integer
    Dim intSub As Integer
    If valAddress_ID.Text = "" Then
      MsgBox("No appointment is selected and therefore no dispo can be saved.")
      Exit Sub
    End If
    valSold.Checked = optDrySold.Checked
    valSold.Tag = "DRY2"
    valDNS.Checked = optDryDNS.Checked
    valDNS.Tag = "DRY2"
    valPrice.Text = txtDryPrice.Text
    valPrice.Tag = "DRY2"
    For intMain = 0 To tpgDry.Controls.Count - 1
      If TypeOf (tpgDry.Controls(intMain)) Is TextBox Then
        If tpgDry.Controls(intMain).Text = "" Then tpgDry.Controls(intMain).Text = "0"
      End If
      If tpgDry.Controls(intMain).HasChildren = True Then
        For intSub = 0 To tpgDry.Controls(intMain).Controls.Count - 1
          If TypeOf (tpgDry.Controls(intMain).Controls(intSub)) Is TextBox Then
            If tpgDry.Controls(intMain).Controls(intSub).Text = "" Then tpgDry.Controls(intMain).Controls(intSub).Text = "0"
          End If
        Next
      End If
    Next
    Compiler("INSERT INTO", Me, "DRY", "DRY2", "BOTH")
    Dim mySQLString As String = strSaveSQL3
    Dim myValues As String = strValues3
    TableUpdate("INSERT INTO", "Dry_Dispo", mySQLString, myValues, Me)
    If flgFailed = True Then
      MsgBox("Dispo Failed to save before any records were created.")
      Exit Sub
    End If
    Compiler("UPDATE", Me, "DRY", "DRY2")
    Connect("Dry_Dispo", DryDispo, drDryDispo, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
    drDryDispo = 0
    TableUpdate("Update", "Dry_Dispo", strSaveSQL1, strValues1, Me, "ID = " & DryDispo.Rows(drDryDispo).Item("ID"))
    TableUpdate("Update", "Dry_Dispo", strSaveSQL2, strValues2, Me, "ID = " & DryDispo.Rows(drDryDispo).Item("ID"))
    If flgFailed = True Then Exit Sub
    If optDrySold.Checked = True Then
      strSaveSQL1 = Replace(strSaveSQL1, "[Offer]", "[Installed]") & "Dispo_ID = " & DryDispo.Rows(0).Item("ID") & ""
      TableUpdate("INSERT INTO", "Dry_Production", mySQLString, myValues, Me)
      Connect("Dry_Production", DryProduction, drDryProduction, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
      drDryProduction = 0
      TableUpdate("Update", "Dry_Production", strSaveSQL1, strValues1, Me, "ID = " & DryProduction.Rows(drDryProduction).Item("ID"))
      If flgFailed = True Then Exit Sub
      If valCombined.Checked = False Then
        Manual_Update("Insert Into", "Production_Info", "(Production_ID) Values(7)")
        Connect("Production_Info", General, 0)
        Manual_Update("Update", "Dry_Production", "Info_ID = " & General.Rows(General.Rows.Count - 1).Item("ID"), "ID = " & DryProduction.Rows(drDryProduction).Item("ID"))
      ElseIf optMagicSold.Checked = True Then
        Connect("Production_Info", General, 0)
        Manual_Update("Update", "Dry_Production", "Info_ID = " & General.Rows(General.Rows.Count - 1).Item("ID"))
      End If
    End If
    Connect("Dry_Dispo", DryDispo, drDryDispo, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
    drDryDispo = 0
    Manual_Update("UPDATE", "Dry_Dispo", "Print_Offer = '" & txtDry.Text & "'", "ID = " & DryDispo.Rows(drDryDispo).Item("ID") & "")
    Connect("Dry_Dispo", DryDispo, drDryDispo, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
    drDryDispo = 0
    If optDrySold.Checked = True Then
      Connect("Dry_Production", DryProduction, drDryProduction, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
      drDryProduction = 0
      Manual_Update("UPDATE", "Dry_Production", "Print_Installed = '" & txtDry.Text & "'", "ID = " & DryProduction.Rows(drDryProduction).Item("ID") & "")
      If flgFailed = True Then Exit Sub
    End If
    valDry_Dispo_ID.Text = DryDispo.Rows(drDryDispo).Item("ID")
    Manual_Update("UPDATE", "Schedule", "Dispo_Done = 1", "Date = " & FilterByDate(datDispo) & " AND Person = '" & valEstimator.Text & "' AND Address_ID = " & valAddress_ID.Text, Address)
    If valCombined.Checked = True Then Manual_Update("UPDATE", "Magic_Dispo", "Dry_Dispo_ID = " & valDry_Dispo_ID.Text & ", Combined = 1", "ID = " & valMagic_Dispo_ID.Text & "")
    If flgFailed = False Then
      Manual_Update("Update", "Addresses", "Dry_New = 0", "ID = " & dblAddressID)
      MsgBox("DISPO SAVED.")
      flgDryDone = True
    End If
    valCombined.Checked = True
    If flgDryDone = True And flgMagicDone = True Then
      Appt_Load()
    End If
  End Sub

  Private Sub cmdMagicSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMagicSave.Click
    Dim intMain As Integer
    Dim intSub As Integer

    If valAddress_ID.Text = "" Then
      MsgBox("No appointment is selected and therefore no dispo can be saved.")
      Exit Sub
    End If
    valSold.Checked = optMagicSold.Checked
    valSold.Tag = "MAGIC2"
    valDNS.Checked = optMagicDNS.Checked
    valDNS.Tag = "MAGIC2"
    valPrice.Text = txtMagicPrice.Text
    valPrice.Tag = "MAGIC2"
    For intMain = 0 To tpgMagic.Controls.Count - 1
      If TypeOf (tpgMagic.Controls(intMain)) Is TextBox Then
        If tpgMagic.Controls(intMain).Text = "" Then tpgMagic.Controls(intMain).Text = "0"
      End If
      If tpgMagic.Controls(intMain).HasChildren = True Then
        For intSub = 0 To tpgMagic.Controls(intMain).Controls.Count - 1
          If TypeOf (tpgMagic.Controls(intMain).Controls(intSub)) Is TextBox Then
            If tpgMagic.Controls(intMain).Controls(intSub).Text = "" Then tpgMagic.Controls(intMain).Controls(intSub).Text = "0"
          End If
        Next
      End If
    Next
    Compiler("INSERT INTO", Me, "MAGIC", "MAGIC2", "BOTH")
    Dim mySQLString As String = strSaveSQL3
    Dim myValues As String = strValues3
    TableUpdate("INSERT INTO", "MAGIC_Dispo", mySQLString, myValues, Me)
    If flgFailed = True Then
      MsgBox("Dispo Failed to save before any records were created.")
      Exit Sub
    End If
    Compiler("UPDATE", Me, "MAGIC", "MAGIC2")
    Connect("MAGIC_Dispo", MagicDispo, drMagicDispo, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
    drMagicDispo = 0
    TableUpdate("Update", "MAGIC_Dispo", strSaveSQL1, strValues1, Me, "ID = " & MagicDispo.Rows(drMagicDispo).Item("ID"))
    TableUpdate("Update", "MAGIC_Dispo", strSaveSQL2, strValues2, Me, "ID = " & MagicDispo.Rows(drMagicDispo).Item("ID"))
    If flgFailed = True Then Exit Sub
    If optMagicSold.Checked = True Then
      strSaveSQL1 = Replace(strSaveSQL1, "[Offer]", "[Installed]")
      TableUpdate("INSERT INTO", "MAGIC_Production", mySQLString, myValues, Me)
      Connect("MAGIC_Production", MagicProduction, drMagicProduction, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
      drMagicProduction = 0
      TableUpdate("Update", "MAGIC_Production", strSaveSQL1, strValues1, Me, "ID = " & MagicProduction.Rows(drMagicProduction).Item("ID"))
      If flgFailed = True Then Exit Sub
      If valCombined.Checked = False Then
        Manual_Update("Insert Into", "Production_Info", "() Values()")
        Connect("Production_Info", General, 0)
        Manual_Update("Update", "Magic_Production", "Info_ID = " & General.Rows(General.Rows.Count - 1).Item("ID"))
      ElseIf optDrySold.Checked = True Then
        Connect("Production_Info", General, 0)
        Manual_Update("Update", "Magic_Production", "Info_ID = " & General.Rows(General.Rows.Count - 1).Item("ID"))
      End If
    End If
    Connect("MAGIC_Dispo", MagicDispo, drMagicDispo, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
    drMagicDispo = 0
    Manual_Update("UPDATE", "MAGIC_Dispo", "Print_Offer = '" & txtMAGIC.Text & "'", "ID = " & MagicDispo.Rows(drMagicDispo).Item("ID") & "")
    Connect("MAGIC_Dispo", MagicDispo, drMagicDispo, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
    drMagicDispo = 0
    If optMagicSold.Checked = True Then
      Connect("MAGIC_Production", MagicProduction, drMagicProduction, "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & "")
      drMagicProduction = 0
      Manual_Update("UPDATE", "MAGIC_Production", "Print_Installed = '" & txtMAGIC.Text & "'", "ID = " & MagicProduction.Rows(drMagicProduction).Item("ID") & "")
      If flgFailed = True Then Exit Sub
    End If
    valMagic_Dispo_ID.Text = MagicDispo.Rows(drMagicDispo).Item("ID")
    Manual_Update("UPDATE", "Schedule", "Dispo_Done = 1", "Date = " & FilterByDate(datDispo) & " AND Person = '" & valEstimator.Text & "' AND Address_ID = " & valAddress_ID.Text, Address)
    If valCombined.Checked = True Then Manual_Update("UPDATE", "Dry_Dispo", "MAGIC_Dispo_ID = " & valMagic_Dispo_ID.Text & ", Combined = 1", "ID = " & valDry_Dispo_ID.Text & "")
    If flgFailed = False Then
      Manual_Update("Update", "Addresses", "Magic_New = 0", "ID = " & dblAddressID)
      MsgBox("DISPO SAVED.")
      flgMagicDone = True
    End If
    valCombined.Checked = True
    If flgMagicDone = True And flgMagicDone = True Then
      Appt_Load()
    End If
  End Sub

  Private Sub cmdPickUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPickUp.Click
    frmSearch.Tag = "PUP"
    frmSearch.ShowDialog(Me)

    cboEstimator.Items.Clear()
    Connect("Employees", General, drGeneral, "[Active] = 1 AND Group_Name = 'Estimators'", "[Position]")
    drGeneral = 0
    For drGeneral As Double = 0 To General.Rows.Count - 1
      cboEstimator.Items.Add(General.Rows(drGeneral).Item("Display_Name"))
      Estimator_Remove(General.Rows(drGeneral).Item("Display_Name"))
    Next

    valAppt_Date.Text = datDispo
    Appt_Load()
  End Sub

  Private Sub fraDryActions_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fraDryActions.Enter

  End Sub

  Private Sub fraAppointments_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fraAppointments.Enter

  End Sub

  Private Sub cmdDryReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDryReset.Click
    fraReset.Tag = "Dry_Dispo"
    fraReset.Visible = True
    cmdResetSave.Visible = True
    tabDispo.Enabled = False
  End Sub

  Private Sub cmdDryDNQ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDryDNQ.Click
    fraReset.Tag = "Dry_Dispo"
    fraReset.Visible = True
    cmdDNQSave.Visible = True
    tabDispo.Enabled = False
  End Sub

  Private Sub cmdSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSchedule.Click
    Try
      dblAddressID = Address.Rows(drAddress).Item("ID")
      datDate = Now
      frmSchedule.valSchedule = "Estimators"
      frmSchedule.Tag = "LEAD"
      frmSchedule.Schedule_Maker("estimators")
      frmSchedule.ShowInTaskbar = True
      frmSchedule.WindowState = FormWindowState.Maximized
      frmSchedule.ShowDialog()
    Catch ex As Exception
    End Try
  End Sub

  Private Sub cmdResetSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdResetSave.Click
    Compiler("INSERT INTO", Me, "BOTH")
    Dim mySQLString As String = strSaveSQL1
    Dim myValues As String = strValues1
    TableUpdate("INSERT INTO", fraReset.Tag.ToString, mySQLString, myValues, Me)
    If flgFailed = True Then
      MsgBox("Dispo Failed to save before any records were created.")
      Exit Sub
    End If

    Manual_Update("Update", fraReset.Tag.ToString, "[RESET] = True, RESET_Reason = '" & txtReason.Text & "', Print_Offer = '" & txtReason.Text & "'", "Appt_Date = " & FilterByDate(Date.Parse(valAppt_Date.Text)) & " And Address_ID = " & valAddress_ID.Text & " AND Estimator = '" & valEstimator.Text & "'")
    Manual_Update("UPDATE", "Schedule", "Dispo_Done = 1", "Date = " & FilterByDate(datDispo) & " AND Person = '" & valEstimator.Text & "' AND Address_ID = " & valAddress_ID.Text, Address)
    Manual_Update("INSERT INTO", "Call_List_Info", "(Address_ID, Call_List, Active_Date) Values(" & valAddress_ID.Text & ", 'RESET', " & FilterByDate(dtpCall_Date.Value.Date) & ")")
    If LCase(fraReset.Tag.ToString).Contains("dry") = True Then
      flgDryDone = True
    End If
    If LCase(fraReset.Tag.ToString).Contains("magic") = True Then
      flgMagicDone = True
    End If
    cmdExit_Click(Me, e)
    If flgDryDone = True And flgMagicDone = True Then
      Appt_Load()
    End If
  End Sub

  Private Sub cmdDNQSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDNQSave.Click
    Compiler("INSERT INTO", Me, "BOTH")
    Dim mySQLString As String = strSaveSQL1
    Dim myValues As String = strValues1
    TableUpdate("INSERT INTO", fraReset.Tag, mySQLString, myValues, Me)
    If flgFailed = True Then
      MsgBox("Dispo Failed to save before any records were created.")
      Exit Sub
    End If
    Manual_Update("Update", fraReset.Tag, "DNQ = True, DNQ_Reason = '" & txtReason.Text & "', Print_Offer = '" & txtReason.Text & "'", "Appt_Date = " & FilterByDate(datDispo) & " And Address_ID = " & dblAddressID & " AND Estimator = '" & valEstimator.Text & "'")
    Manual_Update("UPDATE", "Schedule", "Dispo_Done = 1", "Date = " & FilterByDate(datDispo) & " AND Person = '" & valEstimator.Text & "' AND Address_ID = " & valAddress_ID.Text, Address)
    If LCase(Me.Tag.ToString).Contains("dry") = True Then
      flgDryDone = True
    End If
    If LCase(Me.Tag.ToString).Contains("magic") = True Then
      flgMagicDone = True
    End If
    cmdExit_Click(Me, e)
    If flgDryDone = True And flgMagicDone = True Then
      Appt_Load()
    End If
  End Sub

  Private Sub cmdMagicReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMagicReset.Click
    fraReset.Tag = "Magic_Dispo"
    fraReset.Visible = True
    cmdResetSave.Visible = True
    tabDispo.Enabled = False
  End Sub

  Private Sub cmdMagicDNQ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMagicDNQ.Click
    fraReset.Tag = "Magic_Dispo"
    fraReset.Visible = True
    cmdDNQSave.Visible = True
    tabDispo.Enabled = False
  End Sub

  Private Sub cmdExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExit.Click
    fraReset.Tag = ""
    cmdResetSave.Visible = False
    cmdDNQSave.Visible = False
    txtReason.Text = ""
    dtpCall_Date.Value = Now
    tabDispo.Enabled = True
    fraReset.Visible = False
  End Sub

  Private Sub timLock_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timLock.Tick
    picDry.Width = tpgDry.Width
    picDry.Height = tpgDry.Height
    picMagicDone.Size = picDry.Size
    picDry.Visible = flgDryDone
    picMagicDone.Visible = flgMagicDone
  End Sub

  Private Sub cmdAddReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReset.Click
    frmSearch.Tag = "RESET"
    frmSearch.ShowDialog(Me)

    cboEstimator.Items.Clear()
    Connect("Employees", General, drGeneral, "[Active] = 1 AND Group_Name = 'Estimators'", "[Position]")
    drGeneral = 0
    For drGeneral As Double = 0 To General.Rows.Count - 1
      cboEstimator.Items.Add(General.Rows(drGeneral).Item("Display_Name"))
      Estimator_Remove(General.Rows(drGeneral).Item("Display_Name"))
    Next

    valAppt_Date.Text = datDispo

    Appt_Load()
  End Sub
End Class