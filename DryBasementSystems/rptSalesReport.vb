﻿Public Class rptSalesReport
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form Specific Declarations
  Dim Numbers As New DataTable
  Dim drNumbers As Double
  Dim Test As New DataTable
  Dim drTest As Double
  Dim datToKeeper As Date
  Dim datFromKeeper As Date
  Dim datMTD_Start As Date
  Dim datMTD_End As Date
  Dim flgYTD As Boolean

  Private Sub SalesReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    dtpFrom.Tag = dtpFrom.Value
    dtpTo.Tag = dtpTo.Value
    Connect("Estimators", General, drGeneral, "Working = 1", "Alias")
    drGeneral = 0
    cboEstimator.Items.Clear()
    For drGeneral As Double = 0 To General.Rows.Count - 1
      cboEstimator.Items.Add(General.Rows(drGeneral).Item("Alias"))
    Next

    Connect("Source", General, drGeneral, "Company = 'DRY'", "Source")
    drGeneral = 0
    cboSource.Items.Clear()
    For drGeneral As Double = 0 To General.Rows.Count - 1
      cboSource.Items.Add(General.Rows(drGeneral).Item("Source"))
    Next

    Connect("Zips", General, drGeneral, "Zips.[Default] = 1", "Zip")
    drGeneral = 0
    cboZip.Items.Clear()
    For drGeneral As Double = 0 To General.Rows.Count - 1
      cboZip.Items.Add(General.Rows(drGeneral).Item("Zip"))
    Next

    cboMonth.Items.Clear()
    cboMonth.Items.Add("JANUARY")
    cboMonth.Items.Add("FEBRUARY")
    cboMonth.Items.Add("MARCH")
    cboMonth.Items.Add("APRIL")
    cboMonth.Items.Add("MAY")
    cboMonth.Items.Add("JUNE")
    cboMonth.Items.Add("JULY")
    cboMonth.Items.Add("AUGUST")
    cboMonth.Items.Add("SEPTEMBER")
    cboMonth.Items.Add("OCTOBER")
    cboMonth.Items.Add("NOVEMBER")
    cboMonth.Items.Add("DECEMBER")

    cboYear.Items.Clear()
    datDate = Now.Date
    While datDate.Year >= 2007
      cboYear.Items.Add(datDate.Year)
      datDate = DateAdd(DateInterval.Year, -1, datDate)
    End While

    Connect("Report_Names", General, drGeneral, "Active = 1", "[Position]")
    drGeneral = 0
    cboReportType.Items.Clear()
    For drGeneral As Double = 0 To General.Rows.Count - 1
      cboReportType.Items.Add(General.Rows(drGeneral).Item("Report_Name"))
    Next

    Try
      cboReportType.SelectedItem = cboReportType.Items(0)
    Catch ex As Exception
      'do nothing
    End Try
    cmdThisMonth_Click(Me, e)
  End Sub

  Private Sub cmdCompile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCompile.Click
    'Try
    '  dtpFrom.Value = datMTD_Start
    '  dtpTo.Value = datMTD_End
    'Catch ex As Exception
    '  'do nothing
    'End Try

    Me.UseWaitCursor = True
    If cboReportType.Text = "Dispo" Then
      Try
        CallByName(Me, cboReportType.Text.Replace(" ", "_"), CallType.Method, Me)
      Catch ex As Exception
        MsgBox("No corresponding report was found.")
        Me.UseWaitCursor = False
        Exit Sub
      End Try
      'CallByName(Me, Replace(cboReportType.Text, " ", "_"), CallType.Method, Me)
      Me.UseWaitCursor = False
      Exit Sub
    End If
    Date_Reformat()
    Try
      CallByName(Me, cboReportType.Text.Replace(" ", "_"), CallType.Method, Me)
    Catch ex As Exception
      MsgBox("No corresponding report was found.")
      Me.UseWaitCursor = False
      Exit Sub
    End Try
    'CallByName(Me, Replace(cboReportType.Text, " ", "_"), CallType.Method, Me)
    Me.UseWaitCursor = False
  End Sub

  Public Sub Dispo(ByVal sender As System.Object)
    Dim Schedule As New DataTable
    Dim intMagicDispo As Integer

    Connect("Dry_Dispo", DryDispo, 0, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " And Appt_Date <= " & FilterByDate(dtpTo.Value), "Appt_Date, Estimator")
    Connect("Magic_Dispo", MagicDispo, 0, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " And Appt_Date <= " & FilterByDate(dtpTo.Value), "Combined, Appt_Date, Estimator")

    intMagicDispo = 0
    With grdNumbers
      .RowCount = 1
      .ColumnCount = 10

      .Columns(0).Width = 100
      .Columns(1).Width = 50
      .Columns(2).Width = 100
      .Columns(3).Width = 100
      .Columns(4).Width = 50
      .Columns(5).Width = 250
      .Columns(6).Width = 200
      .Columns(7).Width = 50
      .Columns(8).Width = 50
      .Columns(9).Width = 50

      .Columns(0).HeaderText = "EST"
      .Columns(1).HeaderText = "TIME"
      .Columns(2).HeaderText = "NAME"
      .Columns(3).HeaderText = "CITY"
      .Columns(4).HeaderText = "ZIP"
      .Columns(5).HeaderText = "DISPO"
      .Columns(6).HeaderText = "BM"
      .Columns(7).HeaderText = "SOURCE"
      .Columns(8).HeaderText = "BY"
      .Columns(9).HeaderText = "TYPE"

      For intCounter As Integer = 0 To DryDispo.Rows.Count - 1
        .RowCount = .RowCount + 1
        Connect("Addresses", Address, 0, "ID = " & DryDispo.Rows(intCounter).Item("Address_ID"))
        Connect("Clients", Clients, 0, "ID = " & Address.Rows(0).Item("Current_Client_ID"))
        Connect("Schedule", Schedule, 0, "Address_ID = " & Address.Rows(0).Item("ID") & " AND Date = " & FilterByDate(DryDispo.Rows(intCounter).Item("Appt_Date")))

        .Item(0, intCounter).Value = DryDispo.Rows(intCounter).Item("Estimator")
        If Schedule.Rows.Count > 0 Then
          .Item(1, intCounter).Value = FormatDateTime(Schedule.Rows(0).Item("Start_Time"), DateFormat.ShortTime)
        Else
          .Item(1, intCounter).Value = "N/A"
        End If
        .Item(2, intCounter).Value = Clients.Rows(0).Item("Last_Name")
        .Item(3, intCounter).Value = Address.Rows(0).Item("City")
        .Item(4, intCounter).Value = Address.Rows(0).Item("Zip")
        .Item(5, intCounter).Value = DryDispo.Rows(intCounter).Item("print_offer")
        If .Item(5, intCounter).Value.ToString.Contains("SOLD") Then .Item(5, intCounter).Style.ForeColor = Color.Red
        If DryDispo.Rows(intCounter).Item("Combined") = True Then
          .Item(6, intCounter).Value = MagicDispo.Rows(intMagicDispo).Item("print_offer")
          If .Item(6, intCounter).Value.ToString.Contains("SOLD") Then .Item(6, intCounter).Style.ForeColor = Color.Red
          intMagicDispo += 1
        Else
          .Item(6, intCounter).Value = "N/A"
        End If
        .Item(7, intCounter).Value = DryDispo.Rows(intCounter).Item("Source")
        .Item(8, intCounter).Value = Address.Rows(0).Item("Bonus_Sig")
        If Schedule.Rows.Count > 0 Then
          .Item(9, intCounter).Value = Schedule.Rows(0).Item("Appt_Type")
        Else
          .Item(9, intCounter).Value = "PUP"
        End If

      Next

      While intMagicDispo < MagicDispo.Rows.Count - 1
        .RowCount = .RowCount + 1
        Connect("Addresses", Address, 0, "ID = " & DryDispo.Rows(intMagicDispo).Item("Address_ID"))
        Connect("Clients", Clients, 0, "ID = " & Address.Rows(0).Item("Current_Client_ID"))
        Connect("Schedule", Schedule, 0, "Address_ID = " & Address.Rows(0).Item("ID") & " AND Date = " & FilterByDate(DryDispo.Rows(intMagicDispo).Item("Appt_Date")))

        .Item(0, .RowCount - 2).Value = DryDispo.Rows(intMagicDispo).Item("Estimator")
        If Schedule.Rows.Count > 0 Then
          .Item(1, .RowCount - 2).Value = FormatDateTime(Schedule.Rows(0).Item("Start_Time"), DateFormat.ShortTime)
        Else
          .Item(1, .RowCount - 2).Value = "N/A"
        End If
        .Item(2, .RowCount - 2).Value = Clients.Rows(0).Item("Last_Name")
        .Item(3, .RowCount - 2).Value = Address.Rows(0).Item("City")
        .Item(4, .RowCount - 2).Value = Address.Rows(0).Item("Zip")
        .Item(5, .RowCount - 2).Value = "N/A"
        .Item(6, .RowCount - 2).Value = MagicDispo.Rows(intMagicDispo).Item("print_offer")
        If .Item(6, .RowCount - 2).Value.ToString.Contains("SOLD") Then .Item(6, .RowCount - 2).Style.ForeColor = Color.Red
        .Item(7, .RowCount - 2).Value = DryDispo.Rows(intMagicDispo).Item("Source")
        .Item(8, .RowCount - 2).Value = Address.Rows(0).Item("Bonus_Sig")
        If Schedule.Rows.Count > 0 Then
          .Item(9, .RowCount - 2).Value = Schedule.Rows(0).Item("Appt_Type")
        Else
          .Item(9, .RowCount - 2).Value = "PUP"
        End If

        intMagicDispo += 1

      End While
      .RowCount = .RowCount - 1

      For intRow As Integer = 0 To .RowCount - 1
        For intCol As Integer = 0 To .ColumnCount - 1
          .Item(intCol, intRow).Style.Font = Font_Change("Arial", 9, FontStyle.Regular)
          .Item(intCol, intRow).Style.Alignment = DataGridViewContentAlignment.MiddleLeft
        Next
      Next
    End With


  End Sub

  Public Sub Standard_Sales_Company(ByVal sender As System.Object)
    Dim dblEstVolume As Double
    Dim flgDry As Boolean
    Dim flgMagic As Boolean
    With grdNumbers

      Report_Date_Format(flgYTD, True, True)
      If datMTD_Start.DayOfWeek = DayOfWeek.Sunday Then datMTD_Start = DateAdd(DateInterval.Day, 1, datMTD_Start)
      .BackgroundColor = Color.Black
      .GridColor = Color.Black
      Connect("Dry_Dispo", DryDispo, drDryDispo, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " And Appt_Date <= " & FilterByDate(dtpTo.Value) & " And Zip Like '" & cboZip.Text & "%' And Source Like '" & cboSource.Text & "%'", "Appt_Date, Estimator")
      drDryDispo = 0
      Connect("Dry_Dispo", DryDispo, drDryDispo, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " And Appt_Date <= " & FilterByDate(dtpTo.Value), "Appt_Date, Estimator")
      Connect("Magic_Dispo", MagicDispo, drMagicDispo, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " And Appt_Date <= " & FilterByDate(dtpTo.Value) & " And Combined = 0 And Zip Like '" & cboZip.Text & "%' And Source Like '" & cboSource.Text & "%'", "Appt_Date, Estimator")
      drMagicDispo = 0
      Estimator_Count()
      .ColumnCount = 17 + valEstimators.Items.Count
      .RowCount = DateDiff(DateInterval.Day, dtpFrom.Value, dtpTo.Value) + 1
      For r As Integer = 0 To .RowCount - 1
        .Rows(r).DividerHeight = 1
        .Rows(r).Height = 19
        For c As Integer = 0 To .ColumnCount - 1
          .Item(c, r).Style.WrapMode = DataGridViewTriState.True
          .Item(c, r).Style.Alignment = DataGridViewContentAlignment.MiddleCenter
          If c = 0 Then
            datDate = DateAdd(DateInterval.Day, r, dtpFrom.Value)
            .Item(c, r).Value = datDate.Month & "/" & datDate.Day
            .Item(c, r).Tag = datDate.Date
          Else
            .Item(c, r).Value = "0"
          End If

          .Item(c, r).Style.Font = Font_Change("Arial", 7, FontStyle.Regular)

        Next
      Next
      For c As Integer = 0 To .ColumnCount - 1
        .Columns(c).HeaderCell.Style.WrapMode = DataGridViewTriState.True
        .Columns(c).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        .Columns(c).HeaderCell.Style.Font = Font_Change("Arial", 6, FontStyle.Bold)
        .Columns(c).DividerWidth = 0.5

        If c = 0 Then
          .Columns(c).Width = 34
        ElseIf c >= 3 And c <= 14 Then
          .Columns(c).Width = 40
        ElseIf c >= 15 And c <= 16 Or c = 1 Or c = 2 Then
          .Columns(c).Width = 50
        ElseIf c > 16 Then
          .Columns(c).Width = 55
        End If
      Next

      .Columns(0).HeaderCell.Value = "DATE"
      .Columns(1).HeaderCell.Value = "VOLUME" & Chr(13) + Chr(10) & "MTD"
      .Columns(2).HeaderCell.Value = "GOAL"
      .Columns(3).HeaderCell.Value = "APPTS"
      .Columns(4).HeaderCell.Value = "APPTS" & Chr(13) + Chr(10) & "MTD"
      .Columns(5).HeaderCell.Value = "LEADS"
      .Columns(6).HeaderCell.Value = "LEADS" & Chr(13) + Chr(10) & "MTD"
      .Columns(7).HeaderCell.Value = "FTR"
      .Columns(8).HeaderCell.Value = "SITS"
      .Columns(9).HeaderCell.Value = "SITS" & Chr(13) + Chr(10) & "MTD"
      .Columns(10).HeaderCell.Value = "SALES"
      .Columns(11).HeaderCell.Value = "SALES" & Chr(13) + Chr(10) & "MTD"
      .Columns(12).HeaderCell.Value = "CLOS"
      .Columns(13).HeaderCell.Value = "ROL"
      .Columns(14).HeaderCell.Value = "VPO"
      .Columns(15).HeaderCell.Value = "VOLUME"
      .Columns(16).HeaderCell.Value = "BM VOLUME"

      For i As Integer = 0 To valEstimators.Items.Count - 1
        valEstimators.SelectedIndex = i
        .Columns((17 + i)).HeaderCell.Value = valEstimators.Text
      Next

      Connect("Dry_Dispo", DryDispo, drDryDispo, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " and Appt_Date <=" & FilterByDate(dtpTo.Value) & "  And Zip Like '" & cboZip.Text & "%' And Source Like '" & cboSource.Text & "%'", "Appt_Date, Estimator")
      drDryDispo = 0
      Connect("Magic_Dispo", MagicDispo, drMagicDispo, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " and Appt_Date <=" & FilterByDate(dtpTo.Value) & " And Combined = 0 And Zip Like '" & cboZip.Text & "%' And Source Like '" & cboSource.Text & "%'", "Appt_Date, Estimator")
      drMagicDispo = 0
      c = 0
      r = 0
      Dim intEstCol As Integer

      dblEstVolume = 0
      flgDry = False
      flgMagic = False
      For r As Integer = 0 To .RowCount - 1
        intEstCol = 0
        If DryDispo.Rows.Count > drDryDispo And flgDry = False Then
          If .Item(0, r).Value = DateValue(DryDispo.Rows(drDryDispo).Item("Appt_Date")).Month & "/" & DateValue(DryDispo.Rows(drDryDispo).Item("Appt_Date")).Day Then
            flgDry = False
            While .Columns(intEstCol).HeaderText <> DryDispo.Rows(drDryDispo).Item("Estimator")
              intEstCol = intEstCol + 1
              If intEstCol > .ColumnCount - 1 Then
                MsgBox("The program could not find a given estimator. Please check that all the given information is correct:" & DryDispo.Rows(drDryDispo).Item("Estimator") & "")
                Exit Sub
              End If
            End While
            .Item(3, r).Value = .Item(3, r).Value + 1   'Appts
            'If DryDispo.Rows(drDryDispo).Item("Lead") = True Then .Item(5, r).Value = .Item(5, r).Value + 1 'Leads
            If DryDispo.Rows(drDryDispo).Item("Reset") = False And DryDispo.Rows(drDryDispo).Item("DNQ") = False Then .Item(8, r).Value = .Item(8, r).Value + 1 'Sits
            If DryDispo.Rows(drDryDispo).Item("Sold") = True Then
              .Item(10, r).Value = .Item(10, r).Value + 1 'Sales
              If IsNumeric(DryDispo.Rows(drDryDispo).Item("Price")) = True Then
                If DryDispo.Rows(drDryDispo).Item("Combined") = True Then
                  Connect("Magic_Dispo", Numbers, drNumbers, "ID = " & DryDispo.Rows(drDryDispo).Item("Magic_Dispo_ID") & "")
                  drNumbers = 0
                  If Numbers.Rows(drNumbers).Item("Sold") = True Then
                    .Item(15, r).Value = FormatNumber(.Item(15, r).Value + Int(DryDispo.Rows(drDryDispo).Item("Price")), 0, , , TriState.True)    'DBI Volume
                    .Item(intEstCol, r).Value = FormatNumber(.Item(intEstCol, r).Value + Int(DryDispo.Rows(drDryDispo).Item("Price")) + Int(Numbers.Rows(drNumbers).Item("Price")), 0, , , TriState.True)    'Estimator Volume
                    .Item(16, r).Value = FormatNumber(.Item(16, r).Value + Int(Numbers.Rows(drNumbers).Item("Price")), 0, , , TriState.True)    'Magic Volume
                  Else
                    .Item(15, r).Value = FormatNumber(.Item(15, r).Value + Int(DryDispo.Rows(drDryDispo).Item("Price")), 0, , , TriState.True)    'DBI Volume
                    .Item(intEstCol, r).Value = FormatNumber(.Item(intEstCol, r).Value + Int(DryDispo.Rows(drDryDispo).Item("Price")), 0, , , TriState.True)    'Estimator Volume
                  End If
                Else
                  .Item(15, r).Value = FormatNumber(.Item(15, r).Value + Int(DryDispo.Rows(drDryDispo).Item("Price")), 0, , , TriState.True)    'DBI Volume
                  .Item(intEstCol, r).Value = FormatNumber(.Item(intEstCol, r).Value + Int(DryDispo.Rows(drDryDispo).Item("Price")), 0, , , TriState.True)    'Estimator Volume
                End If
              End If
            End If
            drDryDispo = drDryDispo + 1
          Else
            flgDry = True
          End If
          r = r - 1
        ElseIf MagicDispo.Rows.Count > drMagicDispo And flgMagic = False Then
          If .Item(0, r).Value = DateValue(MagicDispo.Rows(drMagicDispo).Item("Appt_Date")).Month & "/" & DateValue(MagicDispo.Rows(drMagicDispo).Item("Appt_Date")).Day Then
            flgMagic = False
            While .Columns(intEstCol).HeaderText <> MagicDispo.Rows(drMagicDispo).Item("Estimator")
              intEstCol = intEstCol + 1
              If intEstCol > .ColumnCount - 1 Then
                MsgBox("The program could not find a given estimator. Please check that all the given information is correct:" & DryDispo.Rows(drDryDispo).Item("Estimator") & "")
                Exit Sub
              End If
            End While
            .Item(3, r).Value = .Item(3, r).Value + 1   'Appts
            'If DryDispo.Rows(drDryDispo).Item("Lead") = True Then .Item(5, r).Value = .Item(5, r).Value + 1 'Leads
            If MagicDispo.Rows(drMagicDispo).Item("Reset") = False And MagicDispo.Rows(drMagicDispo).Item("DNQ") = False Then .Item(8, r).Value = .Item(8, r).Value + 1 'Sits
            If MagicDispo.Rows(drMagicDispo).Item("Sold") = True Then
              .Item(10, r).Value = .Item(10, r).Value + 1 'Sales
              If IsNumeric(MagicDispo.Rows(drMagicDispo).Item("Price")) = True Then
                .Item(intEstCol, r).Value = FormatNumber(.Item(intEstCol, r).Value + Int(MagicDispo.Rows(drMagicDispo).Item("Price")), 0, , , TriState.True)    'Estimator Volume
                .Item(16, r).Value = FormatNumber(.Item(16, r).Value + Int(MagicDispo.Rows(drMagicDispo).Item("Price")), 0, , , TriState.True)    'Magic Volume
              End If
            End If

            drMagicDispo = drMagicDispo + 1
          Else
            flgMagic = True
          End If
          r = r - 1
        Else
          flgDry = False
          flgMagic = False
          If DateValue(.Item(0, r).Tag).Date > datMTD_Start And DateValue(.Item(0, r).Tag).Date <= datMTD_End Then
            If DateValue(.Item(0, r - 1).Tag).DayOfWeek = DayOfWeek.Sunday Then
              If DateValue(.Item(0, r - 2).Tag).Date >= datMTD_Start Then
                .Item(4, r).Value = Int(.Item(3, r).Value) + Int(.Item(4, r - 2).Value)
                .Item(6, r).Value = Int(.Item(5, r).Value) + Int(.Item(6, r - 2).Value)
                .Item(9, r).Value = Int(.Item(8, r).Value) + Int(.Item(9, r - 2).Value)
                .Item(11, r).Value = Int(.Item(10, r).Value) + Int(.Item(11, r - 2).Value)
                .Item(1, r).Value = FormatNumber(Int(.Item(15, r).Value) + Int(.Item(16, r).Value) + Int(.Item(1, r - 2).Value), 0, , , TriState.True)
                For c As Integer = 17 To .ColumnCount - 1
                  .Item(c, r).Value = FormatNumber(Int(.Item(c, r).Value) + Int(.Item(c, r - 2).Value), 0, , , TriState.True)
                Next
              End If
            Else
              .Item(4, r).Value = Int(.Item(3, r).Value) + Int(.Item(4, r - 1).Value)
              .Item(6, r).Value = Int(.Item(5, r).Value) + Int(.Item(6, r - 1).Value)
              .Item(9, r).Value = Int(.Item(8, r).Value) + Int(.Item(9, r - 1).Value)
              .Item(11, r).Value = Int(.Item(10, r).Value) + Int(.Item(11, r - 1).Value)
              .Item(1, r).Value = FormatNumber(Int(.Item(15, r).Value) + Int(.Item(16, r).Value) + Int(.Item(1, r - 1).Value), 0, , , TriState.True)
              For c As Integer = 17 To .ColumnCount - 1
                .Item(c, r).Value = FormatNumber(Int(.Item(c, r).Value) + Int(.Item(c, r - 1).Value), 0, , , TriState.True)
              Next
            End If
          ElseIf DateValue(.Item(0, r).Tag).Date = datMTD_Start Then
            .Item(4, r).Value = Int(.Item(3, r).Value) + Int(.Item(4, r).Value)
            .Item(6, r).Value = Int(.Item(5, r).Value) + Int(.Item(6, r).Value)
            .Item(9, r).Value = Int(.Item(8, r).Value) + Int(.Item(9, r).Value)
            .Item(11, r).Value = Int(.Item(10, r).Value) + Int(.Item(11, r).Value)
            .Item(1, r).Value = FormatNumber(Int(.Item(15, r).Value) + Int(.Item(16, r).Value) + Int(.Item(1, r).Value), 0, , , TriState.True)
            For c As Integer = 17 To .ColumnCount - 1
              .Item(c, r).Value = FormatNumber(Int(.Item(c, r).Value) + Int(.Item(c, r).Value), 0, , , TriState.True)
            Next
          End If
          'FTR
          If .Item(4, r).Value = "0" Then
            .Item(7, r).Value = "0%"
          Else
            .Item(7, r).Value = (Int(.Item(4, r).Value) - Int(.Item(9, r).Value)) / Int(.Item(4, r).Value)
            .Item(7, r).Value = FormatPercent(.Item(7, r).Value, 1)
          End If
          'Closing
          If .Item(9, r).Value = "0" Then
            .Item(12, r).Value = "0%"
          Else
            .Item(12, r).Value = Int(.Item(11, r).Value) / Int(.Item(9, r).Value)
            .Item(12, r).Value = FormatPercent(.Item(12, r).Value, 1)
          End If
          'ROL
          If .Item(6, r).Value = "0" Then
            .Item(13, r).Value = "0"
          Else
            .Item(13, r).Value = Int(.Item(1, r).Value) / Int(.Item(6, r).Value)
            .Item(13, r).Value = FormatNumber(.Item(13, r).Value, 0, , , TriState.True)
          End If
          'VPO
          If .Item(11, r).Value = "0" Then
            .Item(14, r).Value = "0"
          Else
            .Item(14, r).Value = Int(.Item(1, r).Value) / Int(.Item(11, r).Value)
            .Item(14, r).Value = FormatNumber(.Item(14, r).Value, 0, , , TriState.True)
          End If


        End If
      Next

      Dim dblWeekTracker As Double = 0

      For r As Integer = 0 To .RowCount - 1
        If DateValue(.Item(0, r).Tag).DayOfWeek = DayOfWeek.Sunday Then
          .Item(0, r).Value = "WEEK"
          .Item(1, r).Value = ""
          .Item(2, r).Value = ""
          For intTemp As Integer = (r - 6) To r - 1
            .Item(3, r).Value = Int(.Item(3, r).Value) + Int(.Item(3, intTemp).Value)
            .Item(5, r).Value = Int(.Item(5, r).Value) + Int(.Item(5, intTemp).Value)
            .Item(8, r).Value = Int(.Item(8, r).Value) + Int(.Item(8, intTemp).Value)
            .Item(10, r).Value = Int(.Item(10, r).Value) + Int(.Item(10, intTemp).Value)
            .Item(15, r).Value = Int(.Item(15, r).Value) + Int(.Item(15, intTemp).Value)
            .Item(16, r).Value = Int(.Item(16, r).Value) + Int(.Item(16, intTemp).Value)
            For intColTemp As Integer = 17 To .ColumnCount - 1
              If intTemp = r - 6 Then
                .Item(intColTemp, r).Value = 0
                Try
                  dblWeekTracker = Int(.Item(intColTemp, intTemp - 2).Value & "") + 0
                Catch ex As Exception
                  dblWeekTracker = 0
                End Try
              Else
                dblWeekTracker = Int(.Item(intColTemp, intTemp - 1).Value)
              End If
              If Int(.Item(intColTemp, intTemp).Value) > 0 Then
                .Item(intColTemp, r).Value = FormatNumber(Int(.Item(intColTemp, r).Value) + (Int(.Item(intColTemp, intTemp).Value) - dblWeekTracker), 0, , , TriState.True)
              Else
                'do nothing
              End If
            Next
          Next
          .Item(4, r).Value = .Item(3, r).Value
          .Item(6, r).Value = .Item(5, r).Value
          .Item(9, r).Value = .Item(8, r).Value
          .Item(11, r).Value = .Item(10, r).Value
          For intColTemp As Integer = 0 To .ColumnCount - 1
            .Item(intColTemp, r).Style.BackColor = Color.Yellow
          Next

          'FTR
          If .Item(3, r).Value = "0" Then
            .Item(7, r).Value = "0%"
          Else
            .Item(7, r).Value = (Int(.Item(3, r).Value) - Int(.Item(8, r).Value)) / Int(.Item(3, r).Value)
            .Item(7, r).Value = FormatPercent(.Item(7, r).Value, 1)
          End If
          'Closing
          If .Item(8, r).Value = "0" Then
            .Item(12, r).Value = "0%"
          Else
            .Item(12, r).Value = Int(.Item(10, r).Value) / Int(.Item(8, r).Value)
            .Item(12, r).Value = FormatPercent(.Item(12, r).Value, 1)
          End If
          'ROL
          If .Item(5, r).Value = "0" Then
            .Item(13, r).Value = "0"
          Else
            .Item(13, r).Value = Int(.Item(15, r).Value) / Int(.Item(5, r).Value)
            .Item(13, r).Value = FormatNumber(.Item(13, r).Value, 0, , , TriState.True)
          End If
          'VPO
          If .Item(10, r).Value = "0" Then
            .Item(14, r).Value = "0"
          Else
            .Item(14, r).Value = Int(.Item(15, r).Value) / Int(.Item(10, r).Value)
            .Item(14, r).Value = FormatNumber(.Item(14, r).Value, 0, , , TriState.True)
          End If

        End If
      Next
    End With
    Grid_Lock(Me, grdNumbers)

  End Sub

  Public Sub Report_Date_Format(ByVal Year As Boolean, ByVal Month As Boolean, ByVal Week As Boolean)
    datToKeeper = dtpTo.Value.Date
    datFromKeeper = dtpFrom.Value.Date
    If Year = True Then
      dtpFrom.Value = DateValue("1/1/" & Now.Year).Date
      dtpTo.Value = DateValue("12/31/" & Now.Year).Date
      datMTD_Start = dtpFrom.Value
      datMTD_End = dtpTo.Value
    End If
    If Month = True Then
      dtpFrom.Value = DateValue(dtpFrom.Value.Month.ToString & "/1/" & dtpFrom.Value.Year.ToString)
      dtpTo.Value = dtpTo.Value.Month & "/" & Date.DaysInMonth(dtpTo.Value.Year, dtpTo.Value.Month) & "/" & dtpTo.Value.Year
      If Year = False Then
        datMTD_Start = dtpFrom.Value
        datMTD_End = dtpTo.Value
      End If
    End If
    If Week = True Then
      If dtpFrom.Value.DayOfWeek = DayOfWeek.Sunday Then
        dtpFrom.Value = DateAdd(DateInterval.Day, 1, dtpFrom.Value)
      Else
        While dtpFrom.Value.DayOfWeek <> DayOfWeek.Monday
          dtpFrom.Value = DateAdd(DateInterval.Day, -1, dtpFrom.Value)
        End While
      End If
      While dtpTo.Value.DayOfWeek <> DayOfWeek.Sunday
        dtpTo.Value = DateAdd(DateInterval.Day, 1, dtpTo.Value)
      End While
      If Year = False And Month = False Then
        datMTD_Start = dtpFrom.Value
        datMTD_End = dtpTo.Value
      End If
    End If
  End Sub

  Public Sub Estimator_Count()

    valEstimators.Items.Clear()
    If DryDispo.Rows.Count > 0 Then
      For dblMine As Double = 0 To DryDispo.Rows.Count - 1
        With DryDispo.Rows(dblMine)
          If valEstimators.Items.Contains(.Item("Estimator")) = False Then
            valEstimators.Items.Add(.Item("Estimator"))
          End If
        End With
      Next
    End If

    If MagicDispo.Rows.Count > 0 Then
      For dblMine As Double = 0 To MagicDispo.Rows.Count - 1
        With MagicDispo.Rows(dblMine)
          If valEstimators.Items.Contains(.Item("Estimator")) = False Then
            valEstimators.Items.Add(.Item("Estimator"))
          End If
        End With
      Next
    End If
    valEstimators.Sorted = True
    Try
      valEstimators.SelectedIndex = 0
      If valEstimators.Text = "" Then valEstimators.Items.RemoveAt(0)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub rptSalesReport_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    grdNumbers.Width = Me.Width - grdNumbers.Left - 40
    grdNumbers.Height = Me.Height - grdNumbers.Top - 40
  End Sub

  Private Sub cmdLastMonth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLastMonth.Click
    Date_Reformat()

    dtpTo.Value = DateAdd(DateInterval.Month, -1, dtpTo.Value)

    dtpFrom.Value = DateAdd(DateInterval.Month, -1, dtpFrom.Value)
    cmdCompile_Click(Me, e)
  End Sub

  Private Sub cmdNextMonth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNextMonth.Click
    Date_Reformat()

    dtpTo.Value = DateAdd(DateInterval.Month, 1, dtpTo.Value)

    dtpFrom.Value = DateAdd(DateInterval.Month, 1, dtpFrom.Value)
    cmdCompile_Click(Me, e)
  End Sub

  Private Sub cmdThisMonth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdThisMonth.Click
    dtpFrom.Value = DateValue(Now.Month & "/1/" & Now.Year).Date
    dtpTo.Value = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, DateValue(Now.Month & "/1/" & Now.Year)))
    cmdCompile_Click(Me, e)
  End Sub

  Private Sub cmdYTD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdYTD.Click
    flgYTD = True
    cmdCompile_Click(Me, e)
    flgYTD = False
  End Sub

  Private Sub cmdPriorYTD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPriorYTD.Click
    dtpFrom.Value = DateValue("1/1/" & DateValue(DateAdd(DateInterval.Year, -1, Now.Date)).Year).Date
    dtpTo.Value = DateValue("12/31/" & DateValue(DateAdd(DateInterval.Year, -1, Now.Date)).Year).Date
    flgYTD = True
    cmdCompile_Click(Me, e)
    flgYTD = False
  End Sub

  Private Sub cmdPriorYear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPriorYear.Click
    Date_Reformat()
    dtpTo.Value = DateAdd(DateInterval.Year, -1, dtpTo.Value)
    dtpFrom.Value = DateAdd(DateInterval.Year, -1, dtpFrom.Value)
    cmdCompile_Click(Me, e)
  End Sub

  Public Sub Estimator_Sales_Numbers(ByVal sender As System.Object)
    Dim dblEstVolume As Double
    Dim flgDry As Boolean
    Dim flgMagic As Boolean

    With grdNumbers

      Report_Date_Format(flgYTD, True, True)
      .BackgroundColor = Color.Black
      .GridColor = Color.Black
      Connect("Dry_Dispo", DryDispo, drDryDispo, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " And Appt_Date <= " & FilterByDate(dtpTo.Value), "Appt_Date, Estimator")
      drDryDispo = 0
      Connect("Magic_Dispo", MagicDispo, drMagicDispo, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " And Appt_Date <= " & FilterByDate(dtpTo.Value) & " And Combined = 0", "Appt_Date, Estimator")
      drMagicDispo = 0

      .ColumnCount = 23
      .RowCount = DateDiff(DateInterval.Day, dtpFrom.Value, dtpTo.Value) + 1
      For r As Integer = 0 To .RowCount - 1
        .Rows(r).DividerHeight = 1
        .Rows(r).Height = 19
        For c As Integer = 0 To .ColumnCount - 1
          .Item(c, r).Style.WrapMode = DataGridViewTriState.True
          .Item(c, r).Style.Alignment = DataGridViewContentAlignment.MiddleCenter
          If c = 0 Then
            datDate = DateAdd(DateInterval.Day, r, dtpFrom.Value)
            .Item(c, r).Value = datDate.Month & "/" & datDate.Day
            .Item(c, r).Tag = datDate.Date
          Else
            .Item(c, r).Value = "0"
          End If

          .Item(c, r).Style.Font = Font_Change("Arial", 7, FontStyle.Regular)

        Next
      Next
      For c As Integer = 0 To .ColumnCount - 1
        .Columns(c).HeaderCell.Style.WrapMode = DataGridViewTriState.True
        .Columns(c).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        .Columns(c).HeaderCell.Style.Font = Font_Change("Arial", 6, FontStyle.Bold)
        .Columns(c).DividerWidth = 0.5

        If c = 0 Then
          .Columns(c).Width = 34
        ElseIf c >= 3 And c <= 14 Then
          .Columns(c).Width = 40
        ElseIf c >= 15 And c <= 16 Or c = 1 Or c = 2 Then
          .Columns(c).Width = 50
        ElseIf c > 16 Then
          .Columns(c).Width = 55
        End If
      Next

      .Columns(0).HeaderCell.Value = "DATE"
      .Columns(1).HeaderCell.Value = "APPTS"
      .Columns(2).HeaderCell.Value = "APPTS" & Chr(13) + Chr(10) & "MTD"
      .Columns(3).HeaderCell.Value = "LEADS"
      .Columns(4).HeaderCell.Value = "LEADS" & Chr(13) + Chr(10) & "MTD"
      .Columns(5).HeaderCell.Value = "RESETS"
      .Columns(6).HeaderCell.Value = "RESETS" & Chr(13) + Chr(10) & "MTD"
      .Columns(7).HeaderCell.Value = "DNQS"
      .Columns(8).HeaderCell.Value = "DNQS" & Chr(13) + Chr(10) & "MTD"
      .Columns(9).HeaderCell.Value = "FTR"
      .Columns(10).HeaderCell.Value = "SITS"
      .Columns(11).HeaderCell.Value = "SITS" & Chr(13) + Chr(10) & "MTD"
      .Columns(12).HeaderCell.Value = "SALES"
      .Columns(13).HeaderCell.Value = "SALES" & Chr(13) + Chr(10) & "MTD"
      .Columns(14).HeaderCell.Value = "CLOS"
      .Columns(15).HeaderCell.Value = "VOLUME"
      .Columns(16).HeaderCell.Value = "VPO"
      .Columns(17).HeaderCell.Value = "ROL"
      .Columns(18).HeaderCell.Value = "PU/MAIL"
      .Columns(19).HeaderCell.Value = "PU/MAIL" & Chr(13) + Chr(10) & "MTD"
      .Columns(20).HeaderCell.Value = "VMTD"
      .Columns(21).HeaderCell.Value = "BM"
      .Columns(22).HeaderCell.Value = "BM" & Chr(13) + Chr(10) & "MTD"

      Connect("Dry_Dispo", DryDispo, drDryDispo, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " and Appt_Date <=" & FilterByDate(dtpTo.Value) & " And Estimator = '" & cboEstimator.Text & "'", "Appt_Date, Estimator")
      drDryDispo = 0
      Connect("Magic_Dispo", MagicDispo, drMagicDispo, "Appt_Date >= " & FilterByDate(dtpFrom.Value) & " and Appt_Date <=" & FilterByDate(dtpTo.Value) & " And Combined = 0 And Estimator = '" & cboEstimator.Text & "'", "Appt_Date, Estimator")
      drMagicDispo = 0
      c = 0
      r = 0
      Dim intEstCol As Integer

      dblEstVolume = 0
      flgDry = False
      flgMagic = False
      For r As Integer = 0 To .RowCount - 1
        intEstCol = 0
        If DryDispo.Rows.Count > drDryDispo And flgDry = False Then
          If .Item(0, r).Value = DateValue(DryDispo.Rows(drDryDispo).Item("Appt_Date")).Month & "/" & DateValue(DryDispo.Rows(drDryDispo).Item("Appt_Date")).Day Then
            flgDry = False
            .Item(1, r).Value = .Item(1, r).Value + 1   'Appts
            'If DryDispo.Rows(drDryDispo).Item("Lead") = True Then .Item(5, r).Value = .Item(5, r).Value + 1 'Leads
            .Item(10, r).Value = .Item(10, r).Value + 1 'Sits
            If DryDispo.Rows(drDryDispo).Item("Reset") = True Then
              .Item(5, r).Value = .Item(5, r).Value + 1 'Resets
              .Item(10, r).Value = .Item(10, r).Value - 1 'Sit Subtract
            End If
            If DryDispo.Rows(drDryDispo).Item("DNQ") = True Then
              .Item(8, r).Value = .Item(8, r).Value + 1 'DNQ
              .Item(10, r).Value = .Item(10, r).Value - 1 'Sit Subtract
            End If
            If DryDispo.Rows(drDryDispo).Item("Sold") = True Then
              .Item(12, r).Value = .Item(12, r).Value + 1 'Sales
              If IsNumeric(DryDispo.Rows(drDryDispo).Item("Price")) = True Then
                If DryDispo.Rows(drDryDispo).Item("Combined") = True Then
                  Connect("Magic_Dispo", Numbers, drNumbers, "ID = " & DryDispo.Rows(drDryDispo).Item("Magic_Dispo_ID") & "")
                  drNumbers = 0
                  If Numbers.Rows(drNumbers).Item("Sold") = True Then
                    .Item(15, r).Value = FormatNumber(.Item(15, r).Value + Int(DryDispo.Rows(drDryDispo).Item("Price")) + Int(Numbers.Rows(drNumbers).Item("Price")), 0, , , TriState.True)    'Company Volume
                    .Item(22, r).Value = FormatNumber(.Item(22, r).Value + Int(Numbers.Rows(drNumbers).Item("Price")), 0, , , TriState.True)    'Magic Volume
                    .Item(21, r).Value = .Item(21, r).Value + 1 'BM Sales
                  Else
                    .Item(15, r).Value = FormatNumber(.Item(15, r).Value + Int(DryDispo.Rows(drDryDispo).Item("Price")), 0, , , TriState.True)    'Company Volume
                  End If
                Else
                  .Item(15, r).Value = FormatNumber(.Item(15, r).Value + Int(DryDispo.Rows(drDryDispo).Item("Price")), 0, , , TriState.True)    'Company Volume
                End If
              End If
            ElseIf DryDispo.Rows(drDryDispo).Item("Combined") = True Then
              Connect("Magic_Dispo", Numbers, drNumbers, "ID = " & DryDispo.Rows(drDryDispo).Item("Magic_Dispo_ID") & "")
              drNumbers = 0
              If Numbers.Rows(drNumbers).Item("Sold") = True Then
                .Item(12, r).Value = .Item(12, r).Value + 1 'Sales
                .Item(21, r).Value = .Item(21, r).Value + 1 'BM Sales
                .Item(15, r).Value = FormatNumber(.Item(15, r).Value + Int(Numbers.Rows(drNumbers).Item("Price")), 0, , , TriState.True)    'Company Volume
                .Item(22, r).Value = FormatNumber(.Item(22, r).Value + Int(Numbers.Rows(drNumbers).Item("Price")), 0, , , TriState.True)    'Magic Volume
              End If
            End If
            drDryDispo = drDryDispo + 1
          Else
            flgDry = True
          End If
          r = r - 1
        ElseIf MagicDispo.Rows.Count > drMagicDispo And flgMagic = False Then
          If .Item(0, r).Value = DateValue(MagicDispo.Rows(drMagicDispo).Item("Appt_Date")).Month & "/" & DateValue(MagicDispo.Rows(drMagicDispo).Item("Appt_Date")).Day Then
            flgMagic = False
            .Item(1, r).Value = .Item(1, r).Value + 1   'Appts
            'If magicDispo.Rows(drmagicDispo).Item("Lead") = True Then .Item(5, r).Value = .Item(5, r).Value + 1 'Leads
            .Item(10, r).Value = .Item(10, r).Value + 1 'Sits
            If MagicDispo.Rows(drMagicDispo).Item("Reset") = True Then
              .Item(5, r).Value = .Item(5, r).Value + 1 'Resets
              .Item(10, r).Value = .Item(10, r).Value - 1 'Sit Subtract
            End If
            If MagicDispo.Rows(drMagicDispo).Item("DNQ") = True Then
              .Item(8, r).Value = .Item(8, r).Value + 1 'DNQ
              .Item(10, r).Value = .Item(10, r).Value - 1 'Sit Subtract
            End If
            If MagicDispo.Rows(drMagicDispo).Item("Sold") = True Then
              .Item(12, r).Value = .Item(12, r).Value + 1 'Sales
              .Item(21, r).Value = .Item(21, r).Value + 1 'BM Sale
              If IsNumeric(MagicDispo.Rows(drMagicDispo).Item("Price")) = True Then
                .Item(15, r).Value = FormatNumber(.Item(15, r).Value + Int(MagicDispo.Rows(drMagicDispo).Item("Price")), 0, , , TriState.True)    'Company Volume
                .Item(22, r).Value = FormatNumber(.Item(22, r).Value + Int(MagicDispo.Rows(drMagicDispo).Item("Price")), 0, , , TriState.True)    'Magic Volume
              End If
            End If
            drMagicDispo = drMagicDispo + 1
          Else
            flgMagic = True
          End If
          r = r - 1
        Else
          flgDry = False
          flgMagic = False
          If DateValue(.Item(0, r).Tag).Date > datMTD_Start And DateValue(.Item(0, r).Tag).Date <= datMTD_End Then
            If DateValue(.Item(0, r - 1).Tag).DayOfWeek = DayOfWeek.Sunday Then
              If DateValue(.Item(0, r - 2).Tag).Date >= datMTD_Start Then
                .Item(2, r).Value = Int(.Item(1, r).Value) + Int(.Item(2, r - 2).Value) 'Appts MTD
                .Item(4, r).Value = Int(.Item(3, r).Value) + Int(.Item(4, r - 2).Value) 'Leads MTD
                .Item(6, r).Value = Int(.Item(5, r).Value) + Int(.Item(6, r - 2).Value) 'Resets MTD
                .Item(8, r).Value = Int(.Item(7, r).Value) + Int(.Item(8, r - 2).Value) 'DNQs MTD
                .Item(11, r).Value = Int(.Item(10, r).Value) + Int(.Item(11, r - 2).Value) 'Sits MTD
                .Item(13, r).Value = Int(.Item(12, r).Value) + Int(.Item(13, r - 2).Value) 'Sales MTD
                .Item(19, r).Value = Int(.Item(18, r).Value) + Int(.Item(19, r - 2).Value) 'PUs MTD
                .Item(20, r).Value = FormatNumber(Int(.Item(15, r).Value) + Int(.Item(20, r - 2).Value), 0, , , TriState.True) 'Volume MTD
              End If
            Else
              .Item(2, r).Value = Int(.Item(1, r).Value) + Int(.Item(2, r - 1).Value) 'Appts MTD
              .Item(4, r).Value = Int(.Item(3, r).Value) + Int(.Item(4, r - 1).Value) 'Leads MTD
              .Item(6, r).Value = Int(.Item(5, r).Value) + Int(.Item(6, r - 1).Value) 'Resets MTD
              .Item(8, r).Value = Int(.Item(7, r).Value) + Int(.Item(8, r - 1).Value) 'DNQs MTD
              .Item(11, r).Value = Int(.Item(10, r).Value) + Int(.Item(11, r - 1).Value) 'Sits MTD
              .Item(13, r).Value = Int(.Item(12, r).Value) + Int(.Item(13, r - 1).Value) 'Sales MTD
              .Item(19, r).Value = Int(.Item(18, r).Value) + Int(.Item(19, r - 1).Value) 'PUs MTD
              .Item(20, r).Value = FormatNumber(Int(.Item(15, r).Value) + Int(.Item(20, r - 1).Value), 0, , , TriState.True) 'Volume MTD
            End If
          ElseIf DateValue(.Item(0, r).Tag).Date = datMTD_Start Then
            .Item(2, r).Value = Int(.Item(1, r).Value) + Int(.Item(2, r).Value) 'Appts MTD
            .Item(4, r).Value = Int(.Item(3, r).Value) + Int(.Item(4, r).Value) 'Leads MTD
            .Item(6, r).Value = Int(.Item(5, r).Value) + Int(.Item(6, r).Value) 'Resets MTD
            .Item(8, r).Value = Int(.Item(7, r).Value) + Int(.Item(8, r).Value) 'DNQs MTD
            .Item(11, r).Value = Int(.Item(10, r).Value) + Int(.Item(11, r).Value) 'Sits MTD
            .Item(13, r).Value = Int(.Item(12, r).Value) + Int(.Item(13, r).Value) 'Sales MTD
            .Item(19, r).Value = Int(.Item(18, r).Value) + Int(.Item(19, r).Value) 'PUs MTD
            .Item(20, r).Value = FormatNumber(Int(.Item(15, r).Value) + Int(.Item(20, r).Value), 0, , , TriState.True) 'Volume MTD
          End If
          Debug.Print(.Item(20, r).Value & "/" & .Item(15, r).Value)
          'FTR
          If .Item(2, r).Value = "0" Or Int(.Item(6, r).Value) + Int(.Item(8, r).Value) = 0 Then
            .Item(9, r).Value = "0%"
          Else
            .Item(9, r).Value = (Int(.Item(6, r).Value) + Int(.Item(8, r).Value)) / Int(.Item(2, r).Value)
            .Item(9, r).Value = FormatPercent(.Item(9, r).Value, 1)
          End If
          'Closing
          If .Item(11, r).Value = "0" Then
            .Item(14, r).Value = "0%"
          Else
            .Item(14, r).Value = Int(.Item(13, r).Value) / Int(.Item(11, r).Value)
            .Item(14, r).Value = FormatPercent(.Item(14, r).Value, 1)
          End If
          'ROL
          If .Item(4, r).Value = "0" Then
            .Item(17, r).Value = "0"
          Else
            .Item(17, r).Value = Int(.Item(20, r).Value) / Int(.Item(4, r).Value)
            .Item(17, r).Value = FormatNumber(.Item(17, r).Value, 0, , , TriState.True)
          End If
          'VPO
          If .Item(13, r).Value = "0" Then
            .Item(16, r).Value = "0"
          Else
            .Item(16, r).Value = Int(.Item(20, r).Value) / Int(.Item(13, r).Value)
            .Item(16, r).Value = FormatNumber(.Item(16, r).Value, 0, , , TriState.True)
          End If
        End If
      Next

      For r As Integer = 0 To .RowCount - 1
        If DateValue(.Item(0, r).Tag).DayOfWeek = DayOfWeek.Sunday Then
          .Item(0, r).Value = "WEEK"
          For intTemp As Integer = (r - 6) To r - 1
            .Item(1, r).Value = Int(.Item(1, r).Value) + Int(.Item(1, intTemp).Value) 'Appts
            .Item(3, r).Value = Int(.Item(3, r).Value) + Int(.Item(3, intTemp).Value) 'Leads
            .Item(5, r).Value = Int(.Item(5, r).Value) + Int(.Item(5, intTemp).Value) 'Resets
            .Item(7, r).Value = Int(.Item(7, r).Value) + Int(.Item(7, intTemp).Value) 'DNQs
            .Item(10, r).Value = Int(.Item(10, r).Value) + Int(.Item(10, intTemp).Value) 'Sits
            .Item(12, r).Value = Int(.Item(12, r).Value) + Int(.Item(12, intTemp).Value) 'Sales
            .Item(15, r).Value = Int(.Item(15, r).Value) + Int(.Item(15, intTemp).Value) 'Volume
            .Item(18, r).Value = Int(.Item(18, r).Value) + Int(.Item(18, intTemp).Value) 'PUs
            .Item(21, r).Value = Int(.Item(21, r).Value) + Int(.Item(21, intTemp).Value) 'Magic Sales
            .Item(22, r).Value = Int(.Item(22, r).Value) + Int(.Item(22, intTemp).Value) 'Magic Volume
          Next
          .Item(2, r).Value = .Item(1, r).Value 'Appts
          .Item(4, r).Value = .Item(3, r).Value 'Leads
          .Item(6, r).Value = .Item(5, r).Value 'Resets
          .Item(8, r).Value = .Item(7, r).Value 'DNQs
          .Item(11, r).Value = .Item(10, r).Value 'Sits
          .Item(13, r).Value = .Item(12, r).Value 'Sales
          .Item(20, r).Value = .Item(15, r).Value 'Volume
          .Item(19, r).Value = .Item(18, r).Value 'PUs
          For intColTemp As Integer = 0 To .ColumnCount - 1
            .Item(intColTemp, r).Style.BackColor = Color.Yellow
          Next

          'FTR
          If .Item(2, r).Value = "0" Then
            .Item(9, r).Value = "0%"
          Else
            .Item(9, r).Value = (Int(.Item(6, r).Value) + Int(.Item(8, r).Value)) / Int(.Item(2, r).Value)
            .Item(9, r).Value = FormatPercent(.Item(9, r).Value, 1)
          End If
          'Closing
          If .Item(11, r).Value = "0" Then
            .Item(14, r).Value = "0%"
          Else
            .Item(14, r).Value = Int(.Item(13, r).Value) / Int(.Item(11, r).Value)
            .Item(14, r).Value = FormatPercent(.Item(14, r).Value, 1)
          End If
          'ROL
          If .Item(4, r).Value = "0" Then
            .Item(17, r).Value = "0"
          Else
            .Item(17, r).Value = Int(.Item(20, r).Value) / Int(.Item(4, r).Value)
            .Item(17, r).Value = FormatNumber(.Item(17, r).Value, 0, , , TriState.True)
          End If
          'VPO
          If .Item(13, r).Value = "0" Then
            .Item(16, r).Value = "0"
          Else
            .Item(16, r).Value = Int(.Item(20, r).Value) / Int(.Item(13, r).Value)
            .Item(16, r).Value = FormatNumber(.Item(16, r).Value, 0, , , TriState.True)
          End If
        End If
      Next
    End With
    Grid_Lock(Me, grdNumbers)
  End Sub

  Public Sub Lead_Tracking(ByVal sender As System.Object)
    Dim Source As New DataTable
    Dim Schedule As New DataTable




  End Sub

  Public Sub Advertising_Analysis(ByVal sender As System.Object)
    Dim Source As New DataTable
    Dim drSource As Double
    Dim dblVolume As Double
    Dim dblTotalVolume As Double
    Dim dblTotalCost As Double
    With grdNumbers

      Report_Date_Format(flgYTD, True, False)
      .RowCount = 0
      .ColumnCount = 0
      Connect("Source", Source, drSource, "Company = 'DRY'", "Source")
      drSource = 0
      .ColumnCount = 5
      .RowCount = 2 + Source.Rows.Count

      .Columns(0).Width = 150
      .Columns(1).Width = 100
      .Columns(2).Width = 80
      .Columns(3).Width = 100
      .Columns(4).Width = 80

      .Columns(0).HeaderCell.Value = "Source"
      .Columns(1).HeaderCell.Value = "Income"
      .Columns(2).HeaderCell.Value = "Income %"
      .Columns(3).HeaderCell.Value = "Expenses"
      .Columns(4).HeaderCell.Value = "%"

      For r As Integer = 0 To Source.Rows.Count - 1
        .Item(0, r).Value = Source.Rows(r).Item("Source")
        .Rows(r).Height = 22
      Next
      .Item(0, .RowCount - 2).Value = "TOTALS"
      .Item(0, .RowCount - 1).Value = "MISC"
      dblTotalVolume = 0
      dblTotalCost = 0
      For r As Integer = 0 To .RowCount - 1
        For c As Integer = 0 To .ColumnCount - 1
          .Item(c, r).Style.Font = Font_Change("Arial", 9, FontStyle.Regular)
          .Item(c, r).Style.BackColor = Color.White
          If c > 0 Then
            .Item(c, r).Style.Alignment = DataGridViewContentAlignment.MiddleRight
          End If
        Next
        'Income
        Connect("Dry_Production", Source, drSource, "Source = '" & .Item(0, r).Value & "' And Start_Date >= " & FilterByDate(dtpFrom.Value) & " And Start_Date <= " & FilterByDate(dtpTo.Value), "Start_Date")
        Debug.Print(.Item(0, r).Value)
        drSource = 0
        dblVolume = 0
        While drSource < Source.Rows.Count
          dblVolume = dblVolume + Int(Source.Rows(drSource).Item("Price"))
          dblTotalVolume = dblTotalVolume + Int(Source.Rows(drSource).Item("Price"))
          drSource = drSource + 1
        End While
        .Item(1, r).Value = FormatNumber(dblVolume, 2, , TriState.True, TriState.True)



        'Expenses
        Connect("Expenses", Source, drSource, "Source = '" & .Item(0, r).Value & "' And Date >= " & FilterByDate(dtpFrom.Value) & " And Date <= " & FilterByDate(dtpTo.Value), "Date")
        drSource = 0
        dblVolume = 0
        While drSource < Source.Rows.Count
          dblVolume = dblVolume + Int(Source.Rows(drSource).Item("Amount"))
          If Source.Rows(drSource).Item("Source") <> "MISC" Then dblTotalCost = dblTotalCost + Int(Source.Rows(drSource).Item("Amount"))
          drSource = drSource + 1
        End While
        .Item(3, r).Value = FormatNumber(dblVolume, 2, , TriState.True, TriState.True)
        .Item(4, r).Value = FormatPercent(Int(.Item(3, r).Value) / Int(.Item(1, r).Value), 2, , TriState.True, TriState.True)
        If .Item(4, r).Value = "NaN" Then
          .Item(4, r).Value = ("0.00%")
        ElseIf .Item(4, r).Value = "Infinity" Then
          .Item(4, r).Value = "∞"
        End If
      Next
      .Item(1, .RowCount - 1).Value = ""
      .Item(1, .RowCount - 2).Value = FormatNumber(dblTotalVolume, 2, , TriState.True, TriState.True)
      .Item(3, .RowCount - 2).Value = FormatNumber(dblTotalCost, 2, , TriState.True, TriState.True)
      .Item(4, .RowCount - 2).Value = FormatPercent(Int(.Item(3, .RowCount - 2).Value) / Int(.Item(1, .RowCount - 2).Value), 2, , TriState.True, TriState.True)
      .Item(4, .RowCount - 1).Value = FormatPercent(Int(.Item(3, .RowCount - 1).Value) / Int(.Item(1, .RowCount - 2).Value), 2, , TriState.True, TriState.True)
      r = 0
      While Not r = .RowCount - 2
        .Item(2, r).Value = FormatPercent(Int(.Item(1, r).Value) / Int(.Item(1, .RowCount - 2).Value), 2, , TriState.True, TriState.True)
        If .Item(2, r).Value = "NaN" Then .Item(2, r).Value = "0.00%"
        r = r + 1
      End While
    End With
    Grid_Lock(Me, grdNumbers)
  End Sub

  Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
    Dim myExcel As New excel

    myExcel.Excel_Open_File("C:\Everything.xls")
    myExcel.Cell("A1").value = cboReportType.Text

    For intCol As Integer = 0 To grdNumbers.ColumnCount - 1

      myExcel.Cell(myExcel.Excel_Cell(2, intCol)).value = grdNumbers.Columns(intCol).HeaderText & ""
      myExcel.Border(2, intCol, True, True, True, True)
    Next
    For intRow As Integer = 0 To grdNumbers.RowCount - 1
      For intCol As Integer = 0 To grdNumbers.ColumnCount - 1
        myExcel.Cell(myExcel.Excel_Cell(intRow + 3, intCol)).value = grdNumbers.Item(intCol, intRow).Value & ""
        myExcel.Border(intRow + 3, intCol, True, True, True, True)
        myExcel.Cell(myExcel.Excel_Cell(intRow + 3, intCol)).font.size = 9
        myExcel.Cell(myExcel.Excel_Cell(intRow + 3, intCol)).shrinktofit = True

      Next
    Next
    myExcel.FitToPage(1, 1)
    myExcel.Excel_Show()

  End Sub

  Private Sub ToolStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ToolStrip1.ItemClicked

  End Sub

  Private Sub fraCompany_Standard_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fraCompany_Standard.Enter

  End Sub

  Private Sub grdNumbers_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdNumbers.CellContentClick

  End Sub

  Private Sub grdNumbers_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdNumbers.CellContentDoubleClick
    Dim col As Integer
    Dim row As Integer

        ''If cboReportType.Text = "Advertising Analysis" Then
        ''  With frmGraph.chtGraph
        ''    .ColumnCount = grdNumbers.RowCount - 2
        ''    .RowCount = 2
        ''    .chartType = MSChart20Lib.VtChChartType.VtChChartType2dPie
        ''    .ShowLegend = True
        ''    row = 1
        ''    While row <= .RowCount
        ''      .Row = row
        ''      col = 1
        ''      While col <= .ColumnCount
        ''        .Column = col
        ''        .ColumnLabel = grdNumbers.Item(0, col - 1).Value
        ''        If row = 1 Then
        ''          .Data = grdNumbers.Item(1, col - 1).Value
        ''        Else
        ''          .Data = grdNumbers.Item(3, col - 1).Value
        ''        End If
        ''        col = col + 1
        ''      End While
        ''      row = row + 1
        ''    End While

        ''  End With
        ''End If
        ''frmGraph.Show()
  End Sub

  Public Sub Date_Reformat()
    While dtpTo.Value.Day < 20
      dtpTo.Value = DateAdd(DateInterval.Day, -1, dtpTo.Value)
    End While
    While dtpFrom.Value.Day > 8
      dtpFrom.Value = DateAdd(DateInterval.Day, 1, dtpFrom.Value)
    End While
  End Sub
End Class