﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class rptSalesReport
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rptSalesReport))
    Me.grdNumbers = New System.Windows.Forms.DataGridView()
    Me.fraCompany_Standard = New System.Windows.Forms.GroupBox()
    Me.valEstimators = New System.Windows.Forms.ComboBox()
    Me.cmdCompile = New System.Windows.Forms.Button()
    Me.fraReportType = New System.Windows.Forms.GroupBox()
    Me.cboReportType = New System.Windows.Forms.ComboBox()
    Me.lblReportType = New System.Windows.Forms.Label()
    Me.fraShortcuts = New System.Windows.Forms.GroupBox()
    Me.cmdPriorYear = New System.Windows.Forms.Button()
    Me.cmdThisMonth = New System.Windows.Forms.Button()
    Me.cmdLastMonth = New System.Windows.Forms.Button()
    Me.cmdNextMonth = New System.Windows.Forms.Button()
    Me.cmdPriorYTD = New System.Windows.Forms.Button()
    Me.cmdYTD = New System.Windows.Forms.Button()
    Me.fraValues = New System.Windows.Forms.GroupBox()
    Me.cboZip = New System.Windows.Forms.ComboBox()
    Me.lblZip = New System.Windows.Forms.Label()
    Me.cboSource = New System.Windows.Forms.ComboBox()
    Me.lblSource = New System.Windows.Forms.Label()
    Me.cboEstimator = New System.Windows.Forms.ComboBox()
    Me.lblEstimator = New System.Windows.Forms.Label()
    Me.fraDate = New System.Windows.Forms.GroupBox()
    Me.cboYear = New System.Windows.Forms.ComboBox()
    Me.lblYear = New System.Windows.Forms.Label()
    Me.cboMonth = New System.Windows.Forms.ComboBox()
    Me.lblMonth = New System.Windows.Forms.Label()
    Me.lblTo = New System.Windows.Forms.Label()
    Me.dtpTo = New System.Windows.Forms.DateTimePicker()
    Me.lblFrom = New System.Windows.Forms.Label()
    Me.dtpFrom = New System.Windows.Forms.DateTimePicker()
    Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
    Me.mnuPrint = New System.Windows.Forms.ToolStripButton()
    CType(Me.grdNumbers, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.fraCompany_Standard.SuspendLayout()
    Me.fraReportType.SuspendLayout()
    Me.fraShortcuts.SuspendLayout()
    Me.fraValues.SuspendLayout()
    Me.fraDate.SuspendLayout()
    Me.ToolStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'grdNumbers
    '
    Me.grdNumbers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdNumbers.Location = New System.Drawing.Point(15, 174)
    Me.grdNumbers.Name = "grdNumbers"
    Me.grdNumbers.Size = New System.Drawing.Size(928, 356)
    Me.grdNumbers.TabIndex = 0
    '
    'fraCompany_Standard
    '
    Me.fraCompany_Standard.Controls.Add(Me.valEstimators)
    Me.fraCompany_Standard.Controls.Add(Me.cmdCompile)
    Me.fraCompany_Standard.Controls.Add(Me.fraReportType)
    Me.fraCompany_Standard.Controls.Add(Me.fraShortcuts)
    Me.fraCompany_Standard.Controls.Add(Me.fraValues)
    Me.fraCompany_Standard.Controls.Add(Me.fraDate)
    Me.fraCompany_Standard.Location = New System.Drawing.Point(12, 5)
    Me.fraCompany_Standard.Name = "fraCompany_Standard"
    Me.fraCompany_Standard.Size = New System.Drawing.Size(931, 165)
    Me.fraCompany_Standard.TabIndex = 1
    Me.fraCompany_Standard.TabStop = False
    Me.fraCompany_Standard.Text = "Options"
    '
    'valEstimators
    '
    Me.valEstimators.FormattingEnabled = True
    Me.valEstimators.Location = New System.Drawing.Point(280, 122)
    Me.valEstimators.Name = "valEstimators"
    Me.valEstimators.Size = New System.Drawing.Size(200, 21)
    Me.valEstimators.TabIndex = 10
    Me.valEstimators.Visible = False
    '
    'cmdCompile
    '
    Me.cmdCompile.Location = New System.Drawing.Point(735, 115)
    Me.cmdCompile.Name = "cmdCompile"
    Me.cmdCompile.Size = New System.Drawing.Size(93, 38)
    Me.cmdCompile.TabIndex = 9
    Me.cmdCompile.Text = "Compile"
    Me.cmdCompile.UseVisualStyleBackColor = True
    '
    'fraReportType
    '
    Me.fraReportType.Controls.Add(Me.cboReportType)
    Me.fraReportType.Controls.Add(Me.lblReportType)
    Me.fraReportType.Location = New System.Drawing.Point(6, 19)
    Me.fraReportType.Name = "fraReportType"
    Me.fraReportType.Size = New System.Drawing.Size(268, 52)
    Me.fraReportType.TabIndex = 7
    Me.fraReportType.TabStop = False
    Me.fraReportType.Text = "Report Type"
    '
    'cboReportType
    '
    Me.cboReportType.FormattingEnabled = True
    Me.cboReportType.Location = New System.Drawing.Point(62, 18)
    Me.cboReportType.Name = "cboReportType"
    Me.cboReportType.Size = New System.Drawing.Size(200, 21)
    Me.cboReportType.TabIndex = 1
    '
    'lblReportType
    '
    Me.lblReportType.AutoSize = True
    Me.lblReportType.Location = New System.Drawing.Point(6, 21)
    Me.lblReportType.Name = "lblReportType"
    Me.lblReportType.Size = New System.Drawing.Size(44, 13)
    Me.lblReportType.TabIndex = 0
    Me.lblReportType.Text = "Reports"
    '
    'fraShortcuts
    '
    Me.fraShortcuts.Controls.Add(Me.cmdPriorYear)
    Me.fraShortcuts.Controls.Add(Me.cmdThisMonth)
    Me.fraShortcuts.Controls.Add(Me.cmdLastMonth)
    Me.fraShortcuts.Controls.Add(Me.cmdNextMonth)
    Me.fraShortcuts.Controls.Add(Me.cmdPriorYTD)
    Me.fraShortcuts.Controls.Add(Me.cmdYTD)
    Me.fraShortcuts.Location = New System.Drawing.Point(6, 77)
    Me.fraShortcuts.Name = "fraShortcuts"
    Me.fraShortcuts.Size = New System.Drawing.Size(268, 73)
    Me.fraShortcuts.TabIndex = 8
    Me.fraShortcuts.TabStop = False
    Me.fraShortcuts.Text = "Shortcuts"
    '
    'cmdPriorYear
    '
    Me.cmdPriorYear.Location = New System.Drawing.Point(176, 47)
    Me.cmdPriorYear.Name = "cmdPriorYear"
    Me.cmdPriorYear.Size = New System.Drawing.Size(79, 20)
    Me.cmdPriorYear.TabIndex = 5
    Me.cmdPriorYear.Text = "Prior Year Month"
    Me.cmdPriorYear.UseVisualStyleBackColor = True
    '
    'cmdThisMonth
    '
    Me.cmdThisMonth.Location = New System.Drawing.Point(176, 19)
    Me.cmdThisMonth.Name = "cmdThisMonth"
    Me.cmdThisMonth.Size = New System.Drawing.Size(79, 20)
    Me.cmdThisMonth.TabIndex = 4
    Me.cmdThisMonth.Text = "This Month"
    Me.cmdThisMonth.UseVisualStyleBackColor = True
    '
    'cmdLastMonth
    '
    Me.cmdLastMonth.Location = New System.Drawing.Point(6, 19)
    Me.cmdLastMonth.Name = "cmdLastMonth"
    Me.cmdLastMonth.Size = New System.Drawing.Size(79, 20)
    Me.cmdLastMonth.TabIndex = 3
    Me.cmdLastMonth.Text = "Prev Month"
    Me.cmdLastMonth.UseVisualStyleBackColor = True
    '
    'cmdNextMonth
    '
    Me.cmdNextMonth.Location = New System.Drawing.Point(91, 19)
    Me.cmdNextMonth.Name = "cmdNextMonth"
    Me.cmdNextMonth.Size = New System.Drawing.Size(79, 20)
    Me.cmdNextMonth.TabIndex = 2
    Me.cmdNextMonth.Text = "Next Month"
    Me.cmdNextMonth.UseVisualStyleBackColor = True
    '
    'cmdPriorYTD
    '
    Me.cmdPriorYTD.Location = New System.Drawing.Point(91, 47)
    Me.cmdPriorYTD.Name = "cmdPriorYTD"
    Me.cmdPriorYTD.Size = New System.Drawing.Size(79, 20)
    Me.cmdPriorYTD.TabIndex = 1
    Me.cmdPriorYTD.Text = "Prior YTD"
    Me.cmdPriorYTD.UseVisualStyleBackColor = True
    '
    'cmdYTD
    '
    Me.cmdYTD.Location = New System.Drawing.Point(6, 47)
    Me.cmdYTD.Name = "cmdYTD"
    Me.cmdYTD.Size = New System.Drawing.Size(79, 20)
    Me.cmdYTD.TabIndex = 0
    Me.cmdYTD.Text = "YTD"
    Me.cmdYTD.UseVisualStyleBackColor = True
    '
    'fraValues
    '
    Me.fraValues.Controls.Add(Me.cboZip)
    Me.fraValues.Controls.Add(Me.lblZip)
    Me.fraValues.Controls.Add(Me.cboSource)
    Me.fraValues.Controls.Add(Me.lblSource)
    Me.fraValues.Controls.Add(Me.cboEstimator)
    Me.fraValues.Controls.Add(Me.lblEstimator)
    Me.fraValues.Location = New System.Drawing.Point(280, 19)
    Me.fraValues.Name = "fraValues"
    Me.fraValues.Size = New System.Drawing.Size(268, 97)
    Me.fraValues.TabIndex = 6
    Me.fraValues.TabStop = False
    Me.fraValues.Text = "Values"
    '
    'cboZip
    '
    Me.cboZip.FormattingEnabled = True
    Me.cboZip.Location = New System.Drawing.Point(62, 66)
    Me.cboZip.Name = "cboZip"
    Me.cboZip.Size = New System.Drawing.Size(200, 21)
    Me.cboZip.TabIndex = 5
    '
    'lblZip
    '
    Me.lblZip.AutoSize = True
    Me.lblZip.Location = New System.Drawing.Point(6, 69)
    Me.lblZip.Name = "lblZip"
    Me.lblZip.Size = New System.Drawing.Size(22, 13)
    Me.lblZip.TabIndex = 4
    Me.lblZip.Text = "Zip"
    '
    'cboSource
    '
    Me.cboSource.FormattingEnabled = True
    Me.cboSource.Location = New System.Drawing.Point(62, 39)
    Me.cboSource.Name = "cboSource"
    Me.cboSource.Size = New System.Drawing.Size(200, 21)
    Me.cboSource.TabIndex = 3
    '
    'lblSource
    '
    Me.lblSource.AutoSize = True
    Me.lblSource.Location = New System.Drawing.Point(6, 42)
    Me.lblSource.Name = "lblSource"
    Me.lblSource.Size = New System.Drawing.Size(41, 13)
    Me.lblSource.TabIndex = 2
    Me.lblSource.Text = "Source"
    '
    'cboEstimator
    '
    Me.cboEstimator.FormattingEnabled = True
    Me.cboEstimator.Location = New System.Drawing.Point(62, 12)
    Me.cboEstimator.Name = "cboEstimator"
    Me.cboEstimator.Size = New System.Drawing.Size(200, 21)
    Me.cboEstimator.TabIndex = 1
    '
    'lblEstimator
    '
    Me.lblEstimator.AutoSize = True
    Me.lblEstimator.Location = New System.Drawing.Point(6, 15)
    Me.lblEstimator.Name = "lblEstimator"
    Me.lblEstimator.Size = New System.Drawing.Size(50, 13)
    Me.lblEstimator.TabIndex = 0
    Me.lblEstimator.Text = "Estimator"
    '
    'fraDate
    '
    Me.fraDate.Controls.Add(Me.cboYear)
    Me.fraDate.Controls.Add(Me.lblYear)
    Me.fraDate.Controls.Add(Me.cboMonth)
    Me.fraDate.Controls.Add(Me.lblMonth)
    Me.fraDate.Controls.Add(Me.lblTo)
    Me.fraDate.Controls.Add(Me.dtpTo)
    Me.fraDate.Controls.Add(Me.lblFrom)
    Me.fraDate.Controls.Add(Me.dtpFrom)
    Me.fraDate.Location = New System.Drawing.Point(560, 19)
    Me.fraDate.Name = "fraDate"
    Me.fraDate.Size = New System.Drawing.Size(268, 65)
    Me.fraDate.TabIndex = 5
    Me.fraDate.TabStop = False
    Me.fraDate.Text = "Date"
    '
    'cboYear
    '
    Me.cboYear.FormattingEnabled = True
    Me.cboYear.Location = New System.Drawing.Point(170, 12)
    Me.cboYear.Name = "cboYear"
    Me.cboYear.Size = New System.Drawing.Size(92, 21)
    Me.cboYear.TabIndex = 12
    '
    'lblYear
    '
    Me.lblYear.AutoSize = True
    Me.lblYear.Location = New System.Drawing.Point(138, 15)
    Me.lblYear.Name = "lblYear"
    Me.lblYear.Size = New System.Drawing.Size(29, 13)
    Me.lblYear.TabIndex = 11
    Me.lblYear.Text = "Year"
    '
    'cboMonth
    '
    Me.cboMonth.FormattingEnabled = True
    Me.cboMonth.Location = New System.Drawing.Point(40, 11)
    Me.cboMonth.Name = "cboMonth"
    Me.cboMonth.Size = New System.Drawing.Size(92, 21)
    Me.cboMonth.TabIndex = 10
    '
    'lblMonth
    '
    Me.lblMonth.AutoSize = True
    Me.lblMonth.Location = New System.Drawing.Point(6, 15)
    Me.lblMonth.Name = "lblMonth"
    Me.lblMonth.Size = New System.Drawing.Size(37, 13)
    Me.lblMonth.TabIndex = 9
    Me.lblMonth.Text = "Month"
    '
    'lblTo
    '
    Me.lblTo.AutoSize = True
    Me.lblTo.Location = New System.Drawing.Point(138, 39)
    Me.lblTo.Name = "lblTo"
    Me.lblTo.Size = New System.Drawing.Size(20, 13)
    Me.lblTo.TabIndex = 8
    Me.lblTo.Text = "To"
    '
    'dtpTo
    '
    Me.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpTo.Location = New System.Drawing.Point(170, 39)
    Me.dtpTo.Name = "dtpTo"
    Me.dtpTo.Size = New System.Drawing.Size(92, 20)
    Me.dtpTo.TabIndex = 7
    '
    'lblFrom
    '
    Me.lblFrom.AutoSize = True
    Me.lblFrom.Location = New System.Drawing.Point(6, 39)
    Me.lblFrom.Name = "lblFrom"
    Me.lblFrom.Size = New System.Drawing.Size(30, 13)
    Me.lblFrom.TabIndex = 6
    Me.lblFrom.Text = "From"
    '
    'dtpFrom
    '
    Me.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpFrom.Location = New System.Drawing.Point(40, 39)
    Me.dtpFrom.Name = "dtpFrom"
    Me.dtpFrom.Size = New System.Drawing.Size(92, 20)
    Me.dtpFrom.TabIndex = 5
    '
    'ToolStrip1
    '
    Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint})
    Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
    Me.ToolStrip1.Name = "ToolStrip1"
    Me.ToolStrip1.Size = New System.Drawing.Size(964, 25)
    Me.ToolStrip1.TabIndex = 2
    Me.ToolStrip1.Text = "ToolStrip1"
    '
    'mnuPrint
    '
    Me.mnuPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
    Me.mnuPrint.Image = CType(resources.GetObject("mnuPrint.Image"), System.Drawing.Image)
    Me.mnuPrint.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.mnuPrint.Name = "mnuPrint"
    Me.mnuPrint.Size = New System.Drawing.Size(36, 22)
    Me.mnuPrint.Text = "Print"
    '
    'rptSalesReport
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(964, 556)
    Me.Controls.Add(Me.ToolStrip1)
    Me.Controls.Add(Me.fraCompany_Standard)
    Me.Controls.Add(Me.grdNumbers)
    Me.Name = "rptSalesReport"
    Me.Text = "SalesReport"
    CType(Me.grdNumbers, System.ComponentModel.ISupportInitialize).EndInit()
    Me.fraCompany_Standard.ResumeLayout(False)
    Me.fraReportType.ResumeLayout(False)
    Me.fraReportType.PerformLayout()
    Me.fraShortcuts.ResumeLayout(False)
    Me.fraValues.ResumeLayout(False)
    Me.fraValues.PerformLayout()
    Me.fraDate.ResumeLayout(False)
    Me.fraDate.PerformLayout()
    Me.ToolStrip1.ResumeLayout(False)
    Me.ToolStrip1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents grdNumbers As System.Windows.Forms.DataGridView
  Friend WithEvents fraCompany_Standard As System.Windows.Forms.GroupBox
  Friend WithEvents fraDate As System.Windows.Forms.GroupBox
  Friend WithEvents lblTo As System.Windows.Forms.Label
  Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
  Friend WithEvents lblFrom As System.Windows.Forms.Label
  Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
  Friend WithEvents fraValues As System.Windows.Forms.GroupBox
  Friend WithEvents cboEstimator As System.Windows.Forms.ComboBox
  Friend WithEvents cboMonth As System.Windows.Forms.ComboBox
  Friend WithEvents lblMonth As System.Windows.Forms.Label
  Friend WithEvents lblEstimator As System.Windows.Forms.Label
  Friend WithEvents cboYear As System.Windows.Forms.ComboBox
  Friend WithEvents lblYear As System.Windows.Forms.Label
  Friend WithEvents fraShortcuts As System.Windows.Forms.GroupBox
  Friend WithEvents cmdLastMonth As System.Windows.Forms.Button
  Friend WithEvents cmdNextMonth As System.Windows.Forms.Button
  Friend WithEvents cmdPriorYTD As System.Windows.Forms.Button
  Friend WithEvents cmdYTD As System.Windows.Forms.Button
  Friend WithEvents fraReportType As System.Windows.Forms.GroupBox
  Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
  Friend WithEvents lblReportType As System.Windows.Forms.Label
  Friend WithEvents cmdPriorYear As System.Windows.Forms.Button
  Friend WithEvents cmdThisMonth As System.Windows.Forms.Button
  Friend WithEvents cmdCompile As System.Windows.Forms.Button
  Friend WithEvents cboZip As System.Windows.Forms.ComboBox
  Friend WithEvents lblZip As System.Windows.Forms.Label
  Friend WithEvents cboSource As System.Windows.Forms.ComboBox
  Friend WithEvents lblSource As System.Windows.Forms.Label
  Friend WithEvents valEstimators As System.Windows.Forms.ComboBox
  Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
  Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripButton
  Friend WithEvents PrintForm1 As Microsoft.VisualBasic.PowerPacks.Printing.PrintForm
End Class
