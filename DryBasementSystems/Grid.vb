﻿Imports Microsoft.VisualBasic.PowerPacks

Public Class Grid

  Public Sub RowHeight(ByVal Row As Integer, ByVal Height As Integer)
    Dim r As Integer
    Dim c As Integer
    If Row + 1 = Me.Rows Then
      For c = 0 To Me.Cols - 1
        Me.Controls(Row & "," & c).Height = Height
      Next
    End If
    For r = Row + 1 To Me.Rows - 1
      For c = 0 To Me.Cols - 1
        If r = Row + 1 Then
          Me.Controls(Row & "," & c).Height = Height
        End If
        Me.Controls(r & "," & c).Top = Me.Controls(r - 1 & "," & c).Top + Me.Controls(r - 1 & "," & c).Height
      Next
    Next
  End Sub

  Public Sub ColWidth(ByVal Col As Integer, ByVal Width As Integer)
    Dim r As Integer
    Dim c As Integer
    For r = 0 To Me.Rows - 1
      Me.Controls(r & "," & Col).Width = Width
      For c = Col + 1 To Me.Cols - 1
        Me.Controls(r & "," & c).Left = Me.Controls(r & "," & c - 1).Left + Me.Controls(r & "," & c - 1).Width
      Next
    Next

  End Sub

  Public Property Top_Row() As String
    Get
      Return Me.txtTop_Row.text
    End Get
    Set(ByVal value As String)
      Me.txtTop_Row.text = value
    End Set
  End Property

  Public Property Bottom_Row() As String
    Get
      Return Me.txtBottom_Row.text
    End Get
    Set(ByVal value As String)
      Me.txtBottom_Row.text = value
    End Set
  End Property

  Public Property Left_Col() As String
    Get
      Return Me.txtLeft_Col.Text
    End Get
    Set(ByVal value As String)
      Me.txtLeft_Col.Text = value
    End Set
  End Property

  Public Property Right_Col() As String
    Get
      Return Me.txtRight_Col.Text
    End Get
    Set(ByVal value As String)
      Me.txtRight_Col.Text = value
    End Set
  End Property

  Public Property Rows() As String
    Get
      Return Me.txtRow.Text
    End Get
    Set(ByVal value As String)
      Me.txtRow.Text = value
    End Set
  End Property

  Public Property Cols() As String
    Get
      Return Me.txtCol.Text
    End Get
    Set(ByVal value As String)
      Me.txtCol.Text = value
    End Set
  End Property

  Public Property Cell(Optional ByVal Row As Integer = 0, Optional ByVal Col As Integer = 0) As System.Object
    Get
      Dim dummy As GridText
      Dim dummy2 As GridText
      'Debug.Print(Me.ParentForm.Name)
      dummy = Me.Controls(Row & "," & Col)
      If dummy.MergedCell = False Then
        Return Me.Controls(Row & "," & Col).Controls("txtTEXT")
      Else
        dummy = Me.Controls(Row & "," & Col)
        dummy2 = Me.Controls(dummy.MergedOwner.name)
        Return dummy2.Controls("txtTEXT")
      End If
    End Get
    Set(ByVal value As System.Object)
      If value IsNot Nothing Then
        If value.mergedcell = False Then
          Me.Cell = value
        Else
          Me.Cell = Me.Controls(value.mergedowner).controls("txtTEXT")
        End If
      End If
    End Set
  End Property

  Public Property CellBorder(Optional ByVal Row As Integer = 0, Optional ByVal Col As Integer = 0) As System.Object
    Get
      Return Me.Controls(Row & "," & Col)
    End Get
    Set(ByVal value As System.Object)
      Me.Cell = value
    End Set
  End Property

  Public Sub Create(ByVal ColWidth As Integer, ByVal RowHeight As Integer, ByVal sender As System.Object)
    Dim txt As GridText
    Dim control As Control
    flgLoading = True
    Me.Left_Col = 0
    Me.Right_Col = Int(Me.Width / ColWidth)
    If Me.Right_Col > Me.Cols - 1 Then Me.Right_Col = Me.Cols - 1
    Me.Top_Row = 0
    Me.Bottom_Row = Int(Me.Height / RowHeight)
    If Me.Bottom_Row > Me.Rows - 1 Then Me.Bottom_Row = Me.Rows - 1    
    With Me
      For Each control In .Controls
        .Controls.Remove(control)
        control.Dispose()
      Next
      Me.Controls.Clear()
      For r As Integer = 0 To Me.Rows - 1
        For c As Integer = 0 To Me.Cols - 1
          txt = New GridText
          txt.Name = r & "," & c
          txt.Row = r
          txt.Col = c
          txt.Width = ColWidth
          txt.Height = RowHeight
          txt.txtText.Width = ColWidth - 3
          txt.txtText.Height = RowHeight - 3
          txt.Top = (RowHeight * r)
          txt.Left = (ColWidth * c)
          txt.Tag = txt.Left
          txt.Visible = True
          'txt.Tag = "SKIP"
          Me.Controls.Add(txt)
        Next
      Next
    End With
    
    flgLoading = False

  End Sub

  Public Sub Merge(ByVal Cell1 As String, ByVal Cell2 As String)
    Dim StartCell As System.Object
    Dim EndCell As System.Object
    Dim r As Double
    Dim c As Double

    StartCell = Me.Controls(Cell1)
    EndCell = Me.Controls(Cell2)

    StartCell.width = (EndCell.left - StartCell.left + EndCell.width)
    StartCell.height = (EndCell.top - StartCell.top + EndCell.height)

    For r = StartCell.row To EndCell.row
      For c = StartCell.col To EndCell.col
        Me.Cell(r, c).parent.MergedOwner = StartCell
        Me.Cell(r, c).parent.mergedcell = True
      Next
    Next
  End Sub
  Public Sub ApplyRowFormat(ByVal Row As Integer, ByVal Col As Integer)
    Dim constant As System.Object
    constant = Me.Cell(Row, Col)
    With constant
      For c As Integer = Col To Me.Cols - 1
        Me.Cell(Row, c).backcolor = .backcolor
        Me.Cell(Row, c).ForeColor = .forecolor
        Me.Cell(Row, c).TextAlign = .TextAlign
        Me.CellBorder(Row, c).topborder = Me.CellBorder(Row, Col).topborder
        Me.CellBorder(Row, c).Bottomborder = Me.CellBorder(Row, Col).Bottomborder
        Me.CellBorder(Row, c).Leftborder = Me.CellBorder(Row, Col).Leftborder
        Me.CellBorder(Row, c).Rightborder = Me.CellBorder(Row, Col).Rightborder
      Next
    End With
  End Sub

  Public Sub ApplyColFormat(ByVal Row As Integer, ByVal Col As Integer)
    Dim constant As System.Object
    constant = Me.Cell(Row, Col)
    With constant
      For r As Integer = Row To Me.Rows - 1
        Me.Cell(r, Col).backcolor = .backcolor
        Me.Cell(r, Col).ForeColor = .forecolor
        Me.Cell(r, Col).TextAlign = .TextAlign

        Me.CellBorder(r, Col).topborder = Me.CellBorder(Row, Col).topborder
        Me.CellBorder(r, Col).Bottomborder = Me.CellBorder(Row, Col).Bottomborder
        Me.CellBorder(r, Col).Leftborder = Me.CellBorder(Row, Col).Leftborder
        Me.CellBorder(r, Col).Rightborder = Me.CellBorder(Row, Col).Rightborder
      Next
    End With
  End Sub

  Public Sub Clear(ByVal Row As Integer, ByVal Col As Integer)
    On Error Resume Next
    For r As Integer = Row To Me.Rows - 1
      For c As Integer = Col To Me.Cols - 1
        Me.Cell(r, c).text = ""
        Me.Cell(r, c).backcolor = SystemColors.Window
        Me.Cell(r, c).forecolor = Color.Black
        Me.Controls(r & "," & c).BackColor = Color.Transparent
        Me.Cell(r, c).font = New System.Drawing.Font("MS Seriff", 6, FontStyle.Regular)
        

      Next
    Next
  End Sub

  Public Shared Sub Cell_DblClick(ByVal sender As Object, ByVal Owner As Object, ByVal CurrentRow As Double, ByVal CurrentCol As Double)
    RaiseEvent CellDoubleClick(sender, Owner, CurrentRow, CurrentCol)
  End Sub

  Public Shared Event CellDoubleClick(ByVal sender As Object, ByVal Owner As Object, ByVal CurrentRow As Double, ByVal CurrentCol As Double)

  Private Sub Grid_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed

  End Sub

  Private Sub Grid_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles Me.Scroll

  End Sub

  Public Function Valid_Cell(ByVal sender As Object)
    If Me.ParentForm Is Nothing Then
      Return False
      Exit Function
    ElseIf sender.parent.parentform.name <> Me.ParentForm.Name Then
      Return False
      Exit Function
    End If
    Return True
  End Function

  Private Sub Grid_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
  End Sub

  Private Sub Grid_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    Dim dblRow As Double
    Dim dblCol As Double
    If flgLoading = True Then Exit Sub
    Me.VerticalScroll.Value = 0
    Me.HorizontalScroll.Value = 0
    If IsDBNull(Me.Cols) = True Or Me.Cols = "" Then Exit Sub
    For dblCol = 0 To Me.Cols - 1
      If Me.Controls("0," & dblCol).Left < Me.Left + Me.Width Then
        Me.Right_Col = dblCol
      End If
    Next
    For dblRow = 0 To Me.Rows - 1
      If Me.Controls(dblRow & ",0").Top < Me.Top + Me.Height Then
        Me.Bottom_Row = dblRow
      End If
    Next
    For dblRow = 0 To Me.Bottom_Row
      For dblCol = 0 To Me.Right_Col
        Me.Controls(dblRow & "," & dblCol).Visible = True
      Next
    Next

  End Sub
End Class
