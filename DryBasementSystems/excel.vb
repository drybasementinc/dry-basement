﻿Imports Microsoft.Office.Interop.Excel

Public Class excel
  Dim xlWorkBook As Workbook
  Dim xlWorkSheet As Worksheet
  Dim xlApplication = New Microsoft.Office.Interop.Excel.Application

  Sub Create_File(ByVal File_Name As String)
    xlWorkBook = xlApplication.workbooks.add
    xlWorkSheet = xlWorkBook.Sheets.Item(1)
    Try
      xlWorkSheet.SaveAs(File_Name)
    Catch ex As Exception
      MsgBox("Unable to open associated file.")
    End Try
  End Sub

  Sub Excel_Open_File(ByVal File_Name As String)
    Try
      xlWorkBook = xlApplication.workbooks.open(File_Name)

      xlWorkSheet = xlWorkBook.Sheets.Item(1)
    Catch ex As Exception
      MsgBox("Unable to locate the file associated with this form. Please consult an administrator.")
    End Try

  End Sub

  Sub Excel_Close_File(Optional ByVal Save As Boolean = False)
    Try
      xlWorkBook.Close(Save)
      xlApplication.quit()
      xlWorkSheet = Nothing
      xlWorkBook = Nothing
      System.GC.Collect()
    Catch ex As Exception
      'do nothing
    End Try
  End Sub

  Function Excel_Cell(ByVal Row As Integer, ByVal Column As Integer) As String
    Dim intLeadLetter As Integer = 0
    Dim strCell As String = ""

    While Column > 25
      intLeadLetter += 1
      Column -= 26
    End While
    If intLeadLetter > 0 Then
      strCell = Chr(64 + intLeadLetter)
    End If
    strCell += Chr(65 + Column) & Row
    Return strCell
  End Function

  Sub Excel_Show(Optional ByVal Release As Boolean = True)
    xlWorkSheet.Visible = True
    xlApplication.visible = True
    If Release = False Then Exit Sub
    xlWorkSheet = Nothing
    xlWorkSheet = Nothing
    xlApplication = Nothing
  End Sub

  Function Cell(ByVal myCell As String)
    Try
      Return xlWorkSheet.Range(myCell, myCell)
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  Function Cell(ByVal row As Integer, ByVal col As Integer)
    Try
      Return xlWorkSheet.Range(Excel_Cell(row, col), Excel_Cell(row, col))
    Catch ex As Exception
      Return Nothing
    End Try
    Return Nothing
  End Function

  Sub Border(ByVal myCell As String, Optional ByVal Top As Boolean = False, Optional ByVal Right As Boolean = False, Optional ByVal Bottom As Boolean = False, Optional ByVal Left As Boolean = False)
    If Top = True Then
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeTop).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous
    Else
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeTop).LineStyle = Nothing
    End If
    If Right = True Then
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeRight).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous
    Else
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeRight).LineStyle = Nothing
    End If
    If Bottom = True Then
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeBottom).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous
    Else
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeBottom).LineStyle = Nothing
    End If
    If Left = True Then
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeLeft).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous
    Else
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeLeft).LineStyle = Nothing
    End If
  End Sub

  Sub Border(ByVal row As Integer, ByVal col As Integer, Optional ByVal Top As Boolean = False, Optional ByVal Right As Boolean = False, Optional ByVal Bottom As Boolean = False, Optional ByVal Left As Boolean = False)
    Dim myCell As String = Excel_Cell(row, col)
    If Top = True Then
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeTop).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous
    Else
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeTop).LineStyle = Nothing
    End If
    If Right = True Then
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeRight).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous
    Else
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeRight).LineStyle = Nothing
    End If
    If Bottom = True Then
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeBottom).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous
    Else
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeBottom).LineStyle = Nothing
    End If
    If Left = True Then
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeLeft).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous
    Else
      xlWorkSheet.Range(myCell, myCell).Borders(XlBordersIndex.xlEdgeLeft).LineStyle = Nothing
    End If
  End Sub

  Sub FitToPage(ByVal Wide As Integer, ByVal Tall As Integer)
    xlWorkSheet.PageSetup.BottomMargin = 0
    xlWorkSheet.PageSetup.TopMargin = 0
    xlWorkSheet.PageSetup.LeftFooter = 0
    xlWorkSheet.PageSetup.RightFooter = 0
    xlWorkSheet.PageSetup.Orientation = XlPageOrientation.xlLandscape
    xlWorkSheet.PageSetup.FitToPagesWide = Wide
    xlWorkSheet.PageSetup.FitToPagesTall = Tall
  End Sub

  Sub Print(ByVal printer As String, ByVal preview As Boolean)
    xlWorkSheet.PrintOut(, , , preview, printer)
  End Sub

  Sub SaveAs(ByVal path As String)
    xlWorkBook.SaveAs(path)
  End Sub
End Class
