﻿Public Class frmLead
    'General Declarations
    Dim Address As New DataTable
    Dim Clients As New DataTable
    Dim General As New DataTable
    Dim DryDispo As New DataTable
    Dim MagicDispo As New DataTable
    Dim DryProduction As New DataTable
    Dim MagicProduction As New DataTable
    Dim Est_Schedule As New DataTable
    Dim CallLog As New DataTable

    Dim drGeneral As Double
    Dim drAddress As Double
    Dim drClients As Double
    Dim drDryDispo As Double
    Dim drMagicDispo As Double
    Dim drDryProduction As Double
    Dim drMagicProduction As Double
    Dim drEst_Schedule As Double
    Dim drCallLog As Double

    Dim i As Integer
    Dim r As Integer
    Dim c As Integer

    'Form Specific Declarations
    Dim Est_Info As New DataTable
    Dim Annual_Info As New DataTable
    Dim Always_Info As New DataTable
    Dim Service As New DataTable
    Dim drEst_Info As Double
    Dim drAnnual_Info As Double
    Dim drAlways_Info As Double
    Dim drService As Double
    Public valNew As Boolean = False
    Dim flgApptSet As Boolean = False

    Private Sub Lead_Post_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

    End Sub

    Private Sub frmLead_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Connect("Foundation_Types", General, drGeneral, , "Type")
        drGeneral = 0
        cboFoundation.Items.Clear()
        While Not drGeneral > General.Rows.Count - 1
            cboFoundation.Items.Add(General.Rows(drGeneral).Item("Type"))
            drGeneral = drGeneral + 1
        End While

        Connect("Source", General, drGeneral, "Company = 'Dry'", "Source")
        drGeneral = 0
        cboSource.Items.Clear()
        While Not drGeneral > General.Rows.Count - 1
            cboSource.Items.Add(General.Rows(drGeneral).Item("Source"))
            drGeneral = drGeneral + 1
        End While

        Connect("Address_Directions", General, drGeneral)
        drGeneral = 0
        cboAddress_Direction.Items.Clear()
        While Not drGeneral > General.Rows.Count - 1
            cboAddress_Direction.Items.Add(General.Rows(drGeneral).Item("Direction"))
            drGeneral = drGeneral + 1
        End While

        Connect("Address_Endings", General, drGeneral)
        drGeneral = 0
        cboAddress_Ending.Items.Clear()
        While Not drGeneral > General.Rows.Count - 1
            cboAddress_Ending.Items.Add(General.Rows(drGeneral).Item("Endings"))
            drGeneral = drGeneral + 1
        End While

        'grdDispos
        grdDispo.RowCount = 1
        grdDispo.ColumnCount = 4
        grdDispo.Columns(0).HeaderText = "APPT DATE"
        grdDispo.Columns(1).HeaderText = "COMPANY"
        grdDispo.Columns(2).HeaderText = "ESTIMATOR"
        grdDispo.Columns(3).HeaderText = "DISPO"
        grdDispo.Columns(3).Width = 500
        Grid_Lock(Me, grdDispo, , True)

        'grdProduction
        grdProduction.RowCount = 1
        grdProduction.ColumnCount = 3
        grdProduction.Columns(0).HeaderText = "START DATE"
        grdProduction.Columns(1).HeaderText = "COMPANY"
        grdProduction.Columns(2).HeaderText = "INSTALLED"
        grdProduction.Columns(2).Width = 500
        Grid_Lock(Me, grdProduction, , True)

        'grdEstimator
        grdEst_Appts.RowCount = 0
        grdEst_Appts.ColumnCount = 8
        For c As Integer = 0 To grdEst_Appts.ColumnCount - 1
            grdEst_Appts.Columns(c).Width = 100
        Next
        grdEst_Appts.Columns(0).HeaderText = "DATE"
        grdEst_Appts.Columns(1).HeaderText = "TIME"
        grdEst_Appts.Columns(2).HeaderText = "PERSON"
        grdEst_Appts.Columns(3).HeaderText = "TYPE"
        grdEst_Appts.Columns(4).HeaderText = "SET DATE"
        grdEst_Appts.Columns(5).HeaderText = "CONFIRMED"
        grdEst_Appts.Columns(6).HeaderText = "BONUS"
        grdEst_Appts.Columns(7).HeaderText = "DISPO DONE"

        'grdAnnual
        grdAnnual.RowCount = 0
        grdAnnual.ColumnCount = 4
        For c As Integer = 0 To grdAnnual.ColumnCount - 1
            grdAnnual.Columns(c).Width = 100
        Next
        grdAnnual.Columns(0).HeaderText = "DATE"
        grdAnnual.Columns(1).HeaderText = "TIME"
        grdAnnual.Columns(2).HeaderText = "WORK DONE"
        grdAnnual.Columns(3).HeaderText = "SET DATE"

        'grdService
        grdService.RowCount = 0
        grdService.ColumnCount = 5
        For c As Integer = 0 To grdService.ColumnCount - 1
            grdService.Columns(c).Width = 100
        Next
        grdService.Columns(0).HeaderText = "DATE"
        grdService.Columns(1).HeaderText = "TIME"
        grdService.Columns(2).HeaderText = "PERSON"
        grdService.Columns(3).HeaderText = "TYPE"
        grdService.Columns(4).HeaderText = "SET DATE"

        'grdAlways
        grdAlways.RowCount = 0
        grdAlways.ColumnCount = 4
        For c As Integer = 0 To grdAlways.ColumnCount - 1
            grdAlways.Columns(c).Width = 100
        Next
        grdAlways.Columns(0).HeaderText = "DATE"
        grdAlways.Columns(1).HeaderText = "TIME"
        grdAlways.Columns(2).HeaderText = "PERSON"
        grdAlways.Columns(3).HeaderText = "SET DATE"

        'grdCallLog
        grdCallLog.RowCount = 0
        grdCallLog.ColumnCount = 4
        For c As Integer = 0 To grdCallLog.ColumnCount - 1
            grdCallLog.Columns(c).Width = 100
        Next
        grdCallLog.Columns(0).HeaderText = "DATE"
        grdCallLog.Columns(0).HeaderCell.Tag = "DATE"
        grdCallLog.Columns(1).HeaderText = "PERSON"
        grdCallLog.Columns(1).HeaderCell.Tag = "PERSON"
        grdCallLog.Columns(2).HeaderText = "TYPE"
        grdCallLog.Columns(2).HeaderCell.Tag = "TYPE"
        grdCallLog.Columns(3).HeaderText = "NOTES"
        grdCallLog.Columns(3).HeaderCell.Tag = "NOTES"

        If valNew = True Then
            tabPost.Visible = False
            cmdSearch.Visible = True
            cmdSave.Visible = False
            Exit Sub
        Else
            Connect("Addresses", Address, drAddress, "ID = " & dblAddressID & "")
            drAddress = 0
            Connect("Clients", Clients, drClients, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID") & "")
            drClients = 0
            dblClientID = Clients.Rows(drClients).Item("ID")
            Post_Load()
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If flgDispo_Add = True Then
            Manual_Update("UPDATE", "Addresses", "Current_Appt_Date = " & frmDispo.valAppt_Date.Text & ", Estimator = " & frmDispo.valEstimator.Text & ", Appt_Time = #1:00 AM#, Appt_Type = 'PUP'", "ID = " & dblAddressID & "")
            flgDispo_Add = False
            Me.Close()
            Exit Sub
        End If

        If txtPrice.Text = "" Then txtPrice.Text = "0"
        If txtPump_Count.Text = "" Then txtPump_Count.Text = "0"
        'frmStart
        Compiler("UPDATE", fraStart, "Addresses", "Clients")
        TableUpdate("UPDATE", "Addresses", strSaveSQL1, strValues1, Me, "ID = " & Address.Rows(drAddress).Item("ID"), Address)
        If flgFailed = True Then Exit Sub
        TableUpdate("UPDATE", "Clients", strSaveSQL2, strValues2, Me, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID"), Clients)
        If flgFailed = True Then Exit Sub
        'tpgInfo
        Compiler("UPDATE", tpgInfo, "Addresses", "Clients")
        TableUpdate("UPDATE", "Addresses", strSaveSQL1, strValues1, Me, "ID = " & Address.Rows(drAddress).Item("ID"), Address)
        If flgFailed = True Then Exit Sub
        TableUpdate("UPDATE", "Clients", strSaveSQL2, strValues2, Me, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID"), Clients)
        If flgFailed = True Then Exit Sub
        'tpgEstimate
        Compiler("UPDATE", tpgEstimate, "Est_Info")
        TableUpdate("UPDATE", "Est_Info", strSaveSQL1, strValues1, Me, "Address_ID = " & Address.Rows(drAddress).Item("ID"))
        If flgFailed = True Then Exit Sub
        'tpgAnnual
        Compiler("UPDATE", tpgAnnual, "Annual")
        TableUpdate("UPDATE", "Annual_Info", strSaveSQL1, strValues1, Me, "Address_ID = " & Address.Rows(drAddress).Item("ID"))
        If flgFailed = True Then Exit Sub
        'tpgAlways
        Compiler("UPDATE", tpgAlways, "Always")
        TableUpdate("UPDATE", "Always_Info", strSaveSQL1, strValues1, Me, "Address_ID = " & Address.Rows(drAddress).Item("ID"))
        If flgFailed = True Then Exit Sub
        'flgDirty = False
        MsgBox("Record Saved")

    End Sub

    Private Sub cmdDispoView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmDispo_Post.Show()
    End Sub

    Private Sub valAppt_Type_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles valAppt_Type.TextChanged
        If valAppt_Type.Text <> "NORMAL" Then
            cmdSchedule.Text = valAppt_Type.Text & " " & valPerson.Text & " " & valDate.Text & " " & valStart_Time.Text
        Else
            cmdSchedule.Text = valPerson.Text & " " & valDate.Text & " " & valStart_Time.Text
        End If
    End Sub

    Private Sub valEstimator_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles valPerson.TextChanged
        If valAppt_Type.Text <> "NORMAL" Then
            cmdSchedule.Text = valAppt_Type.Text & " " & valPerson.Text & " " & valDate.Text & " " & valStart_Time.Text
        Else
            cmdSchedule.Text = valPerson.Text & " " & valDate.Text & " " & valStart_Time.Text
        End If
    End Sub

    Private Sub valAppt_Time_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles valStart_Time.TextChanged
        If valAppt_Type.Text <> "NORMAL" Then
            cmdSchedule.Text = valAppt_Type.Text & " " & valPerson.Text & " " & valDate.Text & " " & valStart_Time.Text
        Else
            cmdSchedule.Text = valPerson.Text & " " & valDate.Text & " " & valStart_Time.Text
        End If
    End Sub

    Private Sub txtCurrent_Appt_Date_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles valDate.TextChanged
        If valAppt_Type.Text <> "NORMAL" Then
            cmdSchedule.Text = valAppt_Type.Text & " " & valPerson.Text & " " & valDate.Text & " " & valStart_Time.Text
        Else
            cmdSchedule.Text = valPerson.Text & " " & valDate.Text & " " & valStart_Time.Text
        End If
    End Sub

    Private Sub cmdAppt_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        strResponse = ""
        strResponse = MsgBox("Do you want to cancel this appointment?", MsgBoxStyle.YesNo)
        If strResponse = "" Or strResponse = vbNo Then Exit Sub
        TableUpdate("DELETE", "Estimators_Schedule", "", "", Me, "Address_ID = " & dblAddressID & " AND Date = " & FilterByDate(Date.Parse(valDate.Text)))
        valStart_Time.Text = "1:00 AM"
        valAppt_Type.Text = ""
        valDate.Text = "1/1/1901"
        TableUpdate("UPDATE", "Addresses", "Current_Appt_Date, Appt_Time, Appt_Type,", "@valCurrent_Appt_Date, @valAppt_Time, @valAppt_Type,", Me, "ID = " & dblAddressID, Address)
    End Sub

    Private Sub cmdConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Manual_Update("UPDATE", "Addresses", "Confirmed = True,", "ID = " & dblAddressID & "")
        Manual_Update("UPDATE", "Estimators_Schedule", "Bold = True,", "Address_ID = " & dblAddressID & "")
    End Sub

    Private Sub grdDispo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmdProductionView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmProduction.Show()
    End Sub

    Private Sub txtDirections_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDirections.LostFocus
        Dim intTime As Integer
        Dim intKeeper As Integer

        If Len(txtDirections.Text) < 20 Then Exit Sub

        While txtDirections.Text.Contains("  ") = True
            txtDirections.Text = Replace(txtDirections.Text, "  ", " ")
        End While
        While txtDirections.Text.Contains((Chr(13) + Chr(10)) & " " & (Chr(13) + Chr(10))) = True
            txtDirections.Text = Replace(txtDirections.Text, (Chr(13) + Chr(10)) & " " & (Chr(13) + Chr(10)), (Chr(13) + Chr(10)))
        End While

        While txtDirections.Text.Contains((Chr(13) + Chr(10)) & (Chr(13) + Chr(10))) = True
            txtDirections.Text = Replace(txtDirections.Text, (Chr(13) + Chr(10)) & " " & (Chr(13) + Chr(10)), (Chr(13) + Chr(10)))
        End While

        intKeeper = InStr(UCase(txtDirections.Text), "MILES", CompareMethod.Text) - 1
        While txtDirections.SelectedText <> " "
            intKeeper = intKeeper - 1
            txtDirections.Select(intKeeper - 1, 1)
        End While
        txtDirections.Select(intKeeper + 1, 4)
        txtDirections.Select(intKeeper, (InStr(UCase(txtDirections.Text), "MILES", CompareMethod.Text)) - (intKeeper + 2))
        txtMiles.Text = txtDirections.SelectedText

        intTime = 0
        intKeeper = InStr(UCase(txtDirections.Text), "HOUR", CompareMethod.Text) - 1
        If intKeeper > 0 Then
            While txtDirections.SelectedText <> " "
                intKeeper = intKeeper - 1
                txtDirections.Select(intKeeper - 1, 1)
            End While
            txtDirections.Select(intKeeper + 1, 4)
            txtDirections.Select(intKeeper, (InStr(UCase(txtDirections.Text), "HOUR", CompareMethod.Text)) - (intKeeper + 2))
            intTime = txtDirections.SelectedText * 60
        End If

        intKeeper = InStr(UCase(txtDirections.Text), "MINUTE", CompareMethod.Text) - 1
        If intKeeper > 0 Then
            While txtDirections.SelectedText <> " "
                intKeeper = intKeeper - 1
                txtDirections.Select(intKeeper - 1, 1)
            End While
            txtDirections.Select(intKeeper + 1, 4)
            txtDirections.Select(intKeeper, (InStr(UCase(txtDirections.Text), "MINUTE", CompareMethod.Text)) - (intKeeper + 2))
            intTime = intTime + txtDirections.SelectedText
        End If
        txtDriveTime.Text = intTime


        Select Case intTime
            Case Is < 46
                cboZone.Text = "0"
            Case Is < 61
                cboZone.Text = "1"
            Case Is < 76
                cboZone.Text = "2"
            Case Is > 75
                cboZone.Text = "3"

        End Select
        Exit Sub
        txtDirections.SelectionStart = InStr(txtDirections.Text, "Estimated Time:") + 15
        txtDirections.SelectionLength = InStr(txtDirections.Text, "Estimated Distance:") - (txtDirections.SelectionStart + 1)
        txtDriveTime.Text = (Trim(txtDirections.SelectedText))
        txtDirections.SelectionStart = txtDirections.SelectionStart + txtDirections.SelectionLength + 20
        txtDirections.SelectionLength = Len(txtDirections.Text) - txtDirections.SelectionStart
        txtMiles.Text = (Trim(txtDirections.SelectedText))
        txtMiles.SelectionStart = 0
        txtMiles.SelectionLength = InStr(txtMiles.Text, "miles") + 4
        txtMiles.Text = txtMiles.SelectedText
        intTime = 0
        If InStr(txtDriveTime.Text, "hour") > 0 Then
            txtDriveTime.Select(0, InStr(txtDriveTime.Text, "hour") - 1)
            intTime = 60 * Val(txtDriveTime.SelectedText)
        End If
        If InStr(txtDriveTime.Text, "minute") > 0 Then
            If InStr(txtDriveTime.Text, "hour") > 0 Then
                txtDriveTime.Select(InStr(txtDriveTime.Text, "hour") + 3, InStr(txtDriveTime.Text, "minute") - 1)
                intTime = intTime + Val(txtDriveTime.SelectedText)
            Else
                txtDriveTime.Select(0, InStr(txtDriveTime.Text, "minute") - 1)
                intTime = intTime + Val(txtDriveTime.SelectedText)
            End If
        End If

        Select Case intTime
            Case Is < 46
                cboZone.Text = "0"
            Case Is < 61
                cboZone.Text = "1"
            Case Is < 76
                cboZone.Text = "2"
            Case Is > 75
                cboZone.Text = "3"

        End Select
    End Sub

    Private Sub txtDirections_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmdSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If Trim(txtCompany_Name.Text & txtFirst_Name.Text & txtLast_Name.Text & txtFirst_Name2.Text & txtLast_Name2.Text) = "" Then
            MsgBox("Please input a name before moving forward.")
            Exit Sub
        End If
        If Trim(txtAddress.Text) = "" Or cboCity.Text = "" Or cboState.Text = "" Or Trim(txtZip.Text) = "" Then
            MsgBox("Please input an address before moving forward.")
            Exit Sub
        End If

        Connect("Addresses", Address, drAddress, "Address_Numbers = '" & txtAddress_Numbers.Text & "' AND Address_Direction = '" & cboAddress_Direction.Text & "' AND Address = '" & txtAddress.Text & "' And Address_Ending = '" & cboAddress_Ending.Text & "' AND City = '" & cboCity.Text & "' AND Zip = " & txtZip.Text & "")
        drAddress = 0
        If Address.Rows.Count = 1 Then
            'simply move on to finding the client
        Else
            Connect("Addresses", Address, drAddress, "Address = '" & txtAddress.Text & "' AND Zip = " & txtZip.Text & "")
            drAddress = 0
            If Address.Rows.Count > 0 Then
                With frmMultiAddress.grdMulti
                    .ColumnCount = 1
                    .Columns(0).Width = 500
                    .Columns(0).HeaderCell.Value = "Information"
                    .Columns(0).HeaderCell.Style.BackColor = Color.LightGray
                    .Columns(0).HeaderCell.Style.Alignment = HorizontalAlignment.Center
                    .Columns(0).HeaderCell.Style.Font = New Font("MS Seriff", 12, FontStyle.Regular)

                    .RowCount = Address.Rows.Count
                    For i As Integer = 0 To Address.Rows.Count - 1
                        .Item(0, i).Style.Alignment = HorizontalAlignment.Center
                        .Item(0, i).Tag = Address.Rows(i).Item("ID")
                        Connect("Clients", Clients, drClients, "ID = " & Address.Rows(i).Item("Current_Client_ID"))
                        .Item(0, i).Style.WrapMode = DataGridViewTriState.True
                        .Rows(i).Height = 30

                        Try
                            .Item(0, i).Value = Clients.Rows(drClients).Item("First_Name") & " " & Clients.Rows(drClients).Item("Last_Name") & (Chr(13) + Chr(10)) & Address.Rows(i).Item("Address_Numbers") & " " & Address.Rows(i).Item("Address_Direction") & " " & Address.Rows(i).Item("Address") & " " & Address.Rows(i).Item("Address_Ending")
                        Catch ex As Exception
                            .Item(0, i).Value = "NONE" & (Chr(13) + Chr(10)) & Address.Rows(i).Item("Address_Numbers") & " " & Address.Rows(i).Item("Address_Direction") & " " & Address.Rows(i).Item("Address") & " " & Address.Rows(i).Item("Address_Ending")
                        End Try
                    Next
                End With
                frmMultiAddress.ShowDialog()
                If frmMultiAddress.dblMulti_ID > 0 Then
                    Connect("Addresses", Address, drAddress, "ID = " & frmMultiAddress.dblMulti_ID & "")
                    drAddress = 0
                Else
                    Manual_Update("INSERT INTO", "Addresses", "(Address_Numbers, Address_Direction, Address, Address_Ending, City, State, Zip) VALUES ('" & txtAddress_Numbers.Text & "', '" & cboAddress_Direction.Text & "', '" & txtAddress.Text & "', '" & cboAddress_Ending.Text & "', '" & cboCity.Text & "','" & cboState.Text & "', " & txtZip.Text & ")")
                    Connect("Addresses", Address, drAddress, "Address_Numbers = '" & txtAddress_Numbers.Text & "' AND Address_Direction = '" & cboAddress_Direction.Text & "' AND Address = '" & txtAddress.Text & "' And Address_Ending = '" & cboAddress_Ending.Text & "' AND City = '" & cboCity.Text & "' AND Zip = " & txtZip.Text & "")
                    drAddress = 0
                    Manual_Update("INSERT INTO", "Est_Info", "(Address_ID) VALUES(" & Address.Rows(drAddress).Item("ID") & ")")
                    Manual_Update("INSERT INTO", "Annual_Info", "(Address_ID) VALUES(" & Address.Rows(drAddress).Item("ID") & ")")
                    Manual_Update("INSERT INTO", "Always_Info", "(Address_ID) VALUES(" & Address.Rows(drAddress).Item("ID") & ")")
                    Manual_Update("INSERT INTO", "Call_Log", "(Address_ID) VALUES(" & Address.Rows(drAddress).Item("ID") & ")")
                End If
            Else
                Manual_Update("INSERT INTO", "Addresses", "(Address_Numbers, Address_Direction, Address, Address_Ending, City, State, Zip) VALUES ('" & txtAddress_Numbers.Text & "', '" & cboAddress_Direction.Text & "', '" & txtAddress.Text & "', '" & cboAddress_Ending.Text & "', '" & cboCity.Text & "','" & cboState.Text & "', " & txtZip.Text & ")")
                Connect("Addresses", Address, drAddress, "Address_Numbers = '" & txtAddress_Numbers.Text & "' AND Address_Direction = '" & cboAddress_Direction.Text & "' AND Address = '" & txtAddress.Text & "' And Address_Ending = '" & cboAddress_Ending.Text & "' AND City = '" & cboCity.Text & "' AND Zip = " & txtZip.Text & "")
                drAddress = 0
                Manual_Update("INSERT INTO", "Est_Info", "(Address_ID) VALUES(" & Address.Rows(drAddress).Item("ID") & ")")
                Manual_Update("INSERT INTO", "Annual_Info", "(Address_ID) VALUES(" & Address.Rows(drAddress).Item("ID") & ")")
                Manual_Update("INSERT INTO", "Always_Info", "(Address_ID) VALUES(" & Address.Rows(drAddress).Item("ID") & ")")
                Manual_Update("INSERT INTO", "Call_Log", "(Address_ID) VALUES(" & Address.Rows(drAddress).Item("ID") & ")")
            End If
        End If

        Connect("Clients", Clients, drClients, "Company_Name Like '%" & txtCompany_Name.Text & "%' And First_Name Like '%" & txtFirst_Name.Text & "%' And Last_Name Like '%" & txtLast_Name.Text & "%' And First_Name2 Like '%" & txtFirst_Name2.Text & "%' And Last_Name2 Like '%" & txtLast_Name2.Text & "%'")
        drClients = 0
        If Clients.Rows.Count = 1 Then
            If Clients.Rows(drClients).Item("ID") = Address.Rows(drAddress).Item("Current_Client_ID") & "" Then
                'all done
            Else
                strResponse = MsgBox("The caller's information does not match the information for this address. Are they the new owners?", vbYesNo)
                If strResponse = vbNo Then
                    Connect("Clients", Clients, drClients, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID") & "")
                    drClients = 0
                Else
                    With frmMultiAddress.grdMulti
                        .ColumnCount = 1
                        .Columns(0).Width = 500
                        .Columns(0).HeaderCell.Value = "Information"
                        .Columns(0).HeaderCell.Style.BackColor = Color.LightGray
                        .Columns(0).HeaderCell.Style.Alignment = HorizontalAlignment.Center
                        .Columns(0).HeaderCell.Style.Font = New Font("MS Seriff", 12, FontStyle.Regular)
                        If Clients.Rows.Count > 0 Then
                            .RowCount = Clients.Rows.Count
                            For i As Integer = 0 To Clients.Rows.Count - 1
                                .Item(0, i).Style.Alignment = HorizontalAlignment.Center
                                .Item(0, i).Tag = Clients.Rows(i).Item("ID")
                                .Item(0, i).Style.WrapMode = DataGridViewTriState.True
                                .Rows(i).Height = 30

                                Try
                                    .Item(0, i).Value = Clients.Rows(i).Item("First_Name") & " " & Clients.Rows(i).Item("Last_Name") & (Chr(13) + Chr(10)) & Clients.Rows(i).Item("Last_Address")
                                Catch ex As Exception
                                    .Item(0, i).Value = "ERROR"
                                End Try
                            Next
                        End If
                    End With
                    frmMultiAddress.ShowDialog()
                    If frmMultiAddress.dblMulti_ID = 0 Then
                        Manual_Update("INSERT INTO", "Clients", "(Company_Name, First_Name, Last_Name, First_Name2, Last_Name2) VALUES('" & txtCompany_Name.Text & "', '" & txtFirst_Name.Text & "', '" & txtLast_Name.Text & "', '" & txtFirst_Name2.Text & "', '" & txtLast_Name2.Text & "')")
                        Connect("Clients", Clients, drClients, "Company_Name Like '%" & txtCompany_Name.Text & "%' And First_Name Like '%" & txtFirst_Name.Text & "%' And Last_Name Like '%" & txtLast_Name.Text & "%' And First_Name2 Like '%" & txtFirst_Name2.Text & "%' And Last_Name2 Like '%" & txtLast_Name2.Text & "%'")
                        drClients = Clients.Rows.Count - 1
                        Manual_Update("UPDATE", "Addresses", "Current_Client_ID = " & Clients.Rows(drClients).Item("ID"), "ID = " & Address.Rows(drAddress).Item("ID"), Address)
                        Connect("Addresses", Address, drAddress, "ID = " & Address.Rows(drAddress).Item("ID") & "")
                    Else
                        Connect("Clients", Clients, drClients, "ID = " & frmMultiAddress.dblMulti_ID & "")
                        drClients = 0
                        Manual_Update("UPDATE", "Addresses", "Current_Client_ID = " & Clients.Rows(drClients).Item("ID"), "ID = " & Address.Rows(drAddress).Item("ID"), Address)
                        Connect("Addresses", Address, drAddress, "ID = " & Address.Rows(drAddress).Item("ID") & "")
                    End If
                End If
            End If
        ElseIf Clients.Rows.Count = 0 Then
            Manual_Update("INSERT INTO", "Clients", "(Company_Name, First_Name, Last_Name, First_Name2, Last_Name2) VALUES('" & txtCompany_Name.Text & "', '" & txtFirst_Name.Text & "', '" & txtLast_Name.Text & "', '" & txtFirst_Name2.Text & "', '" & txtLast_Name2.Text & "')")
            Connect("Clients", Clients, drClients, "Company_Name Like '%" & txtCompany_Name.Text & "%' And First_Name Like '%" & txtFirst_Name.Text & "%' And Last_Name Like '%" & txtLast_Name.Text & "%' And First_Name2 Like '%" & txtFirst_Name2.Text & "%' And Last_Name2 Like '%" & txtLast_Name2.Text & "%'")
            drClients = Clients.Rows.Count - 1
            If IsDBNull(Address.Rows(drAddress).Item("Current_Client_ID")) = False Then
                strResponse = MsgBox("Is this the new owner of the property?", vbYesNo)
                If strResponse = vbYes Then
                    Manual_Update("UPDATE", "Addresses", "Current_Client_ID = " & Clients.Rows(drClients).Item("ID"), "ID = " & Address.Rows(drAddress).Item("ID"), Address)
                    Connect("Addresses", Address, drAddress, "ID = " & Address.Rows(drAddress).Item("ID") & "")
                Else
                    Connect("Clients", Clients, drClients, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID") & "")
                    drClients = 0
                End If
            Else
                Manual_Update("UPDATE", "Addresses", "Current_Client_ID = " & Clients.Rows(drClients).Item("ID"), "ID = " & Address.Rows(drAddress).Item("ID"), Address)
                Connect("Addresses", Address, drAddress, "ID = " & Address.Rows(drAddress).Item("ID") & "")
            End If
        Else
            strResponse = ""
            For drClients As Double = 0 To Clients.Rows.Count - 1
                If Clients.Rows(drClients).Item("ID") = Address.Rows(drAddress).Item("Current_Client_ID") & "" Then strResponse = "MATCH"
            Next
            If strResponse = "MATCH" Then
                'all done
            Else
                strResponse = MsgBox("The caller's information does not match the information for this address. Are they the new owners?", vbYesNo)
                If strResponse = vbNo Then
                    Connect("Clients", Clients, drClients, "ID = " & Address.Rows(drAddress).Item("Current_Client_ID") & "")
                    drClients = 0
                Else
                    With frmMultiAddress.grdMulti
                        .ColumnCount = 1
                        .Columns(0).Width = 500
                        .Columns(0).HeaderCell.Value = "Information"
                        .Columns(0).HeaderCell.Style.BackColor = Color.LightGray
                        .Columns(0).HeaderCell.Style.Alignment = HorizontalAlignment.Center
                        .Columns(0).HeaderCell.Style.Font = New Font("MS Seriff", 12, FontStyle.Regular)
                        If Clients.Rows.Count > 0 Then
                            .RowCount = Clients.Rows.Count
                            For i As Integer = 0 To Clients.Rows.Count - 1
                                .Item(0, i).Style.Alignment = HorizontalAlignment.Center
                                .Item(0, i).Tag = Clients.Rows(i).Item("ID")
                                .Item(0, i).Style.WrapMode = DataGridViewTriState.True
                                .Rows(i).Height = 30

                                Try
                                    .Item(0, i).Value = Clients.Rows(i).Item("First_Name") & " " & Clients.Rows(i).Item("Last_Name") & (Chr(13) + Chr(10)) & Clients.Rows(i).Item("Last_Address")
                                Catch ex As Exception
                                    .Item(0, i).Value = "ERROR"
                                End Try
                            Next
                        End If
                    End With
                    frmMultiAddress.ShowDialog()
                    If frmMultiAddress.dblMulti_ID = 0 Then
                        Manual_Update("INSERT INTO", "Clients", "(Company_Name, First_Name, Last_Name, First_Name2, Last_Name2) VALUES('" & txtCompany_Name.Text & "', '" & txtFirst_Name.Text & "', '" & txtLast_Name.Text & "', '" & txtFirst_Name2.Text & "', '" & txtLast_Name2.Text & "')")
                        Connect("Clients", Clients, drClients, "Company_Name Like '%" & txtCompany_Name.Text & "%' And First_Name Like '%" & txtFirst_Name.Text & "%' And Last_Name Like '%" & txtLast_Name.Text & "%' And First_Name2 Like '%" & txtFirst_Name2.Text & "%' And Last_Name2 Like '%" & txtLast_Name2.Text & "%'")
                        drClients = Clients.Rows.Count - 1
                        Manual_Update("UPDATE", "Addresses", "Current_Client_ID = " & Clients.Rows(drClients).Item("ID"), "ID = " & Address.Rows(drAddress).Item("ID"), Address)
                        Connect("Addresses", Address, drAddress, "ID = " & Address.Rows(drAddress).Item("ID") & "")
                    Else
                        Connect("Clients", Clients, drClients, "ID = " & frmMultiAddress.dblMulti_ID & "")
                        drClients = 0
                        Manual_Update("UPDATE", "Addresses", "Current_Client_ID = " & Clients.Rows(drClients).Item("ID"), "ID = " & Address.Rows(drAddress).Item("ID"), Address)
                        Connect("Addresses", Address, drAddress, "ID = " & Address.Rows(drAddress).Item("ID") & "")
                    End If
                End If
            End If
            'With frmMultiAddress.grdMulti
            '  .ColumnCount = 1
            '  .Columns(0).Width = 500
            '  .Columns(0).HeaderCell.Value = "Information"
            '  .Columns(0).HeaderCell.Style.BackColor = Color.LightGray
            '  .Columns(0).HeaderCell.Style.Alignment = HorizontalAlignment.Center
            '  .Columns(0).HeaderCell.Style.Font = New Font("MS Seriff", 12, FontStyle.Regular)
            '  If Clients.Rows.Count > 0 Then
            '    .RowCount = Clients.Rows.Count
            '    For i As Integer = 0 To Clients.Rows.Count - 1
            '      .Item(0, i).Style.Alignment = HorizontalAlignment.Center
            '      .Item(0, i).Tag = Clients.Rows(i).Item("ID")
            '      .Item(0, i).Style.WrapMode = DataGridViewTriState.True
            '      .Rows(i).Height = 30

            '      Try
            '        .Item(0, i).Value = Clients.Rows(i).Item("First_Name") & " " & Clients.Rows(i).Item("Last_Name") & (Chr(13) + Chr(10)) & Clients.Rows(i).Item("Last_Address")
            '      Catch ex As Exception
            '        .Item(0, i).Value = "ERROR"
            '      End Try
            '    Next
            '  End If
            'End With
            'frmMultiAddress.ShowDialog()
            'If frmMultiAddress.dblMulti_ID = 0 Then
            '  Manual_Update("INSERT INTO", "Clients", "(Company_Name, First_Name, Last_Name, First_Name2, Last_Name2) VALUES('" & txtCompany_Name.Text & "', '" & txtFirst_Name.Text & "', '" & txtLast_Name.Text & "', '" & txtFirst_Name2.Text & "', '" & txtLast_Name2.Text & "')")
            '  Connect("Clients", Clients, drClients, "Company_Name Like '%" & txtCompany_Name.Text & "%' And First_Name Like '%" & txtFirst_Name.Text & "%' And Last_Name Like '%" & txtLast_Name.Text & "%' And First_Name2 Like '%" & txtFirst_Name2.Text & "%' And Last_Name2 Like '%" & txtLast_Name2.Text & "%'")
            '  drClients = 0
            '  Manual_Update("UPDATE", "Addresses", "Current_Client_ID = " & Clients.Rows(drClients).Item("ID"), "ID = " & Address.Rows(drAddress).Item("ID"), Address)
            '  Connect("Addresses", Address, drAddress, "ID = " & Address.Rows(drAddress).Item("ID") & "")
            'Else
            '  Connect("Clients", Clients, drClients, "ID = " & frmMultiAddress.dblMulti_ID & "")
            '  drClients = 0
            '  Manual_Update("UPDATE", "Addresses", "Current_Client_ID = " & Clients.Rows(drClients).Item("ID"), "ID = " & Address.Rows(drAddress).Item("ID"), Address)
            '  Connect("Addresses", Address, drAddress, "ID = " & Address.Rows(drAddress).Item("ID") & "")
            'End If
        End If
        valNew = False
        tabPost.Visible = True
        cmdSearch.Visible = False
        cmdSave.Visible = True
        Form_Loader(Me.tabPost, "Addresses", Address, drAddress, "Clients", Clients, drClients)
    End Sub

    Private Sub cmdDirections_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDirections.Click
        Shell("C:\Program Files\Internet Explorer\IEXPLORE.EXE http://www.mapquest.com/maps?1c=Kansas+City&1s=MO&1a=5121+E+Front+St&1z=64120-1137&2c=Kansas+City&2s=MO&2a=" & txtAddress_Numbers.Text & "+" & cboAddress_Direction.Text & "+" & txtAddress.Text & "&2z=" & txtZip.Text, AppWinStyle.MinimizedNoFocus)
    End Sub

    Private Sub Post_Load()

        Form_Loader(fraStart, "Addresses", Address, drAddress, "Clients", Clients, drClients)
        Form_Loader(tpgInfo, "Addresses", Address, drAddress, "Clients", Clients, drClients, "Est_Schedule", Est_Schedule, drEst_Schedule)
        Connect("Dry_Dispo", DryDispo, drDryDispo, "Address_ID = " & dblAddressID, "Appt_Date DESC")
        Connect("Magic_Dispo", MagicDispo, drMagicDispo, "Address_ID = " & dblAddressID, "Appt_Date DESC")
        Connect("Dry_Production", DryProduction, drDryProduction, "Address_ID = " & dblAddressID, "Start_Date DESC")
        Connect("Magic_Production", MagicProduction, drMagicProduction, "Address_ID = " & dblAddressID, "Start_Date DESC")
        Connect("Annual_Info", Annual_Info, drAnnual_Info, "Address_ID = " & Address.Rows(drAddress).Item("ID") & "")
        'Connect("Service_Info", Service, drService, "Address_ID = " & Address.Rows(drAddress).Item("ID") & "")
        Connect("Always_Info", Always_Info, drAlways_Info, "Address_ID = " & Address.Rows(drAddress).Item("ID") & "")
        Connect("Est_Info", Est_Info, drEst_Info, "Address_ID = " & Address.Rows(drAddress).Item("ID") & "")
        Connect("Call_Log", CallLog, drCallLog, "Address_ID = " & Address.Rows(drAddress).Item("ID"))

        Form_Loader(tpgAnnual, "Annual", Annual_Info, drAnnual_Info)
        Form_Loader(tpgAlways, "Always", Always_Info, drAlways_Info)
        Form_Loader(tpgEstimate, "Est_Info", Est_Info, drEst_Info)

        grdDispo.RowCount = DryDispo.Rows.Count + MagicDispo.Rows.Count

        drDryDispo = 0
        drMagicDispo = 0
        For r As Integer = 0 To grdDispo.RowCount - 1
            If r < DryDispo.Rows.Count Then
                grdDispo.Item(0, r).Value = DateValue(DryDispo.Rows(drDryDispo).Item("Appt_Date")).Date
                grdDispo.Item(1, r).Value = "DBI"
                grdDispo.Item(2, r).Value = DryDispo.Rows(drDryDispo).Item("Estimator")
                grdDispo.Item(3, r).Value = DryDispo.Rows(drDryDispo).Item("Print_Offer")
                drDryDispo += 1
            Else
                grdDispo.Item(0, r).Value = DateValue(MagicDispo.Rows(drMagicDispo).Item("Appt_Date")).Date
                grdDispo.Item(1, r).Value = "BM"
                grdDispo.Item(2, r).Value = MagicDispo.Rows(drMagicDispo).Item("Estimator")
                grdDispo.Item(3, r).Value = MagicDispo.Rows(drMagicDispo).Item("Print_Offer")
                drMagicDispo += 1
            End If
        Next

        grdDispo.Sort(grdDispo.Columns(0), System.ComponentModel.ListSortDirection.Descending)

        Try
            grdProduction.RowCount = DryProduction.Rows.Count + MagicProduction.Rows.Count
        Catch ex As Exception
            grdProduction.RowCount = 1
        End Try

        drDryProduction = 0
        drMagicProduction = 0
        For r As Integer = 0 To grdProduction.RowCount - 1
            If r < DryProduction.Rows.Count Then
                grdProduction.Item(0, r).Value = SafeFormat(DryProduction.Rows(drDryProduction).Item("Start_Date"), "short date")
                grdProduction.Item(1, r).Value = "DBI"
                grdProduction.Item(2, r).Value = DryProduction.Rows(drDryProduction).Item("Print_Installed")
                drDryProduction += 1
            Else
                grdProduction.Item(0, r).Value = SafeFormat(MagicProduction.Rows(drMagicProduction).Item("Start_Date"), "short date")
                grdProduction.Item(1, r).Value = "BM"
                grdProduction.Item(2, r).Value = MagicProduction.Rows(drMagicProduction).Item("Print_Installed")
                drMagicProduction += 1
            End If
        Next

        grdProduction.Sort(grdProduction.Columns(0), System.ComponentModel.ListSortDirection.Descending)

        If flgDispo_Add = True Then
            grdDispo.Enabled = False
            grdProduction.Enabled = False
            cmdSave.Text = "ADD"
        End If

        Form_Loader(tpgEstimate, "Est_Info", Est_Info, drEst_Info)

        Form_Loader(tpgAlways, "Always", Always_Info, drAlways_Info)

        Form_Loader(tpgAnnual, "Annual", Annual_Info, drAnnual_Info)

        Schedule_Refresh()

        Connect("Call_List_Info", General, 0, "Address_ID = " & Address.Rows(drAddress).Item("ID"))
        For intRow As Integer = 0 To General.Rows.Count - 1
            cboCall_List.Items.Add(General.Rows(intRow).Item("Call_List"))
        Next

        grdCallLog.RowCount = CallLog.Rows.Count
        Form_Loader(tpgCall_Log, "CallLog", CallLog, 0)
        Grid_Lock(Me, grdCallLog, True)
        Grid_Lock(Me, grdAlways, True)
        Grid_Lock(Me, grdAnnual, True)
        Grid_Lock(Me, grdDispo, True)
        Grid_Lock(Me, grdEst_Appts, True)
        Grid_Lock(Me, grdProduction, True)
        Grid_Lock(Me, grdService, True)
    End Sub

    Private Function SafeFormat(ByRef value As Object, ByVal formatString As String)

        Dim result As String = ""

        If Not IsDBNull(value) Then
            result = Format(value, formatString)
        End If

        Return result

    End Function

    Public Sub Schedule_Grid_Load(ByVal Grid As DataGridView, ByVal Table As DataTable)
        Dim strItem As String = ""
        Dim intAppts As Integer

        intAppts = Grid.RowCount
        Grid_Lock(Me, Grid)
        With Grid
            .RowCount = Table.Rows.Count
            For c As Integer = 0 To Grid.Columns.Count - 1
                strItem = Replace(.Columns(c).HeaderText, " ", "_")
                For r As Integer = 0 To Table.Rows.Count - 1
                    If LCase(strItem) = "time" Then strItem = "Start_Time"
                    If LCase(strItem) = "type" Then strItem = "Appt_Type"
                    If LCase(strItem) = "bonus" Then strItem = "Bonus_Sig"
                    Try
                        .Item(c, r).Value = Table.Rows(r).Item(strItem)
                    Catch ex As Exception
                        'do nothing
                    End Try
                    If IsDate(.Item(c, r).Value) = True Then
                        If strItem.Contains("Time") = True Then .Item(c, r).Value = Format(.Item(c, r).Value, "h:mm tt")
                        If strItem.Contains("DATE") = True Then .Item(c, r).Value = Format(.Item(c, r).Value, "short date")
                    End If
                    .Item(c, r).Tag = Table.Rows(r).Item("Address_ID")
                Next
            Next
        End With
        If Grid.RowCount > intAppts Then flgApptSet = True
    End Sub

    Private Sub cmdEst_Schedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEst_Schedule.Click

        Try
            If cboSource.Text = "" Then
                MsgBox("You must select a source to schedule an appointment.")
                Exit Sub
            End If
            strSource = cboSource.Text
            dblAddressID = Address.Rows(drAddress).Item("ID")
            datDate = Now
            frmSchedule.Close()
            frmSchedule.valSchedule = "Estimators"
            frmSchedule.Tag = "LEAD"
            frmSchedule.Schedule_Maker("estimators")

            frmSchedule.ShowInTaskbar = True
            frmSchedule.WindowState = FormWindowState.Maximized
            frmSchedule.ShowDialog()
        Catch ex As Exception
        End Try
        Schedule_Refresh()
    End Sub

    Private Sub txtZip_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtZip.LostFocus
        If txtZip.Text = "" Then Exit Sub
        Connect("Zips", General, drGeneral, "Zip = '" & txtZip.Text & "'")
        drGeneral = 0

        cboCity.Items.Clear()
        cboState.Items.Clear()

        If General.Rows.Count > 0 Then
            While Not drGeneral > General.Rows.Count - 1
                cboCity.Items.Add(General.Rows(drGeneral).Item("City"))
                If General.Rows(drGeneral).Item("Default") = True Then cboCity.SelectedItem = cboCity.Items(drGeneral)
                drGeneral = drGeneral + 1
            End While
            cboState.Items.Add(General.Rows(0).Item("State"))
            cboState.SelectedItem = cboState.Items(0)
        Else
            MsgBox("There is no match found. Please verify that the information is accurate and either input the zip code again or add the neccasary information.")
        End If
    End Sub

    Private Sub txtZip_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtZip.TextChanged

    End Sub

    Private Sub txtPhone1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPhone1.LostFocus
        Dim strPhone As String = ""

        If txtPhone1.Text.Count = 10 Then
            txtPhone1.Select(0, 3)
            strPhone = "(" & txtPhone1.SelectedText & ") "
            txtPhone1.Select(3, 3)
            strPhone = strPhone & txtPhone1.SelectedText & "-"
            txtPhone1.Select(6, 4)
            strPhone = strPhone & txtPhone1.SelectedText
            txtPhone1.Text = strPhone
        End If
    End Sub

    Private Sub txtPhone2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPhone2.LostFocus
        Dim strPhone As String = ""

        If txtPhone2.Text.Count = 10 Then
            txtPhone2.Select(0, 3)
            strPhone = "(" & txtPhone2.SelectedText & ") "
            txtPhone2.Select(3, 3)
            strPhone = strPhone & txtPhone2.SelectedText & "-"
            txtPhone2.Select(6, 4)
            strPhone = strPhone & txtPhone2.SelectedText
            txtPhone2.Text = strPhone
        ElseIf txtPhone2.Text = "SAME" Then
            cboPhone_Type2.Text = cboPhone_Type1.Text
            txtPhone2.Text = txtPhone1.Text
        End If
    End Sub

    Private Sub txtPhone2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPhone2.TextChanged

    End Sub

    Private Sub cboAddress_Direction_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAddress_Direction.SelectedIndexChanged

    End Sub

    Private Sub dtpLast_Annual_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpLast_Annual.ValueChanged
        dtpLast_Annual.Value = dtpLast_Annual.Value.Date
    End Sub

    Private Sub grdDispo_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDispo.CellContentClick

    End Sub

    Private Sub cmdAddCallLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddCallLog.Click
        cboCallType.SelectedIndex = -1
        txtCallNotes.Text = ""
        fraCallLog.Visible = True
    End Sub

    Private Sub grdDispo_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDispo.CellDoubleClick
        dblAddressID = Address.Rows(drAddress).Item("ID")
        frmDispo_Post.ShowDialog(Me)
    End Sub

    Private Sub cmdCallLogSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCallLogSave.Click
        Manual_Update("Insert into", "Call_Log", "(Address_ID, [Date], [Type], Notes, Person) Values(" & Address.Rows(drAddress).Item("ID") & ", " & FilterByDate(Now.Date) & ", '" & cboCallType.Text & "', '" & txtCallNotes.Text & "', '" & strUserName & "')")
        fraCallLog.Visible = False

        Connect("Call_Log", CallLog, drCallLog, "Address_ID = " & Address.Rows(drAddress).Item("ID"))
        grdCallLog.RowCount = CallLog.Rows.Count
        Form_Loader(tpgCall_Log, "CallLog", CallLog, 0)
        Grid_Lock(Me, grdCallLog, True)
    End Sub

    Private Sub cmdCallLogClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCallLogClose.Click
        fraCallLog.Visible = False
    End Sub

    Private Sub cmdAlwaysSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAlwaysSchedule.Click
        Try
            dblAddressID = Address.Rows(drAddress).Item("ID")
            datDate = Now
            frmSchedule.valSchedule = "plumbing"
            frmSchedule.Schedule_Maker("plumbing")
            frmSchedule.ShowInTaskbar = True
            frmSchedule.WindowState = FormWindowState.Maximized
            frmSchedule.ShowDialog()
        Catch ex As Exception
        End Try
        Schedule_Refresh()
    End Sub

    Private Sub cmdAnnualSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAnnualSchedule.Click
        Try
            dblAddressID = Address.Rows(drAddress).Item("ID")
            datDate = Now
            frmSchedule.valSchedule = "annual"
            frmSchedule.Schedule_Maker("annual")
            frmSchedule.ShowInTaskbar = True
            frmSchedule.WindowState = FormWindowState.Maximized
            frmSchedule.ShowDialog()
        Catch ex As Exception
        End Try

        Schedule_Refresh()
    End Sub

    Private Sub grdProduction_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdProduction.CellContentClick

    End Sub

    Private Sub grdProduction_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdProduction.CellDoubleClick
        dblAddressID = Address.Rows(drAddress).Item("ID")
        frmProduction.ShowDialog(Me)
    End Sub

    Private Sub grdEst_Appts_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdEst_Appts.CellDoubleClick
        If DateDiff(DateInterval.Day, DateValue(grdEst_Appts.Item(0, e.RowIndex).Value), Now.Date) < 1 Then
            strResponse = MsgBox("Are you confirming this appointment? Hitting no will prompt you to delete appointment.", vbYesNo)
            If strResponse = vbYes Then
                Manual_Update("Update", "schedule", "Confirmed = 1", "Address_ID = " & Address.Rows(drAddress).Item("ID") & " And Date = " & FilterByDate(DateValue(grdEst_Appts.Item(0, e.RowIndex).Value)))
                Schedule_Refresh()
                Exit Sub
            End If
            strResponse = MsgBox("Would you like to delete this appointment?", vbYesNo)
            If strResponse = vbNo Then Exit Sub
            strResponse = InputBox("Reason for Cancel:")
            Manual_Update("Insert Into", "Call_Log", "([Address_Id], [Date], [Type], [Notes], [Person]) Values(" & Address.Rows(drAddress).Item("ID") & ", " & FilterByDate(Now.Date) & ", 'Activity', '" & strResponse & "', '" & strUserName & "')")
            TableUpdate("DELETE", "schedule", "", "", Me, "Address_ID = " & Address.Rows(drAddress).Item("ID") & " And Date = " & FilterByDate(DateValue(grdEst_Appts.Item(0, e.RowIndex).Value)))
        Else
            If LCase(grdEst_Appts.Item(7, e.RowIndex).Value & "") = "false" Then
                strResponse = MsgBox("In order to give the dispo for this appointment the lead form will have to close. Would you like to give the dispo for this appointment?", vbYesNo)
                If strResponse = vbNo Then Exit Sub
                datDispo = DateValue(grdEst_Appts.Item(0, e.RowIndex).Value)
                frmDispo.Show()
                Me.Close()
            End If
        End If
    End Sub

    Private Sub Schedule_Refresh()
        Connect("Schedule", Est_Schedule, drEst_Schedule, "Address_ID = " & dblAddressID & " And Estimator_Appt = 1", "Date DESC")
        Schedule_Grid_Load(grdEst_Appts, Est_Schedule)

        Connect("Schedule", Est_Schedule, drEst_Schedule, "Address_ID = " & dblAddressID & " And Annual_Appt = 1", "Date DESC")
        Schedule_Grid_Load(grdAnnual, Est_Schedule)

        Connect("Schedule", Est_Schedule, drEst_Schedule, "Address_ID = " & dblAddressID & " And Always_Appt = 1", "Date DESC")
        Schedule_Grid_Load(grdAlways, Est_Schedule)
    End Sub

    Private Sub cmdContacted_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdContacted.Click
        Dim CallNumbers As New DataTable

        If cboCall_List.SelectedIndex = -1 Then
            MsgBox("You must select a valid call list to continue.")
            Exit Sub
        End If

        Manual_Update("UPDATE", "Call_List_Info", "Last_Contact = " & FilterByDate(Now.Date), "Address_ID = " & Address.Rows(drAddress).Item("ID") & " AND Call_List = '" & cboCall_List.Text & "'")
        Connect("Call_List_Numbers", CallNumbers, 0, "Caller = '" & strUserName & "' AND [Date] = " & FilterByDate(Now.Date) & " AND Call_List = '" & cboCall_List.Text & "'")
        If CallNumbers.Rows.Count = 0 Then
            Manual_Update("Insert Into", "Call_List_Numbers", "([Caller], [Date], [Call_List]) Values('" & strUserName & "', " & FilterByDate(Now.Date) & ", '" & cboCall_List.Text & "')")
            Connect("Call_List_Numbers", CallNumbers, 0, "Caller = '" & strUserName & "' AND [Date] = " & FilterByDate(Now.Date) & " AND Call_List = '" & cboCall_List.Text & "'")
        End If
        Dim intDead As Integer = 0
        Dim intContacted As Integer = 0
        Dim intSet As Integer = 0
        Dim intWorkDone As Integer = 0
        If sender Is cmdList_Remove Then
            intContacted += 1
            intDead += 1
        ElseIf sender Is cmdDelay Then
            intContacted += 1
        End If
        If flgApptSet = True Then
            intSet += 1
            intContacted += 1
        End If

        If chkWorkDone.Checked = True Then intWorkDone += 1

        Manual_Update("Update", "Call_List_Numbers", "[Calls] = " & Val(CallNumbers.Rows(0).Item("Calls")) + 1 & ", [Contacts] = " & Val(CallNumbers.Rows(0).Item("Contacts")) + intContacted & ", [Dead] = " & Val(CallNumbers.Rows(0).Item("Dead")) + intDead & ", [Set] = " & Val(CallNumbers.Rows(0).Item("Set")) + intSet & ",[Work_Done] = " & Val(CallNumbers.Rows(0).Item("Work_Done")) + intWorkDone, "ID = " & CallNumbers.Rows(0).Item("ID"))
    End Sub

    Private Sub cmdList_Remove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdList_Remove.Click
        If cboCall_List.SelectedIndex = -1 Then
            MsgBox("You must select a valid call list to continue.")
            Exit Sub
        End If
        cmdContacted_Click(cmdList_Remove, e)
        Manual_Update("DELETE", "Call_List_Info", "", "Address_ID = " & Address.Rows(drAddress).Item("ID") & " AND Call_List = '" & cboCall_List.Text & "'")
    End Sub

    Private Sub cmdDelay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelay.Click
        If cboCall_List.SelectedIndex = -1 Then
            MsgBox("You must select a valid call list to continue.")
            Exit Sub
        End If
        strResponse = InputBox("Please enter the date you would like this to return to the call list.")

        While IsDate(DateValue(strResponse)) = False
            strResponse = InputBox("Please enter date you would like this to return to the call list. Typing nothing will end this process.")
            If strResponse = "" Then Exit Sub
        End While
        cmdContacted_Click(cmdDelay, e)
        Manual_Update("UPDATE", "Call_List_Info", "Active_Date = " & FilterByDate(DateValue(strResponse)), "Address_ID = " & Address.Rows(drAddress).Item("ID") & " AND Call_List = '" & cboCall_List.Text & "'")
    End Sub

    Private Sub cboCall_List_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCall_List.SelectedIndexChanged
        Connect("Call_List", General, 0, "Name = '" & cboCall_List.Text & "'")

        Try
            txtScript.Text = General.Rows(0).Item("Script") & ""
            Connect("ScriptPhrases", General, 0)
            For intRow As Integer = 0 To General.Rows.Count - 1
                'txtScript.Text = 
            Next
        Catch ex As Exception
            txtScript.Text = "No script or failure to load."
        End Try
    End Sub

    Private Sub txtDirections_LostFocus1(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDirections.LostFocus

    End Sub

    Private Sub grdEst_Appts_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdEst_Appts.CellContentClick

    End Sub

    Private Sub txtScript_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtScript.TextChanged

    End Sub

    Private Sub mnuLeadPrint_Click(sender As System.Object, e As System.EventArgs) Handles mnuLeadPrint.Click

        Lead_Print(Address.Rows(drAddress).Item("ID"), Me)

    End Sub

    Private Sub mnuLeadEmail_Click(sender As System.Object, e As System.EventArgs) Handles mnuLeadEmail.Click
        Lead_Print(Address.Rows(drAddress).Item("ID"), Me, , True)
    End Sub

    Private Sub txtNewComment_LostFocus(sender As Object, e As System.EventArgs) Handles txtNewComment.LostFocus
        If txtNewComment.Text <> "" Then
            txtComments.Text = UCase(Trim(txtNewComment.Text)) & (Chr(13) + Chr(10)) & txtComments.Text
        End If
    End Sub

    Private Sub txtNewComment_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtNewComment.TextChanged

    End Sub

    Private Sub mnuPrinterChange_Click(sender As System.Object, e As System.EventArgs) Handles mnuPrinterChange.Click

    End Sub
End Class