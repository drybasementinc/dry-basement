﻿Public Class frmDispo_Post
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form Specific Declarations
  Dim flgDryUsed As Boolean
  Dim flgMagicUsed As Boolean
  Dim dblDryDispoID As Double = 0

  Private Sub Control_Purge(ByVal owner As Object)
    For Each Control As Object In owner.Controls
      If Control.haschildren = True Then
        While Control.controls.count > 0
          Control.controls(0).dispose()
        End While
        Control.dispose()
        Control_Purge(owner)
        Exit Sub
      End If
    Next
  End Sub

  Private Sub frmDispo_Post_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

  End Sub

  Private Sub Dispo_Post_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Dim Dummy As New DataTable
    Dim drDummy As Double
    Dim intTop As Integer
    Dim intLeft As Integer
    Dim intFrameTop As Integer
    Dim intCompany As Integer
    Dim strCompany As String
    Dim Older As GroupBox

    If strUserName = "Ray" Then fraValues.Visible = True
    Connect("Dispo_Categories", General, drGeneral, , "Company, Placement")
    drGeneral = 0

    intFrameTop = 0
    intCompany = 0
    strCompany = General.Rows(drGeneral).Item("Company")
    For drGeneral As Double = 0 To General.Rows.Count - 1
      'gets a count on how many products will be in the group box
      Connect("Products", Dummy, drDummy, "Category = '" & General.Rows(drGeneral).Item("Category") & "'", "Active,Placement,Product")
      If General.Rows(drGeneral).Item("Company") <> strCompany Then
        strCompany = General.Rows(drGeneral).Item("Company")
        intCompany = 0
        intFrameTop = 0
      End If

      Dim frame As New GroupBox
      frame.Name = Replace(General.Rows(drGeneral).Item("Category"), " ", "_")
      frame.Text = General.Rows(drGeneral).Item("Category")
      frame.Width = 140 + (Int(Int(Dummy.Rows.Count - 1) / 5) * 140)
      frame.Height = 150
      Older = Nothing
      If intCompany > 0 Then
        Older = tabDispo.Controls("tpg" & General.Rows(drGeneral).Item("Company")).controls(Replace(General.Rows(drGeneral - 1).Item("Category"), " ", "_"))
        If Older.Left + Older.Width + frame.Width > Older.Parent.Width - 10 Then
          intFrameTop = intFrameTop + 1
        End If
      End If
      frame.Top = 75 + (155 * intFrameTop)
      If intCompany > 0 Then
        If frame.Top = Older.Top Then
          frame.Left = Older.Left + Older.Width + 10
        Else
          frame.Left = 5
        End If
      Else
        frame.Left = 5
      End If
      tabDispo.Controls("tpg" & General.Rows(drGeneral).Item("Company")).controls.add(frame)
      intTop = 0
      intLeft = 0
      For drDummy = 0 To Dummy.Rows.Count - 1
        Dim text As New TextBox
        Dim label As New Label
        text.Name = "txt" & Replace(Dummy.Rows(drDummy).Item("Product_Field"), " ", "")
        text.Width = 30
        text.Top = text.Height + (intTop * text.Height) + 10
        text.Left = 100 + (intLeft * (text.Width + 100))
        text.Tag = Dummy.Rows(drDummy).Item("Company")
        text.ReadOnly = True
        text.BackColor = Color.White
        If Dummy.Rows(drDummy).Item("Active") = False Then
          text.Enabled = False
        End If
        frame.Controls.Add(text)
        text.Visible = True
        label.Name = "lbl" & Replace(Dummy.Rows(drDummy).Item("Product_Field"), " ", "")
        label.Text = Replace(Dummy.Rows(drDummy).Item("Product"), "_", " ")
        label.Tag = Dummy.Rows(drDummy).Item("Abbr")
        label.Top = label.Height + (intTop * label.Height) + 5
        label.Width = 90
        label.Left = 5 + (intLeft * (label.Width + text.Width)) + (10 * intLeft)
        frame.Controls.Add(label)
        label.Show()
        intTop = intTop + 1
        If intTop > 4 Then
          intTop = 0
          intLeft = intLeft + 1
        End If
        intCompany = intCompany + 1
      Next
      frame.Visible = True
    Next

    Connect("Dry_Dispo", DryDispo, drDryDispo, "Address_ID = " & dblAddressID, "Appt_Date DESC")
    drDryDispo = 0
    Connect("Magic_Dispo", MagicDispo, drMagicDispo, "Address_ID = " & dblAddressID, "Appt_Date DESC")
    drMagicDispo = 0
    'If DryDispo.Rows.Count = 0 Then flgDryUsed = True
    'If MagicDispo.Rows.Count = 0 Then flgMagicUsed = True

    grdDispo.Rows = DryDispo.Rows.Count + MagicDispo.Rows.Count + 1
    grdDispo.Cols = 4
    grdDispo.Create(400, 25, Me)
    grdDispo.ColWidth(0, 80)
    grdDispo.ColWidth(1, 110)
    grdDispo.Cell(0, 0).backcolor = Color.LightGray
    grdDispo.Cell(0, 0).text = "APPT DATE"
    grdDispo.Cell(0, 1).text = "ESTIMATOR"
    grdDispo.Cell(0, 2).text = "DRY"
    grdDispo.Cell(0, 3).text = "MAGIC"
    grdDispo.CellBorder(0, 0).topborder = True
    grdDispo.CellBorder(0, 0).bottomborder = True
    grdDispo.CellBorder(0, 0).leftborder = True
    grdDispo.CellBorder(0, 0).rightborder = True
    grdDispo.ApplyRowFormat(0, 0)
    For drDryDispo As Double = 0 To DryDispo.Rows.Count - 1
      Try
        grdDispo.Cell(drDryDispo + 1, 2).tag = DryDispo.Rows(drDryDispo).Item("ID")
        grdDispo.Cell(drDryDispo + 1, 0).text = DryDispo.Rows(drDryDispo).Item("Appt_Date") & ""
        grdDispo.Cell(drDryDispo + 1, 1).text = DryDispo.Rows(drDryDispo).Item("Estimator") & ""
        grdDispo.Cell(drDryDispo + 1, 2).text = DryDispo.Rows(drDryDispo).Item("Print_Offer") & ""
        grdDispo.Cell(drDryDispo + 1, 3).tag = "0"
        grdDispo.Cell(drDryDispo + 1, 0).backcolor = Color.White
        grdDispo.ApplyRowFormat(drDryDispo + 1, 0)
      Catch ex As Exception
      End Try
    Next

    For drMagicDispo As Double = 0 To MagicDispo.Rows.Count - 1
      For r As Integer = 1 To grdDispo.Rows - 1
        If grdDispo.Cell(r, 0).text = "" Then
          grdDispo.Cell(r, 0).text = MagicDispo.Rows(drMagicDispo).Item("Appt_Date")
          grdDispo.Cell(r, 1).text = MagicDispo.Rows(drMagicDispo).Item("Estimator")
          grdDispo.Cell(r, 3).text = MagicDispo.Rows(drMagicDispo).Item("Print_Offer") & ""
          grdDispo.Cell(r, 3).tag = MagicDispo.Rows(drMagicDispo).Item("ID")
          grdDispo.Cell(r, 2).tag = "0"
          grdDispo.Cell(r, 0).backcolor = Color.White
          grdDispo.ApplyRowFormat(drDryDispo + 1, 0)
          r = grdDispo.Rows
        ElseIf grdDispo.Cell(r, 0).text = MagicDispo.Rows(drMagicDispo).Item("Appt_Date") Then
          grdDispo.Cell(r, 3).text = MagicDispo.Rows(drMagicDispo).Item("Print_Offer") & ""
          grdDispo.Cell(r, 3).tag = MagicDispo.Rows(drMagicDispo).Item("ID")
          r = grdDispo.Rows
        End If
      Next
    Next

    For r As Integer = 1 To grdDispo.Rows - 1
      If grdDispo.Cell(r, 0).text = "" Then
        For c As Integer = 0 To grdDispo.Cols - 1
          grdDispo.Cell(r, c).visible = False
        Next
      End If
    Next
  End Sub

  Private Sub Dry_Load()
    Dim int1 As Integer
    Form_Loader(tpgDry, "DRY", DryDispo, drDryDispo)
    txtDryPrice.Text = DryDispo.Rows(drDryDispo).Item("Price")
    optDryDNS.Checked = DryDispo.Rows(drDryDispo).Item("DNS")
    optDrySold.Checked = DryDispo.Rows(drDryDispo).Item("SOLD")
    txtDry.Text = DryDispo.Rows(drDryDispo).Item("Print_Offer") & ""
    For i As Integer = 0 To tpgDry.Controls.Count - 1
      If TypeOf (tpgDry.Controls(i)) Is TextBox Then
        If tpgDry.Controls(i).Text = 0 Then tpgDry.Controls(i).Text = ""
      ElseIf tpgDry.Controls(i).HasChildren = True Then
        For int1 = 0 To tpgDry.Controls(i).Controls.Count - 1
          If TypeOf (tpgDry.Controls(i).Controls(int1)) Is TextBox Then
            If tpgDry.Controls(i).Controls(int1).Text = "0" Then tpgDry.Controls(i).Controls(int1).Text = ""
          End If
        Next
      End If
    Next
    tpgDry.Tag = DryDispo.Rows(drDryDispo).Item("ID")
  End Sub

  Private Sub Magic_Load()
    Form_Loader(tpgMagic, "MAGIC", MagicDispo, drMagicDispo)
    txtMagicPrice.Text = MagicDispo.Rows(drMagicDispo).Item("Price")
    optMagicDNS.Checked = MagicDispo.Rows(drMagicDispo).Item("DNS")
    optMagicSold.Checked = MagicDispo.Rows(drMagicDispo).Item("SOLD")
    txtMagic.Text = MagicDispo.Rows(drMagicDispo).Item("Print_Offer") & ""
    For i As Integer = 0 To tpgMagic.Controls.Count - 1
      If TypeOf (tpgMagic.Controls(i)) Is TextBox Then
        If tpgMagic.Controls(i).Text = 0 Then tpgMagic.Controls(i).Text = ""
      ElseIf tpgMagic.Controls(i).HasChildren = True Then
        For int1 = 0 To tpgMagic.Controls(i).Controls.Count - 1
          If TypeOf (tpgMagic.Controls(i).Controls(int1)) Is TextBox Then
            If tpgMagic.Controls(i).Controls(int1).Text = "0" Then tpgMagic.Controls(i).Controls(int1).Text = ""
          End If
        Next
      End If
    Next
    tpgMagic.Tag = MagicDispo.Rows(drMagicDispo).Item("ID")
  End Sub

  Private Sub frmDispo_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    On Error Resume Next
    tabDispo.Width = Me.Width - 25
    tabDispo.Height = Me.Height - tabDispo.Top - 40
    grdDispo.Width = Me.Width - 25
  End Sub

  Private Sub Dispo_Clear()
    Dim int1 As Integer

    txtDryPrice.Text = ""
    optDryDNS.Checked = False
    optDrySold.Checked = False
    For i As Integer = 0 To tpgDry.Controls.Count - 1
      If TypeOf (tpgDry.Controls(i)) Is TextBox Then
        tpgDry.Controls(i).Text = ""
      ElseIf tpgDry.Controls(i).HasChildren = True Then
        For int1 = 0 To tpgDry.Controls(i).Controls.Count - 1
          If TypeOf (tpgDry.Controls(i).Controls(int1)) Is TextBox Then
            tpgDry.Controls(i).Controls(int1).Text = ""
          End If
        Next
      End If
    Next

    txtMagicPrice.Text = ""
    optMagicDNS.Checked = False
    optMagicSold.Checked = False
    For i As Integer = 0 To tpgMagic.Controls.Count - 1
      If TypeOf (tpgMagic.Controls(i)) Is TextBox Then
        tpgMagic.Controls(i).Text = ""
      ElseIf tpgMagic.Controls(i).HasChildren = True Then
        For int1 = 0 To tpgMagic.Controls(i).Controls.Count - 1
          If TypeOf (tpgMagic.Controls(i).Controls(int1)) Is TextBox Then
            tpgMagic.Controls(i).Controls(int1).Text = ""
          End If
        Next
      End If
    Next
    tpgDry.Tag = "0"
    tpgMagic.Tag = "0"
  End Sub

  Private Sub grdDispo_CellDoubleClick(ByVal sender As Object, ByVal Owner As Object, ByVal CurrentRow As Double, ByVal CurrentCol As Double) Handles grdDispo.CellDoubleClick
    If grdDispo.Valid_Cell(sender) = False Then Exit Sub

    Dispo_Clear()
    Connect("Dry_Dispo", DryDispo, drDryDispo, "ID = " & grdDispo.Cell(CurrentRow, 2).tag & "")
    dblDryDispoId = grdDispo.Cell(CurrentRow, 2).tag
    drDryDispo = 0
    Connect("Magic_Dispo", MagicDispo, drMagicDispo, "ID = " & grdDispo.Cell(CurrentRow, 3).tag & "")
    drMagicDispo = 0

    If DryDispo.Rows.Count > 0 Then Dry_Load()
    If MagicDispo.Rows.Count > 0 Then Magic_Load()
    txtEstimator.Text = grdDispo.Cell(CurrentRow, 1).text
  End Sub

  Private Sub grdDispo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdDispo.Load

  End Sub

  Private Sub cmdRedo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim Numbers As New DataTable
    Dim drNumbers As Double

    strResponse = ""
    While Not strResponse = strPassword
      strResponse = InputBox("Password:")
      If strResponse = "" Then Exit Sub
    End While

    Try
      datDispo = DryDispo.Rows(drDryDispo).Item("Appt_Date")
      dblAddressID = DryDispo.Rows(drDryDispo).Item("Address_ID")
      frmDispo.valSource.Text = DryDispo.Rows(drDryDispo).Item("Source")
    Catch ex As Exception
      datDispo = MagicDispo.Rows(drMagicDispo).Item("Appt_Date")
      dblAddressID = MagicDispo.Rows(drMagicDispo).Item("Address_ID")
      frmDispo.valSource.Text = MagicDispo.Rows(drMagicDispo).Item("Source")
    End Try

    Connect("Estimator_Numbers", Numbers, drNumbers, "Estimator = '" & txtEstimator.Text & "' And Date = " & FilterByDate(datDispo) & "")
    drNumbers = 0
    If Numbers.Rows.Count <> 1 Then
      MsgBox("An error has occured and the sales numbers could not be updated.")
      Exit Sub
    End If
    With Numbers.Rows(drNumbers)
      If DryDispo.Rows.Count > 0 And MagicDispo.Rows.Count > 0 Then
        .Item("Combined_Appts") = .Item("Combined_Appts") - 1
        If DryDispo.Rows(drDryDispo).Item("Sold") = True And MagicDispo.Rows(drMagicDispo).Item("Sold") = True Then
          .Item("Combined_Sales") = .Item("Combined_Sales") - 1
          .Item("Combined_Volume") = Val(.Item("Combined_Volume")) - (Val(DryDispo.Rows(drDryDispo).Item("Price") + Val(MagicDispo.Rows(drMagicDispo).Item("Price"))))
        End If
        .Item("Combined_Sits") = .Item("Combined_Sits") - 1
        If DryDispo.Rows(drDryDispo).Item("Reset") = True And MagicDispo.Rows(drMagicDispo).Item("Reset") = True Then
          .Item("Combined_Resets") = .Item("Combined_Resets") - 1
          .Item("Combined_Sits") = .Item("Combined_Sits") + 1
        End If
        If DryDispo.Rows(drDryDispo).Item("DNQ") = True And MagicDispo.Rows(drMagicDispo).Item("DNQ") = True Then
          .Item("Combined_DNQs") = .Item("Combined_DNQs") - 1
          .Item("Combined_Sits") = .Item("Combined_Sits") + 1
        End If
      End If

      If DryDispo.Rows.Count > 0 Then
        .Item("Dry_Appts") = .Item("Dry_Appts") - 1
        If DryDispo.Rows(drDryDispo).Item("Sold") = True Then
          .Item("Dry_Sales") = .Item("Dry_Sales") - 1
          .Item("Dry_Volume") = Val(.Item("Dry_Volume")) - Val(DryDispo.Rows(drDryDispo).Item("Price"))
        End If
        .Item("Dry_Sits") = .Item("Dry_Sits") - 1
        If DryDispo.Rows(drDryDispo).Item("Reset") = True Then
          .Item("Dry_Resets") = .Item("Dry_Resets") - 1
          .Item("Dry_Sits") = .Item("Dry_Sits") + 1
        End If
        If DryDispo.Rows(drDryDispo).Item("DNQ") = True Then
          .Item("Dry_DNQs") = .Item("Dry_DNQs") - 1
          .Item("Dry_Sits") = .Item("Dry_Sits") + 1
        End If
        If DryDispo.Rows(drDryDispo).Item("Sold") = True Then
          Manual_Update("DELETE", "Dry_Production", "", "", "Dispo_ID = " & DryDispo.Rows(0).Item("ID") & "")
        End If
        Manual_Update("DELETE", "Dry_Dispo", "", "", "ID = " & DryDispo.Rows(0).Item("ID") & "")
      End If

      If MagicDispo.Rows.Count > 0 Then
        .Item("Magic_Appts") = .Item("Magic_Appts") - 1
        If MagicDispo.Rows(drMagicDispo).Item("Sold") = True Then
          .Item("Magic_Sales") = .Item("Magic_Sales") - 1
          .Item("Magic_Volume") = Val(.Item("Magic_Volume")) - Val(MagicDispo.Rows(drMagicDispo).Item("Price"))
        End If
        .Item("Magic_Sits") = .Item("Magic_Sits") - 1
        If MagicDispo.Rows(drMagicDispo).Item("Reset") = True Then
          .Item("Magic_Resets") = .Item("Magic_Resets") - 1
          .Item("Magic_Sits") = .Item("Magic_Sits") + 1
        End If
        If MagicDispo.Rows(drMagicDispo).Item("DNQ") = True Then
          .Item("Magic_DNQs") = .Item("Magic_DNQs") - 1
          .Item("Magic_Sits") = .Item("Magic_Sits") + 1
        End If
        If MagicDispo.Rows(drMagicDispo).Item("Sold") = True Then
          Manual_Update("DELETE", "Magic_Production", "", "", "Dispo_ID = " & MagicDispo.Rows(0).Item("ID") & "")
        End If
        Manual_Update("DELETE", "Magic_Dispo", "", "", "ID = " & MagicDispo.Rows(0).Item("ID") & "")
      End If
      Table_Compiler(Numbers, drNumbers)
      strFields1 = Replace(strFields1, "[Date], ", "")
      strValues1 = Replace(strValues1, "@" & datDispo & ", ", "")
      Manual_Update("UPDATE", "Estimator_Numbers", strFields1, strValues1, "ID = " & .Item("ID") & "")
      Manual_Update("UPDATE", "Addresses", "[Dry_Dispo_Done] = 0, [Magic_Dispo_Done] = 0", "ID = " & dblAddressID & "")
      frmDispo.valAddress_ID.Text = dblAddressID

      frmDispo.ShowDialog(Me)
    End With
  End Sub

  Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
    If DryDispo.Rows.Count = 0 And MagicDispo.Rows.Count = 0 Then Exit Sub
    strResponse = MsgBox("Proceeding will remove this dispo and all attached production records. Do you want to proceed?", vbYesNo)
    If strResponse = vbNo Then Exit Sub
    strResponse = MsgBox("Would you like to flag this appointment as needing a dispo?", vbYesNo)
    If strResponse = vbYes Then
      Manual_Update("Update", "Schedule", "Dispo_Done = 0", "Date = " & FilterByDate(DryDispo.Rows(drDryDispo).Item("Appt_Date")) & " And Person = '" & DryDispo.Rows(drDryDispo).Item("Estimator") & "'")
    End If
    If DryDispo.Rows.Count > 0 Then
      If DryDispo.Rows(drDryDispo).Item("Combined") & "" = True Then
        Manual_Update("DELETE", "Magic_Dispo", "", "ID = " & DryDispo.Rows(drDryDispo).Item("Magic_Dispo_ID"))
        Manual_Update("DELETE", "Magic_Production", "", "Dispo_ID = " & DryDispo.Rows(drDryDispo).Item("Magic_Dispo_ID"))
      End If
      Manual_Update("DELETE", "Dry_Production", "", "Dispo_ID = " & dblDryDispoID & "")
      Manual_Update("DELETE", "Dry_Dispo", "", "ID = " & dblDryDispoID & "")
    ElseIf MagicDispo.Rows.Count > 0 Then
      Manual_Update("DELETE", "Magic_Dispo", "", "ID = " & MagicDispo.Rows(drMagicDispo).Item("ID"))
      Manual_Update("DELETE", "Magic_Production", "", "Dispo_ID = " & MagicDispo.Rows(drMagicDispo).Item("ID"))
    End If
      Dispo_Clear()
      Me.Close()
      Me.Visible = False
      Me.Show()
  End Sub
End Class