﻿Module Connection_Manager
  Public conString As String
  Public con As System.Data.SqlClient.SqlConnection
  Public dadapter As System.Data.SqlClient.SqlDataAdapter
  Dim Container As System.Object

  Public strValues1 As String
  Public strValues2 As String
  Public strValues3 As String
  Public strValues4 As String
  Public strValues5 As String
  Public strFields1 As String
  Public strFields2 As String
  Public strFields3 As String
  Public strFields4 As String
  Public strFields5 As String
  Public strSaveSQL1 As String
  Public strSaveSQL2 As String
  Public strSaveSQL3 As String
  Public strSaveSQL4 As String
  Public strSaveSQL5 As String
  Public strInsert_Values1 As String
  Public strInsert_Values2 As String
  Public strInsert_Values3 As String
  Public strInsert_Values4 As String
  Public strInsert_Values5 As String

  Public Sub Open_DB()

    conString = My.Settings.DefaultConnectionString

    con = New SqlClient.SqlConnection(conString)

    Try
      con.Open()
    Catch ex As Exception
      MsgBox("Unable to open database. Closing program.")
      LogException(ex)
      frmStart.Close()
    End Try

  End Sub

  Public Sub Close_DB()
    con.Close()
  End Sub

  Public Sub Connect(
    ByVal Table As String,
    ByVal RST As DataTable,
    ByVal ROW As Double,
    Optional ByVal Filter As String = "1=1",
    Optional ByVal Order As String = "ID")

    dadapter = New System.Data.SqlClient.SqlDataAdapter(Trim("Select * From " & Table & " Where " & Filter & " Order By " & Order), con)

    If IsDBNull(dadapter) = True Then
      MsgBox("Failed to read: " & Table)
      con.Close()
      Exit Sub
    End If
    RST.Clear()
    Try
      dadapter.Fill(RST)
    Catch ex As Exception
      MsgBox(Err.Number & " / " & Err.Description)
      LogException(ex)
    End Try
    ROW = 0

  End Sub

  Public Sub LogException(ByVal ex As Exception)

    Try
      Dim filepath As String = System.IO.Path.Combine(My.Application.Info.DirectoryPath, "ExceptionLog.txt")

      System.IO.File.AppendAllText(
        filepath,
        String.Format(
          "{0} {1} {2} {3} {4} {5}{6}",
          DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"),
          DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"),
          Environment.MachineName,
          Environment.UserName,
          "02.20131014.01",
          ex.ToString(),
          Environment.NewLine & Environment.NewLine))

    Catch ex2 As Exception
      ' do nothing
    End Try

  End Sub

  Public Function FilterByDate(ByVal DateValue As Date)

    Return " '" & DateValue.ToString("yyyy-MM-dd") & "' "

  End Function

  Public Sub Compiler(
    ByVal Save_Type As String,
    ByVal Source As System.Object,
    ByVal tag1 As String,
    Optional ByVal tag2 As String = "NONE",
    Optional ByVal tag3 As String = "NONE",
    Optional ByVal tag4 As String = "NONE",
    Optional ByVal tag5 As String = "NONE",
    Optional ByVal Combine As Boolean = False)
    strValues1 = ""
    strValues2 = ""
    strValues3 = ""
    strValues4 = ""
    strValues5 = ""
    strFields1 = ""
    strFields2 = ""
    strFields3 = ""
    strFields4 = ""
    strFields5 = ""
    strInsert_Values1 = ""
    strInsert_Values2 = ""
    strInsert_Values3 = ""
    strInsert_Values4 = ""
    strInsert_Values5 = ""
    If UCase(Save_Type) = "UPDATE" Then
      strSaveSQL1 = "Set "
      strSaveSQL2 = "Set "
      strSaveSQL3 = "Set "
      strSaveSQL4 = "Set "
      strSaveSQL5 = "Set "
    Else
      strSaveSQL1 = ""
      strSaveSQL2 = ""
      strSaveSQL3 = ""
      strSaveSQL4 = ""
      strSaveSQL5 = ""
    End If
    Try
      Save_Compiler(Save_Type, Source, tag1, tag2, tag3, tag4, tag5)
      If UCase(Save_Type) = "UPDATE" Then
        strSaveSQL1 = RT(strSaveSQL1, 2)
        strSaveSQL2 = RT(strSaveSQL2, 2)
        strSaveSQL3 = RT(strSaveSQL3, 2)
        strSaveSQL4 = RT(strSaveSQL4, 2)
        strSaveSQL5 = RT(strSaveSQL5, 2)
      ElseIf UCase(Save_Type) = "INSERT INTO" Then
        If Combine = False Then
          strSaveSQL1 = "(" & RT(strFields1, 2) & ") Values(" & RT(strInsert_Values1, 2) & ") "
          strSaveSQL2 = "(" & RT(strFields2, 2) & ") Values(" & RT(strInsert_Values2, 2) & ") "
          strSaveSQL3 = "(" & RT(strFields3, 2) & ") Values(" & RT(strInsert_Values3, 2) & ") "
          strSaveSQL4 = "(" & RT(strFields4, 2) & ") Values(" & RT(strInsert_Values4, 2) & ") "
          strSaveSQL5 = "(" & RT(strFields5, 2) & ") Values(" & RT(strInsert_Values5, 2) & ") "
        Else
          strSaveSQL1 = "(" & RT(strFields1 & strFields2 & strFields3 & strFields4 & strFields5, 2) & ") Values(" & RT(strInsert_Values1 & strInsert_Values2 & strInsert_Values3 & strInsert_Values4 & strInsert_Values5, 2) & ") "
          strValues1 = strValues1 & strValues2
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Public Sub Save_Compiler(ByVal Save_Type As String, ByVal Source As System.Object, ByVal tag1 As String, Optional ByVal tag2 As String = "NONE", Optional ByVal tag3 As String = "NONE", Optional ByVal tag4 As String = "NONE", Optional ByVal tag5 As String = "NONE", Optional ByVal Location As String = "")
    Dim int1 As Integer
    Dim objKeeper As Object
    Dim strLocation As String = ""

    If Location = "" Then
      If Source.GetType.BaseType.Name <> "Form" Then
        strLocation = Source.name
        objKeeper = Source
        While objKeeper.parent.GetType.BaseType.Name <> "Form"
          strLocation = objKeeper.parent.name & "." & strLocation
          objKeeper = objKeeper.parent
        End While
        Location = strLocation
      End If
      Location = Source.name
    End If

    If UCase(Save_Type) = "INSERT INTO" Then
      Try
        'Debug.Print(Str(Source.name))
        For int1 = 0 To Source.Controls.Count - 1
          If Source.Controls(int1).Tag & "" = tag1 Then
            'obtains the corresponding field name to be used later
            strFields1 = strFields1 & "[" & LT(Source.Controls(int1).Name, 3) & "], "
            strInsert_Values1 = strInsert_Values1 & "@" & Source.Controls(int1).Name & ", "
            If Location <> "" Then
              strValues1 = strValues1 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues1 = strValues1 & "@" & Source.controls(int1).name & ""
            End If
          ElseIf Source.Controls(int1).Tag & "" = tag2 Then
            'obtains the corresponding field name to be used later
            strFields2 = strFields2 & "[" & LT(Source.Controls(int1).Name, 3) & "], "
            strInsert_Values2 = strInsert_Values2 & "@" & Source.Controls(int1).Name & ", "
            If Location <> "" Then
              strValues2 = strValues2 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues2 = strValues2 & "@" & Source.controls(int1).name & ""
            End If
          ElseIf Source.Controls(int1).Tag & "" = tag3 Then
            'obtains the corresponding field name to be used later
            strFields3 = strFields3 & "[" & LT(Source.Controls(int1).Name, 3) & "], "
            strInsert_Values3 = strInsert_Values3 & "@" & Source.Controls(int1).Name & ", "
            If Location <> "" Then
              strValues3 = strValues3 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues3 = strValues3 & "@" & Source.controls(int1).name & ""
            End If
          ElseIf Source.Controls(int1).Tag & "" = tag4 Then
            'obtains the corresponding field name to be used later
            strFields4 = strFields4 & "[" & LT(Source.Controls(int1).Name, 3) & "], "
            strInsert_Values4 = strInsert_Values4 & "@" & Source.Controls(int1).Name & ", "
            If Location <> "" Then
              strValues4 = strValues4 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues4 = strValues4 & "@" & Source.controls(int1).name & ""
            End If
          ElseIf Source.Controls(int1).Tag & "" = tag5 Then
            'obtains the corresponding field name to be used later
            strFields5 = strFields5 & "[" & LT(Source.Controls(int1).Name, 3) & "], "
            strInsert_Values5 = strInsert_Values5 & "@" & Source.Controls(int1).Name & ", "
            If Location <> "" Then
              strValues5 = strValues5 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues5 = strValues5 & "@" & Source.controls(int1).name & ""
            End If
          ElseIf Source.Controls(int1).HasChildren = True Then
            Save_Compiler(Save_Type, Source.controls(int1), tag1, tag2, tag3, tag4, tag5, Location & "." & Source.controls(int1).name)
          End If
        Next

      Catch ex As Exception
        MsgBox("Could not compile information. Please make sure the correct information was provided.")
      End Try
    ElseIf UCase(Save_Type) = "UPDATE" Then
      Try
        Debug.Print(Source.name)
        For int1 = 0 To Source.Controls.Count - 1
          If Source.Controls(int1).Tag & "" = tag1 Then
            'obtains the complete reference name for future use
            If Location <> "" Then
              strValues1 = strValues1 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues1 = strValues1 & "@" & Source.controls(int1).name & ""
            End If
            strSaveSQL1 = strSaveSQL1 & "[" & LT(Source.Controls(int1).Name, 3) & "] = @" & Source.Controls(int1).Name & ", "
          ElseIf Source.Controls(int1).Tag & "" = tag2 Then
            'obtains the complete reference name for future use
            If Location <> "" Then
              strValues2 = strValues2 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues2 = strValues2 & "@" & Source.controls(int1).name & ""
            End If
            strSaveSQL2 = strSaveSQL2 & "[" & LT(Source.Controls(int1).Name, 3) & "] = @" & Source.Controls(int1).Name & ", "
          ElseIf Source.Controls(int1).Tag & "" = tag3 Then
            'obtains the complete reference name for future use
            If Location <> "" Then
              strValues3 = strValues3 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues3 = strValues3 & "@" & Source.controls(int1).name & ""
            End If
            strSaveSQL3 = strSaveSQL3 & "[" & LT(Source.Controls(int1).Name, 3) & "] = @" & Source.Controls(int1).Name & ", "
          ElseIf Source.Controls(int1).Tag & "" = tag4 Then
            'obtains the complete reference name for future use
            If Location <> "" Then
              strValues4 = strValues4 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues4 = strValues4 & "@" & Source.controls(int1).name & ""
            End If
            strSaveSQL4 = strSaveSQL4 & "[" & LT(Source.Controls(int1).Name, 3) & "] = @" & Source.Controls(int1).Name & ", "
          ElseIf Source.Controls(int1).Tag & "" = tag5 Then
            'obtains the complete reference name for future use
            If Location <> "" Then
              strValues5 = strValues5 & "@" & Location & "." & Source.controls(int1).name & ""
            Else
              strValues5 = strValues5 & "@" & Source.controls(int1).name & ""
            End If
            strSaveSQL5 = strSaveSQL5 & "[" & LT(Source.Controls(int1).Name, 3) & "] = @" & Source.Controls(int1).Name & ", "
          ElseIf Source.Controls(int1).HasChildren = True Then
            Save_Compiler(Save_Type, Source.controls(int1), tag1, tag2, tag3, tag4, tag5, Location & "." & Source.controls(int1).name)
          End If
        Next
      Catch ex As Exception
        MsgBox("Could not compile information. Please make sure the correct information was provided.")
      End Try
    End If

  End Sub

  Public Function Control_Return(ByVal Source_Form As Object, ByVal Complete_Name As String)
    Dim control As Control
    Dim txtTemp As New TextBox

    If Source_Form.GetType.BaseType.Name <> "Form" Then
      Source_Form = Source_Form.parent
    End If
    control = Source_Form
    If Complete_Name.Contains(Source_Form.name) = False Then
      While Complete_Name.Contains(".")
        Complete_Name = LT(Complete_Name, InStr(Complete_Name, ".", CompareMethod.Text))
      End While
      Return Control_Finder(Source_Form, Complete_Name)
    Else
      Complete_Name = Complete_Name.Replace(Source_Form.name, "")
      Complete_Name = LT(Complete_Name, 1)
      txtTemp.Text = Complete_Name
      While txtTemp.Text.Contains(".")
        txtTemp.Select(0, InStr(txtTemp.Text, ".", CompareMethod.Text) - 1)
        control = control.Controls(txtTemp.SelectedText)
        txtTemp.Text = LT(txtTemp.Text, Len(txtTemp.SelectedText) + 1)
      End While
      Return control.Controls(txtTemp.Text)
    End If


    Return Nothing
  End Function

  Public Sub TableUpdate(
  ByVal Action As String,
  ByVal Table As String,
  ByVal Save_oledb As String,
  ByVal Values As String,
  ByVal Source As System.Object,
  Optional ByVal Where As String = "1=1",
  Optional ByVal RSTUPDATE As DataTable = Nothing)
    Dim text As New TextBox
    Dim Owner As System.Object
    Dim dblRows As Double
    flgFailed = False

    Dim cmd As SqlClient.SqlCommand
    If UCase(Action) = "INSERT INTO" Then
      cmd = New SqlClient.SqlCommand(Action & " " & Table & " " & Save_oledb, con)
      If RSTUPDATE IsNot Nothing Then
        RSTUPDATE.NewRow()
      End If
    ElseIf UCase(Action) = "UPDATE" Then
      cmd = New SqlClient.SqlCommand(Action & " " & Table & " " & Save_oledb & " WHERE " & Where, con)
    ElseIf UCase(Action) = "DELETE" Then
      If Where = "1=1" Then
        MsgBox("An internal error has stopped the program from executing the oledb statement. Please contact an administrator.")
        Exit Sub
      End If
      cmd = New SqlClient.SqlCommand("Delete From " & Table & " Where " & Where, con)
      Try
        cmd.Connection.Open()
        cmd.ExecuteNonQuery()
        con.Close()
      Catch ex As Exception
        MsgBox("Unable to delete records.")
        con.Close()
        Exit Sub
      End Try
    Else
      cmd = Nothing
      MsgBox("Data Table will not update because no action was given.")
      Exit Sub
    End If

    text.Text = Values
    While InStr(text.Text, "@", CompareMethod.Text) > 0
      text.Text = Replace(text.Text, "@", "", , 1)
      text.Select(0, InStr(text.Text, "@", CompareMethod.Text) - 1)

      If text.SelectedText = "" Then
        Owner = Control_Return(Source, text.Text)
      Else
        Owner = Control_Return(Source, text.SelectedText)
      End If
      strResponse = "@" & Owner.name
      text.Text = LT(text.Text, Len(text.SelectedText))
      Try
        Debug.Print(Owner.name & "/" & Owner.text)
        Debug.Print(TypeOf (Owner) Is CheckBox)
        If TypeOf Owner Is CheckBox Or TypeOf Owner Is RadioButton Then
          cmd.Parameters.AddWithValue("@" & Owner.name, Owner.checked)
          If UCase(Action) = "INSERT INTO" And RSTUPDATE IsNot Nothing Then
            RSTUPDATE.Rows(RSTUPDATE.Rows.Count - 1).Item(LT(Owner.name, 3)) = Owner.checked
          ElseIf RSTUPDATE IsNot Nothing Then
            Dim test() As DataRow = RSTUPDATE.Select(Where)

            For dblRows = 0 To UBound(test)
              test(dblRows)(LT(Owner.name, 3)) = Owner.checked
            Next
          End If
        Else
          'Debug.Print(strResponse & " " & Trim(UCase(Owner.text)))
          If TypeOf Owner Is DateTimePicker Then
            cmd.Parameters.AddWithValue("@" & Owner.name, Trim(UCase(Owner.value)) & "")
          Else
            cmd.Parameters.AddWithValue("@" & Owner.name, Trim(UCase(Owner.Text)) & "")
          End If
          'cmd.Parameters.AddWithValue("@" & Owner.name, "7/16/2010")
          If UCase(Action) = "INSERT INTO" And RSTUPDATE IsNot Nothing Then
            RSTUPDATE.Rows(RSTUPDATE.Rows.Count - 1).Item(LT(Owner.name, 3)) = Trim(UCase(Owner.Text))
            RSTUPDATE.AcceptChanges()
          ElseIf RSTUPDATE IsNot Nothing Then
            Dim test() As DataRow = RSTUPDATE.Select(Where)
            For dblRows = 0 To UBound(test)
              test(dblRows)(LT(Owner.name, 3)) = Trim(UCase(Owner.text))
            Next
            RSTUPDATE.AcceptChanges()
          End If
        End If
      Catch ex As Exception
        flgFailed = MsgBox("Save Failed.")
        If RSTUPDATE IsNot Nothing Then RSTUPDATE.RejectChanges()
        con.Close()
        Exit Sub
      End Try
    End While
    Try
      cmd.ExecuteNonQuery()
    Catch ex As Exception
      flgFailed = MsgBox("Save Failed.")
      If RSTUPDATE IsNot Nothing Then
        RSTUPDATE.RejectChanges()
      End If
      con.Close()
      Exit Sub
    End Try
    If RSTUPDATE IsNot Nothing Then
      RSTUPDATE.AcceptChanges()
    End If
    'con.Close()
  End Sub

  Public Sub Manual_Update(ByVal Action As String, ByVal Table As String, ByVal Values As String, Optional ByVal Where As String = "1=1", Optional ByVal RSTUPDATE As DataTable = Nothing)

    flgFailed = False

    Dim cmd As SqlClient.SqlCommand
    If UCase(Action) = "INSERT INTO" Then
      cmd = New SqlClient.SqlCommand(Action & " " & Table & " " & Values, con)
      If RSTUPDATE IsNot Nothing Then
        RSTUPDATE.NewRow()
      End If
    ElseIf UCase(Action) = "UPDATE" Then
      cmd = New SqlClient.SqlCommand(Action & " " & Table & " SET " & Values & " WHERE " & Where, con)
    ElseIf UCase(Action) = "DELETE" Then
      If Where = "1=1" Then
        MsgBox("An internal error has stopped the program from executing the oledb statement. Please contact an administrator.")
        Exit Sub
      End If
      cmd = New SqlClient.SqlCommand("Delete From " & Table & " Where " & Where, con)
      Try
        cmd.Connection.Open()
        cmd.ExecuteNonQuery()
        con.Close()
      Catch ex As Exception
        MsgBox("Unable to delete records.")
        con.Close()
        Exit Sub
      End Try
    Else
      cmd = Nothing
    End If

    Try
      cmd.ExecuteNonQuery()
    Catch ex As Exception
      flgFailed = MsgBox("Save Failed. " & ErrorToString())
      If RSTUPDATE IsNot Nothing Then
        RSTUPDATE.RejectChanges()
      End If
      con.Close()
      Exit Sub
    End Try
    If RSTUPDATE IsNot Nothing Then
      RSTUPDATE.AcceptChanges()
    End If
    'con.Close()
  End Sub

  Public Function Control_Finder(ByVal Source As System.Object, ByVal Control_Name As String)
    Dim int1 As Integer
    Dim myControlName As System.Object
    myControlName = Nothing
    Try
      For int1 = 0 To Source.Controls.Count - 1
        If Source.Controls(int1).Name = Control_Name Then
          Debug.Print(Source.controls(int1).name)
          Return Source.Controls(int1)
          Exit Function
        ElseIf Source.Controls(int1).HasChildren = True Then
          Try
            myControlName = Control_Finder(Source.controls(int1), Control_Name)
            If myControlName.name = Control_Name Then
              Debug.Print(Source.controls(int1).name)
              Return myControlName
              Exit Function
            End If
          Catch ex As Exception When myControlName = Nothing
            'do nothing
          End Try
        End If
      Next
      Return Nothing
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  Public Sub Table_Compiler(ByVal Table As DataTable, ByVal Row As Double)
    Dim intFields As Integer

    strFields1 = ""
    strValues1 = ""
    Try
      For intFields = 1 To Table.Columns.Count - 1
        strFields1 = strFields1 & "[" & Table.Columns(intFields).Caption & "], "
        strValues1 = strValues1 & "@" & Table.Rows(Row).Item(Table.Columns(intFields).Caption) & ", "
      Next
    Catch ex As Exception
      flgFailed = MsgBox("Could not compile.")
    End Try
  End Sub
End Module
