﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUp_Keep
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.tabUpKeep = New System.Windows.Forms.TabControl()
    Me.tpdProducts = New System.Windows.Forms.TabPage()
    Me.cmdAddProduct = New System.Windows.Forms.Button()
    Me.fraProduct = New System.Windows.Forms.GroupBox()
    Me.cmdProductExit = New System.Windows.Forms.Button()
    Me.lblPlacement = New System.Windows.Forms.Label()
    Me.lblCategory = New System.Windows.Forms.Label()
    Me.lblMeasurement = New System.Windows.Forms.Label()
    Me.lblAbbr = New System.Windows.Forms.Label()
    Me.lblCompany = New System.Windows.Forms.Label()
    Me.lblProductField = New System.Windows.Forms.Label()
    Me.lblProduct = New System.Windows.Forms.Label()
    Me.txtPlacement = New System.Windows.Forms.TextBox()
    Me.chkActive = New System.Windows.Forms.CheckBox()
    Me.cboCategory = New System.Windows.Forms.ComboBox()
    Me.cboMeasurement = New System.Windows.Forms.ComboBox()
    Me.txtAbbr = New System.Windows.Forms.TextBox()
    Me.cboCompany = New System.Windows.Forms.ComboBox()
    Me.txtProduct_Field = New System.Windows.Forms.TextBox()
    Me.txtProduct = New System.Windows.Forms.TextBox()
    Me.cmdProductSave = New System.Windows.Forms.Button()
    Me.grdProducts = New System.Windows.Forms.DataGridView()
    Me.tpdInventory = New System.Windows.Forms.TabPage()
    Me.fraInventory = New System.Windows.Forms.GroupBox()
    Me.txtInvProduct = New System.Windows.Forms.TextBox()
    Me.cmdInvClear = New System.Windows.Forms.Button()
    Me.txtMeasurement = New System.Windows.Forms.TextBox()
    Me.lblFormula = New System.Windows.Forms.Label()
    Me.txtQuanity = New System.Windows.Forms.TextBox()
    Me.lblQuanity = New System.Windows.Forms.Label()
    Me.cboItemNumber = New System.Windows.Forms.ComboBox()
    Me.lblInvNumber = New System.Windows.Forms.Label()
    Me.cboItem = New System.Windows.Forms.ComboBox()
    Me.lblItem = New System.Windows.Forms.Label()
    Me.lblInvProduct = New System.Windows.Forms.Label()
    Me.cmdInvExit = New System.Windows.Forms.Button()
    Me.cmdInvSave = New System.Windows.Forms.Button()
    Me.grdInventory = New System.Windows.Forms.DataGridView()
    Me.tpgSource = New System.Windows.Forms.TabPage()
    Me.cmdSourceAdd = New System.Windows.Forms.Button()
    Me.grdSource = New System.Windows.Forms.DataGridView()
    Me.DataGridView1 = New System.Windows.Forms.DataGridView()
    Me.tabUpKeep.SuspendLayout()
    Me.tpdProducts.SuspendLayout()
    Me.fraProduct.SuspendLayout()
    CType(Me.grdProducts, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.tpdInventory.SuspendLayout()
    Me.fraInventory.SuspendLayout()
    CType(Me.grdInventory, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.tpgSource.SuspendLayout()
    CType(Me.grdSource, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'tabUpKeep
    '
    Me.tabUpKeep.Controls.Add(Me.tpdProducts)
    Me.tabUpKeep.Controls.Add(Me.tpdInventory)
    Me.tabUpKeep.Controls.Add(Me.tpgSource)
    Me.tabUpKeep.Location = New System.Drawing.Point(12, 12)
    Me.tabUpKeep.Name = "tabUpKeep"
    Me.tabUpKeep.SelectedIndex = 0
    Me.tabUpKeep.Size = New System.Drawing.Size(795, 407)
    Me.tabUpKeep.TabIndex = 0
    '
    'tpdProducts
    '
    Me.tpdProducts.Controls.Add(Me.cmdAddProduct)
    Me.tpdProducts.Controls.Add(Me.fraProduct)
    Me.tpdProducts.Controls.Add(Me.grdProducts)
    Me.tpdProducts.Location = New System.Drawing.Point(4, 22)
    Me.tpdProducts.Name = "tpdProducts"
    Me.tpdProducts.Padding = New System.Windows.Forms.Padding(3)
    Me.tpdProducts.Size = New System.Drawing.Size(787, 381)
    Me.tpdProducts.TabIndex = 0
    Me.tpdProducts.Text = "Products"
    Me.tpdProducts.UseVisualStyleBackColor = True
    '
    'cmdAddProduct
    '
    Me.cmdAddProduct.Location = New System.Drawing.Point(6, 6)
    Me.cmdAddProduct.Name = "cmdAddProduct"
    Me.cmdAddProduct.Size = New System.Drawing.Size(72, 26)
    Me.cmdAddProduct.TabIndex = 4
    Me.cmdAddProduct.Text = "Add Product"
    Me.cmdAddProduct.UseVisualStyleBackColor = True
    '
    'fraProduct
    '
    Me.fraProduct.Controls.Add(Me.cmdProductExit)
    Me.fraProduct.Controls.Add(Me.lblPlacement)
    Me.fraProduct.Controls.Add(Me.lblCategory)
    Me.fraProduct.Controls.Add(Me.lblMeasurement)
    Me.fraProduct.Controls.Add(Me.lblAbbr)
    Me.fraProduct.Controls.Add(Me.lblCompany)
    Me.fraProduct.Controls.Add(Me.lblProductField)
    Me.fraProduct.Controls.Add(Me.lblProduct)
    Me.fraProduct.Controls.Add(Me.txtPlacement)
    Me.fraProduct.Controls.Add(Me.chkActive)
    Me.fraProduct.Controls.Add(Me.cboCategory)
    Me.fraProduct.Controls.Add(Me.cboMeasurement)
    Me.fraProduct.Controls.Add(Me.txtAbbr)
    Me.fraProduct.Controls.Add(Me.cboCompany)
    Me.fraProduct.Controls.Add(Me.txtProduct_Field)
    Me.fraProduct.Controls.Add(Me.txtProduct)
    Me.fraProduct.Controls.Add(Me.cmdProductSave)
    Me.fraProduct.Location = New System.Drawing.Point(211, 61)
    Me.fraProduct.Name = "fraProduct"
    Me.fraProduct.Size = New System.Drawing.Size(385, 286)
    Me.fraProduct.TabIndex = 3
    Me.fraProduct.TabStop = False
    Me.fraProduct.Text = "Product Edit"
    Me.fraProduct.Visible = False
    '
    'cmdProductExit
    '
    Me.cmdProductExit.Location = New System.Drawing.Point(359, 0)
    Me.cmdProductExit.Name = "cmdProductExit"
    Me.cmdProductExit.Size = New System.Drawing.Size(26, 24)
    Me.cmdProductExit.TabIndex = 18
    Me.cmdProductExit.Text = "X"
    Me.cmdProductExit.UseVisualStyleBackColor = True
    '
    'lblPlacement
    '
    Me.lblPlacement.AutoSize = True
    Me.lblPlacement.Location = New System.Drawing.Point(6, 207)
    Me.lblPlacement.Name = "lblPlacement"
    Me.lblPlacement.Size = New System.Drawing.Size(57, 13)
    Me.lblPlacement.TabIndex = 16
    Me.lblPlacement.Text = "Placement"
    '
    'lblCategory
    '
    Me.lblCategory.AutoSize = True
    Me.lblCategory.Location = New System.Drawing.Point(6, 180)
    Me.lblCategory.Name = "lblCategory"
    Me.lblCategory.Size = New System.Drawing.Size(49, 13)
    Me.lblCategory.TabIndex = 15
    Me.lblCategory.Text = "Category"
    '
    'lblMeasurement
    '
    Me.lblMeasurement.AutoSize = True
    Me.lblMeasurement.Location = New System.Drawing.Point(6, 153)
    Me.lblMeasurement.Name = "lblMeasurement"
    Me.lblMeasurement.Size = New System.Drawing.Size(71, 13)
    Me.lblMeasurement.TabIndex = 14
    Me.lblMeasurement.Text = "Measurement"
    '
    'lblAbbr
    '
    Me.lblAbbr.AutoSize = True
    Me.lblAbbr.Location = New System.Drawing.Point(6, 127)
    Me.lblAbbr.Name = "lblAbbr"
    Me.lblAbbr.Size = New System.Drawing.Size(29, 13)
    Me.lblAbbr.TabIndex = 13
    Me.lblAbbr.Text = "Abbr"
    '
    'lblCompany
    '
    Me.lblCompany.AutoSize = True
    Me.lblCompany.Location = New System.Drawing.Point(6, 101)
    Me.lblCompany.Name = "lblCompany"
    Me.lblCompany.Size = New System.Drawing.Size(51, 13)
    Me.lblCompany.TabIndex = 12
    Me.lblCompany.Text = "Company"
    '
    'lblProductField
    '
    Me.lblProductField.AutoSize = True
    Me.lblProductField.Location = New System.Drawing.Point(6, 74)
    Me.lblProductField.Name = "lblProductField"
    Me.lblProductField.Size = New System.Drawing.Size(69, 13)
    Me.lblProductField.TabIndex = 11
    Me.lblProductField.Text = "Product Field"
    '
    'lblProduct
    '
    Me.lblProduct.AutoSize = True
    Me.lblProduct.Location = New System.Drawing.Point(6, 48)
    Me.lblProduct.Name = "lblProduct"
    Me.lblProduct.Size = New System.Drawing.Size(44, 13)
    Me.lblProduct.TabIndex = 10
    Me.lblProduct.Text = "Product"
    '
    'txtPlacement
    '
    Me.txtPlacement.Location = New System.Drawing.Point(105, 200)
    Me.txtPlacement.Name = "txtPlacement"
    Me.txtPlacement.Size = New System.Drawing.Size(274, 20)
    Me.txtPlacement.TabIndex = 8
    Me.txtPlacement.Tag = "Product"
    '
    'chkActive
    '
    Me.chkActive.AutoSize = True
    Me.chkActive.Location = New System.Drawing.Point(105, 253)
    Me.chkActive.Name = "chkActive"
    Me.chkActive.Size = New System.Drawing.Size(56, 17)
    Me.chkActive.TabIndex = 7
    Me.chkActive.Tag = "Product"
    Me.chkActive.Text = "Active"
    Me.chkActive.UseVisualStyleBackColor = True
    '
    'cboCategory
    '
    Me.cboCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboCategory.FormattingEnabled = True
    Me.cboCategory.Location = New System.Drawing.Point(105, 173)
    Me.cboCategory.Name = "cboCategory"
    Me.cboCategory.Size = New System.Drawing.Size(274, 21)
    Me.cboCategory.TabIndex = 6
    Me.cboCategory.Tag = "Product"
    '
    'cboMeasurement
    '
    Me.cboMeasurement.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboMeasurement.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboMeasurement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboMeasurement.FormattingEnabled = True
    Me.cboMeasurement.Location = New System.Drawing.Point(105, 146)
    Me.cboMeasurement.Name = "cboMeasurement"
    Me.cboMeasurement.Size = New System.Drawing.Size(274, 21)
    Me.cboMeasurement.TabIndex = 5
    Me.cboMeasurement.Tag = "Product"
    '
    'txtAbbr
    '
    Me.txtAbbr.Location = New System.Drawing.Point(105, 120)
    Me.txtAbbr.Name = "txtAbbr"
    Me.txtAbbr.Size = New System.Drawing.Size(274, 20)
    Me.txtAbbr.TabIndex = 4
    Me.txtAbbr.Tag = "Product"
    '
    'cboCompany
    '
    Me.cboCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboCompany.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboCompany.FormattingEnabled = True
    Me.cboCompany.Items.AddRange(New Object() {"DRY", "MAGIC"})
    Me.cboCompany.Location = New System.Drawing.Point(105, 93)
    Me.cboCompany.Name = "cboCompany"
    Me.cboCompany.Size = New System.Drawing.Size(274, 21)
    Me.cboCompany.TabIndex = 3
    Me.cboCompany.Tag = "Product"
    '
    'txtProduct_Field
    '
    Me.txtProduct_Field.Location = New System.Drawing.Point(105, 67)
    Me.txtProduct_Field.Name = "txtProduct_Field"
    Me.txtProduct_Field.Size = New System.Drawing.Size(274, 20)
    Me.txtProduct_Field.TabIndex = 2
    Me.txtProduct_Field.Tag = "Product"
    '
    'txtProduct
    '
    Me.txtProduct.Location = New System.Drawing.Point(105, 41)
    Me.txtProduct.Name = "txtProduct"
    Me.txtProduct.Size = New System.Drawing.Size(274, 20)
    Me.txtProduct.TabIndex = 1
    Me.txtProduct.Tag = "Product"
    '
    'cmdProductSave
    '
    Me.cmdProductSave.Location = New System.Drawing.Point(291, 257)
    Me.cmdProductSave.Name = "cmdProductSave"
    Me.cmdProductSave.Size = New System.Drawing.Size(88, 23)
    Me.cmdProductSave.TabIndex = 0
    Me.cmdProductSave.Text = "Save"
    Me.cmdProductSave.UseVisualStyleBackColor = True
    '
    'grdProducts
    '
    Me.grdProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdProducts.Location = New System.Drawing.Point(6, 38)
    Me.grdProducts.Name = "grdProducts"
    Me.grdProducts.Size = New System.Drawing.Size(775, 337)
    Me.grdProducts.TabIndex = 0
    '
    'tpdInventory
    '
    Me.tpdInventory.Controls.Add(Me.fraInventory)
    Me.tpdInventory.Controls.Add(Me.grdInventory)
    Me.tpdInventory.Location = New System.Drawing.Point(4, 22)
    Me.tpdInventory.Name = "tpdInventory"
    Me.tpdInventory.Padding = New System.Windows.Forms.Padding(3)
    Me.tpdInventory.Size = New System.Drawing.Size(787, 381)
    Me.tpdInventory.TabIndex = 1
    Me.tpdInventory.Text = "Inventory"
    Me.tpdInventory.UseVisualStyleBackColor = True
    '
    'fraInventory
    '
    Me.fraInventory.Controls.Add(Me.txtInvProduct)
    Me.fraInventory.Controls.Add(Me.cmdInvClear)
    Me.fraInventory.Controls.Add(Me.txtMeasurement)
    Me.fraInventory.Controls.Add(Me.lblFormula)
    Me.fraInventory.Controls.Add(Me.txtQuanity)
    Me.fraInventory.Controls.Add(Me.lblQuanity)
    Me.fraInventory.Controls.Add(Me.cboItemNumber)
    Me.fraInventory.Controls.Add(Me.lblInvNumber)
    Me.fraInventory.Controls.Add(Me.cboItem)
    Me.fraInventory.Controls.Add(Me.lblItem)
    Me.fraInventory.Controls.Add(Me.lblInvProduct)
    Me.fraInventory.Controls.Add(Me.cmdInvExit)
    Me.fraInventory.Controls.Add(Me.cmdInvSave)
    Me.fraInventory.Location = New System.Drawing.Point(211, 61)
    Me.fraInventory.Name = "fraInventory"
    Me.fraInventory.Size = New System.Drawing.Size(385, 215)
    Me.fraInventory.TabIndex = 6
    Me.fraInventory.TabStop = False
    Me.fraInventory.Text = "Inventory Edit"
    Me.fraInventory.Visible = False
    '
    'txtInvProduct
    '
    Me.txtInvProduct.Location = New System.Drawing.Point(123, 45)
    Me.txtInvProduct.Name = "txtInvProduct"
    Me.txtInvProduct.ReadOnly = True
    Me.txtInvProduct.Size = New System.Drawing.Size(256, 20)
    Me.txtInvProduct.TabIndex = 30
    '
    'cmdInvClear
    '
    Me.cmdInvClear.Location = New System.Drawing.Point(265, 1)
    Me.cmdInvClear.Name = "cmdInvClear"
    Me.cmdInvClear.Size = New System.Drawing.Size(88, 23)
    Me.cmdInvClear.TabIndex = 29
    Me.cmdInvClear.Text = "Clear All"
    Me.cmdInvClear.UseVisualStyleBackColor = True
    '
    'txtMeasurement
    '
    Me.txtMeasurement.Location = New System.Drawing.Point(123, 152)
    Me.txtMeasurement.Name = "txtMeasurement"
    Me.txtMeasurement.Size = New System.Drawing.Size(256, 20)
    Me.txtMeasurement.TabIndex = 28
    Me.txtMeasurement.Tag = "Inventory"
    '
    'lblFormula
    '
    Me.lblFormula.AutoSize = True
    Me.lblFormula.Location = New System.Drawing.Point(6, 152)
    Me.lblFormula.Name = "lblFormula"
    Me.lblFormula.Size = New System.Drawing.Size(44, 13)
    Me.lblFormula.TabIndex = 27
    Me.lblFormula.Text = "Formula"
    '
    'txtQuanity
    '
    Me.txtQuanity.Location = New System.Drawing.Point(123, 126)
    Me.txtQuanity.Name = "txtQuanity"
    Me.txtQuanity.Size = New System.Drawing.Size(256, 20)
    Me.txtQuanity.TabIndex = 26
    Me.txtQuanity.Tag = "Inventory"
    '
    'lblQuanity
    '
    Me.lblQuanity.AutoSize = True
    Me.lblQuanity.Location = New System.Drawing.Point(6, 126)
    Me.lblQuanity.Name = "lblQuanity"
    Me.lblQuanity.Size = New System.Drawing.Size(43, 13)
    Me.lblQuanity.TabIndex = 25
    Me.lblQuanity.Text = "Quanity"
    '
    'cboItemNumber
    '
    Me.cboItemNumber.FormattingEnabled = True
    Me.cboItemNumber.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40"})
    Me.cboItemNumber.Location = New System.Drawing.Point(123, 71)
    Me.cboItemNumber.Name = "cboItemNumber"
    Me.cboItemNumber.Size = New System.Drawing.Size(256, 21)
    Me.cboItemNumber.TabIndex = 24
    '
    'lblInvNumber
    '
    Me.lblInvNumber.AutoSize = True
    Me.lblInvNumber.Location = New System.Drawing.Point(6, 71)
    Me.lblInvNumber.Name = "lblInvNumber"
    Me.lblInvNumber.Size = New System.Drawing.Size(67, 13)
    Me.lblInvNumber.TabIndex = 23
    Me.lblInvNumber.Text = "Item Number"
    '
    'cboItem
    '
    Me.cboItem.FormattingEnabled = True
    Me.cboItem.Location = New System.Drawing.Point(123, 99)
    Me.cboItem.Name = "cboItem"
    Me.cboItem.Size = New System.Drawing.Size(256, 21)
    Me.cboItem.TabIndex = 22
    Me.cboItem.Tag = "Inventory"
    '
    'lblItem
    '
    Me.lblItem.AutoSize = True
    Me.lblItem.Location = New System.Drawing.Point(6, 99)
    Me.lblItem.Name = "lblItem"
    Me.lblItem.Size = New System.Drawing.Size(27, 13)
    Me.lblItem.TabIndex = 21
    Me.lblItem.Text = "Item"
    '
    'lblInvProduct
    '
    Me.lblInvProduct.AutoSize = True
    Me.lblInvProduct.Location = New System.Drawing.Point(6, 44)
    Me.lblInvProduct.Name = "lblInvProduct"
    Me.lblInvProduct.Size = New System.Drawing.Size(44, 13)
    Me.lblInvProduct.TabIndex = 19
    Me.lblInvProduct.Text = "Product"
    '
    'cmdInvExit
    '
    Me.cmdInvExit.Location = New System.Drawing.Point(359, 0)
    Me.cmdInvExit.Name = "cmdInvExit"
    Me.cmdInvExit.Size = New System.Drawing.Size(26, 24)
    Me.cmdInvExit.TabIndex = 18
    Me.cmdInvExit.Text = "X"
    Me.cmdInvExit.UseVisualStyleBackColor = True
    '
    'cmdInvSave
    '
    Me.cmdInvSave.Location = New System.Drawing.Point(291, 178)
    Me.cmdInvSave.Name = "cmdInvSave"
    Me.cmdInvSave.Size = New System.Drawing.Size(88, 23)
    Me.cmdInvSave.TabIndex = 0
    Me.cmdInvSave.Text = "Save"
    Me.cmdInvSave.UseVisualStyleBackColor = True
    '
    'grdInventory
    '
    Me.grdInventory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdInventory.Location = New System.Drawing.Point(6, 6)
    Me.grdInventory.Name = "grdInventory"
    Me.grdInventory.Size = New System.Drawing.Size(775, 369)
    Me.grdInventory.TabIndex = 5
    '
    'tpgSource
    '
    Me.tpgSource.Controls.Add(Me.cmdSourceAdd)
    Me.tpgSource.Controls.Add(Me.grdSource)
    Me.tpgSource.Location = New System.Drawing.Point(4, 22)
    Me.tpgSource.Name = "tpgSource"
    Me.tpgSource.Size = New System.Drawing.Size(787, 381)
    Me.tpgSource.TabIndex = 2
    Me.tpgSource.Text = "Sources"
    Me.tpgSource.UseVisualStyleBackColor = True
    '
    'cmdSourceAdd
    '
    Me.cmdSourceAdd.Location = New System.Drawing.Point(6, 6)
    Me.cmdSourceAdd.Name = "cmdSourceAdd"
    Me.cmdSourceAdd.Size = New System.Drawing.Size(72, 26)
    Me.cmdSourceAdd.TabIndex = 7
    Me.cmdSourceAdd.Text = "Add"
    Me.cmdSourceAdd.UseVisualStyleBackColor = True
    '
    'grdSource
    '
    Me.grdSource.AllowUserToAddRows = False
    Me.grdSource.AllowUserToDeleteRows = False
    Me.grdSource.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdSource.Location = New System.Drawing.Point(6, 38)
    Me.grdSource.Name = "grdSource"
    Me.grdSource.Size = New System.Drawing.Size(775, 337)
    Me.grdSource.TabIndex = 5
    '
    'DataGridView1
    '
    Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.DataGridView1.Location = New System.Drawing.Point(6, 38)
    Me.DataGridView1.Name = "DataGridView1"
    Me.DataGridView1.Size = New System.Drawing.Size(775, 337)
    Me.DataGridView1.TabIndex = 0
    '
    'frmUp_Keep
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(819, 452)
    Me.Controls.Add(Me.tabUpKeep)
    Me.Name = "frmUp_Keep"
    Me.Text = "Up Keep"
    Me.tabUpKeep.ResumeLayout(False)
    Me.tpdProducts.ResumeLayout(False)
    Me.fraProduct.ResumeLayout(False)
    Me.fraProduct.PerformLayout()
    CType(Me.grdProducts, System.ComponentModel.ISupportInitialize).EndInit()
    Me.tpdInventory.ResumeLayout(False)
    Me.fraInventory.ResumeLayout(False)
    Me.fraInventory.PerformLayout()
    CType(Me.grdInventory, System.ComponentModel.ISupportInitialize).EndInit()
    Me.tpgSource.ResumeLayout(False)
    CType(Me.grdSource, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tabUpKeep As System.Windows.Forms.TabControl
  Friend WithEvents tpdProducts As System.Windows.Forms.TabPage
  Friend WithEvents fraProduct As System.Windows.Forms.GroupBox
  Friend WithEvents cmdProductExit As System.Windows.Forms.Button
  Friend WithEvents lblPlacement As System.Windows.Forms.Label
  Friend WithEvents lblCategory As System.Windows.Forms.Label
  Friend WithEvents lblMeasurement As System.Windows.Forms.Label
  Friend WithEvents lblAbbr As System.Windows.Forms.Label
  Friend WithEvents lblCompany As System.Windows.Forms.Label
  Friend WithEvents lblProductField As System.Windows.Forms.Label
  Friend WithEvents lblProduct As System.Windows.Forms.Label
  Friend WithEvents txtPlacement As System.Windows.Forms.TextBox
  Friend WithEvents chkActive As System.Windows.Forms.CheckBox
  Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
  Friend WithEvents cboMeasurement As System.Windows.Forms.ComboBox
  Friend WithEvents txtAbbr As System.Windows.Forms.TextBox
  Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
  Friend WithEvents txtProduct_Field As System.Windows.Forms.TextBox
  Friend WithEvents txtProduct As System.Windows.Forms.TextBox
  Friend WithEvents cmdProductSave As System.Windows.Forms.Button
  Friend WithEvents grdProducts As System.Windows.Forms.DataGridView
  Friend WithEvents tpdInventory As System.Windows.Forms.TabPage
  Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
  Friend WithEvents cmdAddProduct As System.Windows.Forms.Button
  Friend WithEvents fraInventory As System.Windows.Forms.GroupBox
  Friend WithEvents cmdInvClear As System.Windows.Forms.Button
  Friend WithEvents txtMeasurement As System.Windows.Forms.TextBox
  Friend WithEvents lblFormula As System.Windows.Forms.Label
  Friend WithEvents txtQuanity As System.Windows.Forms.TextBox
  Friend WithEvents lblQuanity As System.Windows.Forms.Label
  Friend WithEvents cboItemNumber As System.Windows.Forms.ComboBox
  Friend WithEvents lblInvNumber As System.Windows.Forms.Label
  Friend WithEvents cboItem As System.Windows.Forms.ComboBox
  Friend WithEvents lblItem As System.Windows.Forms.Label
  Friend WithEvents lblInvProduct As System.Windows.Forms.Label
  Friend WithEvents cmdInvExit As System.Windows.Forms.Button
  Friend WithEvents cmdInvSave As System.Windows.Forms.Button
  Friend WithEvents grdInventory As System.Windows.Forms.DataGridView
  Friend WithEvents txtInvProduct As System.Windows.Forms.TextBox
  Friend WithEvents tpgSource As System.Windows.Forms.TabPage
  Friend WithEvents cmdSourceAdd As System.Windows.Forms.Button
  Friend WithEvents grdSource As System.Windows.Forms.DataGridView
End Class
