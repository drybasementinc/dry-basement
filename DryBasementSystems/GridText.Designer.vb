﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GridText
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.txtText = New System.Windows.Forms.TextBox

    Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Me.BorderRight = New Microsoft.VisualBasic.PowerPacks.LineShape
    Me.BorderLeft = New Microsoft.VisualBasic.PowerPacks.LineShape
    Me.BorderBottom = New Microsoft.VisualBasic.PowerPacks.LineShape
    Me.BorderTop = New Microsoft.VisualBasic.PowerPacks.LineShape
    Me.CellRow = New System.Windows.Forms.TextBox
    Me.CellCol = New System.Windows.Forms.TextBox
    Me.Fixed = New System.Windows.Forms.CheckBox
    Me.Merged = New System.Windows.Forms.CheckBox
    Me.SuspendLayout()
    '
    'txtText
    '
    Me.txtText.BorderStyle = System.Windows.Forms.BorderStyle.None
    Me.txtText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtText.Location = New System.Drawing.Point(2, 2)
    Me.txtText.Margin = New System.Windows.Forms.Padding(0)
    Me.txtText.Multiline = True
    Me.txtText.Name = "txtText"
    Me.txtText.ReadOnly = True
    Me.txtText.Size = New System.Drawing.Size(103, 22)
    Me.txtText.TabIndex = 0
    '
    'ShapeContainer1
    '
    Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
    Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
    Me.ShapeContainer1.Name = "ShapeContainer1"
    Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.BorderRight, Me.BorderLeft, Me.BorderBottom, Me.BorderTop})
    Me.ShapeContainer1.Size = New System.Drawing.Size(107, 27)
    Me.ShapeContainer1.TabIndex = 1
    Me.ShapeContainer1.TabStop = False
    '
    'BorderRight
    '
    Me.BorderRight.BorderWidth = 2
    Me.BorderRight.Name = "BorderRight"
    Me.BorderRight.Visible = False
    Me.BorderRight.X1 = 105
    Me.BorderRight.X2 = 105
    Me.BorderRight.Y1 = 1
    Me.BorderRight.Y2 = 25
    '
    'BorderLeft
    '
    Me.BorderLeft.BorderWidth = 2
    Me.BorderLeft.Name = "BorderLeft"
    Me.BorderLeft.Visible = False
    Me.BorderLeft.X1 = 1
    Me.BorderLeft.X2 = 1
    Me.BorderLeft.Y1 = 1
    Me.BorderLeft.Y2 = 25
    '
    'BorderBottom
    '
    Me.BorderBottom.BorderWidth = 2
    Me.BorderBottom.Name = "BorderBottom"
    Me.BorderBottom.Visible = False
    Me.BorderBottom.X1 = 1
    Me.BorderBottom.X2 = 105
    Me.BorderBottom.Y1 = 25
    Me.BorderBottom.Y2 = 25
    '
    'BorderTop
    '
    Me.BorderTop.BorderWidth = 2
    Me.BorderTop.Name = "BorderTop"
    Me.BorderTop.Visible = False
    Me.BorderTop.X1 = 1
    Me.BorderTop.X2 = 105
    Me.BorderTop.Y1 = 1
    Me.BorderTop.Y2 = 1
    '
    'CellRow
    '
    Me.CellRow.Location = New System.Drawing.Point(16, 46)
    Me.CellRow.Name = "CellRow"
    Me.CellRow.Size = New System.Drawing.Size(38, 20)
    Me.CellRow.TabIndex = 2
    Me.CellRow.Visible = False
    '
    'CellCol
    '
    Me.CellCol.Location = New System.Drawing.Point(60, 46)
    Me.CellCol.Name = "CellCol"
    Me.CellCol.Size = New System.Drawing.Size(38, 20)
    Me.CellCol.TabIndex = 3
    Me.CellCol.Visible = False
    '
    'Fixed
    '
    Me.Fixed.AutoSize = True
    Me.Fixed.Location = New System.Drawing.Point(16, 72)
    Me.Fixed.Name = "Fixed"
    Me.Fixed.Size = New System.Drawing.Size(81, 17)
    Me.Fixed.TabIndex = 4
    Me.Fixed.Text = "CheckBox1"
    Me.Fixed.UseVisualStyleBackColor = True
    Me.Fixed.Visible = False
    '
    'Merged
    '
    Me.Merged.AutoSize = True
    Me.Merged.Location = New System.Drawing.Point(16, 95)
    Me.Merged.Name = "Merged"
    Me.Merged.Size = New System.Drawing.Size(81, 17)
    Me.Merged.TabIndex = 5
    Me.Merged.Text = "CheckBox2"
    Me.Merged.UseVisualStyleBackColor = True
    Me.Merged.Visible = False
    '
    'GridText
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackColor = System.Drawing.Color.Transparent
    Me.Controls.Add(Me.Merged)
    Me.Controls.Add(Me.Fixed)
    Me.Controls.Add(Me.CellCol)
    Me.Controls.Add(Me.CellRow)
    Me.Controls.Add(Me.txtText)
    Me.Controls.Add(Me.ShapeContainer1)
    Me.Name = "GridText"
    Me.Size = New System.Drawing.Size(107, 27)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents txtText As System.Windows.Forms.TextBox
  Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
  Friend WithEvents BorderBottom As Microsoft.VisualBasic.PowerPacks.LineShape
  Friend WithEvents BorderTop As Microsoft.VisualBasic.PowerPacks.LineShape
  Friend WithEvents BorderRight As Microsoft.VisualBasic.PowerPacks.LineShape
  Friend WithEvents BorderLeft As Microsoft.VisualBasic.PowerPacks.LineShape
  Friend WithEvents CellRow As System.Windows.Forms.TextBox
  Friend WithEvents CellCol As System.Windows.Forms.TextBox
  Friend WithEvents Fixed As System.Windows.Forms.CheckBox
  Friend WithEvents Merged As System.Windows.Forms.CheckBox

End Class
