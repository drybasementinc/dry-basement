﻿Public Class frmMultiAddress
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  'Form Specific Declarations
  Public dblMulti_ID As Double

  Private Sub MultiAddress_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    dblMulti_ID = 0

    grdMulti.AlternatingRowsDefaultCellStyle.BackColor = Color.AliceBlue
    Grid_Lock(Me, grdMulti)
  End Sub

  Private Sub cmdNoMatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNoMatch.Click
    dblMulti_ID = 0
    Me.Close()
  End Sub

  Private Sub grdMulti_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

  End Sub

  Private Sub grdMulti_CellDoubleClick1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMulti.CellDoubleClick
   
    dblMulti_ID = grdMulti.Item(e.ColumnIndex, e.RowIndex).Tag
   
    Me.Close()
  End Sub

  Private Sub grdMulti_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMulti.CellContentClick

  End Sub

  Private Sub cmdNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNew.Click
    flgNewAddress = True
  End Sub
End Class