﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInventory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.grdInventory = New System.Windows.Forms.DataGridView()
    Me.fraInventory = New System.Windows.Forms.GroupBox()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.cmdSave = New System.Windows.Forms.Button()
    Me.txtMod = New System.Windows.Forms.TextBox()
    Me.txtQuantity = New System.Windows.Forms.TextBox()
    Me.txtItem_Description = New System.Windows.Forms.TextBox()
    Me.txtIem_Code = New System.Windows.Forms.TextBox()
    Me.cmdClose = New System.Windows.Forms.Button()
    Me.dtpInventory = New System.Windows.Forms.DateTimePicker()
    CType(Me.grdInventory, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.fraInventory.SuspendLayout()
    Me.SuspendLayout()
    '
    'grdInventory
    '
    Me.grdInventory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdInventory.Location = New System.Drawing.Point(12, 55)
    Me.grdInventory.Name = "grdInventory"
    Me.grdInventory.Size = New System.Drawing.Size(700, 248)
    Me.grdInventory.TabIndex = 0
    '
    'fraInventory
    '
    Me.fraInventory.Controls.Add(Me.Label3)
    Me.fraInventory.Controls.Add(Me.Label4)
    Me.fraInventory.Controls.Add(Me.Label2)
    Me.fraInventory.Controls.Add(Me.Label1)
    Me.fraInventory.Controls.Add(Me.cmdSave)
    Me.fraInventory.Controls.Add(Me.txtMod)
    Me.fraInventory.Controls.Add(Me.txtQuantity)
    Me.fraInventory.Controls.Add(Me.txtItem_Description)
    Me.fraInventory.Controls.Add(Me.txtIem_Code)
    Me.fraInventory.Controls.Add(Me.cmdClose)
    Me.fraInventory.Location = New System.Drawing.Point(154, 114)
    Me.fraInventory.Name = "fraInventory"
    Me.fraInventory.Size = New System.Drawing.Size(476, 189)
    Me.fraInventory.TabIndex = 1
    Me.fraInventory.TabStop = False
    Me.fraInventory.Text = "Inventory Management"
    Me.fraInventory.Visible = False
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(6, 110)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(71, 13)
    Me.Label3.TabIndex = 9
    Me.Label3.Text = "Add/Remove"
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Location = New System.Drawing.Point(6, 84)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(46, 13)
    Me.Label4.TabIndex = 8
    Me.Label4.Text = "Quantity"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(6, 58)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(83, 13)
    Me.Label2.TabIndex = 7
    Me.Label2.Text = "Item Description"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(6, 32)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(55, 13)
    Me.Label1.TabIndex = 6
    Me.Label1.Text = "Item Code"
    '
    'cmdSave
    '
    Me.cmdSave.Location = New System.Drawing.Point(356, 149)
    Me.cmdSave.Name = "cmdSave"
    Me.cmdSave.Size = New System.Drawing.Size(92, 28)
    Me.cmdSave.TabIndex = 5
    Me.cmdSave.Text = "Save"
    Me.cmdSave.UseVisualStyleBackColor = True
    '
    'txtMod
    '
    Me.txtMod.Location = New System.Drawing.Point(314, 107)
    Me.txtMod.Name = "txtMod"
    Me.txtMod.Size = New System.Drawing.Size(156, 20)
    Me.txtMod.TabIndex = 4
    '
    'txtQuantity
    '
    Me.txtQuantity.Location = New System.Drawing.Point(314, 81)
    Me.txtQuantity.Name = "txtQuantity"
    Me.txtQuantity.ReadOnly = True
    Me.txtQuantity.Size = New System.Drawing.Size(156, 20)
    Me.txtQuantity.TabIndex = 3
    '
    'txtItem_Description
    '
    Me.txtItem_Description.Location = New System.Drawing.Point(314, 55)
    Me.txtItem_Description.Name = "txtItem_Description"
    Me.txtItem_Description.ReadOnly = True
    Me.txtItem_Description.Size = New System.Drawing.Size(156, 20)
    Me.txtItem_Description.TabIndex = 2
    '
    'txtIem_Code
    '
    Me.txtIem_Code.Location = New System.Drawing.Point(314, 29)
    Me.txtIem_Code.Name = "txtIem_Code"
    Me.txtIem_Code.ReadOnly = True
    Me.txtIem_Code.Size = New System.Drawing.Size(156, 20)
    Me.txtIem_Code.TabIndex = 1
    '
    'cmdClose
    '
    Me.cmdClose.Location = New System.Drawing.Point(448, 0)
    Me.cmdClose.Name = "cmdClose"
    Me.cmdClose.Size = New System.Drawing.Size(22, 23)
    Me.cmdClose.TabIndex = 0
    Me.cmdClose.Text = "X"
    Me.cmdClose.UseVisualStyleBackColor = True
    '
    'dtpInventory
    '
    Me.dtpInventory.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpInventory.Location = New System.Drawing.Point(593, 25)
    Me.dtpInventory.Name = "dtpInventory"
    Me.dtpInventory.Size = New System.Drawing.Size(96, 20)
    Me.dtpInventory.TabIndex = 2
    '
    'frmInventory
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(741, 441)
    Me.Controls.Add(Me.dtpInventory)
    Me.Controls.Add(Me.fraInventory)
    Me.Controls.Add(Me.grdInventory)
    Me.Name = "frmInventory"
    Me.Text = "Inventory"
    CType(Me.grdInventory, System.ComponentModel.ISupportInitialize).EndInit()
    Me.fraInventory.ResumeLayout(False)
    Me.fraInventory.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents grdInventory As System.Windows.Forms.DataGridView
  Friend WithEvents fraInventory As System.Windows.Forms.GroupBox
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents cmdSave As System.Windows.Forms.Button
  Friend WithEvents txtMod As System.Windows.Forms.TextBox
  Friend WithEvents txtQuantity As System.Windows.Forms.TextBox
  Friend WithEvents txtItem_Description As System.Windows.Forms.TextBox
  Friend WithEvents txtIem_Code As System.Windows.Forms.TextBox
  Friend WithEvents cmdClose As System.Windows.Forms.Button
  Friend WithEvents dtpInventory As System.Windows.Forms.DateTimePicker
End Class
