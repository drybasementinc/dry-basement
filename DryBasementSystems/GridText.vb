﻿
Public Class GridText
  Dim OwnerCell As GridText

  Public Property Row() As Double
    Get
      Return CellRow.Text
    End Get
    Set(ByVal value As Double)
      CellRow.Text = value
    End Set
  End Property

  Public Property Col() As Double
    Get
      Return CellCol.Text
    End Get
    Set(ByVal value As Double)
      CellCol.Text = value
    End Set
  End Property

  Public Property TopBorder() As Boolean
    Get
      Return Me.BorderTop.Visible
    End Get
    Set(ByVal value As Boolean)
      Me.BorderTop.Visible = value
      Me.BorderTop.BringToFront()
      BorderTop.X1 = 1
      BorderTop.X2 = Me.Width - 1
      BorderTop.Y1 = 1
      BorderTop.Y2 = 1
    End Set
  End Property

  Public Property BottomBorder() As Boolean
    Get
      Return Me.BorderBottom.Visible
    End Get
    Set(ByVal value As Boolean)
      Me.BorderBottom.Visible = value
      Me.BorderBottom.BringToFront()
      BorderBottom.X1 = 1
      BorderBottom.X2 = Me.Width - 1
      BorderBottom.Y1 = Me.Height - 1
      BorderBottom.Y2 = Me.Height - 1
    End Set
  End Property

  Public Property LeftBorder() As Boolean
    Get
      Return Me.BorderLeft.Visible
    End Get
    Set(ByVal value As Boolean)
      Me.BorderLeft.Visible = value
      Me.BorderLeft.BringToFront()
      BorderLeft.X1 = 1
      BorderLeft.X2 = 1
      BorderLeft.Y1 = 1
      BorderLeft.Y2 = Me.Height - 1
    End Set
  End Property

  Public Property RightBorder() As Boolean
    Get
      Return Me.BorderRight.Visible
    End Get
    Set(ByVal value As Boolean)
      Me.BorderRight.Visible = value
      Me.BorderRight.BringToFront()
      BorderRight.X1 = Me.Width - 1
      BorderRight.X2 = Me.Width - 1
      BorderRight.Y1 = 1
      BorderRight.Y2 = Me.Height - 1
    End Set
  End Property

  Public Property FixedCell() As Boolean
    Get
      Return Fixed.checked
    End Get
    Set(ByVal value As Boolean)
      Fixed.Checked = value
    End Set
  End Property

  Public Property MergedCell() As Boolean
    Get
      Return Merged.Checked
    End Get
    Set(ByVal value As Boolean)
      Merged.Checked = value
    End Set
  End Property

  Public Property MergedOwner() As Object
    Get
      Return OwnerCell
    End Get
    Set(ByVal value As Object)
      OwnerCell = value
    End Set
  End Property

  Private Sub GridText_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    If flgLoading = True Then Exit Sub
    txtText.Width = Me.Width - 3
    txtText.Height = Me.Height - 3

    BorderBottom.X1 = 1
    BorderBottom.X2 = Me.Width - 1
    BorderBottom.Y1 = Me.Height - 1
    BorderBottom.Y2 = Me.Height - 1

    BorderTop.X1 = 1
    BorderTop.X2 = Me.Width - 1
    BorderTop.Y1 = 1
    BorderTop.Y2 = 1

    BorderLeft.X1 = 1
    BorderLeft.X2 = 1
    BorderLeft.Y1 = 1
    BorderLeft.Y2 = Me.Height - 1

    BorderRight.X1 = Me.Width - 1
    BorderRight.X2 = Me.Width - 1
    BorderRight.Y1 = 1
    BorderRight.Y2 = Me.Height - 1
  End Sub

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

  Private Sub txtText_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtText.DoubleClick
    'CellDoubleClick(Me.Text, Me.ParentForm, Row.Text, Col.Text)
    Grid.Cell_DblClick(Me.txtText, Me.ParentForm, Me.Row, Me.Col)

  End Sub

End Class
