﻿Public Class frmInventory

  Dim Inventory As New DataTable
  Dim drInventory As Double = 0
  Dim Production As New DataTable
  Dim drProduction As Double = 0

  Private Sub DataGridView1_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdInventory.CellContentClick

  End Sub

  Private Sub frmInventory_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
    Inv_Update()
    Inv_Update()
  End Sub

  Private Sub grdInventory_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdInventory.CellDoubleClick
    grdInventory.Enabled = False
    fraInventory.Visible = True
    txtIem_Code.Text = grdInventory.Item(0, e.RowIndex).Value
    txtItem_Description.Text = grdInventory.Item(1, e.RowIndex).Value
    txtQuantity.Text = grdInventory.Item(2, e.RowIndex).Value
    fraInventory.Tag = grdInventory.Item(e.ColumnIndex, e.RowIndex).Tag
  End Sub

  Private Sub frmInventory_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
    On Error Resume Next
    grdInventory.Width = Me.Width - 100 - grdInventory.Left
    grdInventory.Height = Me.Height - 100 - grdInventory.Top
  End Sub

  Private Sub cmdSave_Click(sender As System.Object, e As System.EventArgs) Handles cmdSave.Click
    Manual_Update("Update", "Inventory", "Quantity = " & Val(txtQuantity.Text) + Val(txtMod.Text), "ID = " & fraInventory.Tag)
    If flgFailed = False Then
      MsgBox("Saved")
      cmdClose_Click(Me, e)
    End If
  End Sub

  Private Sub cmdClose_Click(sender As System.Object, e As System.EventArgs) Handles cmdClose.Click
    fraInventory.Visible = False
    txtIem_Code.Text = ""
    txtItem_Description.Text = ""
    txtMod.Text = "0"
    txtQuantity.Text = ""
    grdInventory.Enabled = True
    Inv_Update()
  End Sub

  Private Sub Inv_Update()
    Dim datProduction As Date = Now.Date
    Dim exInventory As New excel

    grdInventory.ColumnCount = 0
    grdInventory.RowCount = 1
    Connect("Inventory", Inventory, drInventory, , "Location, Item_Code")
    With grdInventory
      .RowCount = Inventory.Rows.Count
      .ColumnCount = 6

      .Columns(0).HeaderCell.Tag = "Item_Code"
      .Columns(0).HeaderText = "Item Code"

      .Columns(1).HeaderCell.Tag = "Item_Description"
      .Columns(1).HeaderText = "Item Description"

      .Columns(2).HeaderCell.Tag = "Quantity"
      .Columns(2).HeaderText = "Quantity"

      .Columns(3).HeaderText = "End of Current Week"

      .Columns(4).HeaderText = "End of Next Week"

      .Columns(5).HeaderText = "End of Selected Week"

      For intCounter As Integer = 0 To Inventory.Rows.Count - 1
        For intColTemp As Integer = 0 To .ColumnCount - 1
          If intColTemp < 3 Then
            .Item(intColTemp, intCounter).Value = Inventory.Rows(intCounter).Item(.Columns(intColTemp).HeaderCell.Tag & "")
          End If
          .Item(intColTemp, intCounter).Tag = Inventory.Rows(intCounter).Item("ID")
        Next
      Next

      While datProduction.DayOfWeek <> DayOfWeek.Sunday
        datProduction = DateAdd(DateInterval.Day, 1, datProduction)
      End While

      Connect("Dry_Production", Production, drProduction, "Start_Date >= " & FilterByDate(Now.Date) & " AND Start_Date <= " & FilterByDate(datProduction) & " AND Complete_Date is Null")

      For intCounter As Integer = 0 To Production.Rows.Count - 1
        exInventory.Excel_Open_File(Production.Rows(intCounter).Item("Inventory_Path") & "")
        For exRow As Integer = 5 To Inventory.Rows.Count + 10
          If Val(exInventory.Cell(exInventory.Excel_Cell(exRow, 4)).value) > 0 Then
            For intRowTracker As Integer = 0 To grdInventory.RowCount - 1
              Debug.Print(grdInventory.Item(1, intRowTracker).Value & " / " & exInventory.Cell(exInventory.Excel_Cell(exRow, 2)).value)
              If grdInventory.Item(0, intRowTracker).Value = exInventory.Cell(exInventory.Excel_Cell(exRow, 0)).value Then
                grdInventory.Item(3, intRowTracker).Value = Val(grdInventory.Item(2, intRowTracker).Value) - Val(exInventory.Cell(exInventory.Excel_Cell(exRow, 4)).value)
              End If
            Next
          End If
        Next
        exInventory.Excel_Close_File(False)
      Next

      datProduction = DateAdd(DateInterval.Day, 1, datProduction)

      Connect("Dry_Production", Production, drProduction, "Start_Date >= " & FilterByDate(datProduction) & " AND Start_Date <= " & FilterByDate(DateAdd(DateInterval.Day, 6, datProduction)) & " AND Complete_Date is Null")

      For intCounter As Integer = 0 To Production.Rows.Count - 1
        exInventory.Excel_Open_File(Production.Rows(intCounter).Item("Inventory_Path") & "")
        For exRow As Integer = 5 To Inventory.Rows.Count + 10
          If Val(exInventory.Cell(exInventory.Excel_Cell(exRow, 4)).value) > 0 Then
            For intRowTracker As Integer = 0 To grdInventory.RowCount - 1
              Debug.Print(grdInventory.Item(1, intRowTracker).Value & " / " & exInventory.Cell(exInventory.Excel_Cell(exRow, 2)).value)
              If grdInventory.Item(0, intRowTracker).Value = exInventory.Cell(exInventory.Excel_Cell(exRow, 0)).value Then
                grdInventory.Item(3, intRowTracker).Value = Val(grdInventory.Item(2, intRowTracker).Value) - Val(exInventory.Cell(exInventory.Excel_Cell(exRow, 4)).value)
              End If
            Next
          End If
        Next
        exInventory.Excel_Close_File(False)
      Next

      DateAdd(DateInterval.Day, 7, datProduction)

      Connect("Dry_Production", Production, drProduction, "Start_Date >= " & FilterByDate(datProduction) & " AND Start_Date <= " & FilterByDate(dtpInventory.Value.Date) & " AND Complete_Date is Null")

      For intCounter As Integer = 0 To Production.Rows.Count - 1
        exInventory.Excel_Open_File(Production.Rows(intCounter).Item("Inventory_Path") & "")
        For exRow As Integer = 5 To Inventory.Rows.Count + 10
          If Val(exInventory.Cell(exInventory.Excel_Cell(exRow, 4)).value) > 0 Then
            For intRowTracker As Integer = 0 To grdInventory.RowCount - 1
              Debug.Print(grdInventory.Item(1, intRowTracker).Value & " / " & exInventory.Cell(exInventory.Excel_Cell(exRow, 2)).value)
              If grdInventory.Item(0, intRowTracker).Value = exInventory.Cell(exInventory.Excel_Cell(exRow, 0)).value Then
                grdInventory.Item(3, intRowTracker).Value = Val(grdInventory.Item(2, intRowTracker).Value) - Val(exInventory.Cell(exInventory.Excel_Cell(exRow, 4)).value)
              End If
            Next
          End If
        Next
        exInventory.Excel_Close_File(False)
      Next
    End With

  End Sub
End Class