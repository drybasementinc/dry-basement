﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProduction
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.fraValues = New System.Windows.Forms.GroupBox()
    Me.valDry_Dispo_ID = New System.Windows.Forms.TextBox()
    Me.valMagic_Dispo_ID = New System.Windows.Forms.TextBox()
    Me.valCombined = New System.Windows.Forms.CheckBox()
    Me.valAppt_Date = New System.Windows.Forms.TextBox()
    Me.valEstimator = New System.Windows.Forms.TextBox()
    Me.valAddress_ID = New System.Windows.Forms.TextBox()
    Me.lblDryPrice = New System.Windows.Forms.Label()
    Me.txtDryPrice = New System.Windows.Forms.TextBox()
    Me.tpgDry = New System.Windows.Forms.TabPage()
    Me.cmdDrySave = New System.Windows.Forms.Button()
    Me.lblForeman = New System.Windows.Forms.Label()
    Me.cboForeman = New System.Windows.Forms.ComboBox()
    Me.txtDryComplete = New System.Windows.Forms.TextBox()
    Me.txtDryStart = New System.Windows.Forms.TextBox()
    Me.lblDryComplete = New System.Windows.Forms.Label()
    Me.cmdDryComplete = New System.Windows.Forms.Button()
    Me.lblDryStart = New System.Windows.Forms.Label()
    Me.cmdDryStart = New System.Windows.Forms.Button()
    Me.lblDryCover = New System.Windows.Forms.Label()
    Me.cmdDryCancel = New System.Windows.Forms.Button()
    Me.tabProduction = New System.Windows.Forms.TabControl()
    Me.tpgMagic = New System.Windows.Forms.TabPage()
    Me.lblMagicComplete = New System.Windows.Forms.Label()
    Me.lblMagicStart = New System.Windows.Forms.Label()
    Me.lblMagicCover = New System.Windows.Forms.Label()
    Me.txtMagic = New System.Windows.Forms.TextBox()
    Me.txtDry = New System.Windows.Forms.TextBox()
    Me.fraStuff = New System.Windows.Forms.GroupBox()
    Me.cboPermitsPerson = New System.Windows.Forms.ComboBox()
    Me.chkPermits = New System.Windows.Forms.CheckBox()
    Me.cboFinancePerson = New System.Windows.Forms.ComboBox()
    Me.chkFinance = New System.Windows.Forms.CheckBox()
    Me.cboCertPerson = New System.Windows.Forms.ComboBox()
    Me.chkCert = New System.Windows.Forms.CheckBox()
    Me.cboDigRitePerson = New System.Windows.Forms.ComboBox()
    Me.chkDigRite = New System.Windows.Forms.CheckBox()
    Me.cboPreJobPerson = New System.Windows.Forms.ComboBox()
    Me.chkPrejob = New System.Windows.Forms.CheckBox()
    Me.fraWarranties = New System.Windows.Forms.GroupBox()
    Me.txtHHGLG = New System.Windows.Forms.TextBox()
    Me.txtAnchorGLG = New System.Windows.Forms.TextBox()
    Me.txtWaterGLG = New System.Windows.Forms.TextBox()
    Me.cboHHPerson = New System.Windows.Forms.ComboBox()
    Me.chkHHWarranty = New System.Windows.Forms.CheckBox()
    Me.cboAnchorPerson = New System.Windows.Forms.ComboBox()
    Me.chkAnchorWarranty = New System.Windows.Forms.CheckBox()
    Me.cboWaterPerson = New System.Windows.Forms.ComboBox()
    Me.chkWaterWarranty = New System.Windows.Forms.CheckBox()
    Me.cmdSave = New System.Windows.Forms.Button()
    Me.txtEstimator = New System.Windows.Forms.TextBox()
    Me.lblEstimator = New System.Windows.Forms.Label()
    Me.grdProduction = New DryBasementSystems.Grid()
    Me.lblMagicForeman = New System.Windows.Forms.Label()
    Me.cboMagicForeman = New System.Windows.Forms.ComboBox()
    Me.txtMagicComplete = New System.Windows.Forms.TextBox()
    Me.txtMagicStart = New System.Windows.Forms.TextBox()
    Me.cmdMagicComplete = New System.Windows.Forms.Button()
    Me.cmdMagicStart = New System.Windows.Forms.Button()
    Me.lblMagicPrice = New System.Windows.Forms.Label()
    Me.txtMagicPrice = New System.Windows.Forms.TextBox()
    Me.cmdShowInv = New System.Windows.Forms.Button()
    Me.fraValues.SuspendLayout()
    Me.tpgDry.SuspendLayout()
    Me.tabProduction.SuspendLayout()
    Me.tpgMagic.SuspendLayout()
    Me.fraStuff.SuspendLayout()
    Me.fraWarranties.SuspendLayout()
    Me.SuspendLayout()
    '
    'fraValues
    '
    Me.fraValues.Controls.Add(Me.valDry_Dispo_ID)
    Me.fraValues.Controls.Add(Me.valMagic_Dispo_ID)
    Me.fraValues.Controls.Add(Me.valCombined)
    Me.fraValues.Controls.Add(Me.valAppt_Date)
    Me.fraValues.Controls.Add(Me.valEstimator)
    Me.fraValues.Controls.Add(Me.valAddress_ID)
    Me.fraValues.Location = New System.Drawing.Point(894, 12)
    Me.fraValues.Name = "fraValues"
    Me.fraValues.Size = New System.Drawing.Size(73, 165)
    Me.fraValues.TabIndex = 25
    Me.fraValues.TabStop = False
    Me.fraValues.Tag = "SKIP"
    Me.fraValues.Text = "Invisible"
    Me.fraValues.Visible = False
    '
    'valDry_Dispo_ID
    '
    Me.valDry_Dispo_ID.Location = New System.Drawing.Point(10, 139)
    Me.valDry_Dispo_ID.Name = "valDry_Dispo_ID"
    Me.valDry_Dispo_ID.Size = New System.Drawing.Size(54, 20)
    Me.valDry_Dispo_ID.TabIndex = 9
    Me.valDry_Dispo_ID.Tag = "MAGIC"
    '
    'valMagic_Dispo_ID
    '
    Me.valMagic_Dispo_ID.Location = New System.Drawing.Point(10, 113)
    Me.valMagic_Dispo_ID.Name = "valMagic_Dispo_ID"
    Me.valMagic_Dispo_ID.Size = New System.Drawing.Size(54, 20)
    Me.valMagic_Dispo_ID.TabIndex = 8
    Me.valMagic_Dispo_ID.Tag = "DRY"
    '
    'valCombined
    '
    Me.valCombined.AutoSize = True
    Me.valCombined.Location = New System.Drawing.Point(10, 93)
    Me.valCombined.Name = "valCombined"
    Me.valCombined.Size = New System.Drawing.Size(15, 14)
    Me.valCombined.TabIndex = 7
    Me.valCombined.UseVisualStyleBackColor = True
    '
    'valAppt_Date
    '
    Me.valAppt_Date.Location = New System.Drawing.Point(10, 67)
    Me.valAppt_Date.Name = "valAppt_Date"
    Me.valAppt_Date.Size = New System.Drawing.Size(54, 20)
    Me.valAppt_Date.TabIndex = 6
    Me.valAppt_Date.Tag = "BOTH"
    '
    'valEstimator
    '
    Me.valEstimator.Location = New System.Drawing.Point(10, 41)
    Me.valEstimator.Name = "valEstimator"
    Me.valEstimator.Size = New System.Drawing.Size(54, 20)
    Me.valEstimator.TabIndex = 5
    Me.valEstimator.Tag = "BOTH"
    '
    'valAddress_ID
    '
    Me.valAddress_ID.Location = New System.Drawing.Point(10, 15)
    Me.valAddress_ID.Name = "valAddress_ID"
    Me.valAddress_ID.Size = New System.Drawing.Size(54, 20)
    Me.valAddress_ID.TabIndex = 4
    Me.valAddress_ID.Tag = "BOTH"
    '
    'lblDryPrice
    '
    Me.lblDryPrice.AutoSize = True
    Me.lblDryPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblDryPrice.Location = New System.Drawing.Point(241, 24)
    Me.lblDryPrice.Name = "lblDryPrice"
    Me.lblDryPrice.Size = New System.Drawing.Size(40, 17)
    Me.lblDryPrice.TabIndex = 3
    Me.lblDryPrice.Tag = "SKIP"
    Me.lblDryPrice.Text = "Price"
    '
    'txtDryPrice
    '
    Me.txtDryPrice.Location = New System.Drawing.Point(287, 24)
    Me.txtDryPrice.Name = "txtDryPrice"
    Me.txtDryPrice.Size = New System.Drawing.Size(85, 20)
    Me.txtDryPrice.TabIndex = 2
    Me.txtDryPrice.Tag = ""
    '
    'tpgDry
    '
    Me.tpgDry.AutoScroll = True
    Me.tpgDry.BackColor = System.Drawing.SystemColors.Control
    Me.tpgDry.Controls.Add(Me.cmdShowInv)
    Me.tpgDry.Controls.Add(Me.cmdDrySave)
    Me.tpgDry.Controls.Add(Me.lblForeman)
    Me.tpgDry.Controls.Add(Me.cboForeman)
    Me.tpgDry.Controls.Add(Me.txtDryComplete)
    Me.tpgDry.Controls.Add(Me.txtDryStart)
    Me.tpgDry.Controls.Add(Me.lblDryComplete)
    Me.tpgDry.Controls.Add(Me.cmdDryComplete)
    Me.tpgDry.Controls.Add(Me.lblDryStart)
    Me.tpgDry.Controls.Add(Me.cmdDryStart)
    Me.tpgDry.Controls.Add(Me.lblDryCover)
    Me.tpgDry.Controls.Add(Me.lblDryPrice)
    Me.tpgDry.Controls.Add(Me.txtDryPrice)
    Me.tpgDry.Location = New System.Drawing.Point(4, 22)
    Me.tpgDry.Name = "tpgDry"
    Me.tpgDry.Padding = New System.Windows.Forms.Padding(3)
    Me.tpgDry.Size = New System.Drawing.Size(949, 285)
    Me.tpgDry.TabIndex = 0
    Me.tpgDry.Text = "Dry Basement"
    '
    'cmdDrySave
    '
    Me.cmdDrySave.Location = New System.Drawing.Point(852, 13)
    Me.cmdDrySave.Name = "cmdDrySave"
    Me.cmdDrySave.Size = New System.Drawing.Size(89, 30)
    Me.cmdDrySave.TabIndex = 36
    Me.cmdDrySave.Text = "Save Dry Prod"
    Me.cmdDrySave.UseVisualStyleBackColor = True
    '
    'lblForeman
    '
    Me.lblForeman.AutoSize = True
    Me.lblForeman.Location = New System.Drawing.Point(379, 28)
    Me.lblForeman.Name = "lblForeman"
    Me.lblForeman.Size = New System.Drawing.Size(48, 13)
    Me.lblForeman.TabIndex = 37
    Me.lblForeman.Text = "Foreman"
    '
    'cboForeman
    '
    Me.cboForeman.FormattingEnabled = True
    Me.cboForeman.Location = New System.Drawing.Point(433, 23)
    Me.cboForeman.Name = "cboForeman"
    Me.cboForeman.Size = New System.Drawing.Size(104, 21)
    Me.cboForeman.TabIndex = 36
    '
    'txtDryComplete
    '
    Me.txtDryComplete.Location = New System.Drawing.Point(110, 25)
    Me.txtDryComplete.Name = "txtDryComplete"
    Me.txtDryComplete.Size = New System.Drawing.Size(73, 20)
    Me.txtDryComplete.TabIndex = 10
    '
    'txtDryStart
    '
    Me.txtDryStart.Location = New System.Drawing.Point(5, 24)
    Me.txtDryStart.Name = "txtDryStart"
    Me.txtDryStart.Size = New System.Drawing.Size(73, 20)
    Me.txtDryStart.TabIndex = 9
    '
    'lblDryComplete
    '
    Me.lblDryComplete.AutoSize = True
    Me.lblDryComplete.Location = New System.Drawing.Point(107, 8)
    Me.lblDryComplete.Name = "lblDryComplete"
    Me.lblDryComplete.Size = New System.Drawing.Size(77, 13)
    Me.lblDryComplete.TabIndex = 8
    Me.lblDryComplete.Text = "Complete Date"
    '
    'cmdDryComplete
    '
    Me.cmdDryComplete.Location = New System.Drawing.Point(189, 24)
    Me.cmdDryComplete.Name = "cmdDryComplete"
    Me.cmdDryComplete.Size = New System.Drawing.Size(19, 22)
    Me.cmdDryComplete.TabIndex = 7
    Me.cmdDryComplete.Text = "Complete Date"
    Me.cmdDryComplete.UseVisualStyleBackColor = True
    '
    'lblDryStart
    '
    Me.lblDryStart.AutoSize = True
    Me.lblDryStart.Location = New System.Drawing.Point(6, 8)
    Me.lblDryStart.Name = "lblDryStart"
    Me.lblDryStart.Size = New System.Drawing.Size(55, 13)
    Me.lblDryStart.TabIndex = 6
    Me.lblDryStart.Text = "Start Date"
    '
    'cmdDryStart
    '
    Me.cmdDryStart.Location = New System.Drawing.Point(84, 24)
    Me.cmdDryStart.Name = "cmdDryStart"
    Me.cmdDryStart.Size = New System.Drawing.Size(20, 22)
    Me.cmdDryStart.TabIndex = 5
    Me.cmdDryStart.Text = "Start Date"
    Me.cmdDryStart.UseVisualStyleBackColor = True
    '
    'lblDryCover
    '
    Me.lblDryCover.Location = New System.Drawing.Point(6, 8)
    Me.lblDryCover.Name = "lblDryCover"
    Me.lblDryCover.Size = New System.Drawing.Size(252, 13)
    Me.lblDryCover.TabIndex = 4
    Me.lblDryCover.Text = "THERE WAS NO DRY BASEMENT ON THIS JOB!"
    Me.lblDryCover.Visible = False
    '
    'cmdDryCancel
    '
    Me.cmdDryCancel.Location = New System.Drawing.Point(788, 262)
    Me.cmdDryCancel.Name = "cmdDryCancel"
    Me.cmdDryCancel.Size = New System.Drawing.Size(74, 30)
    Me.cmdDryCancel.TabIndex = 11
    Me.cmdDryCancel.Text = "Cancel Job"
    Me.cmdDryCancel.UseVisualStyleBackColor = True
    '
    'tabProduction
    '
    Me.tabProduction.Controls.Add(Me.tpgDry)
    Me.tabProduction.Controls.Add(Me.tpgMagic)
    Me.tabProduction.Location = New System.Drawing.Point(12, 285)
    Me.tabProduction.Name = "tabProduction"
    Me.tabProduction.SelectedIndex = 0
    Me.tabProduction.Size = New System.Drawing.Size(957, 311)
    Me.tabProduction.TabIndex = 21
    '
    'tpgMagic
    '
    Me.tpgMagic.BackColor = System.Drawing.SystemColors.Control
    Me.tpgMagic.Controls.Add(Me.lblMagicForeman)
    Me.tpgMagic.Controls.Add(Me.cboMagicForeman)
    Me.tpgMagic.Controls.Add(Me.txtMagicComplete)
    Me.tpgMagic.Controls.Add(Me.txtMagicStart)
    Me.tpgMagic.Controls.Add(Me.cmdMagicComplete)
    Me.tpgMagic.Controls.Add(Me.cmdMagicStart)
    Me.tpgMagic.Controls.Add(Me.lblMagicPrice)
    Me.tpgMagic.Controls.Add(Me.txtMagicPrice)
    Me.tpgMagic.Controls.Add(Me.lblMagicComplete)
    Me.tpgMagic.Controls.Add(Me.lblMagicStart)
    Me.tpgMagic.Controls.Add(Me.lblMagicCover)
    Me.tpgMagic.Location = New System.Drawing.Point(4, 22)
    Me.tpgMagic.Name = "tpgMagic"
    Me.tpgMagic.Padding = New System.Windows.Forms.Padding(3)
    Me.tpgMagic.Size = New System.Drawing.Size(949, 285)
    Me.tpgMagic.TabIndex = 1
    Me.tpgMagic.Text = "Basement Magic"
    '
    'lblMagicComplete
    '
    Me.lblMagicComplete.AutoSize = True
    Me.lblMagicComplete.Location = New System.Drawing.Point(121, 9)
    Me.lblMagicComplete.Name = "lblMagicComplete"
    Me.lblMagicComplete.Size = New System.Drawing.Size(77, 13)
    Me.lblMagicComplete.TabIndex = 12
    Me.lblMagicComplete.Text = "Complete Date"
    '
    'lblMagicStart
    '
    Me.lblMagicStart.AutoSize = True
    Me.lblMagicStart.Location = New System.Drawing.Point(25, 9)
    Me.lblMagicStart.Name = "lblMagicStart"
    Me.lblMagicStart.Size = New System.Drawing.Size(55, 13)
    Me.lblMagicStart.TabIndex = 10
    Me.lblMagicStart.Text = "Start Date"
    '
    'lblMagicCover
    '
    Me.lblMagicCover.Location = New System.Drawing.Point(6, 9)
    Me.lblMagicCover.Name = "lblMagicCover"
    Me.lblMagicCover.Size = New System.Drawing.Size(263, 13)
    Me.lblMagicCover.TabIndex = 6
    Me.lblMagicCover.Text = "THERE WAS NO BASEMENT MAGIC ON THIS JOB!"
    Me.lblMagicCover.Visible = False
    '
    'txtMagic
    '
    Me.txtMagic.Location = New System.Drawing.Point(12, 132)
    Me.txtMagic.Name = "txtMagic"
    Me.txtMagic.ReadOnly = True
    Me.txtMagic.Size = New System.Drawing.Size(860, 20)
    Me.txtMagic.TabIndex = 24
    '
    'txtDry
    '
    Me.txtDry.Location = New System.Drawing.Point(12, 106)
    Me.txtDry.Name = "txtDry"
    Me.txtDry.ReadOnly = True
    Me.txtDry.Size = New System.Drawing.Size(860, 20)
    Me.txtDry.TabIndex = 23
    '
    'fraStuff
    '
    Me.fraStuff.Controls.Add(Me.cboPermitsPerson)
    Me.fraStuff.Controls.Add(Me.chkPermits)
    Me.fraStuff.Controls.Add(Me.cboFinancePerson)
    Me.fraStuff.Controls.Add(Me.chkFinance)
    Me.fraStuff.Controls.Add(Me.cboCertPerson)
    Me.fraStuff.Controls.Add(Me.chkCert)
    Me.fraStuff.Controls.Add(Me.cboDigRitePerson)
    Me.fraStuff.Controls.Add(Me.chkDigRite)
    Me.fraStuff.Controls.Add(Me.cboPreJobPerson)
    Me.fraStuff.Controls.Add(Me.chkPrejob)
    Me.fraStuff.Location = New System.Drawing.Point(12, 158)
    Me.fraStuff.Name = "fraStuff"
    Me.fraStuff.Size = New System.Drawing.Size(411, 116)
    Me.fraStuff.TabIndex = 31
    Me.fraStuff.TabStop = False
    Me.fraStuff.Text = "Special Needs"
    '
    'cboPermitsPerson
    '
    Me.cboPermitsPerson.FormattingEnabled = True
    Me.cboPermitsPerson.Location = New System.Drawing.Point(7, 89)
    Me.cboPermitsPerson.Name = "cboPermitsPerson"
    Me.cboPermitsPerson.Size = New System.Drawing.Size(123, 21)
    Me.cboPermitsPerson.TabIndex = 9
    Me.cboPermitsPerson.Tag = "INFO"
    '
    'chkPermits
    '
    Me.chkPermits.AutoSize = True
    Me.chkPermits.Location = New System.Drawing.Point(7, 68)
    Me.chkPermits.Name = "chkPermits"
    Me.chkPermits.Size = New System.Drawing.Size(60, 17)
    Me.chkPermits.TabIndex = 8
    Me.chkPermits.Tag = "INFO"
    Me.chkPermits.Text = "Permits"
    Me.chkPermits.UseVisualStyleBackColor = True
    '
    'cboFinancePerson
    '
    Me.cboFinancePerson.FormattingEnabled = True
    Me.cboFinancePerson.Location = New System.Drawing.Point(264, 41)
    Me.cboFinancePerson.Name = "cboFinancePerson"
    Me.cboFinancePerson.Size = New System.Drawing.Size(123, 21)
    Me.cboFinancePerson.TabIndex = 7
    Me.cboFinancePerson.Tag = "INFO"
    '
    'chkFinance
    '
    Me.chkFinance.AutoSize = True
    Me.chkFinance.Location = New System.Drawing.Point(264, 19)
    Me.chkFinance.Name = "chkFinance"
    Me.chkFinance.Size = New System.Drawing.Size(64, 17)
    Me.chkFinance.TabIndex = 6
    Me.chkFinance.Tag = "INFO"
    Me.chkFinance.Text = "Finance"
    Me.chkFinance.UseVisualStyleBackColor = True
    '
    'cboCertPerson
    '
    Me.cboCertPerson.FormattingEnabled = True
    Me.cboCertPerson.Location = New System.Drawing.Point(135, 90)
    Me.cboCertPerson.Name = "cboCertPerson"
    Me.cboCertPerson.Size = New System.Drawing.Size(123, 21)
    Me.cboCertPerson.TabIndex = 5
    Me.cboCertPerson.Tag = "INFO"
    '
    'chkCert
    '
    Me.chkCert.AutoSize = True
    Me.chkCert.Location = New System.Drawing.Point(135, 68)
    Me.chkCert.Name = "chkCert"
    Me.chkCert.Size = New System.Drawing.Size(126, 17)
    Me.chkCert.TabIndex = 4
    Me.chkCert.Tag = "INFO"
    Me.chkCert.Text = "Engineer Certification"
    Me.chkCert.UseVisualStyleBackColor = True
    '
    'cboDigRitePerson
    '
    Me.cboDigRitePerson.FormattingEnabled = True
    Me.cboDigRitePerson.Location = New System.Drawing.Point(135, 41)
    Me.cboDigRitePerson.Name = "cboDigRitePerson"
    Me.cboDigRitePerson.Size = New System.Drawing.Size(123, 21)
    Me.cboDigRitePerson.TabIndex = 3
    Me.cboDigRitePerson.Tag = "INFO"
    '
    'chkDigRite
    '
    Me.chkDigRite.AutoSize = True
    Me.chkDigRite.Location = New System.Drawing.Point(135, 19)
    Me.chkDigRite.Name = "chkDigRite"
    Me.chkDigRite.Size = New System.Drawing.Size(64, 17)
    Me.chkDigRite.TabIndex = 2
    Me.chkDigRite.Tag = "INFO"
    Me.chkDigRite.Text = "Dig Rite"
    Me.chkDigRite.UseVisualStyleBackColor = True
    '
    'cboPreJobPerson
    '
    Me.cboPreJobPerson.FormattingEnabled = True
    Me.cboPreJobPerson.Location = New System.Drawing.Point(6, 40)
    Me.cboPreJobPerson.Name = "cboPreJobPerson"
    Me.cboPreJobPerson.Size = New System.Drawing.Size(123, 21)
    Me.cboPreJobPerson.TabIndex = 1
    Me.cboPreJobPerson.Tag = "INFO"
    '
    'chkPrejob
    '
    Me.chkPrejob.AutoSize = True
    Me.chkPrejob.Location = New System.Drawing.Point(6, 19)
    Me.chkPrejob.Name = "chkPrejob"
    Me.chkPrejob.Size = New System.Drawing.Size(59, 17)
    Me.chkPrejob.TabIndex = 0
    Me.chkPrejob.Tag = "INFO"
    Me.chkPrejob.Text = "PreJob"
    Me.chkPrejob.UseVisualStyleBackColor = True
    '
    'fraWarranties
    '
    Me.fraWarranties.Controls.Add(Me.txtHHGLG)
    Me.fraWarranties.Controls.Add(Me.txtAnchorGLG)
    Me.fraWarranties.Controls.Add(Me.txtWaterGLG)
    Me.fraWarranties.Controls.Add(Me.cboHHPerson)
    Me.fraWarranties.Controls.Add(Me.chkHHWarranty)
    Me.fraWarranties.Controls.Add(Me.cboAnchorPerson)
    Me.fraWarranties.Controls.Add(Me.chkAnchorWarranty)
    Me.fraWarranties.Controls.Add(Me.cboWaterPerson)
    Me.fraWarranties.Controls.Add(Me.chkWaterWarranty)
    Me.fraWarranties.Location = New System.Drawing.Point(572, 158)
    Me.fraWarranties.Name = "fraWarranties"
    Me.fraWarranties.Size = New System.Drawing.Size(394, 95)
    Me.fraWarranties.TabIndex = 32
    Me.fraWarranties.TabStop = False
    Me.fraWarranties.Text = "Warranties"
    '
    'txtHHGLG
    '
    Me.txtHHGLG.Location = New System.Drawing.Point(264, 64)
    Me.txtHHGLG.Name = "txtHHGLG"
    Me.txtHHGLG.Size = New System.Drawing.Size(122, 20)
    Me.txtHHGLG.TabIndex = 8
    Me.txtHHGLG.Tag = ""
    '
    'txtAnchorGLG
    '
    Me.txtAnchorGLG.Location = New System.Drawing.Point(135, 64)
    Me.txtAnchorGLG.Name = "txtAnchorGLG"
    Me.txtAnchorGLG.Size = New System.Drawing.Size(123, 20)
    Me.txtAnchorGLG.TabIndex = 7
    Me.txtAnchorGLG.Tag = ""
    '
    'txtWaterGLG
    '
    Me.txtWaterGLG.Location = New System.Drawing.Point(6, 64)
    Me.txtWaterGLG.Name = "txtWaterGLG"
    Me.txtWaterGLG.Size = New System.Drawing.Size(122, 20)
    Me.txtWaterGLG.TabIndex = 6
    Me.txtWaterGLG.Tag = ""
    '
    'cboHHPerson
    '
    Me.cboHHPerson.FormattingEnabled = True
    Me.cboHHPerson.Location = New System.Drawing.Point(264, 41)
    Me.cboHHPerson.Name = "cboHHPerson"
    Me.cboHHPerson.Size = New System.Drawing.Size(123, 21)
    Me.cboHHPerson.TabIndex = 5
    Me.cboHHPerson.Tag = "INFO"
    '
    'chkHHWarranty
    '
    Me.chkHHWarranty.AutoSize = True
    Me.chkHHWarranty.Location = New System.Drawing.Point(264, 19)
    Me.chkHHWarranty.Name = "chkHHWarranty"
    Me.chkHHWarranty.Size = New System.Drawing.Size(91, 17)
    Me.chkHHWarranty.TabIndex = 4
    Me.chkHHWarranty.Tag = "INFO"
    Me.chkHHWarranty.Text = "House Holder"
    Me.chkHHWarranty.UseVisualStyleBackColor = True
    '
    'cboAnchorPerson
    '
    Me.cboAnchorPerson.FormattingEnabled = True
    Me.cboAnchorPerson.Location = New System.Drawing.Point(135, 41)
    Me.cboAnchorPerson.Name = "cboAnchorPerson"
    Me.cboAnchorPerson.Size = New System.Drawing.Size(123, 21)
    Me.cboAnchorPerson.TabIndex = 3
    Me.cboAnchorPerson.Tag = "INFO"
    '
    'chkAnchorWarranty
    '
    Me.chkAnchorWarranty.AutoSize = True
    Me.chkAnchorWarranty.Location = New System.Drawing.Point(135, 19)
    Me.chkAnchorWarranty.Name = "chkAnchorWarranty"
    Me.chkAnchorWarranty.Size = New System.Drawing.Size(60, 17)
    Me.chkAnchorWarranty.TabIndex = 2
    Me.chkAnchorWarranty.Tag = "INFO"
    Me.chkAnchorWarranty.Text = "Anchor"
    Me.chkAnchorWarranty.UseVisualStyleBackColor = True
    '
    'cboWaterPerson
    '
    Me.cboWaterPerson.FormattingEnabled = True
    Me.cboWaterPerson.Location = New System.Drawing.Point(6, 40)
    Me.cboWaterPerson.Name = "cboWaterPerson"
    Me.cboWaterPerson.Size = New System.Drawing.Size(123, 21)
    Me.cboWaterPerson.TabIndex = 1
    Me.cboWaterPerson.Tag = "INFO"
    '
    'chkWaterWarranty
    '
    Me.chkWaterWarranty.AutoSize = True
    Me.chkWaterWarranty.Location = New System.Drawing.Point(6, 19)
    Me.chkWaterWarranty.Name = "chkWaterWarranty"
    Me.chkWaterWarranty.Size = New System.Drawing.Size(55, 17)
    Me.chkWaterWarranty.TabIndex = 0
    Me.chkWaterWarranty.Tag = "INFO"
    Me.chkWaterWarranty.Text = "Water"
    Me.chkWaterWarranty.UseVisualStyleBackColor = True
    '
    'cmdSave
    '
    Me.cmdSave.Location = New System.Drawing.Point(868, 262)
    Me.cmdSave.Name = "cmdSave"
    Me.cmdSave.Size = New System.Drawing.Size(89, 30)
    Me.cmdSave.TabIndex = 33
    Me.cmdSave.Text = "Save Job Info"
    Me.cmdSave.UseVisualStyleBackColor = True
    '
    'txtEstimator
    '
    Me.txtEstimator.BackColor = System.Drawing.SystemColors.Window
    Me.txtEstimator.Location = New System.Drawing.Point(630, 259)
    Me.txtEstimator.Name = "txtEstimator"
    Me.txtEstimator.ReadOnly = True
    Me.txtEstimator.Size = New System.Drawing.Size(99, 20)
    Me.txtEstimator.TabIndex = 35
    '
    'lblEstimator
    '
    Me.lblEstimator.AutoSize = True
    Me.lblEstimator.Location = New System.Drawing.Point(574, 262)
    Me.lblEstimator.Name = "lblEstimator"
    Me.lblEstimator.Size = New System.Drawing.Size(50, 13)
    Me.lblEstimator.TabIndex = 34
    Me.lblEstimator.Text = "Estimator"
    '
    'grdProduction
    '
    Me.grdProduction.AutoScroll = True
    Me.grdProduction.BackColor = System.Drawing.SystemColors.ControlDarkDark
    Me.grdProduction.Bottom_Row = ""
    Me.grdProduction.Cols = ""
    Me.grdProduction.Left_Col = ""
    Me.grdProduction.Location = New System.Drawing.Point(10, 12)
    Me.grdProduction.Name = "grdProduction"
    Me.grdProduction.Right_Col = ""
    Me.grdProduction.Rows = ""
    Me.grdProduction.Size = New System.Drawing.Size(864, 88)
    Me.grdProduction.TabIndex = 26
    Me.grdProduction.Top_Row = ""
    '
    'lblMagicForeman
    '
    Me.lblMagicForeman.AutoSize = True
    Me.lblMagicForeman.Location = New System.Drawing.Point(380, 29)
    Me.lblMagicForeman.Name = "lblMagicForeman"
    Me.lblMagicForeman.Size = New System.Drawing.Size(48, 13)
    Me.lblMagicForeman.TabIndex = 45
    Me.lblMagicForeman.Text = "Foreman"
    '
    'cboMagicForeman
    '
    Me.cboMagicForeman.FormattingEnabled = True
    Me.cboMagicForeman.Location = New System.Drawing.Point(434, 24)
    Me.cboMagicForeman.Name = "cboMagicForeman"
    Me.cboMagicForeman.Size = New System.Drawing.Size(104, 21)
    Me.cboMagicForeman.TabIndex = 44
    '
    'txtMagicComplete
    '
    Me.txtMagicComplete.Location = New System.Drawing.Point(111, 26)
    Me.txtMagicComplete.Name = "txtMagicComplete"
    Me.txtMagicComplete.Size = New System.Drawing.Size(73, 20)
    Me.txtMagicComplete.TabIndex = 43
    '
    'txtMagicStart
    '
    Me.txtMagicStart.Location = New System.Drawing.Point(6, 25)
    Me.txtMagicStart.Name = "txtMagicStart"
    Me.txtMagicStart.Size = New System.Drawing.Size(73, 20)
    Me.txtMagicStart.TabIndex = 42
    '
    'cmdMagicComplete
    '
    Me.cmdMagicComplete.Location = New System.Drawing.Point(190, 25)
    Me.cmdMagicComplete.Name = "cmdMagicComplete"
    Me.cmdMagicComplete.Size = New System.Drawing.Size(19, 22)
    Me.cmdMagicComplete.TabIndex = 41
    Me.cmdMagicComplete.Text = "Complete Date"
    Me.cmdMagicComplete.UseVisualStyleBackColor = True
    '
    'cmdMagicStart
    '
    Me.cmdMagicStart.Location = New System.Drawing.Point(85, 25)
    Me.cmdMagicStart.Name = "cmdMagicStart"
    Me.cmdMagicStart.Size = New System.Drawing.Size(20, 22)
    Me.cmdMagicStart.TabIndex = 40
    Me.cmdMagicStart.Text = "Start Date"
    Me.cmdMagicStart.UseVisualStyleBackColor = True
    '
    'lblMagicPrice
    '
    Me.lblMagicPrice.AutoSize = True
    Me.lblMagicPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMagicPrice.Location = New System.Drawing.Point(242, 25)
    Me.lblMagicPrice.Name = "lblMagicPrice"
    Me.lblMagicPrice.Size = New System.Drawing.Size(40, 17)
    Me.lblMagicPrice.TabIndex = 39
    Me.lblMagicPrice.Tag = "SKIP"
    Me.lblMagicPrice.Text = "Price"
    '
    'txtMagicPrice
    '
    Me.txtMagicPrice.Location = New System.Drawing.Point(288, 25)
    Me.txtMagicPrice.Name = "txtMagicPrice"
    Me.txtMagicPrice.Size = New System.Drawing.Size(85, 20)
    Me.txtMagicPrice.TabIndex = 38
    Me.txtMagicPrice.Tag = ""
    '
    'cmdShowInv
    '
    Me.cmdShowInv.Location = New System.Drawing.Point(757, 14)
    Me.cmdShowInv.Name = "cmdShowInv"
    Me.cmdShowInv.Size = New System.Drawing.Size(89, 30)
    Me.cmdShowInv.TabIndex = 38
    Me.cmdShowInv.Text = "Inventory/Cost"
    Me.cmdShowInv.UseVisualStyleBackColor = True
    '
    'frmProduction
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoScroll = True
    Me.ClientSize = New System.Drawing.Size(994, 605)
    Me.Controls.Add(Me.txtEstimator)
    Me.Controls.Add(Me.lblEstimator)
    Me.Controls.Add(Me.cmdDryCancel)
    Me.Controls.Add(Me.cmdSave)
    Me.Controls.Add(Me.fraWarranties)
    Me.Controls.Add(Me.fraStuff)
    Me.Controls.Add(Me.fraValues)
    Me.Controls.Add(Me.tabProduction)
    Me.Controls.Add(Me.grdProduction)
    Me.Controls.Add(Me.txtMagic)
    Me.Controls.Add(Me.txtDry)
    Me.Name = "frmProduction"
    Me.Text = "Production"
    Me.fraValues.ResumeLayout(False)
    Me.fraValues.PerformLayout()
    Me.tpgDry.ResumeLayout(False)
    Me.tpgDry.PerformLayout()
    Me.tabProduction.ResumeLayout(False)
    Me.tpgMagic.ResumeLayout(False)
    Me.tpgMagic.PerformLayout()
    Me.fraStuff.ResumeLayout(False)
    Me.fraStuff.PerformLayout()
    Me.fraWarranties.ResumeLayout(False)
    Me.fraWarranties.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents fraValues As System.Windows.Forms.GroupBox
  Friend WithEvents valDry_Dispo_ID As System.Windows.Forms.TextBox
  Friend WithEvents valMagic_Dispo_ID As System.Windows.Forms.TextBox
  Friend WithEvents valCombined As System.Windows.Forms.CheckBox
  Friend WithEvents valAppt_Date As System.Windows.Forms.TextBox
  Friend WithEvents valEstimator As System.Windows.Forms.TextBox
  Friend WithEvents valAddress_ID As System.Windows.Forms.TextBox
  Friend WithEvents lblDryPrice As System.Windows.Forms.Label
  Friend WithEvents txtDryPrice As System.Windows.Forms.TextBox
  Friend WithEvents tpgDry As System.Windows.Forms.TabPage
  Friend WithEvents tabProduction As System.Windows.Forms.TabControl
  Friend WithEvents tpgMagic As System.Windows.Forms.TabPage
  Friend WithEvents grdProduction As DryBasementSystems.Grid
  Friend WithEvents txtMagic As System.Windows.Forms.TextBox
  Friend WithEvents txtDry As System.Windows.Forms.TextBox
  Friend WithEvents lblDryCover As System.Windows.Forms.Label
  Friend WithEvents lblMagicCover As System.Windows.Forms.Label
  Friend WithEvents cmdDryStart As System.Windows.Forms.Button
  Friend WithEvents lblDryStart As System.Windows.Forms.Label
  Friend WithEvents lblDryComplete As System.Windows.Forms.Label
  Friend WithEvents cmdDryComplete As System.Windows.Forms.Button
  Friend WithEvents lblMagicComplete As System.Windows.Forms.Label
  Friend WithEvents lblMagicStart As System.Windows.Forms.Label
  Friend WithEvents txtDryComplete As System.Windows.Forms.TextBox
  Friend WithEvents txtDryStart As System.Windows.Forms.TextBox
  Friend WithEvents cmdDryCancel As System.Windows.Forms.Button
  Friend WithEvents fraStuff As System.Windows.Forms.GroupBox
  Friend WithEvents chkPrejob As System.Windows.Forms.CheckBox
  Friend WithEvents cboDigRitePerson As System.Windows.Forms.ComboBox
  Friend WithEvents chkDigRite As System.Windows.Forms.CheckBox
  Friend WithEvents cboPreJobPerson As System.Windows.Forms.ComboBox
  Friend WithEvents cboCertPerson As System.Windows.Forms.ComboBox
  Friend WithEvents chkCert As System.Windows.Forms.CheckBox
  Friend WithEvents fraWarranties As System.Windows.Forms.GroupBox
  Friend WithEvents txtHHGLG As System.Windows.Forms.TextBox
  Friend WithEvents txtAnchorGLG As System.Windows.Forms.TextBox
  Friend WithEvents txtWaterGLG As System.Windows.Forms.TextBox
  Friend WithEvents cboHHPerson As System.Windows.Forms.ComboBox
  Friend WithEvents chkHHWarranty As System.Windows.Forms.CheckBox
  Friend WithEvents cboAnchorPerson As System.Windows.Forms.ComboBox
  Friend WithEvents chkAnchorWarranty As System.Windows.Forms.CheckBox
  Friend WithEvents cboWaterPerson As System.Windows.Forms.ComboBox
  Friend WithEvents chkWaterWarranty As System.Windows.Forms.CheckBox
  Friend WithEvents cboFinancePerson As System.Windows.Forms.ComboBox
  Friend WithEvents chkFinance As System.Windows.Forms.CheckBox
  Friend WithEvents cboPermitsPerson As System.Windows.Forms.ComboBox
  Friend WithEvents chkPermits As System.Windows.Forms.CheckBox
  Friend WithEvents cmdSave As System.Windows.Forms.Button
  Friend WithEvents lblForeman As System.Windows.Forms.Label
  Friend WithEvents cboForeman As System.Windows.Forms.ComboBox
  Friend WithEvents txtEstimator As System.Windows.Forms.TextBox
  Friend WithEvents lblEstimator As System.Windows.Forms.Label
  Friend WithEvents cmdDrySave As System.Windows.Forms.Button
  Friend WithEvents lblMagicForeman As System.Windows.Forms.Label
  Friend WithEvents cboMagicForeman As System.Windows.Forms.ComboBox
  Friend WithEvents txtMagicComplete As System.Windows.Forms.TextBox
  Friend WithEvents txtMagicStart As System.Windows.Forms.TextBox
  Friend WithEvents cmdMagicComplete As System.Windows.Forms.Button
  Friend WithEvents cmdMagicStart As System.Windows.Forms.Button
  Friend WithEvents lblMagicPrice As System.Windows.Forms.Label
  Friend WithEvents txtMagicPrice As System.Windows.Forms.TextBox
  Friend WithEvents cmdShowInv As System.Windows.Forms.Button
End Class
