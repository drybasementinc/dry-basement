﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLead
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.fraInvisible = New System.Windows.Forms.GroupBox()
    Me.valStart_Time = New System.Windows.Forms.TextBox()
    Me.valDate = New System.Windows.Forms.TextBox()
    Me.valPerson = New System.Windows.Forms.TextBox()
    Me.valAppt_Type = New System.Windows.Forms.TextBox()
    Me.cmdSave = New System.Windows.Forms.Button()
    Me.fraStart = New System.Windows.Forms.GroupBox()
    Me.lblCompanyName = New System.Windows.Forms.Label()
    Me.txtCompany_Name = New System.Windows.Forms.TextBox()
    Me.cboCity = New System.Windows.Forms.ComboBox()
    Me.txtZip = New System.Windows.Forms.TextBox()
    Me.lblZip = New System.Windows.Forms.Label()
    Me.cboState = New System.Windows.Forms.ComboBox()
    Me.lblState = New System.Windows.Forms.Label()
    Me.lblCity = New System.Windows.Forms.Label()
    Me.cboAddress_Ending = New System.Windows.Forms.ComboBox()
    Me.cboAddress_Direction = New System.Windows.Forms.ComboBox()
    Me.txtAddress_Numbers = New System.Windows.Forms.TextBox()
    Me.txtAddress = New System.Windows.Forms.TextBox()
    Me.lblAddress = New System.Windows.Forms.Label()
    Me.lblLast_Name2 = New System.Windows.Forms.Label()
    Me.txtLast_Name2 = New System.Windows.Forms.TextBox()
    Me.lblFirst_Name2 = New System.Windows.Forms.Label()
    Me.txtFirst_Name2 = New System.Windows.Forms.TextBox()
    Me.lblLast_Name = New System.Windows.Forms.Label()
    Me.txtLast_Name = New System.Windows.Forms.TextBox()
    Me.lblFirst_Name = New System.Windows.Forms.Label()
    Me.txtFirst_Name = New System.Windows.Forms.TextBox()
    Me.mnuMain = New System.Windows.Forms.ToolStrip()
    Me.mnuFile = New System.Windows.Forms.ToolStripDropDownButton()
    Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuLeadPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuLeadEmail = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuForms = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuSVC_Form = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuAM_Form = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuAP_Form = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuBEST_Form = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuPrinterOptions = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuPrinterChange = New System.Windows.Forms.ToolStripMenuItem()
    Me.tabPost = New System.Windows.Forms.TabControl()
    Me.tpgInfo = New System.Windows.Forms.TabPage()
    Me.cmdDirections = New System.Windows.Forms.Button()
    Me.lblDriveTime = New System.Windows.Forms.Label()
    Me.txtDriveTime = New System.Windows.Forms.TextBox()
    Me.txtMiles = New System.Windows.Forms.TextBox()
    Me.txtDirections = New System.Windows.Forms.TextBox()
    Me.txtPhone2_EXT = New System.Windows.Forms.TextBox()
    Me.txtPhone1_EXT = New System.Windows.Forms.TextBox()
    Me.txtNewComment = New System.Windows.Forms.TextBox()
    Me.txtComments = New System.Windows.Forms.TextBox()
    Me.txtEmail = New System.Windows.Forms.TextBox()
    Me.txtPhone2 = New System.Windows.Forms.TextBox()
    Me.txtPhone1 = New System.Windows.Forms.TextBox()
    Me.lblMiles = New System.Windows.Forms.Label()
    Me.lblDirections = New System.Windows.Forms.Label()
    Me.lblComments = New System.Windows.Forms.Label()
    Me.lblSchedule = New System.Windows.Forms.Label()
    Me.cmdSchedule = New System.Windows.Forms.Button()
    Me.cboFoundation = New System.Windows.Forms.ComboBox()
    Me.lblFoundation = New System.Windows.Forms.Label()
    Me.cboPhone_Type2 = New System.Windows.Forms.ComboBox()
    Me.cboPhone_Type1 = New System.Windows.Forms.ComboBox()
    Me.lblEmail = New System.Windows.Forms.Label()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.valSet_Date = New System.Windows.Forms.TextBox()
    Me.valConfirmer = New System.Windows.Forms.TextBox()
    Me.valConfirmed = New System.Windows.Forms.CheckBox()
    Me.valDispo_Done = New System.Windows.Forms.CheckBox()
    Me.TextBox1 = New System.Windows.Forms.TextBox()
    Me.TextBox2 = New System.Windows.Forms.TextBox()
    Me.TextBox3 = New System.Windows.Forms.TextBox()
    Me.TextBox4 = New System.Windows.Forms.TextBox()
    Me.tpgEstimate = New System.Windows.Forms.TabPage()
    Me.cboSource2 = New System.Windows.Forms.ComboBox()
    Me.cboSource = New System.Windows.Forms.ComboBox()
    Me.lblSource = New System.Windows.Forms.Label()
    Me.cmdEst_Schedule = New System.Windows.Forms.Button()
    Me.lblEst_Schedule = New System.Windows.Forms.Label()
    Me.grdEst_Appts = New System.Windows.Forms.DataGridView()
    Me.txtEstimator_Comments = New System.Windows.Forms.TextBox()
    Me.lblZone = New System.Windows.Forms.Label()
    Me.cboZone = New System.Windows.Forms.ComboBox()
    Me.lblEstimatorComments = New System.Windows.Forms.Label()
    Me.fraProblem = New System.Windows.Forms.GroupBox()
    Me.txtOther_Text = New System.Windows.Forms.TextBox()
    Me.chkCracks = New System.Windows.Forms.CheckBox()
    Me.chkOther = New System.Windows.Forms.CheckBox()
    Me.chkWater = New System.Windows.Forms.CheckBox()
    Me.chkRemodeling = New System.Windows.Forms.CheckBox()
    Me.chkSettling = New System.Windows.Forms.CheckBox()
    Me.chkMold = New System.Windows.Forms.CheckBox()
    Me.chkBowed_Walls = New System.Windows.Forms.CheckBox()
    Me.tpgDispos = New System.Windows.Forms.TabPage()
    Me.grdProduction = New System.Windows.Forms.DataGridView()
    Me.grdDispo = New System.Windows.Forms.DataGridView()
    Me.tpgAnnual = New System.Windows.Forms.TabPage()
    Me.lblAnnualPrice = New System.Windows.Forms.Label()
    Me.txtPrice = New System.Windows.Forms.TextBox()
    Me.cmdAnnualSchedule = New System.Windows.Forms.Button()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.grdAnnual = New System.Windows.Forms.DataGridView()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.txtPump_Count = New System.Windows.Forms.TextBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.dtpLast_Annual = New System.Windows.Forms.DateTimePicker()
    Me.tpgService = New System.Windows.Forms.TabPage()
    Me.Button3 = New System.Windows.Forms.Button()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.grdService = New System.Windows.Forms.DataGridView()
    Me.tpgAlways = New System.Windows.Forms.TabPage()
    Me.cmdAlwaysSchedule = New System.Windows.Forms.Button()
    Me.Label6 = New System.Windows.Forms.Label()
    Me.grdAlways = New System.Windows.Forms.DataGridView()
    Me.Label8 = New System.Windows.Forms.Label()
    Me.cboAP_Zone = New System.Windows.Forms.ComboBox()
    Me.GroupBox2 = New System.Windows.Forms.GroupBox()
    Me.cboLowes_Store = New System.Windows.Forms.ComboBox()
    Me.txtAdditional_Text = New System.Windows.Forms.TextBox()
    Me.chkDrain = New System.Windows.Forms.CheckBox()
    Me.chkAdditional = New System.Windows.Forms.CheckBox()
    Me.chkWater_Heater = New System.Windows.Forms.CheckBox()
    Me.chkLowes = New System.Windows.Forms.CheckBox()
    Me.chkToilet = New System.Windows.Forms.CheckBox()
    Me.chkDisposal = New System.Windows.Forms.CheckBox()
    Me.chkFaucet = New System.Windows.Forms.CheckBox()
    Me.tpgCall_Log = New System.Windows.Forms.TabPage()
    Me.fraCallLog = New System.Windows.Forms.GroupBox()
    Me.cmdCallLogClose = New System.Windows.Forms.Button()
    Me.cmdCallLogSave = New System.Windows.Forms.Button()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.lblCallType = New System.Windows.Forms.Label()
    Me.txtCallNotes = New System.Windows.Forms.TextBox()
    Me.cboCallType = New System.Windows.Forms.ComboBox()
    Me.cmdAddCallLog = New System.Windows.Forms.Button()
    Me.grdCallLog = New System.Windows.Forms.DataGridView()
    Me.tpgCallList = New System.Windows.Forms.TabPage()
    Me.chkWorkDone = New System.Windows.Forms.CheckBox()
    Me.txtScript = New System.Windows.Forms.TextBox()
    Me.lblLists = New System.Windows.Forms.Label()
    Me.cboCall_List = New System.Windows.Forms.ComboBox()
    Me.cmdDelay = New System.Windows.Forms.Button()
    Me.cmdList_Remove = New System.Windows.Forms.Button()
    Me.cmdContacted = New System.Windows.Forms.Button()
    Me.cmdSearch = New System.Windows.Forms.Button()
    Me.fraInvisible.SuspendLayout()
    Me.fraStart.SuspendLayout()
    Me.mnuMain.SuspendLayout()
    Me.tabPost.SuspendLayout()
    Me.tpgInfo.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.tpgEstimate.SuspendLayout()
    CType(Me.grdEst_Appts, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.fraProblem.SuspendLayout()
    Me.tpgDispos.SuspendLayout()
    CType(Me.grdProduction, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.grdDispo, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.tpgAnnual.SuspendLayout()
    CType(Me.grdAnnual, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.tpgService.SuspendLayout()
    CType(Me.grdService, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.tpgAlways.SuspendLayout()
    CType(Me.grdAlways, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.GroupBox2.SuspendLayout()
    Me.tpgCall_Log.SuspendLayout()
    Me.fraCallLog.SuspendLayout()
    CType(Me.grdCallLog, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.tpgCallList.SuspendLayout()
    Me.SuspendLayout()
    '
    'fraInvisible
    '
    Me.fraInvisible.Controls.Add(Me.valStart_Time)
    Me.fraInvisible.Controls.Add(Me.valDate)
    Me.fraInvisible.Controls.Add(Me.valPerson)
    Me.fraInvisible.Controls.Add(Me.valAppt_Type)
    Me.fraInvisible.Location = New System.Drawing.Point(305, 13)
    Me.fraInvisible.Name = "fraInvisible"
    Me.fraInvisible.Size = New System.Drawing.Size(209, 10)
    Me.fraInvisible.TabIndex = 106
    Me.fraInvisible.TabStop = False
    Me.fraInvisible.Text = "Invisible Controls"
    Me.fraInvisible.Visible = False
    '
    'valStart_Time
    '
    Me.valStart_Time.Location = New System.Drawing.Point(87, 19)
    Me.valStart_Time.Name = "valStart_Time"
    Me.valStart_Time.Size = New System.Drawing.Size(75, 20)
    Me.valStart_Time.TabIndex = 3
    Me.valStart_Time.Tag = "Est_Schedule"
    '
    'valDate
    '
    Me.valDate.Location = New System.Drawing.Point(87, 46)
    Me.valDate.Name = "valDate"
    Me.valDate.Size = New System.Drawing.Size(75, 20)
    Me.valDate.TabIndex = 2
    Me.valDate.Tag = "Est_Schedule"
    '
    'valPerson
    '
    Me.valPerson.Location = New System.Drawing.Point(6, 45)
    Me.valPerson.Name = "valPerson"
    Me.valPerson.Size = New System.Drawing.Size(75, 20)
    Me.valPerson.TabIndex = 1
    Me.valPerson.Tag = "Est_Schedule"
    '
    'valAppt_Type
    '
    Me.valAppt_Type.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.valAppt_Type.Location = New System.Drawing.Point(6, 19)
    Me.valAppt_Type.Name = "valAppt_Type"
    Me.valAppt_Type.Size = New System.Drawing.Size(75, 20)
    Me.valAppt_Type.TabIndex = 0
    Me.valAppt_Type.Tag = "Est_Schedule"
    '
    'cmdSave
    '
    Me.cmdSave.Location = New System.Drawing.Point(674, 25)
    Me.cmdSave.Name = "cmdSave"
    Me.cmdSave.Size = New System.Drawing.Size(73, 30)
    Me.cmdSave.TabIndex = 105
    Me.cmdSave.Text = "Save"
    Me.cmdSave.UseVisualStyleBackColor = True
    '
    'fraStart
    '
    Me.fraStart.Controls.Add(Me.lblCompanyName)
    Me.fraStart.Controls.Add(Me.txtCompany_Name)
    Me.fraStart.Controls.Add(Me.cboCity)
    Me.fraStart.Controls.Add(Me.txtZip)
    Me.fraStart.Controls.Add(Me.lblZip)
    Me.fraStart.Controls.Add(Me.cboState)
    Me.fraStart.Controls.Add(Me.lblState)
    Me.fraStart.Controls.Add(Me.lblCity)
    Me.fraStart.Controls.Add(Me.cboAddress_Ending)
    Me.fraStart.Controls.Add(Me.cboAddress_Direction)
    Me.fraStart.Controls.Add(Me.txtAddress_Numbers)
    Me.fraStart.Controls.Add(Me.txtAddress)
    Me.fraStart.Controls.Add(Me.lblAddress)
    Me.fraStart.Controls.Add(Me.lblLast_Name2)
    Me.fraStart.Controls.Add(Me.txtLast_Name2)
    Me.fraStart.Controls.Add(Me.lblFirst_Name2)
    Me.fraStart.Controls.Add(Me.txtFirst_Name2)
    Me.fraStart.Controls.Add(Me.lblLast_Name)
    Me.fraStart.Controls.Add(Me.txtLast_Name)
    Me.fraStart.Controls.Add(Me.lblFirst_Name)
    Me.fraStart.Controls.Add(Me.txtFirst_Name)
    Me.fraStart.Location = New System.Drawing.Point(13, 22)
    Me.fraStart.Name = "fraStart"
    Me.fraStart.Size = New System.Drawing.Size(655, 139)
    Me.fraStart.TabIndex = 102
    Me.fraStart.TabStop = False
    '
    'lblCompanyName
    '
    Me.lblCompanyName.AutoSize = True
    Me.lblCompanyName.Location = New System.Drawing.Point(10, 12)
    Me.lblCompanyName.Name = "lblCompanyName"
    Me.lblCompanyName.Size = New System.Drawing.Size(51, 13)
    Me.lblCompanyName.TabIndex = 5
    Me.lblCompanyName.Text = "Company"
    '
    'txtCompany_Name
    '
    Me.txtCompany_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtCompany_Name.Location = New System.Drawing.Point(84, 9)
    Me.txtCompany_Name.Name = "txtCompany_Name"
    Me.txtCompany_Name.Size = New System.Drawing.Size(565, 20)
    Me.txtCompany_Name.TabIndex = 0
    Me.txtCompany_Name.Tag = "Clients"
    '
    'cboCity
    '
    Me.cboCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboCity.FormattingEnabled = True
    Me.cboCity.Location = New System.Drawing.Point(84, 114)
    Me.cboCity.Name = "cboCity"
    Me.cboCity.Size = New System.Drawing.Size(309, 21)
    Me.cboCity.Sorted = True
    Me.cboCity.TabIndex = 10
    Me.cboCity.Tag = "Addresses"
    '
    'txtZip
    '
    Me.txtZip.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtZip.Location = New System.Drawing.Point(536, 113)
    Me.txtZip.Name = "txtZip"
    Me.txtZip.Size = New System.Drawing.Size(113, 20)
    Me.txtZip.TabIndex = 9
    Me.txtZip.Tag = "Addresses"
    '
    'lblZip
    '
    Me.lblZip.AutoSize = True
    Me.lblZip.Location = New System.Drawing.Point(508, 116)
    Me.lblZip.Name = "lblZip"
    Me.lblZip.Size = New System.Drawing.Size(22, 13)
    Me.lblZip.TabIndex = 30
    Me.lblZip.Text = "Zip"
    '
    'cboState
    '
    Me.cboState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboState.FormattingEnabled = True
    Me.cboState.Items.AddRange(New Object() {"KS", "MO"})
    Me.cboState.Location = New System.Drawing.Point(437, 112)
    Me.cboState.Name = "cboState"
    Me.cboState.Size = New System.Drawing.Size(65, 21)
    Me.cboState.Sorted = True
    Me.cboState.TabIndex = 11
    Me.cboState.Tag = "Addresses"
    '
    'lblState
    '
    Me.lblState.AutoSize = True
    Me.lblState.Location = New System.Drawing.Point(399, 116)
    Me.lblState.Name = "lblState"
    Me.lblState.Size = New System.Drawing.Size(32, 13)
    Me.lblState.TabIndex = 28
    Me.lblState.Text = "State"
    '
    'lblCity
    '
    Me.lblCity.AutoSize = True
    Me.lblCity.Location = New System.Drawing.Point(10, 116)
    Me.lblCity.Name = "lblCity"
    Me.lblCity.Size = New System.Drawing.Size(24, 13)
    Me.lblCity.TabIndex = 26
    Me.lblCity.Text = "City"
    '
    'cboAddress_Ending
    '
    Me.cboAddress_Ending.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboAddress_Ending.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboAddress_Ending.FormattingEnabled = True
    Me.cboAddress_Ending.Location = New System.Drawing.Point(532, 86)
    Me.cboAddress_Ending.Name = "cboAddress_Ending"
    Me.cboAddress_Ending.Size = New System.Drawing.Size(117, 21)
    Me.cboAddress_Ending.Sorted = True
    Me.cboAddress_Ending.TabIndex = 8
    Me.cboAddress_Ending.Tag = "Addresses"
    '
    'cboAddress_Direction
    '
    Me.cboAddress_Direction.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboAddress_Direction.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboAddress_Direction.FormattingEnabled = True
    Me.cboAddress_Direction.Location = New System.Drawing.Point(174, 87)
    Me.cboAddress_Direction.Name = "cboAddress_Direction"
    Me.cboAddress_Direction.Size = New System.Drawing.Size(80, 21)
    Me.cboAddress_Direction.Sorted = True
    Me.cboAddress_Direction.TabIndex = 6
    Me.cboAddress_Direction.Tag = "Addresses"
    '
    'txtAddress_Numbers
    '
    Me.txtAddress_Numbers.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtAddress_Numbers.Location = New System.Drawing.Point(84, 87)
    Me.txtAddress_Numbers.Name = "txtAddress_Numbers"
    Me.txtAddress_Numbers.Size = New System.Drawing.Size(84, 20)
    Me.txtAddress_Numbers.TabIndex = 5
    Me.txtAddress_Numbers.Tag = "Addresses"
    '
    'txtAddress
    '
    Me.txtAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtAddress.Location = New System.Drawing.Point(260, 87)
    Me.txtAddress.Name = "txtAddress"
    Me.txtAddress.Size = New System.Drawing.Size(266, 20)
    Me.txtAddress.TabIndex = 7
    Me.txtAddress.Tag = "Addresses"
    '
    'lblAddress
    '
    Me.lblAddress.AutoSize = True
    Me.lblAddress.Location = New System.Drawing.Point(10, 90)
    Me.lblAddress.Name = "lblAddress"
    Me.lblAddress.Size = New System.Drawing.Size(45, 13)
    Me.lblAddress.TabIndex = 21
    Me.lblAddress.Text = "Address"
    '
    'lblLast_Name2
    '
    Me.lblLast_Name2.AutoSize = True
    Me.lblLast_Name2.Location = New System.Drawing.Point(333, 63)
    Me.lblLast_Name2.Name = "lblLast_Name2"
    Me.lblLast_Name2.Size = New System.Drawing.Size(67, 13)
    Me.lblLast_Name2.TabIndex = 20
    Me.lblLast_Name2.Text = "Last Name 2"
    '
    'txtLast_Name2
    '
    Me.txtLast_Name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtLast_Name2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtLast_Name2.Location = New System.Drawing.Point(402, 61)
    Me.txtLast_Name2.Name = "txtLast_Name2"
    Me.txtLast_Name2.Size = New System.Drawing.Size(247, 20)
    Me.txtLast_Name2.TabIndex = 4
    Me.txtLast_Name2.Tag = "Clients"
    '
    'lblFirst_Name2
    '
    Me.lblFirst_Name2.AutoSize = True
    Me.lblFirst_Name2.Location = New System.Drawing.Point(10, 64)
    Me.lblFirst_Name2.Name = "lblFirst_Name2"
    Me.lblFirst_Name2.Size = New System.Drawing.Size(66, 13)
    Me.lblFirst_Name2.TabIndex = 18
    Me.lblFirst_Name2.Text = "First Name 2"
    '
    'txtFirst_Name2
    '
    Me.txtFirst_Name2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtFirst_Name2.Location = New System.Drawing.Point(84, 61)
    Me.txtFirst_Name2.Name = "txtFirst_Name2"
    Me.txtFirst_Name2.Size = New System.Drawing.Size(243, 20)
    Me.txtFirst_Name2.TabIndex = 3
    Me.txtFirst_Name2.Tag = "Clients"
    '
    'lblLast_Name
    '
    Me.lblLast_Name.AutoSize = True
    Me.lblLast_Name.Location = New System.Drawing.Point(333, 37)
    Me.lblLast_Name.Name = "lblLast_Name"
    Me.lblLast_Name.Size = New System.Drawing.Size(58, 13)
    Me.lblLast_Name.TabIndex = 16
    Me.lblLast_Name.Text = "Last Name"
    '
    'txtLast_Name
    '
    Me.txtLast_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtLast_Name.Location = New System.Drawing.Point(402, 35)
    Me.txtLast_Name.Name = "txtLast_Name"
    Me.txtLast_Name.Size = New System.Drawing.Size(247, 20)
    Me.txtLast_Name.TabIndex = 2
    Me.txtLast_Name.Tag = "Clients"
    '
    'lblFirst_Name
    '
    Me.lblFirst_Name.AutoSize = True
    Me.lblFirst_Name.Location = New System.Drawing.Point(10, 38)
    Me.lblFirst_Name.Name = "lblFirst_Name"
    Me.lblFirst_Name.Size = New System.Drawing.Size(57, 13)
    Me.lblFirst_Name.TabIndex = 14
    Me.lblFirst_Name.Text = "First Name"
    '
    'txtFirst_Name
    '
    Me.txtFirst_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtFirst_Name.Location = New System.Drawing.Point(84, 35)
    Me.txtFirst_Name.Name = "txtFirst_Name"
    Me.txtFirst_Name.Size = New System.Drawing.Size(243, 20)
    Me.txtFirst_Name.TabIndex = 1
    Me.txtFirst_Name.Tag = "Clients"
    '
    'mnuMain
    '
    Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile})
    Me.mnuMain.Location = New System.Drawing.Point(0, 0)
    Me.mnuMain.Name = "mnuMain"
    Me.mnuMain.Size = New System.Drawing.Size(777, 25)
    Me.mnuMain.TabIndex = 118
    Me.mnuMain.Text = "ToolStrip1"
    '
    'mnuFile
    '
    Me.mnuFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
    Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint, Me.mnuPrinterOptions})
    Me.mnuFile.ImageTransparentColor = System.Drawing.Color.Magenta
    Me.mnuFile.Name = "mnuFile"
    Me.mnuFile.Size = New System.Drawing.Size(38, 22)
    Me.mnuFile.Text = "File"
    '
    'mnuPrint
    '
    Me.mnuPrint.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLeadPrint, Me.mnuLeadEmail, Me.mnuForms})
    Me.mnuPrint.Name = "mnuPrint"
    Me.mnuPrint.Size = New System.Drawing.Size(154, 22)
    Me.mnuPrint.Text = "Print"
    '
    'mnuLeadPrint
    '
    Me.mnuLeadPrint.Name = "mnuLeadPrint"
    Me.mnuLeadPrint.Size = New System.Drawing.Size(152, 22)
    Me.mnuLeadPrint.Text = "Lead Card"
    '
    'mnuLeadEmail
    '
    Me.mnuLeadEmail.Name = "mnuLeadEmail"
    Me.mnuLeadEmail.Size = New System.Drawing.Size(152, 22)
    Me.mnuLeadEmail.Text = "Email"
    '
    'mnuForms
    '
    Me.mnuForms.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSVC_Form, Me.mnuAM_Form, Me.mnuAP_Form, Me.mnuBEST_Form})
    Me.mnuForms.Name = "mnuForms"
    Me.mnuForms.Size = New System.Drawing.Size(152, 22)
    Me.mnuForms.Text = "Form"
    '
    'mnuSVC_Form
    '
    Me.mnuSVC_Form.Name = "mnuSVC_Form"
    Me.mnuSVC_Form.Size = New System.Drawing.Size(152, 22)
    Me.mnuSVC_Form.Text = "SVC Form"
    '
    'mnuAM_Form
    '
    Me.mnuAM_Form.Name = "mnuAM_Form"
    Me.mnuAM_Form.Size = New System.Drawing.Size(152, 22)
    Me.mnuAM_Form.Text = "AM Form"
    '
    'mnuAP_Form
    '
    Me.mnuAP_Form.Name = "mnuAP_Form"
    Me.mnuAP_Form.Size = New System.Drawing.Size(152, 22)
    Me.mnuAP_Form.Text = "AP Lead"
    '
    'mnuBEST_Form
    '
    Me.mnuBEST_Form.Name = "mnuBEST_Form"
    Me.mnuBEST_Form.Size = New System.Drawing.Size(152, 22)
    Me.mnuBEST_Form.Text = "Eng Lead"
    '
    'mnuPrinterOptions
    '
    Me.mnuPrinterOptions.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrinterChange})
    Me.mnuPrinterOptions.Name = "mnuPrinterOptions"
    Me.mnuPrinterOptions.Size = New System.Drawing.Size(154, 22)
    Me.mnuPrinterOptions.Text = "Printer Options"
    '
    'mnuPrinterChange
    '
    Me.mnuPrinterChange.Name = "mnuPrinterChange"
    Me.mnuPrinterChange.Size = New System.Drawing.Size(153, 22)
    Me.mnuPrinterChange.Text = "Change Printer"
    '
    'tabPost
    '
    Me.tabPost.Controls.Add(Me.tpgInfo)
    Me.tabPost.Controls.Add(Me.tpgEstimate)
    Me.tabPost.Controls.Add(Me.tpgDispos)
    Me.tabPost.Controls.Add(Me.tpgAnnual)
    Me.tabPost.Controls.Add(Me.tpgService)
    Me.tabPost.Controls.Add(Me.tpgAlways)
    Me.tabPost.Controls.Add(Me.tpgCall_Log)
    Me.tabPost.Controls.Add(Me.tpgCallList)
    Me.tabPost.Location = New System.Drawing.Point(13, 168)
    Me.tabPost.Name = "tabPost"
    Me.tabPost.SelectedIndex = 0
    Me.tabPost.Size = New System.Drawing.Size(753, 585)
    Me.tabPost.TabIndex = 119
    '
    'tpgInfo
    '
    Me.tpgInfo.BackColor = System.Drawing.SystemColors.Control
    Me.tpgInfo.Controls.Add(Me.cmdDirections)
    Me.tpgInfo.Controls.Add(Me.lblDriveTime)
    Me.tpgInfo.Controls.Add(Me.txtDriveTime)
    Me.tpgInfo.Controls.Add(Me.txtMiles)
    Me.tpgInfo.Controls.Add(Me.txtDirections)
    Me.tpgInfo.Controls.Add(Me.txtPhone2_EXT)
    Me.tpgInfo.Controls.Add(Me.txtPhone1_EXT)
    Me.tpgInfo.Controls.Add(Me.txtNewComment)
    Me.tpgInfo.Controls.Add(Me.txtComments)
    Me.tpgInfo.Controls.Add(Me.txtEmail)
    Me.tpgInfo.Controls.Add(Me.txtPhone2)
    Me.tpgInfo.Controls.Add(Me.txtPhone1)
    Me.tpgInfo.Controls.Add(Me.lblMiles)
    Me.tpgInfo.Controls.Add(Me.lblDirections)
    Me.tpgInfo.Controls.Add(Me.lblComments)
    Me.tpgInfo.Controls.Add(Me.lblSchedule)
    Me.tpgInfo.Controls.Add(Me.cmdSchedule)
    Me.tpgInfo.Controls.Add(Me.cboFoundation)
    Me.tpgInfo.Controls.Add(Me.lblFoundation)
    Me.tpgInfo.Controls.Add(Me.cboPhone_Type2)
    Me.tpgInfo.Controls.Add(Me.cboPhone_Type1)
    Me.tpgInfo.Controls.Add(Me.lblEmail)
    Me.tpgInfo.Controls.Add(Me.GroupBox1)
    Me.tpgInfo.Location = New System.Drawing.Point(4, 22)
    Me.tpgInfo.Name = "tpgInfo"
    Me.tpgInfo.Padding = New System.Windows.Forms.Padding(3)
    Me.tpgInfo.Size = New System.Drawing.Size(745, 559)
    Me.tpgInfo.TabIndex = 0
    Me.tpgInfo.Text = "Office Info"
    '
    'cmdDirections
    '
    Me.cmdDirections.Location = New System.Drawing.Point(552, 81)
    Me.cmdDirections.Name = "cmdDirections"
    Me.cmdDirections.Size = New System.Drawing.Size(108, 22)
    Me.cmdDirections.TabIndex = 121
    Me.cmdDirections.Text = "Directions"
    Me.cmdDirections.UseVisualStyleBackColor = True
    '
    'lblDriveTime
    '
    Me.lblDriveTime.AutoSize = True
    Me.lblDriveTime.Location = New System.Drawing.Point(267, 89)
    Me.lblDriveTime.Name = "lblDriveTime"
    Me.lblDriveTime.Size = New System.Drawing.Size(58, 13)
    Me.lblDriveTime.TabIndex = 107
    Me.lblDriveTime.Text = "Drive Time"
    '
    'txtDriveTime
    '
    Me.txtDriveTime.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtDriveTime.Location = New System.Drawing.Point(330, 86)
    Me.txtDriveTime.Name = "txtDriveTime"
    Me.txtDriveTime.Size = New System.Drawing.Size(141, 20)
    Me.txtDriveTime.TabIndex = 106
    Me.txtDriveTime.Tag = "Addresses"
    '
    'txtMiles
    '
    Me.txtMiles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtMiles.Location = New System.Drawing.Point(118, 86)
    Me.txtMiles.Name = "txtMiles"
    Me.txtMiles.Size = New System.Drawing.Size(141, 20)
    Me.txtMiles.TabIndex = 104
    Me.txtMiles.Tag = "Addresses"
    '
    'txtDirections
    '
    Me.txtDirections.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtDirections.Location = New System.Drawing.Point(80, 109)
    Me.txtDirections.Multiline = True
    Me.txtDirections.Name = "txtDirections"
    Me.txtDirections.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txtDirections.Size = New System.Drawing.Size(580, 162)
    Me.txtDirections.TabIndex = 102
    Me.txtDirections.Tag = "Addresses"
    '
    'txtPhone2_EXT
    '
    Me.txtPhone2_EXT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtPhone2_EXT.Location = New System.Drawing.Point(603, 6)
    Me.txtPhone2_EXT.Name = "txtPhone2_EXT"
    Me.txtPhone2_EXT.Size = New System.Drawing.Size(58, 20)
    Me.txtPhone2_EXT.TabIndex = 5
    Me.txtPhone2_EXT.Tag = "Clients"
    '
    'txtPhone1_EXT
    '
    Me.txtPhone1_EXT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtPhone1_EXT.Location = New System.Drawing.Point(265, 7)
    Me.txtPhone1_EXT.Name = "txtPhone1_EXT"
    Me.txtPhone1_EXT.Size = New System.Drawing.Size(58, 20)
    Me.txtPhone1_EXT.TabIndex = 2
    '
    'txtNewComment
    '
    Me.txtNewComment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtNewComment.Location = New System.Drawing.Point(81, 277)
    Me.txtNewComment.Name = "txtNewComment"
    Me.txtNewComment.Size = New System.Drawing.Size(581, 20)
    Me.txtNewComment.TabIndex = 19
    Me.txtNewComment.Tag = "SKIP"
    '
    'txtComments
    '
    Me.txtComments.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtComments.Location = New System.Drawing.Point(80, 303)
    Me.txtComments.Multiline = True
    Me.txtComments.Name = "txtComments"
    Me.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txtComments.Size = New System.Drawing.Size(580, 204)
    Me.txtComments.TabIndex = 49
    Me.txtComments.Tag = "Addresses"
    '
    'txtEmail
    '
    Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtEmail.Location = New System.Drawing.Point(80, 32)
    Me.txtEmail.Name = "txtEmail"
    Me.txtEmail.Size = New System.Drawing.Size(581, 20)
    Me.txtEmail.TabIndex = 6
    Me.txtEmail.Tag = "Clients"
    '
    'txtPhone2
    '
    Me.txtPhone2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtPhone2.Location = New System.Drawing.Point(417, 6)
    Me.txtPhone2.Name = "txtPhone2"
    Me.txtPhone2.Size = New System.Drawing.Size(180, 20)
    Me.txtPhone2.TabIndex = 4
    Me.txtPhone2.Tag = "Clients"
    '
    'txtPhone1
    '
    Me.txtPhone1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtPhone1.Location = New System.Drawing.Point(80, 6)
    Me.txtPhone1.Name = "txtPhone1"
    Me.txtPhone1.Size = New System.Drawing.Size(179, 20)
    Me.txtPhone1.TabIndex = 1
    Me.txtPhone1.Tag = "Clients"
    '
    'lblMiles
    '
    Me.lblMiles.AutoSize = True
    Me.lblMiles.Location = New System.Drawing.Point(81, 88)
    Me.lblMiles.Name = "lblMiles"
    Me.lblMiles.Size = New System.Drawing.Size(31, 13)
    Me.lblMiles.TabIndex = 105
    Me.lblMiles.Text = "Miles"
    '
    'lblDirections
    '
    Me.lblDirections.AutoSize = True
    Me.lblDirections.Location = New System.Drawing.Point(6, 112)
    Me.lblDirections.Name = "lblDirections"
    Me.lblDirections.Size = New System.Drawing.Size(54, 13)
    Me.lblDirections.TabIndex = 103
    Me.lblDirections.Text = "Directions"
    '
    'lblComments
    '
    Me.lblComments.AutoSize = True
    Me.lblComments.Location = New System.Drawing.Point(6, 280)
    Me.lblComments.Name = "lblComments"
    Me.lblComments.Size = New System.Drawing.Size(56, 13)
    Me.lblComments.TabIndex = 48
    Me.lblComments.Text = "Comments"
    '
    'lblSchedule
    '
    Me.lblSchedule.AutoSize = True
    Me.lblSchedule.Location = New System.Drawing.Point(5, 527)
    Me.lblSchedule.Name = "lblSchedule"
    Me.lblSchedule.Size = New System.Drawing.Size(52, 13)
    Me.lblSchedule.TabIndex = 45
    Me.lblSchedule.Text = "Schedule"
    Me.lblSchedule.Visible = False
    '
    'cmdSchedule
    '
    Me.cmdSchedule.Location = New System.Drawing.Point(80, 514)
    Me.cmdSchedule.Name = "cmdSchedule"
    Me.cmdSchedule.Size = New System.Drawing.Size(581, 39)
    Me.cmdSchedule.TabIndex = 18
    Me.cmdSchedule.Text = "Schedule"
    Me.cmdSchedule.UseVisualStyleBackColor = True
    Me.cmdSchedule.Visible = False
    '
    'cboFoundation
    '
    Me.cboFoundation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboFoundation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboFoundation.FormattingEnabled = True
    Me.cboFoundation.Location = New System.Drawing.Point(80, 58)
    Me.cboFoundation.Name = "cboFoundation"
    Me.cboFoundation.Size = New System.Drawing.Size(220, 21)
    Me.cboFoundation.Sorted = True
    Me.cboFoundation.TabIndex = 7
    Me.cboFoundation.Tag = "Addresses"
    '
    'lblFoundation
    '
    Me.lblFoundation.AutoSize = True
    Me.lblFoundation.Location = New System.Drawing.Point(6, 61)
    Me.lblFoundation.Name = "lblFoundation"
    Me.lblFoundation.Size = New System.Drawing.Size(60, 13)
    Me.lblFoundation.TabIndex = 39
    Me.lblFoundation.Text = "Foundation"
    '
    'cboPhone_Type2
    '
    Me.cboPhone_Type2.AutoCompleteCustomSource.AddRange(New String() {"HOME", "WORK", "CELL"})
    Me.cboPhone_Type2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboPhone_Type2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboPhone_Type2.FormattingEnabled = True
    Me.cboPhone_Type2.Items.AddRange(New Object() {"HOME", "CELL", "WORK"})
    Me.cboPhone_Type2.Location = New System.Drawing.Point(329, 6)
    Me.cboPhone_Type2.Name = "cboPhone_Type2"
    Me.cboPhone_Type2.Size = New System.Drawing.Size(82, 21)
    Me.cboPhone_Type2.TabIndex = 3
    Me.cboPhone_Type2.Tag = "Clients"
    '
    'cboPhone_Type1
    '
    Me.cboPhone_Type1.AutoCompleteCustomSource.AddRange(New String() {"HOME", "WORK", "CELL"})
    Me.cboPhone_Type1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboPhone_Type1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboPhone_Type1.FormattingEnabled = True
    Me.cboPhone_Type1.Items.AddRange(New Object() {"HOME", "CELL", "WORK"})
    Me.cboPhone_Type1.Location = New System.Drawing.Point(2, 5)
    Me.cboPhone_Type1.Name = "cboPhone_Type1"
    Me.cboPhone_Type1.Size = New System.Drawing.Size(72, 21)
    Me.cboPhone_Type1.TabIndex = 0
    Me.cboPhone_Type1.Tag = "Clients"
    '
    'lblEmail
    '
    Me.lblEmail.AutoSize = True
    Me.lblEmail.Location = New System.Drawing.Point(6, 35)
    Me.lblEmail.Name = "lblEmail"
    Me.lblEmail.Size = New System.Drawing.Size(32, 13)
    Me.lblEmail.TabIndex = 36
    Me.lblEmail.Text = "Email"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.valSet_Date)
    Me.GroupBox1.Controls.Add(Me.valConfirmer)
    Me.GroupBox1.Controls.Add(Me.valConfirmed)
    Me.GroupBox1.Controls.Add(Me.valDispo_Done)
    Me.GroupBox1.Controls.Add(Me.TextBox1)
    Me.GroupBox1.Controls.Add(Me.TextBox2)
    Me.GroupBox1.Controls.Add(Me.TextBox3)
    Me.GroupBox1.Controls.Add(Me.TextBox4)
    Me.GroupBox1.Location = New System.Drawing.Point(682, 7)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(171, 161)
    Me.GroupBox1.TabIndex = 101
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Invisible Controls"
    Me.GroupBox1.Visible = False
    '
    'valSet_Date
    '
    Me.valSet_Date.Location = New System.Drawing.Point(87, 93)
    Me.valSet_Date.Name = "valSet_Date"
    Me.valSet_Date.Size = New System.Drawing.Size(75, 20)
    Me.valSet_Date.TabIndex = 9
    Me.valSet_Date.Tag = "Schedule"
    '
    'valConfirmer
    '
    Me.valConfirmer.Location = New System.Drawing.Point(6, 93)
    Me.valConfirmer.Name = "valConfirmer"
    Me.valConfirmer.Size = New System.Drawing.Size(75, 20)
    Me.valConfirmer.TabIndex = 8
    Me.valConfirmer.Tag = "Schedule"
    '
    'valConfirmed
    '
    Me.valConfirmed.AutoSize = True
    Me.valConfirmed.Location = New System.Drawing.Point(74, 73)
    Me.valConfirmed.Name = "valConfirmed"
    Me.valConfirmed.Size = New System.Drawing.Size(15, 14)
    Me.valConfirmed.TabIndex = 7
    Me.valConfirmed.Tag = "Schedule"
    Me.valConfirmed.UseVisualStyleBackColor = True
    '
    'valDispo_Done
    '
    Me.valDispo_Done.AutoSize = True
    Me.valDispo_Done.Location = New System.Drawing.Point(11, 73)
    Me.valDispo_Done.Name = "valDispo_Done"
    Me.valDispo_Done.Size = New System.Drawing.Size(15, 14)
    Me.valDispo_Done.TabIndex = 4
    Me.valDispo_Done.Tag = "Schedule"
    Me.valDispo_Done.UseVisualStyleBackColor = True
    '
    'TextBox1
    '
    Me.TextBox1.Location = New System.Drawing.Point(87, 19)
    Me.TextBox1.Name = "TextBox1"
    Me.TextBox1.Size = New System.Drawing.Size(75, 20)
    Me.TextBox1.TabIndex = 3
    Me.TextBox1.Tag = "Schedule"
    '
    'TextBox2
    '
    Me.TextBox2.Location = New System.Drawing.Point(87, 46)
    Me.TextBox2.Name = "TextBox2"
    Me.TextBox2.Size = New System.Drawing.Size(75, 20)
    Me.TextBox2.TabIndex = 2
    Me.TextBox2.Tag = "Schedule"
    '
    'TextBox3
    '
    Me.TextBox3.Location = New System.Drawing.Point(6, 45)
    Me.TextBox3.Name = "TextBox3"
    Me.TextBox3.Size = New System.Drawing.Size(75, 20)
    Me.TextBox3.TabIndex = 1
    Me.TextBox3.Tag = "Schedule"
    '
    'TextBox4
    '
    Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.TextBox4.Location = New System.Drawing.Point(6, 19)
    Me.TextBox4.Name = "TextBox4"
    Me.TextBox4.Size = New System.Drawing.Size(75, 20)
    Me.TextBox4.TabIndex = 0
    Me.TextBox4.Tag = "Schedule"
    '
    'tpgEstimate
    '
    Me.tpgEstimate.BackColor = System.Drawing.SystemColors.Control
    Me.tpgEstimate.Controls.Add(Me.cboSource2)
    Me.tpgEstimate.Controls.Add(Me.cboSource)
    Me.tpgEstimate.Controls.Add(Me.lblSource)
    Me.tpgEstimate.Controls.Add(Me.cmdEst_Schedule)
    Me.tpgEstimate.Controls.Add(Me.lblEst_Schedule)
    Me.tpgEstimate.Controls.Add(Me.grdEst_Appts)
    Me.tpgEstimate.Controls.Add(Me.txtEstimator_Comments)
    Me.tpgEstimate.Controls.Add(Me.lblZone)
    Me.tpgEstimate.Controls.Add(Me.cboZone)
    Me.tpgEstimate.Controls.Add(Me.lblEstimatorComments)
    Me.tpgEstimate.Controls.Add(Me.fraProblem)
    Me.tpgEstimate.Location = New System.Drawing.Point(4, 22)
    Me.tpgEstimate.Name = "tpgEstimate"
    Me.tpgEstimate.Padding = New System.Windows.Forms.Padding(3)
    Me.tpgEstimate.Size = New System.Drawing.Size(745, 559)
    Me.tpgEstimate.TabIndex = 1
    Me.tpgEstimate.Text = "DBI Estimate"
    '
    'cboSource2
    '
    Me.cboSource2.AutoCompleteCustomSource.AddRange(New String() {"RESET", "REVAL"})
    Me.cboSource2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboSource2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboSource2.FormattingEnabled = True
    Me.cboSource2.Items.AddRange(New Object() {"RESET", "REVAL"})
    Me.cboSource2.Location = New System.Drawing.Point(288, 359)
    Me.cboSource2.Name = "cboSource2"
    Me.cboSource2.Size = New System.Drawing.Size(145, 21)
    Me.cboSource2.TabIndex = 122
    Me.cboSource2.Tag = "Est_Info"
    '
    'cboSource
    '
    Me.cboSource.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboSource.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboSource.FormattingEnabled = True
    Me.cboSource.Location = New System.Drawing.Point(133, 359)
    Me.cboSource.Name = "cboSource"
    Me.cboSource.Size = New System.Drawing.Size(149, 21)
    Me.cboSource.TabIndex = 121
    Me.cboSource.Tag = "Est_Info"
    '
    'lblSource
    '
    Me.lblSource.AutoSize = True
    Me.lblSource.Location = New System.Drawing.Point(86, 362)
    Me.lblSource.Name = "lblSource"
    Me.lblSource.Size = New System.Drawing.Size(41, 13)
    Me.lblSource.TabIndex = 123
    Me.lblSource.Text = "Source"
    '
    'cmdEst_Schedule
    '
    Me.cmdEst_Schedule.Location = New System.Drawing.Point(6, 353)
    Me.cmdEst_Schedule.Name = "cmdEst_Schedule"
    Me.cmdEst_Schedule.Size = New System.Drawing.Size(73, 30)
    Me.cmdEst_Schedule.TabIndex = 120
    Me.cmdEst_Schedule.Text = "Schedule"
    Me.cmdEst_Schedule.UseVisualStyleBackColor = True
    '
    'lblEst_Schedule
    '
    Me.lblEst_Schedule.AutoSize = True
    Me.lblEst_Schedule.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblEst_Schedule.Location = New System.Drawing.Point(274, 81)
    Me.lblEst_Schedule.Name = "lblEst_Schedule"
    Me.lblEst_Schedule.Size = New System.Drawing.Size(196, 20)
    Me.lblEst_Schedule.TabIndex = 117
    Me.lblEst_Schedule.Text = "Estimate Appointments"
    '
    'grdEst_Appts
    '
    Me.grdEst_Appts.AllowUserToAddRows = False
    Me.grdEst_Appts.AllowUserToDeleteRows = False
    Me.grdEst_Appts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdEst_Appts.Location = New System.Drawing.Point(6, 104)
    Me.grdEst_Appts.Name = "grdEst_Appts"
    Me.grdEst_Appts.Size = New System.Drawing.Size(733, 243)
    Me.grdEst_Appts.TabIndex = 116
    '
    'txtEstimator_Comments
    '
    Me.txtEstimator_Comments.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtEstimator_Comments.Location = New System.Drawing.Point(81, 412)
    Me.txtEstimator_Comments.Multiline = True
    Me.txtEstimator_Comments.Name = "txtEstimator_Comments"
    Me.txtEstimator_Comments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txtEstimator_Comments.Size = New System.Drawing.Size(593, 138)
    Me.txtEstimator_Comments.TabIndex = 1
    Me.txtEstimator_Comments.Tag = "Est_Info"
    '
    'lblZone
    '
    Me.lblZone.AutoSize = True
    Me.lblZone.Location = New System.Drawing.Point(593, 24)
    Me.lblZone.Name = "lblZone"
    Me.lblZone.Size = New System.Drawing.Size(32, 13)
    Me.lblZone.TabIndex = 57
    Me.lblZone.Text = "Zone"
    '
    'cboZone
    '
    Me.cboZone.FormattingEnabled = True
    Me.cboZone.Items.AddRange(New Object() {"0", "1", "2", "3"})
    Me.cboZone.Location = New System.Drawing.Point(661, 21)
    Me.cboZone.Name = "cboZone"
    Me.cboZone.Size = New System.Drawing.Size(81, 21)
    Me.cboZone.TabIndex = 56
    Me.cboZone.Tag = "Est_Info"
    '
    'lblEstimatorComments
    '
    Me.lblEstimatorComments.Location = New System.Drawing.Point(6, 412)
    Me.lblEstimatorComments.Name = "lblEstimatorComments"
    Me.lblEstimatorComments.Size = New System.Drawing.Size(60, 29)
    Me.lblEstimatorComments.TabIndex = 55
    Me.lblEstimatorComments.Text = "Estimator Comments"
    '
    'fraProblem
    '
    Me.fraProblem.Controls.Add(Me.txtOther_Text)
    Me.fraProblem.Controls.Add(Me.chkCracks)
    Me.fraProblem.Controls.Add(Me.chkOther)
    Me.fraProblem.Controls.Add(Me.chkWater)
    Me.fraProblem.Controls.Add(Me.chkRemodeling)
    Me.fraProblem.Controls.Add(Me.chkSettling)
    Me.fraProblem.Controls.Add(Me.chkMold)
    Me.fraProblem.Controls.Add(Me.chkBowed_Walls)
    Me.fraProblem.Location = New System.Drawing.Point(6, 6)
    Me.fraProblem.Name = "fraProblem"
    Me.fraProblem.Size = New System.Drawing.Size(518, 68)
    Me.fraProblem.TabIndex = 111
    Me.fraProblem.TabStop = False
    Me.fraProblem.Text = "Problem"
    '
    'txtOther_Text
    '
    Me.txtOther_Text.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtOther_Text.Location = New System.Drawing.Point(13, 42)
    Me.txtOther_Text.Name = "txtOther_Text"
    Me.txtOther_Text.Size = New System.Drawing.Size(496, 20)
    Me.txtOther_Text.TabIndex = 118
    Me.txtOther_Text.Tag = "Est_Info"
    Me.txtOther_Text.Visible = False
    '
    'chkCracks
    '
    Me.chkCracks.AutoSize = True
    Me.chkCracks.Location = New System.Drawing.Point(74, 19)
    Me.chkCracks.Name = "chkCracks"
    Me.chkCracks.Size = New System.Drawing.Size(59, 17)
    Me.chkCracks.TabIndex = 112
    Me.chkCracks.Tag = "Est_Info"
    Me.chkCracks.Text = "Cracks"
    Me.chkCracks.UseVisualStyleBackColor = True
    '
    'chkOther
    '
    Me.chkOther.AutoSize = True
    Me.chkOther.Location = New System.Drawing.Point(443, 19)
    Me.chkOther.Name = "chkOther"
    Me.chkOther.Size = New System.Drawing.Size(52, 17)
    Me.chkOther.TabIndex = 117
    Me.chkOther.Tag = "Est_Info"
    Me.chkOther.Text = "Other"
    Me.chkOther.UseVisualStyleBackColor = True
    '
    'chkWater
    '
    Me.chkWater.AutoSize = True
    Me.chkWater.Location = New System.Drawing.Point(13, 19)
    Me.chkWater.Name = "chkWater"
    Me.chkWater.Size = New System.Drawing.Size(55, 17)
    Me.chkWater.TabIndex = 111
    Me.chkWater.Tag = "Est_Info"
    Me.chkWater.Text = "Water"
    Me.chkWater.UseVisualStyleBackColor = True
    '
    'chkRemodeling
    '
    Me.chkRemodeling.AutoSize = True
    Me.chkRemodeling.Location = New System.Drawing.Point(355, 19)
    Me.chkRemodeling.Name = "chkRemodeling"
    Me.chkRemodeling.Size = New System.Drawing.Size(82, 17)
    Me.chkRemodeling.TabIndex = 116
    Me.chkRemodeling.Tag = "Est_Info"
    Me.chkRemodeling.Text = "Remodeling"
    Me.chkRemodeling.UseVisualStyleBackColor = True
    '
    'chkSettling
    '
    Me.chkSettling.AutoSize = True
    Me.chkSettling.Location = New System.Drawing.Point(139, 19)
    Me.chkSettling.Name = "chkSettling"
    Me.chkSettling.Size = New System.Drawing.Size(61, 17)
    Me.chkSettling.TabIndex = 113
    Me.chkSettling.Tag = "Est_Info"
    Me.chkSettling.Text = "Settling"
    Me.chkSettling.UseVisualStyleBackColor = True
    '
    'chkMold
    '
    Me.chkMold.AutoSize = True
    Me.chkMold.Location = New System.Drawing.Point(300, 19)
    Me.chkMold.Name = "chkMold"
    Me.chkMold.Size = New System.Drawing.Size(49, 17)
    Me.chkMold.TabIndex = 115
    Me.chkMold.Tag = "Est_Info"
    Me.chkMold.Text = "Mold"
    Me.chkMold.UseVisualStyleBackColor = True
    '
    'chkBowed_Walls
    '
    Me.chkBowed_Walls.AutoSize = True
    Me.chkBowed_Walls.Location = New System.Drawing.Point(206, 19)
    Me.chkBowed_Walls.Name = "chkBowed_Walls"
    Me.chkBowed_Walls.Size = New System.Drawing.Size(88, 17)
    Me.chkBowed_Walls.TabIndex = 114
    Me.chkBowed_Walls.Tag = "Est_Info"
    Me.chkBowed_Walls.Text = "Bowed Walls"
    Me.chkBowed_Walls.UseVisualStyleBackColor = True
    '
    'tpgDispos
    '
    Me.tpgDispos.Controls.Add(Me.grdProduction)
    Me.tpgDispos.Controls.Add(Me.grdDispo)
    Me.tpgDispos.Location = New System.Drawing.Point(4, 22)
    Me.tpgDispos.Name = "tpgDispos"
    Me.tpgDispos.Size = New System.Drawing.Size(745, 559)
    Me.tpgDispos.TabIndex = 2
    Me.tpgDispos.Text = "Dispos & Production"
    Me.tpgDispos.UseVisualStyleBackColor = True
    '
    'grdProduction
    '
    Me.grdProduction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdProduction.Location = New System.Drawing.Point(3, 281)
    Me.grdProduction.Name = "grdProduction"
    Me.grdProduction.Size = New System.Drawing.Size(739, 275)
    Me.grdProduction.TabIndex = 1
    '
    'grdDispo
    '
    Me.grdDispo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdDispo.Location = New System.Drawing.Point(3, 3)
    Me.grdDispo.Name = "grdDispo"
    Me.grdDispo.Size = New System.Drawing.Size(739, 275)
    Me.grdDispo.TabIndex = 0
    '
    'tpgAnnual
    '
    Me.tpgAnnual.BackColor = System.Drawing.SystemColors.Control
    Me.tpgAnnual.Controls.Add(Me.lblAnnualPrice)
    Me.tpgAnnual.Controls.Add(Me.txtPrice)
    Me.tpgAnnual.Controls.Add(Me.cmdAnnualSchedule)
    Me.tpgAnnual.Controls.Add(Me.Label4)
    Me.tpgAnnual.Controls.Add(Me.grdAnnual)
    Me.tpgAnnual.Controls.Add(Me.Label2)
    Me.tpgAnnual.Controls.Add(Me.txtPump_Count)
    Me.tpgAnnual.Controls.Add(Me.Label1)
    Me.tpgAnnual.Controls.Add(Me.dtpLast_Annual)
    Me.tpgAnnual.Location = New System.Drawing.Point(4, 22)
    Me.tpgAnnual.Name = "tpgAnnual"
    Me.tpgAnnual.Size = New System.Drawing.Size(745, 559)
    Me.tpgAnnual.TabIndex = 6
    Me.tpgAnnual.Text = "Annual"
    '
    'lblAnnualPrice
    '
    Me.lblAnnualPrice.AutoSize = True
    Me.lblAnnualPrice.Location = New System.Drawing.Point(405, 16)
    Me.lblAnnualPrice.Name = "lblAnnualPrice"
    Me.lblAnnualPrice.Size = New System.Drawing.Size(31, 13)
    Me.lblAnnualPrice.TabIndex = 123
    Me.lblAnnualPrice.Text = "Price"
    '
    'txtPrice
    '
    Me.txtPrice.Location = New System.Drawing.Point(442, 12)
    Me.txtPrice.Name = "txtPrice"
    Me.txtPrice.Size = New System.Drawing.Size(84, 20)
    Me.txtPrice.TabIndex = 122
    Me.txtPrice.Tag = "Annual"
    '
    'cmdAnnualSchedule
    '
    Me.cmdAnnualSchedule.Location = New System.Drawing.Point(3, 351)
    Me.cmdAnnualSchedule.Name = "cmdAnnualSchedule"
    Me.cmdAnnualSchedule.Size = New System.Drawing.Size(73, 30)
    Me.cmdAnnualSchedule.TabIndex = 121
    Me.cmdAnnualSchedule.Text = "Schedule"
    Me.cmdAnnualSchedule.UseVisualStyleBackColor = True
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label4.Location = New System.Drawing.Point(271, 79)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(181, 20)
    Me.Label4.TabIndex = 119
    Me.Label4.Text = "Annual Appointments"
    '
    'grdAnnual
    '
    Me.grdAnnual.AllowUserToAddRows = False
    Me.grdAnnual.AllowUserToDeleteRows = False
    Me.grdAnnual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdAnnual.Location = New System.Drawing.Point(3, 102)
    Me.grdAnnual.Name = "grdAnnual"
    Me.grdAnnual.Size = New System.Drawing.Size(733, 243)
    Me.grdAnnual.TabIndex = 118
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(197, 16)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(91, 13)
    Me.Label2.TabIndex = 3
    Me.Label2.Text = "Number of Pumps"
    '
    'txtPump_Count
    '
    Me.txtPump_Count.Location = New System.Drawing.Point(294, 13)
    Me.txtPump_Count.Name = "txtPump_Count"
    Me.txtPump_Count.Size = New System.Drawing.Size(84, 20)
    Me.txtPump_Count.TabIndex = 2
    Me.txtPump_Count.Tag = "Annual"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(3, 19)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(83, 13)
    Me.Label1.TabIndex = 1
    Me.Label1.Text = "Last Annul Date"
    '
    'dtpLast_Annual
    '
    Me.dtpLast_Annual.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpLast_Annual.Location = New System.Drawing.Point(100, 13)
    Me.dtpLast_Annual.Name = "dtpLast_Annual"
    Me.dtpLast_Annual.Size = New System.Drawing.Size(84, 20)
    Me.dtpLast_Annual.TabIndex = 0
    Me.dtpLast_Annual.Tag = "Annual"
    Me.dtpLast_Annual.Value = New Date(2011, 5, 7, 0, 0, 0, 0)
    '
    'tpgService
    '
    Me.tpgService.BackColor = System.Drawing.SystemColors.Control
    Me.tpgService.Controls.Add(Me.Button3)
    Me.tpgService.Controls.Add(Me.Label5)
    Me.tpgService.Controls.Add(Me.grdService)
    Me.tpgService.Location = New System.Drawing.Point(4, 22)
    Me.tpgService.Name = "tpgService"
    Me.tpgService.Size = New System.Drawing.Size(745, 559)
    Me.tpgService.TabIndex = 5
    Me.tpgService.Text = "Service"
    '
    'Button3
    '
    Me.Button3.Location = New System.Drawing.Point(9, 330)
    Me.Button3.Name = "Button3"
    Me.Button3.Size = New System.Drawing.Size(73, 36)
    Me.Button3.TabIndex = 125
    Me.Button3.Text = "New Service"
    Me.Button3.UseVisualStyleBackColor = True
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label5.Location = New System.Drawing.Point(290, 10)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(103, 20)
    Me.Label5.TabIndex = 123
    Me.Label5.Text = "Service Log"
    '
    'grdService
    '
    Me.grdService.AllowUserToAddRows = False
    Me.grdService.AllowUserToDeleteRows = False
    Me.grdService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdService.Location = New System.Drawing.Point(3, 33)
    Me.grdService.Name = "grdService"
    Me.grdService.Size = New System.Drawing.Size(733, 291)
    Me.grdService.TabIndex = 122
    '
    'tpgAlways
    '
    Me.tpgAlways.BackColor = System.Drawing.SystemColors.Control
    Me.tpgAlways.Controls.Add(Me.cmdAlwaysSchedule)
    Me.tpgAlways.Controls.Add(Me.Label6)
    Me.tpgAlways.Controls.Add(Me.grdAlways)
    Me.tpgAlways.Controls.Add(Me.Label8)
    Me.tpgAlways.Controls.Add(Me.cboAP_Zone)
    Me.tpgAlways.Controls.Add(Me.GroupBox2)
    Me.tpgAlways.Location = New System.Drawing.Point(4, 22)
    Me.tpgAlways.Name = "tpgAlways"
    Me.tpgAlways.Size = New System.Drawing.Size(745, 559)
    Me.tpgAlways.TabIndex = 4
    Me.tpgAlways.Text = "Always"
    '
    'cmdAlwaysSchedule
    '
    Me.cmdAlwaysSchedule.Location = New System.Drawing.Point(3, 350)
    Me.cmdAlwaysSchedule.Name = "cmdAlwaysSchedule"
    Me.cmdAlwaysSchedule.Size = New System.Drawing.Size(73, 30)
    Me.cmdAlwaysSchedule.TabIndex = 128
    Me.cmdAlwaysSchedule.Text = "Schedule"
    Me.cmdAlwaysSchedule.UseVisualStyleBackColor = True
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label6.Location = New System.Drawing.Point(271, 78)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(180, 20)
    Me.Label6.TabIndex = 127
    Me.Label6.Text = "Always Appointments"
    '
    'grdAlways
    '
    Me.grdAlways.AllowUserToAddRows = False
    Me.grdAlways.AllowUserToDeleteRows = False
    Me.grdAlways.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdAlways.Location = New System.Drawing.Point(3, 101)
    Me.grdAlways.Name = "grdAlways"
    Me.grdAlways.Size = New System.Drawing.Size(733, 243)
    Me.grdAlways.TabIndex = 126
    '
    'Label8
    '
    Me.Label8.AutoSize = True
    Me.Label8.Location = New System.Drawing.Point(590, 32)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(32, 13)
    Me.Label8.TabIndex = 122
    Me.Label8.Text = "Zone"
    '
    'cboAP_Zone
    '
    Me.cboAP_Zone.FormattingEnabled = True
    Me.cboAP_Zone.Items.AddRange(New Object() {"0", "1", "2", "3"})
    Me.cboAP_Zone.Location = New System.Drawing.Point(658, 29)
    Me.cboAP_Zone.Name = "cboAP_Zone"
    Me.cboAP_Zone.Size = New System.Drawing.Size(81, 21)
    Me.cboAP_Zone.TabIndex = 121
    Me.cboAP_Zone.Tag = "Always"
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.cboLowes_Store)
    Me.GroupBox2.Controls.Add(Me.txtAdditional_Text)
    Me.GroupBox2.Controls.Add(Me.chkDrain)
    Me.GroupBox2.Controls.Add(Me.chkAdditional)
    Me.GroupBox2.Controls.Add(Me.chkWater_Heater)
    Me.GroupBox2.Controls.Add(Me.chkLowes)
    Me.GroupBox2.Controls.Add(Me.chkToilet)
    Me.GroupBox2.Controls.Add(Me.chkDisposal)
    Me.GroupBox2.Controls.Add(Me.chkFaucet)
    Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(581, 68)
    Me.GroupBox2.TabIndex = 123
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Problem"
    '
    'cboLowes_Store
    '
    Me.cboLowes_Store.FormattingEnabled = True
    Me.cboLowes_Store.Items.AddRange(New Object() {"0", "1", "2", "3"})
    Me.cboLowes_Store.Location = New System.Drawing.Point(424, 17)
    Me.cboLowes_Store.Name = "cboLowes_Store"
    Me.cboLowes_Store.Size = New System.Drawing.Size(97, 21)
    Me.cboLowes_Store.TabIndex = 129
    Me.cboLowes_Store.Tag = "Always"
    '
    'txtAdditional_Text
    '
    Me.txtAdditional_Text.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
    Me.txtAdditional_Text.Location = New System.Drawing.Point(79, 42)
    Me.txtAdditional_Text.Name = "txtAdditional_Text"
    Me.txtAdditional_Text.Size = New System.Drawing.Size(496, 20)
    Me.txtAdditional_Text.TabIndex = 118
    Me.txtAdditional_Text.Tag = "Always"
    Me.txtAdditional_Text.Visible = False
    '
    'chkDrain
    '
    Me.chkDrain.AutoSize = True
    Me.chkDrain.Location = New System.Drawing.Point(109, 19)
    Me.chkDrain.Name = "chkDrain"
    Me.chkDrain.Size = New System.Drawing.Size(51, 17)
    Me.chkDrain.TabIndex = 112
    Me.chkDrain.Tag = "Always"
    Me.chkDrain.Text = "Drain"
    Me.chkDrain.UseVisualStyleBackColor = True
    '
    'chkAdditional
    '
    Me.chkAdditional.AutoSize = True
    Me.chkAdditional.Location = New System.Drawing.Point(13, 44)
    Me.chkAdditional.Name = "chkAdditional"
    Me.chkAdditional.Size = New System.Drawing.Size(52, 17)
    Me.chkAdditional.TabIndex = 117
    Me.chkAdditional.Tag = "Always"
    Me.chkAdditional.Text = "Other"
    Me.chkAdditional.UseVisualStyleBackColor = True
    '
    'chkWater_Heater
    '
    Me.chkWater_Heater.AutoSize = True
    Me.chkWater_Heater.Location = New System.Drawing.Point(13, 19)
    Me.chkWater_Heater.Name = "chkWater_Heater"
    Me.chkWater_Heater.Size = New System.Drawing.Size(90, 17)
    Me.chkWater_Heater.TabIndex = 111
    Me.chkWater_Heater.Tag = "Always"
    Me.chkWater_Heater.Text = "Water Heater"
    Me.chkWater_Heater.UseVisualStyleBackColor = True
    '
    'chkLowes
    '
    Me.chkLowes.AutoSize = True
    Me.chkLowes.Location = New System.Drawing.Point(361, 19)
    Me.chkLowes.Name = "chkLowes"
    Me.chkLowes.Size = New System.Drawing.Size(57, 17)
    Me.chkLowes.TabIndex = 116
    Me.chkLowes.Tag = "Always"
    Me.chkLowes.Text = "Lowes"
    Me.chkLowes.UseVisualStyleBackColor = True
    '
    'chkToilet
    '
    Me.chkToilet.AutoSize = True
    Me.chkToilet.Location = New System.Drawing.Point(166, 19)
    Me.chkToilet.Name = "chkToilet"
    Me.chkToilet.Size = New System.Drawing.Size(52, 17)
    Me.chkToilet.TabIndex = 113
    Me.chkToilet.Tag = "Always"
    Me.chkToilet.Text = "Toilet"
    Me.chkToilet.UseVisualStyleBackColor = True
    '
    'chkDisposal
    '
    Me.chkDisposal.AutoSize = True
    Me.chkDisposal.Location = New System.Drawing.Point(289, 19)
    Me.chkDisposal.Name = "chkDisposal"
    Me.chkDisposal.Size = New System.Drawing.Size(66, 17)
    Me.chkDisposal.TabIndex = 115
    Me.chkDisposal.Tag = "Always"
    Me.chkDisposal.Text = "Disposal"
    Me.chkDisposal.UseVisualStyleBackColor = True
    '
    'chkFaucet
    '
    Me.chkFaucet.AutoSize = True
    Me.chkFaucet.Location = New System.Drawing.Point(224, 19)
    Me.chkFaucet.Name = "chkFaucet"
    Me.chkFaucet.Size = New System.Drawing.Size(59, 17)
    Me.chkFaucet.TabIndex = 114
    Me.chkFaucet.Tag = "Always"
    Me.chkFaucet.Text = "Faucet"
    Me.chkFaucet.UseVisualStyleBackColor = True
    '
    'tpgCall_Log
    '
    Me.tpgCall_Log.Controls.Add(Me.fraCallLog)
    Me.tpgCall_Log.Controls.Add(Me.cmdAddCallLog)
    Me.tpgCall_Log.Controls.Add(Me.grdCallLog)
    Me.tpgCall_Log.Location = New System.Drawing.Point(4, 22)
    Me.tpgCall_Log.Name = "tpgCall_Log"
    Me.tpgCall_Log.Size = New System.Drawing.Size(745, 559)
    Me.tpgCall_Log.TabIndex = 7
    Me.tpgCall_Log.Text = "Call Log"
    Me.tpgCall_Log.UseVisualStyleBackColor = True
    '
    'fraCallLog
    '
    Me.fraCallLog.Controls.Add(Me.cmdCallLogClose)
    Me.fraCallLog.Controls.Add(Me.cmdCallLogSave)
    Me.fraCallLog.Controls.Add(Me.Label3)
    Me.fraCallLog.Controls.Add(Me.lblCallType)
    Me.fraCallLog.Controls.Add(Me.txtCallNotes)
    Me.fraCallLog.Controls.Add(Me.cboCallType)
    Me.fraCallLog.Location = New System.Drawing.Point(114, 72)
    Me.fraCallLog.Name = "fraCallLog"
    Me.fraCallLog.Size = New System.Drawing.Size(563, 249)
    Me.fraCallLog.TabIndex = 2
    Me.fraCallLog.TabStop = False
    Me.fraCallLog.Text = "Call Log"
    Me.fraCallLog.Visible = False
    '
    'cmdCallLogClose
    '
    Me.cmdCallLogClose.Location = New System.Drawing.Point(543, 0)
    Me.cmdCallLogClose.Name = "cmdCallLogClose"
    Me.cmdCallLogClose.Size = New System.Drawing.Size(20, 21)
    Me.cmdCallLogClose.TabIndex = 5
    Me.cmdCallLogClose.Text = "X"
    Me.cmdCallLogClose.UseVisualStyleBackColor = True
    '
    'cmdCallLogSave
    '
    Me.cmdCallLogSave.Location = New System.Drawing.Point(475, 213)
    Me.cmdCallLogSave.Name = "cmdCallLogSave"
    Me.cmdCallLogSave.Size = New System.Drawing.Size(81, 30)
    Me.cmdCallLogSave.TabIndex = 4
    Me.cmdCallLogSave.Text = "Save"
    Me.cmdCallLogSave.UseVisualStyleBackColor = True
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(6, 51)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(35, 13)
    Me.Label3.TabIndex = 3
    Me.Label3.Text = "Notes"
    '
    'lblCallType
    '
    Me.lblCallType.AutoSize = True
    Me.lblCallType.Location = New System.Drawing.Point(6, 22)
    Me.lblCallType.Name = "lblCallType"
    Me.lblCallType.Size = New System.Drawing.Size(51, 13)
    Me.lblCallType.TabIndex = 2
    Me.lblCallType.Text = "Call Type"
    '
    'txtCallNotes
    '
    Me.txtCallNotes.Location = New System.Drawing.Point(6, 67)
    Me.txtCallNotes.MaxLength = 255
    Me.txtCallNotes.Multiline = True
    Me.txtCallNotes.Name = "txtCallNotes"
    Me.txtCallNotes.Size = New System.Drawing.Size(551, 136)
    Me.txtCallNotes.TabIndex = 1
    '
    'cboCallType
    '
    Me.cboCallType.FormattingEnabled = True
    Me.cboCallType.Location = New System.Drawing.Point(114, 19)
    Me.cboCallType.Name = "cboCallType"
    Me.cboCallType.Size = New System.Drawing.Size(141, 21)
    Me.cboCallType.TabIndex = 0
    '
    'cmdAddCallLog
    '
    Me.cmdAddCallLog.Location = New System.Drawing.Point(621, 3)
    Me.cmdAddCallLog.Name = "cmdAddCallLog"
    Me.cmdAddCallLog.Size = New System.Drawing.Size(121, 24)
    Me.cmdAddCallLog.TabIndex = 1
    Me.cmdAddCallLog.Text = "Add Call Log"
    Me.cmdAddCallLog.UseVisualStyleBackColor = True
    '
    'grdCallLog
    '
    Me.grdCallLog.AllowUserToAddRows = False
    Me.grdCallLog.AllowUserToDeleteRows = False
    Me.grdCallLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdCallLog.Location = New System.Drawing.Point(3, 33)
    Me.grdCallLog.Name = "grdCallLog"
    Me.grdCallLog.Size = New System.Drawing.Size(739, 523)
    Me.grdCallLog.TabIndex = 0
    Me.grdCallLog.Tag = "CallLog"
    '
    'tpgCallList
    '
    Me.tpgCallList.BackColor = System.Drawing.SystemColors.Control
    Me.tpgCallList.Controls.Add(Me.chkWorkDone)
    Me.tpgCallList.Controls.Add(Me.txtScript)
    Me.tpgCallList.Controls.Add(Me.lblLists)
    Me.tpgCallList.Controls.Add(Me.cboCall_List)
    Me.tpgCallList.Controls.Add(Me.cmdDelay)
    Me.tpgCallList.Controls.Add(Me.cmdList_Remove)
    Me.tpgCallList.Controls.Add(Me.cmdContacted)
    Me.tpgCallList.Location = New System.Drawing.Point(4, 22)
    Me.tpgCallList.Name = "tpgCallList"
    Me.tpgCallList.Size = New System.Drawing.Size(745, 559)
    Me.tpgCallList.TabIndex = 8
    Me.tpgCallList.Text = "Call List"
    '
    'chkWorkDone
    '
    Me.chkWorkDone.AutoSize = True
    Me.chkWorkDone.Location = New System.Drawing.Point(9, 79)
    Me.chkWorkDone.Name = "chkWorkDone"
    Me.chkWorkDone.Size = New System.Drawing.Size(81, 17)
    Me.chkWorkDone.TabIndex = 6
    Me.chkWorkDone.Text = "Work Done"
    Me.chkWorkDone.UseVisualStyleBackColor = True
    '
    'txtScript
    '
    Me.txtScript.Enabled = False
    Me.txtScript.Location = New System.Drawing.Point(3, 102)
    Me.txtScript.Multiline = True
    Me.txtScript.Name = "txtScript"
    Me.txtScript.Size = New System.Drawing.Size(727, 436)
    Me.txtScript.TabIndex = 5
    Me.txtScript.Visible = False
    '
    'lblLists
    '
    Me.lblLists.AutoSize = True
    Me.lblLists.Location = New System.Drawing.Point(348, 34)
    Me.lblLists.Name = "lblLists"
    Me.lblLists.Size = New System.Drawing.Size(28, 13)
    Me.lblLists.TabIndex = 4
    Me.lblLists.Text = "Lists"
    '
    'cboCall_List
    '
    Me.cboCall_List.FormattingEnabled = True
    Me.cboCall_List.Location = New System.Drawing.Point(398, 31)
    Me.cboCall_List.Name = "cboCall_List"
    Me.cboCall_List.Size = New System.Drawing.Size(274, 21)
    Me.cboCall_List.TabIndex = 3
    '
    'cmdDelay
    '
    Me.cmdDelay.Location = New System.Drawing.Point(165, 22)
    Me.cmdDelay.Name = "cmdDelay"
    Me.cmdDelay.Size = New System.Drawing.Size(72, 36)
    Me.cmdDelay.TabIndex = 2
    Me.cmdDelay.Text = "Delay Next Call"
    Me.cmdDelay.UseVisualStyleBackColor = True
    '
    'cmdList_Remove
    '
    Me.cmdList_Remove.Location = New System.Drawing.Point(87, 22)
    Me.cmdList_Remove.Name = "cmdList_Remove"
    Me.cmdList_Remove.Size = New System.Drawing.Size(72, 36)
    Me.cmdList_Remove.TabIndex = 1
    Me.cmdList_Remove.Text = "Remove From Lists"
    Me.cmdList_Remove.UseVisualStyleBackColor = True
    '
    'cmdContacted
    '
    Me.cmdContacted.Location = New System.Drawing.Point(9, 22)
    Me.cmdContacted.Name = "cmdContacted"
    Me.cmdContacted.Size = New System.Drawing.Size(72, 36)
    Me.cmdContacted.TabIndex = 0
    Me.cmdContacted.Text = "Called Cust"
    Me.cmdContacted.UseVisualStyleBackColor = True
    '
    'cmdSearch
    '
    Me.cmdSearch.Location = New System.Drawing.Point(674, 25)
    Me.cmdSearch.Name = "cmdSearch"
    Me.cmdSearch.Size = New System.Drawing.Size(73, 30)
    Me.cmdSearch.TabIndex = 120
    Me.cmdSearch.Text = "Search"
    Me.cmdSearch.UseVisualStyleBackColor = True
    Me.cmdSearch.Visible = False
    '
    'frmLead
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(777, 782)
    Me.Controls.Add(Me.tabPost)
    Me.Controls.Add(Me.mnuMain)
    Me.Controls.Add(Me.fraInvisible)
    Me.Controls.Add(Me.cmdSave)
    Me.Controls.Add(Me.fraStart)
    Me.Controls.Add(Me.cmdSearch)
    Me.Name = "frmLead"
    Me.Text = "Customer Info"
    Me.fraInvisible.ResumeLayout(False)
    Me.fraInvisible.PerformLayout()
    Me.fraStart.ResumeLayout(False)
    Me.fraStart.PerformLayout()
    Me.mnuMain.ResumeLayout(False)
    Me.mnuMain.PerformLayout()
    Me.tabPost.ResumeLayout(False)
    Me.tpgInfo.ResumeLayout(False)
    Me.tpgInfo.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.tpgEstimate.ResumeLayout(False)
    Me.tpgEstimate.PerformLayout()
    CType(Me.grdEst_Appts, System.ComponentModel.ISupportInitialize).EndInit()
    Me.fraProblem.ResumeLayout(False)
    Me.fraProblem.PerformLayout()
    Me.tpgDispos.ResumeLayout(False)
    CType(Me.grdProduction, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.grdDispo, System.ComponentModel.ISupportInitialize).EndInit()
    Me.tpgAnnual.ResumeLayout(False)
    Me.tpgAnnual.PerformLayout()
    CType(Me.grdAnnual, System.ComponentModel.ISupportInitialize).EndInit()
    Me.tpgService.ResumeLayout(False)
    Me.tpgService.PerformLayout()
    CType(Me.grdService, System.ComponentModel.ISupportInitialize).EndInit()
    Me.tpgAlways.ResumeLayout(False)
    Me.tpgAlways.PerformLayout()
    CType(Me.grdAlways, System.ComponentModel.ISupportInitialize).EndInit()
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.tpgCall_Log.ResumeLayout(False)
    Me.fraCallLog.ResumeLayout(False)
    Me.fraCallLog.PerformLayout()
    CType(Me.grdCallLog, System.ComponentModel.ISupportInitialize).EndInit()
    Me.tpgCallList.ResumeLayout(False)
    Me.tpgCallList.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents fraInvisible As System.Windows.Forms.GroupBox
  Friend WithEvents valStart_Time As System.Windows.Forms.TextBox
  Friend WithEvents valDate As System.Windows.Forms.TextBox
  Friend WithEvents valPerson As System.Windows.Forms.TextBox
  Friend WithEvents valAppt_Type As System.Windows.Forms.TextBox
  Friend WithEvents cmdSave As System.Windows.Forms.Button
  Friend WithEvents fraStart As System.Windows.Forms.GroupBox
  Friend WithEvents lblCompanyName As System.Windows.Forms.Label
  Friend WithEvents txtCompany_Name As System.Windows.Forms.TextBox
  Friend WithEvents cboCity As System.Windows.Forms.ComboBox
  Friend WithEvents txtZip As System.Windows.Forms.TextBox
  Friend WithEvents lblZip As System.Windows.Forms.Label
  Friend WithEvents cboState As System.Windows.Forms.ComboBox
  Friend WithEvents lblState As System.Windows.Forms.Label
  Friend WithEvents lblCity As System.Windows.Forms.Label
  Friend WithEvents cboAddress_Ending As System.Windows.Forms.ComboBox
  Friend WithEvents cboAddress_Direction As System.Windows.Forms.ComboBox
  Friend WithEvents txtAddress_Numbers As System.Windows.Forms.TextBox
  Friend WithEvents txtAddress As System.Windows.Forms.TextBox
  Friend WithEvents lblAddress As System.Windows.Forms.Label
  Friend WithEvents lblLast_Name2 As System.Windows.Forms.Label
  Friend WithEvents txtLast_Name2 As System.Windows.Forms.TextBox
  Friend WithEvents lblFirst_Name2 As System.Windows.Forms.Label
  Friend WithEvents txtFirst_Name2 As System.Windows.Forms.TextBox
  Friend WithEvents lblLast_Name As System.Windows.Forms.Label
  Friend WithEvents txtLast_Name As System.Windows.Forms.TextBox
  Friend WithEvents lblFirst_Name As System.Windows.Forms.Label
  Friend WithEvents txtFirst_Name As System.Windows.Forms.TextBox
  Friend WithEvents mnuMain As System.Windows.Forms.ToolStrip
  Friend WithEvents mnuFile As System.Windows.Forms.ToolStripDropDownButton
  Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuLeadPrint As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuLeadEmail As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuForms As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuSVC_Form As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuAM_Form As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuAP_Form As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuBEST_Form As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuPrinterOptions As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuPrinterChange As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents tabPost As System.Windows.Forms.TabControl
  Friend WithEvents tpgInfo As System.Windows.Forms.TabPage
  Friend WithEvents lblDriveTime As System.Windows.Forms.Label
  Friend WithEvents txtDriveTime As System.Windows.Forms.TextBox
  Friend WithEvents txtMiles As System.Windows.Forms.TextBox
  Friend WithEvents txtDirections As System.Windows.Forms.TextBox
  Friend WithEvents txtPhone2_EXT As System.Windows.Forms.TextBox
  Friend WithEvents txtPhone1_EXT As System.Windows.Forms.TextBox
  Friend WithEvents txtNewComment As System.Windows.Forms.TextBox
  Friend WithEvents txtComments As System.Windows.Forms.TextBox
  Friend WithEvents txtEmail As System.Windows.Forms.TextBox
  Friend WithEvents txtPhone2 As System.Windows.Forms.TextBox
  Friend WithEvents txtPhone1 As System.Windows.Forms.TextBox
  Friend WithEvents lblMiles As System.Windows.Forms.Label
  Friend WithEvents lblDirections As System.Windows.Forms.Label
  Friend WithEvents lblComments As System.Windows.Forms.Label
  Friend WithEvents lblSchedule As System.Windows.Forms.Label
  Friend WithEvents cmdSchedule As System.Windows.Forms.Button
  Friend WithEvents cboFoundation As System.Windows.Forms.ComboBox
  Friend WithEvents lblFoundation As System.Windows.Forms.Label
  Friend WithEvents cboPhone_Type2 As System.Windows.Forms.ComboBox
  Friend WithEvents cboPhone_Type1 As System.Windows.Forms.ComboBox
  Friend WithEvents lblEmail As System.Windows.Forms.Label
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents valSet_Date As System.Windows.Forms.TextBox
  Friend WithEvents valConfirmer As System.Windows.Forms.TextBox
  Friend WithEvents valConfirmed As System.Windows.Forms.CheckBox
  Friend WithEvents valDispo_Done As System.Windows.Forms.CheckBox
  Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
  Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
  Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
  Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
  Friend WithEvents tpgEstimate As System.Windows.Forms.TabPage
  Friend WithEvents txtEstimator_Comments As System.Windows.Forms.TextBox
  Friend WithEvents lblZone As System.Windows.Forms.Label
  Friend WithEvents cboZone As System.Windows.Forms.ComboBox
  Friend WithEvents lblEstimatorComments As System.Windows.Forms.Label
  Friend WithEvents fraProblem As System.Windows.Forms.GroupBox
  Friend WithEvents txtOther_Text As System.Windows.Forms.TextBox
  Friend WithEvents chkCracks As System.Windows.Forms.CheckBox
  Friend WithEvents chkOther As System.Windows.Forms.CheckBox
  Friend WithEvents chkWater As System.Windows.Forms.CheckBox
  Friend WithEvents chkRemodeling As System.Windows.Forms.CheckBox
  Friend WithEvents chkSettling As System.Windows.Forms.CheckBox
  Friend WithEvents chkMold As System.Windows.Forms.CheckBox
  Friend WithEvents chkBowed_Walls As System.Windows.Forms.CheckBox
  Friend WithEvents tpgAnnual As System.Windows.Forms.TabPage
  Friend WithEvents tpgService As System.Windows.Forms.TabPage
  Friend WithEvents tpgAlways As System.Windows.Forms.TabPage
  Friend WithEvents tpgDispos As System.Windows.Forms.TabPage
  Friend WithEvents cmdEst_Schedule As System.Windows.Forms.Button
  Friend WithEvents lblEst_Schedule As System.Windows.Forms.Label
  Friend WithEvents grdEst_Appts As System.Windows.Forms.DataGridView
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents grdAnnual As System.Windows.Forms.DataGridView
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents txtPump_Count As System.Windows.Forms.TextBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents dtpLast_Annual As System.Windows.Forms.DateTimePicker
  Friend WithEvents tpgCall_Log As System.Windows.Forms.TabPage
  Friend WithEvents cmdAnnualSchedule As System.Windows.Forms.Button
  Friend WithEvents Button3 As System.Windows.Forms.Button
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents grdService As System.Windows.Forms.DataGridView
  Friend WithEvents cmdAlwaysSchedule As System.Windows.Forms.Button
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents grdAlways As System.Windows.Forms.DataGridView
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents cboAP_Zone As System.Windows.Forms.ComboBox
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
  Friend WithEvents txtAdditional_Text As System.Windows.Forms.TextBox
  Friend WithEvents chkDrain As System.Windows.Forms.CheckBox
  Friend WithEvents chkAdditional As System.Windows.Forms.CheckBox
  Friend WithEvents chkWater_Heater As System.Windows.Forms.CheckBox
  Friend WithEvents chkLowes As System.Windows.Forms.CheckBox
  Friend WithEvents chkToilet As System.Windows.Forms.CheckBox
  Friend WithEvents chkDisposal As System.Windows.Forms.CheckBox
  Friend WithEvents chkFaucet As System.Windows.Forms.CheckBox
  Friend WithEvents cmdSearch As System.Windows.Forms.Button
  Friend WithEvents cboSource2 As System.Windows.Forms.ComboBox
  Friend WithEvents cboSource As System.Windows.Forms.ComboBox
  Friend WithEvents lblSource As System.Windows.Forms.Label
  Friend WithEvents cboLowes_Store As System.Windows.Forms.ComboBox
  Friend WithEvents cmdDirections As System.Windows.Forms.Button
  Friend WithEvents grdProduction As System.Windows.Forms.DataGridView
  Friend WithEvents grdDispo As System.Windows.Forms.DataGridView
  Friend WithEvents cmdAddCallLog As System.Windows.Forms.Button
  Friend WithEvents grdCallLog As System.Windows.Forms.DataGridView
  Friend WithEvents fraCallLog As System.Windows.Forms.GroupBox
  Friend WithEvents cmdCallLogSave As System.Windows.Forms.Button
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents lblCallType As System.Windows.Forms.Label
  Friend WithEvents txtCallNotes As System.Windows.Forms.TextBox
  Friend WithEvents cboCallType As System.Windows.Forms.ComboBox
  Friend WithEvents cmdCallLogClose As System.Windows.Forms.Button
  Friend WithEvents lblAnnualPrice As System.Windows.Forms.Label
  Friend WithEvents txtPrice As System.Windows.Forms.TextBox
  Friend WithEvents tpgCallList As System.Windows.Forms.TabPage
  Friend WithEvents lblLists As System.Windows.Forms.Label
  Friend WithEvents cboCall_List As System.Windows.Forms.ComboBox
  Friend WithEvents cmdDelay As System.Windows.Forms.Button
  Friend WithEvents cmdList_Remove As System.Windows.Forms.Button
  Friend WithEvents cmdContacted As System.Windows.Forms.Button
  Friend WithEvents txtScript As System.Windows.Forms.TextBox
  Friend WithEvents chkWorkDone As System.Windows.Forms.CheckBox
End Class
