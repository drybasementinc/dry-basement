﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInventoryReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.StartDatePicker = New System.Windows.Forms.DateTimePicker()
    Me.MagicDispoButton = New System.Windows.Forms.Button()
    Me.MagicProductionButton = New System.Windows.Forms.Button()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.EndDatePicker = New System.Windows.Forms.DateTimePicker()
    Me.SuspendLayout()
    '
    'StartDatePicker
    '
    Me.StartDatePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.StartDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.StartDatePicker.Location = New System.Drawing.Point(154, 36)
    Me.StartDatePicker.MaxDate = New Date(3000, 12, 31, 0, 0, 0, 0)
    Me.StartDatePicker.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
    Me.StartDatePicker.Name = "StartDatePicker"
    Me.StartDatePicker.Size = New System.Drawing.Size(127, 26)
    Me.StartDatePicker.TabIndex = 0
    '
    'MagicDispoButton
    '
    Me.MagicDispoButton.Location = New System.Drawing.Point(64, 115)
    Me.MagicDispoButton.Name = "MagicDispoButton"
    Me.MagicDispoButton.Size = New System.Drawing.Size(114, 48)
    Me.MagicDispoButton.TabIndex = 2
    Me.MagicDispoButton.Text = "Magic Dispo"
    Me.MagicDispoButton.UseVisualStyleBackColor = True
    '
    'MagicProductionButton
    '
    Me.MagicProductionButton.Location = New System.Drawing.Point(64, 169)
    Me.MagicProductionButton.Name = "MagicProductionButton"
    Me.MagicProductionButton.Size = New System.Drawing.Size(114, 48)
    Me.MagicProductionButton.TabIndex = 3
    Me.MagicProductionButton.Text = "Magic Production"
    Me.MagicProductionButton.UseVisualStyleBackColor = True
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(61, 39)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(87, 20)
    Me.Label1.TabIndex = 4
    Me.Label1.Text = "Start Date:"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label2.Location = New System.Drawing.Point(61, 71)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(77, 20)
    Me.Label2.TabIndex = 5
    Me.Label2.Text = "End Date"
    '
    'EndDatePicker
    '
    Me.EndDatePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.EndDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.EndDatePicker.Location = New System.Drawing.Point(154, 68)
    Me.EndDatePicker.MaxDate = New Date(3000, 12, 31, 0, 0, 0, 0)
    Me.EndDatePicker.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
    Me.EndDatePicker.Name = "EndDatePicker"
    Me.EndDatePicker.Size = New System.Drawing.Size(127, 26)
    Me.EndDatePicker.TabIndex = 1
    '
    'frmInventoryReport
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(615, 347)
    Me.Controls.Add(Me.EndDatePicker)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.MagicProductionButton)
    Me.Controls.Add(Me.MagicDispoButton)
    Me.Controls.Add(Me.StartDatePicker)
    Me.Name = "frmInventoryReport"
    Me.Text = "Inventory Report"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents StartDatePicker As System.Windows.Forms.DateTimePicker
  Friend WithEvents MagicDispoButton As System.Windows.Forms.Button
  Friend WithEvents MagicProductionButton As System.Windows.Forms.Button
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents EndDatePicker As System.Windows.Forms.DateTimePicker
End Class
