﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReport_Selection
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.cboReportType = New System.Windows.Forms.ComboBox
    Me.lblReportType = New System.Windows.Forms.Label
    Me.SuspendLayout()
    '
    'cboReportType
    '
    Me.cboReportType.FormattingEnabled = True
    Me.cboReportType.Location = New System.Drawing.Point(95, 12)
    Me.cboReportType.Name = "cboReportType"
    Me.cboReportType.Size = New System.Drawing.Size(445, 21)
    Me.cboReportType.TabIndex = 0
    '
    'lblReportType
    '
    Me.lblReportType.AutoSize = True
    Me.lblReportType.Location = New System.Drawing.Point(12, 15)
    Me.lblReportType.Name = "lblReportType"
    Me.lblReportType.Size = New System.Drawing.Size(66, 13)
    Me.lblReportType.TabIndex = 1
    Me.lblReportType.Text = "Report Type"
    '
    'Report_Selection
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(845, 509)
    Me.Controls.Add(Me.lblReportType)
    Me.Controls.Add(Me.cboReportType)
    Me.Name = "Report_Selection"
    Me.Text = "Report_Selection"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
  Friend WithEvents lblReportType As System.Windows.Forms.Label
End Class
