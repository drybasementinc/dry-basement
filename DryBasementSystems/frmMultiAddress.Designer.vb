﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMultiAddress
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.cmdNoMatch = New System.Windows.Forms.Button
    Me.grdMulti = New System.Windows.Forms.DataGridView
    Me.cmdNew = New System.Windows.Forms.Button
    CType(Me.grdMulti, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'cmdNoMatch
    '
    Me.cmdNoMatch.Location = New System.Drawing.Point(395, 284)
    Me.cmdNoMatch.Name = "cmdNoMatch"
    Me.cmdNoMatch.Size = New System.Drawing.Size(115, 29)
    Me.cmdNoMatch.TabIndex = 1
    Me.cmdNoMatch.Text = "No Match"
    Me.cmdNoMatch.UseVisualStyleBackColor = True
    '
    'grdMulti
    '
    Me.grdMulti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.grdMulti.Location = New System.Drawing.Point(12, 12)
    Me.grdMulti.Name = "grdMulti"
    Me.grdMulti.Size = New System.Drawing.Size(513, 266)
    Me.grdMulti.TabIndex = 2
    '
    'cmdNew
    '
    Me.cmdNew.Location = New System.Drawing.Point(274, 284)
    Me.cmdNew.Name = "cmdNew"
    Me.cmdNew.Size = New System.Drawing.Size(115, 29)
    Me.cmdNew.TabIndex = 3
    Me.cmdNew.Text = "New Address"
    Me.cmdNew.UseVisualStyleBackColor = True
    Me.cmdNew.Visible = False
    '
    'frmMultiAddress
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(537, 339)
    Me.Controls.Add(Me.cmdNew)
    Me.Controls.Add(Me.grdMulti)
    Me.Controls.Add(Me.cmdNoMatch)
    Me.Name = "frmMultiAddress"
    Me.Text = "MultiAddress"
    CType(Me.grdMulti, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cmdNoMatch As System.Windows.Forms.Button
  Friend WithEvents grdMulti As System.Windows.Forms.DataGridView
  Friend WithEvents cmdNew As System.Windows.Forms.Button
End Class
