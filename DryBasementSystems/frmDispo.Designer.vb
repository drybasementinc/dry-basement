﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDispo
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.tabDispo = New System.Windows.Forms.TabControl()
    Me.tpgDry = New System.Windows.Forms.TabPage()
    Me.picDry = New System.Windows.Forms.PictureBox()
    Me.fraReset = New System.Windows.Forms.GroupBox()
    Me.cmdExit = New System.Windows.Forms.Button()
    Me.cmdDNQSave = New System.Windows.Forms.Button()
    Me.cmdResetSave = New System.Windows.Forms.Button()
    Me.cmdSchedule = New System.Windows.Forms.Button()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.dtpCall_Date = New System.Windows.Forms.DateTimePicker()
    Me.lblReason = New System.Windows.Forms.Label()
    Me.txtReason = New System.Windows.Forms.TextBox()
    Me.lblDryPrice = New System.Windows.Forms.Label()
    Me.txtDryPrice = New System.Windows.Forms.TextBox()
    Me.fraDryStatus = New System.Windows.Forms.GroupBox()
    Me.optDryDNS = New System.Windows.Forms.RadioButton()
    Me.optDrySold = New System.Windows.Forms.RadioButton()
    Me.fraDryActions = New System.Windows.Forms.GroupBox()
    Me.cmdDryNA = New System.Windows.Forms.Button()
    Me.cmdDryDNQ = New System.Windows.Forms.Button()
    Me.cmdDryReset = New System.Windows.Forms.Button()
    Me.cmdDrySave = New System.Windows.Forms.Button()
    Me.tpgMagic = New System.Windows.Forms.TabPage()
    Me.picMagicDone = New System.Windows.Forms.PictureBox()
    Me.lblMagicPrice = New System.Windows.Forms.Label()
    Me.txtMagicPrice = New System.Windows.Forms.TextBox()
    Me.fraMagicStatus = New System.Windows.Forms.GroupBox()
    Me.optMagicDNS = New System.Windows.Forms.RadioButton()
    Me.optMagicSold = New System.Windows.Forms.RadioButton()
    Me.fraMagicActions = New System.Windows.Forms.GroupBox()
    Me.cmdMagicNA = New System.Windows.Forms.Button()
    Me.cmdMagicDNQ = New System.Windows.Forms.Button()
    Me.cmdMagicReset = New System.Windows.Forms.Button()
    Me.cmdMagicSave = New System.Windows.Forms.Button()
    Me.cboEstimator = New System.Windows.Forms.ComboBox()
    Me.lblEstimator = New System.Windows.Forms.Label()
    Me.fraAppointments = New System.Windows.Forms.GroupBox()
    Me.cmdPickUp = New System.Windows.Forms.Button()
    Me.cmdAddReset = New System.Windows.Forms.Button()
    Me.txtDry = New System.Windows.Forms.TextBox()
    Me.txtMagic = New System.Windows.Forms.TextBox()
    Me.fraValues = New System.Windows.Forms.GroupBox()
    Me.valZip = New System.Windows.Forms.TextBox()
    Me.valPrice = New System.Windows.Forms.TextBox()
    Me.valDNS = New System.Windows.Forms.CheckBox()
    Me.valSold = New System.Windows.Forms.CheckBox()
    Me.valMagic_New = New System.Windows.Forms.CheckBox()
    Me.valDry_New = New System.Windows.Forms.CheckBox()
    Me.valSource = New System.Windows.Forms.TextBox()
    Me.valDry_Dispo_ID = New System.Windows.Forms.TextBox()
    Me.valMagic_Dispo_ID = New System.Windows.Forms.TextBox()
    Me.valCombined = New System.Windows.Forms.CheckBox()
    Me.valAppt_Date = New System.Windows.Forms.TextBox()
    Me.valEstimator = New System.Windows.Forms.TextBox()
    Me.valAddress_ID = New System.Windows.Forms.TextBox()
    Me.timLock = New System.Windows.Forms.Timer(Me.components)
    Me.tabDispo.SuspendLayout()
    Me.tpgDry.SuspendLayout()
    CType(Me.picDry, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.fraReset.SuspendLayout()
    Me.fraDryStatus.SuspendLayout()
    Me.fraDryActions.SuspendLayout()
    Me.tpgMagic.SuspendLayout()
    CType(Me.picMagicDone, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.fraMagicStatus.SuspendLayout()
    Me.fraMagicActions.SuspendLayout()
    Me.fraValues.SuspendLayout()
    Me.SuspendLayout()
    '
    'tabDispo
    '
    Me.tabDispo.Controls.Add(Me.tpgDry)
    Me.tabDispo.Controls.Add(Me.tpgMagic)
    Me.tabDispo.Location = New System.Drawing.Point(12, 221)
    Me.tabDispo.Name = "tabDispo"
    Me.tabDispo.SelectedIndex = 0
    Me.tabDispo.Size = New System.Drawing.Size(868, 319)
    Me.tabDispo.TabIndex = 0
    '
    'tpgDry
    '
    Me.tpgDry.AutoScroll = True
    Me.tpgDry.BackColor = System.Drawing.SystemColors.Control
    Me.tpgDry.Controls.Add(Me.picDry)
    Me.tpgDry.Controls.Add(Me.fraReset)
    Me.tpgDry.Controls.Add(Me.lblDryPrice)
    Me.tpgDry.Controls.Add(Me.txtDryPrice)
    Me.tpgDry.Controls.Add(Me.fraDryStatus)
    Me.tpgDry.Controls.Add(Me.fraDryActions)
    Me.tpgDry.Location = New System.Drawing.Point(4, 22)
    Me.tpgDry.Name = "tpgDry"
    Me.tpgDry.Padding = New System.Windows.Forms.Padding(3)
    Me.tpgDry.Size = New System.Drawing.Size(860, 293)
    Me.tpgDry.TabIndex = 0
    Me.tpgDry.Text = "Dry Basement"
    '
    'picDry
    '
    Me.picDry.Image = Global.DryBasementSystems.My.Resources.Resources.CHECKMRK
    Me.picDry.Location = New System.Drawing.Point(2, 6)
    Me.picDry.Name = "picDry"
    Me.picDry.Size = New System.Drawing.Size(848, 268)
    Me.picDry.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.picDry.TabIndex = 10
    Me.picDry.TabStop = False
    Me.picDry.Visible = False
    '
    'fraReset
    '
    Me.fraReset.Controls.Add(Me.cmdExit)
    Me.fraReset.Controls.Add(Me.cmdDNQSave)
    Me.fraReset.Controls.Add(Me.cmdResetSave)
    Me.fraReset.Controls.Add(Me.cmdSchedule)
    Me.fraReset.Controls.Add(Me.Label1)
    Me.fraReset.Controls.Add(Me.dtpCall_Date)
    Me.fraReset.Controls.Add(Me.lblReason)
    Me.fraReset.Controls.Add(Me.txtReason)
    Me.fraReset.Location = New System.Drawing.Point(185, 105)
    Me.fraReset.Name = "fraReset"
    Me.fraReset.Size = New System.Drawing.Size(462, 93)
    Me.fraReset.TabIndex = 9
    Me.fraReset.TabStop = False
    Me.fraReset.Tag = "SKIP"
    Me.fraReset.Visible = False
    '
    'cmdExit
    '
    Me.cmdExit.Location = New System.Drawing.Point(432, 10)
    Me.cmdExit.Name = "cmdExit"
    Me.cmdExit.Size = New System.Drawing.Size(24, 20)
    Me.cmdExit.TabIndex = 7
    Me.cmdExit.Text = "X"
    Me.cmdExit.UseVisualStyleBackColor = True
    Me.cmdExit.Visible = False
    '
    'cmdDNQSave
    '
    Me.cmdDNQSave.Location = New System.Drawing.Point(385, 60)
    Me.cmdDNQSave.Name = "cmdDNQSave"
    Me.cmdDNQSave.Size = New System.Drawing.Size(71, 25)
    Me.cmdDNQSave.TabIndex = 6
    Me.cmdDNQSave.Text = "Save"
    Me.cmdDNQSave.UseVisualStyleBackColor = True
    Me.cmdDNQSave.Visible = False
    '
    'cmdResetSave
    '
    Me.cmdResetSave.Location = New System.Drawing.Point(385, 60)
    Me.cmdResetSave.Name = "cmdResetSave"
    Me.cmdResetSave.Size = New System.Drawing.Size(71, 25)
    Me.cmdResetSave.TabIndex = 5
    Me.cmdResetSave.Text = "Save"
    Me.cmdResetSave.UseVisualStyleBackColor = True
    Me.cmdResetSave.Visible = False
    '
    'cmdSchedule
    '
    Me.cmdSchedule.Location = New System.Drawing.Point(308, 60)
    Me.cmdSchedule.Name = "cmdSchedule"
    Me.cmdSchedule.Size = New System.Drawing.Size(71, 25)
    Me.cmdSchedule.TabIndex = 4
    Me.cmdSchedule.Text = "Schedule"
    Me.cmdSchedule.UseVisualStyleBackColor = True
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(6, 66)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(50, 13)
    Me.Label1.TabIndex = 3
    Me.Label1.Text = "Call Date"
    '
    'dtpCall_Date
    '
    Me.dtpCall_Date.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpCall_Date.Location = New System.Drawing.Point(76, 59)
    Me.dtpCall_Date.Name = "dtpCall_Date"
    Me.dtpCall_Date.Size = New System.Drawing.Size(139, 20)
    Me.dtpCall_Date.TabIndex = 2
    Me.dtpCall_Date.Tag = "Reset"
    '
    'lblReason
    '
    Me.lblReason.AutoSize = True
    Me.lblReason.Location = New System.Drawing.Point(6, 33)
    Me.lblReason.Name = "lblReason"
    Me.lblReason.Size = New System.Drawing.Size(44, 13)
    Me.lblReason.TabIndex = 1
    Me.lblReason.Text = "Reason"
    '
    'txtReason
    '
    Me.txtReason.Location = New System.Drawing.Point(76, 33)
    Me.txtReason.Name = "txtReason"
    Me.txtReason.Size = New System.Drawing.Size(380, 20)
    Me.txtReason.TabIndex = 0
    Me.txtReason.Tag = "Reset"
    '
    'lblDryPrice
    '
    Me.lblDryPrice.AutoSize = True
    Me.lblDryPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblDryPrice.Location = New System.Drawing.Point(155, 29)
    Me.lblDryPrice.Name = "lblDryPrice"
    Me.lblDryPrice.Size = New System.Drawing.Size(40, 17)
    Me.lblDryPrice.TabIndex = 3
    Me.lblDryPrice.Tag = "SKIP"
    Me.lblDryPrice.Text = "Price"
    '
    'txtDryPrice
    '
    Me.txtDryPrice.Location = New System.Drawing.Point(213, 29)
    Me.txtDryPrice.Name = "txtDryPrice"
    Me.txtDryPrice.Size = New System.Drawing.Size(85, 20)
    Me.txtDryPrice.TabIndex = 2
    Me.txtDryPrice.Tag = ""
    '
    'fraDryStatus
    '
    Me.fraDryStatus.Controls.Add(Me.optDryDNS)
    Me.fraDryStatus.Controls.Add(Me.optDrySold)
    Me.fraDryStatus.Location = New System.Drawing.Point(6, 6)
    Me.fraDryStatus.Name = "fraDryStatus"
    Me.fraDryStatus.Size = New System.Drawing.Size(118, 58)
    Me.fraDryStatus.TabIndex = 1
    Me.fraDryStatus.TabStop = False
    Me.fraDryStatus.Tag = "SKIP"
    Me.fraDryStatus.Text = "Status"
    '
    'optDryDNS
    '
    Me.optDryDNS.AutoSize = True
    Me.optDryDNS.Location = New System.Drawing.Point(58, 24)
    Me.optDryDNS.Name = "optDryDNS"
    Me.optDryDNS.Size = New System.Drawing.Size(48, 17)
    Me.optDryDNS.TabIndex = 1
    Me.optDryDNS.TabStop = True
    Me.optDryDNS.Tag = ""
    Me.optDryDNS.Text = "DNS"
    Me.optDryDNS.UseVisualStyleBackColor = True
    '
    'optDrySold
    '
    Me.optDrySold.AutoSize = True
    Me.optDrySold.Location = New System.Drawing.Point(6, 24)
    Me.optDrySold.Name = "optDrySold"
    Me.optDrySold.Size = New System.Drawing.Size(54, 17)
    Me.optDrySold.TabIndex = 0
    Me.optDrySold.TabStop = True
    Me.optDrySold.Tag = ""
    Me.optDrySold.Text = "SOLD"
    Me.optDrySold.UseVisualStyleBackColor = True
    '
    'fraDryActions
    '
    Me.fraDryActions.Controls.Add(Me.cmdDryNA)
    Me.fraDryActions.Controls.Add(Me.cmdDryDNQ)
    Me.fraDryActions.Controls.Add(Me.cmdDryReset)
    Me.fraDryActions.Controls.Add(Me.cmdDrySave)
    Me.fraDryActions.Location = New System.Drawing.Point(350, 6)
    Me.fraDryActions.Name = "fraDryActions"
    Me.fraDryActions.Size = New System.Drawing.Size(310, 58)
    Me.fraDryActions.TabIndex = 0
    Me.fraDryActions.TabStop = False
    Me.fraDryActions.Tag = "SKIP"
    Me.fraDryActions.Text = "Actions"
    '
    'cmdDryNA
    '
    Me.cmdDryNA.Location = New System.Drawing.Point(232, 20)
    Me.cmdDryNA.Name = "cmdDryNA"
    Me.cmdDryNA.Size = New System.Drawing.Size(65, 25)
    Me.cmdDryNA.TabIndex = 3
    Me.cmdDryNA.Text = "N/A"
    Me.cmdDryNA.UseVisualStyleBackColor = True
    '
    'cmdDryDNQ
    '
    Me.cmdDryDNQ.Location = New System.Drawing.Point(161, 20)
    Me.cmdDryDNQ.Name = "cmdDryDNQ"
    Me.cmdDryDNQ.Size = New System.Drawing.Size(65, 25)
    Me.cmdDryDNQ.TabIndex = 2
    Me.cmdDryDNQ.Text = "DNQ"
    Me.cmdDryDNQ.UseVisualStyleBackColor = True
    '
    'cmdDryReset
    '
    Me.cmdDryReset.Location = New System.Drawing.Point(89, 20)
    Me.cmdDryReset.Name = "cmdDryReset"
    Me.cmdDryReset.Size = New System.Drawing.Size(65, 25)
    Me.cmdDryReset.TabIndex = 1
    Me.cmdDryReset.Text = "Reset"
    Me.cmdDryReset.UseVisualStyleBackColor = True
    '
    'cmdDrySave
    '
    Me.cmdDrySave.Location = New System.Drawing.Point(18, 20)
    Me.cmdDrySave.Name = "cmdDrySave"
    Me.cmdDrySave.Size = New System.Drawing.Size(65, 25)
    Me.cmdDrySave.TabIndex = 0
    Me.cmdDrySave.Text = "Save"
    Me.cmdDrySave.UseVisualStyleBackColor = True
    '
    'tpgMagic
    '
    Me.tpgMagic.BackColor = System.Drawing.SystemColors.Control
    Me.tpgMagic.Controls.Add(Me.picMagicDone)
    Me.tpgMagic.Controls.Add(Me.lblMagicPrice)
    Me.tpgMagic.Controls.Add(Me.txtMagicPrice)
    Me.tpgMagic.Controls.Add(Me.fraMagicStatus)
    Me.tpgMagic.Controls.Add(Me.fraMagicActions)
    Me.tpgMagic.Location = New System.Drawing.Point(4, 22)
    Me.tpgMagic.Name = "tpgMagic"
    Me.tpgMagic.Padding = New System.Windows.Forms.Padding(3)
    Me.tpgMagic.Size = New System.Drawing.Size(860, 293)
    Me.tpgMagic.TabIndex = 1
    Me.tpgMagic.Text = "Basement Magic"
    '
    'picMagicDone
    '
    Me.picMagicDone.Image = Global.DryBasementSystems.My.Resources.Resources.CHECKMRK
    Me.picMagicDone.Location = New System.Drawing.Point(6, 6)
    Me.picMagicDone.Name = "picMagicDone"
    Me.picMagicDone.Size = New System.Drawing.Size(848, 291)
    Me.picMagicDone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
    Me.picMagicDone.TabIndex = 6
    Me.picMagicDone.TabStop = False
    Me.picMagicDone.Visible = False
    '
    'lblMagicPrice
    '
    Me.lblMagicPrice.AutoSize = True
    Me.lblMagicPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblMagicPrice.Location = New System.Drawing.Point(156, 29)
    Me.lblMagicPrice.Name = "lblMagicPrice"
    Me.lblMagicPrice.Size = New System.Drawing.Size(40, 17)
    Me.lblMagicPrice.TabIndex = 5
    Me.lblMagicPrice.Tag = "SKIP"
    Me.lblMagicPrice.Text = "Price"
    '
    'txtMagicPrice
    '
    Me.txtMagicPrice.Location = New System.Drawing.Point(214, 29)
    Me.txtMagicPrice.Name = "txtMagicPrice"
    Me.txtMagicPrice.Size = New System.Drawing.Size(85, 20)
    Me.txtMagicPrice.TabIndex = 4
    Me.txtMagicPrice.Tag = ""
    '
    'fraMagicStatus
    '
    Me.fraMagicStatus.Controls.Add(Me.optMagicDNS)
    Me.fraMagicStatus.Controls.Add(Me.optMagicSold)
    Me.fraMagicStatus.Location = New System.Drawing.Point(6, 6)
    Me.fraMagicStatus.Name = "fraMagicStatus"
    Me.fraMagicStatus.Size = New System.Drawing.Size(118, 58)
    Me.fraMagicStatus.TabIndex = 3
    Me.fraMagicStatus.TabStop = False
    Me.fraMagicStatus.Tag = "SKIP"
    Me.fraMagicStatus.Text = "Status"
    '
    'optMagicDNS
    '
    Me.optMagicDNS.AutoSize = True
    Me.optMagicDNS.Location = New System.Drawing.Point(58, 24)
    Me.optMagicDNS.Name = "optMagicDNS"
    Me.optMagicDNS.Size = New System.Drawing.Size(48, 17)
    Me.optMagicDNS.TabIndex = 1
    Me.optMagicDNS.TabStop = True
    Me.optMagicDNS.Tag = ""
    Me.optMagicDNS.Text = "DNS"
    Me.optMagicDNS.UseVisualStyleBackColor = True
    '
    'optMagicSold
    '
    Me.optMagicSold.AutoSize = True
    Me.optMagicSold.Location = New System.Drawing.Point(6, 24)
    Me.optMagicSold.Name = "optMagicSold"
    Me.optMagicSold.Size = New System.Drawing.Size(54, 17)
    Me.optMagicSold.TabIndex = 0
    Me.optMagicSold.TabStop = True
    Me.optMagicSold.Tag = ""
    Me.optMagicSold.Text = "SOLD"
    Me.optMagicSold.UseVisualStyleBackColor = True
    '
    'fraMagicActions
    '
    Me.fraMagicActions.Controls.Add(Me.cmdMagicNA)
    Me.fraMagicActions.Controls.Add(Me.cmdMagicDNQ)
    Me.fraMagicActions.Controls.Add(Me.cmdMagicReset)
    Me.fraMagicActions.Controls.Add(Me.cmdMagicSave)
    Me.fraMagicActions.Location = New System.Drawing.Point(350, 6)
    Me.fraMagicActions.Name = "fraMagicActions"
    Me.fraMagicActions.Size = New System.Drawing.Size(307, 58)
    Me.fraMagicActions.TabIndex = 2
    Me.fraMagicActions.TabStop = False
    Me.fraMagicActions.Tag = "SKIP"
    Me.fraMagicActions.Text = "Actions"
    '
    'cmdMagicNA
    '
    Me.cmdMagicNA.Location = New System.Drawing.Point(232, 20)
    Me.cmdMagicNA.Name = "cmdMagicNA"
    Me.cmdMagicNA.Size = New System.Drawing.Size(65, 25)
    Me.cmdMagicNA.TabIndex = 3
    Me.cmdMagicNA.Text = "N/A"
    Me.cmdMagicNA.UseVisualStyleBackColor = True
    '
    'cmdMagicDNQ
    '
    Me.cmdMagicDNQ.Location = New System.Drawing.Point(161, 20)
    Me.cmdMagicDNQ.Name = "cmdMagicDNQ"
    Me.cmdMagicDNQ.Size = New System.Drawing.Size(65, 25)
    Me.cmdMagicDNQ.TabIndex = 2
    Me.cmdMagicDNQ.Text = "DNQ"
    Me.cmdMagicDNQ.UseVisualStyleBackColor = True
    '
    'cmdMagicReset
    '
    Me.cmdMagicReset.Location = New System.Drawing.Point(89, 20)
    Me.cmdMagicReset.Name = "cmdMagicReset"
    Me.cmdMagicReset.Size = New System.Drawing.Size(65, 25)
    Me.cmdMagicReset.TabIndex = 1
    Me.cmdMagicReset.Text = "Reset"
    Me.cmdMagicReset.UseVisualStyleBackColor = True
    '
    'cmdMagicSave
    '
    Me.cmdMagicSave.Location = New System.Drawing.Point(18, 20)
    Me.cmdMagicSave.Name = "cmdMagicSave"
    Me.cmdMagicSave.Size = New System.Drawing.Size(65, 25)
    Me.cmdMagicSave.TabIndex = 0
    Me.cmdMagicSave.Text = "Save"
    Me.cmdMagicSave.UseVisualStyleBackColor = True
    '
    'cboEstimator
    '
    Me.cboEstimator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboEstimator.FormattingEnabled = True
    Me.cboEstimator.Location = New System.Drawing.Point(66, 12)
    Me.cboEstimator.Name = "cboEstimator"
    Me.cboEstimator.Size = New System.Drawing.Size(146, 21)
    Me.cboEstimator.TabIndex = 1
    '
    'lblEstimator
    '
    Me.lblEstimator.AutoSize = True
    Me.lblEstimator.Location = New System.Drawing.Point(9, 15)
    Me.lblEstimator.Name = "lblEstimator"
    Me.lblEstimator.Size = New System.Drawing.Size(50, 13)
    Me.lblEstimator.TabIndex = 2
    Me.lblEstimator.Text = "Estimator"
    '
    'fraAppointments
    '
    Me.fraAppointments.Location = New System.Drawing.Point(12, 44)
    Me.fraAppointments.Name = "fraAppointments"
    Me.fraAppointments.Size = New System.Drawing.Size(864, 98)
    Me.fraAppointments.TabIndex = 3
    Me.fraAppointments.TabStop = False
    Me.fraAppointments.Text = "Appointments"
    '
    'cmdPickUp
    '
    Me.cmdPickUp.Location = New System.Drawing.Point(243, 13)
    Me.cmdPickUp.Name = "cmdPickUp"
    Me.cmdPickUp.Size = New System.Drawing.Size(85, 25)
    Me.cmdPickUp.TabIndex = 4
    Me.cmdPickUp.Text = "Add PickUp"
    Me.cmdPickUp.UseVisualStyleBackColor = True
    '
    'cmdAddReset
    '
    Me.cmdAddReset.Location = New System.Drawing.Point(334, 13)
    Me.cmdAddReset.Name = "cmdAddReset"
    Me.cmdAddReset.Size = New System.Drawing.Size(85, 25)
    Me.cmdAddReset.TabIndex = 5
    Me.cmdAddReset.Text = "Add Reset"
    Me.cmdAddReset.UseVisualStyleBackColor = True
    '
    'txtDry
    '
    Me.txtDry.Location = New System.Drawing.Point(15, 148)
    Me.txtDry.Name = "txtDry"
    Me.txtDry.ReadOnly = True
    Me.txtDry.Size = New System.Drawing.Size(860, 20)
    Me.txtDry.TabIndex = 6
    '
    'txtMagic
    '
    Me.txtMagic.Location = New System.Drawing.Point(15, 174)
    Me.txtMagic.Name = "txtMagic"
    Me.txtMagic.ReadOnly = True
    Me.txtMagic.Size = New System.Drawing.Size(860, 20)
    Me.txtMagic.TabIndex = 7
    Me.txtMagic.Tag = ""
    '
    'fraValues
    '
    Me.fraValues.Controls.Add(Me.valZip)
    Me.fraValues.Controls.Add(Me.valPrice)
    Me.fraValues.Controls.Add(Me.valDNS)
    Me.fraValues.Controls.Add(Me.valSold)
    Me.fraValues.Controls.Add(Me.valMagic_New)
    Me.fraValues.Controls.Add(Me.valDry_New)
    Me.fraValues.Controls.Add(Me.valSource)
    Me.fraValues.Controls.Add(Me.valDry_Dispo_ID)
    Me.fraValues.Controls.Add(Me.valMagic_Dispo_ID)
    Me.fraValues.Controls.Add(Me.valCombined)
    Me.fraValues.Controls.Add(Me.valAppt_Date)
    Me.fraValues.Controls.Add(Me.valEstimator)
    Me.fraValues.Controls.Add(Me.valAddress_ID)
    Me.fraValues.Location = New System.Drawing.Point(802, 3)
    Me.fraValues.Name = "fraValues"
    Me.fraValues.Size = New System.Drawing.Size(73, 234)
    Me.fraValues.TabIndex = 8
    Me.fraValues.TabStop = False
    Me.fraValues.Tag = "SKIP"
    Me.fraValues.Text = "Invisible"
    Me.fraValues.Visible = False
    '
    'valZip
    '
    Me.valZip.Location = New System.Drawing.Point(6, 237)
    Me.valZip.Name = "valZip"
    Me.valZip.Size = New System.Drawing.Size(54, 20)
    Me.valZip.TabIndex = 16
    Me.valZip.Tag = "BOTH"
    '
    'valPrice
    '
    Me.valPrice.Location = New System.Drawing.Point(6, 211)
    Me.valPrice.Name = "valPrice"
    Me.valPrice.Size = New System.Drawing.Size(54, 20)
    Me.valPrice.TabIndex = 15
    Me.valPrice.Tag = ""
    '
    'valDNS
    '
    Me.valDNS.AutoSize = True
    Me.valDNS.Location = New System.Drawing.Point(31, 191)
    Me.valDNS.Name = "valDNS"
    Me.valDNS.Size = New System.Drawing.Size(15, 14)
    Me.valDNS.TabIndex = 14
    Me.valDNS.Tag = ""
    Me.valDNS.UseVisualStyleBackColor = True
    '
    'valSold
    '
    Me.valSold.AutoSize = True
    Me.valSold.Location = New System.Drawing.Point(10, 191)
    Me.valSold.Name = "valSold"
    Me.valSold.Size = New System.Drawing.Size(15, 14)
    Me.valSold.TabIndex = 13
    Me.valSold.Tag = ""
    Me.valSold.UseVisualStyleBackColor = True
    '
    'valMagic_New
    '
    Me.valMagic_New.AutoSize = True
    Me.valMagic_New.Location = New System.Drawing.Point(49, 93)
    Me.valMagic_New.Name = "valMagic_New"
    Me.valMagic_New.Size = New System.Drawing.Size(15, 14)
    Me.valMagic_New.TabIndex = 12
    Me.valMagic_New.Tag = "MAGIC2"
    Me.valMagic_New.UseVisualStyleBackColor = True
    '
    'valDry_New
    '
    Me.valDry_New.AutoSize = True
    Me.valDry_New.Location = New System.Drawing.Point(31, 93)
    Me.valDry_New.Name = "valDry_New"
    Me.valDry_New.Size = New System.Drawing.Size(15, 14)
    Me.valDry_New.TabIndex = 11
    Me.valDry_New.Tag = "DRY2"
    Me.valDry_New.UseVisualStyleBackColor = True
    '
    'valSource
    '
    Me.valSource.Location = New System.Drawing.Point(10, 165)
    Me.valSource.Name = "valSource"
    Me.valSource.Size = New System.Drawing.Size(54, 20)
    Me.valSource.TabIndex = 10
    Me.valSource.Tag = "BOTH"
    '
    'valDry_Dispo_ID
    '
    Me.valDry_Dispo_ID.Location = New System.Drawing.Point(10, 139)
    Me.valDry_Dispo_ID.Name = "valDry_Dispo_ID"
    Me.valDry_Dispo_ID.Size = New System.Drawing.Size(54, 20)
    Me.valDry_Dispo_ID.TabIndex = 9
    Me.valDry_Dispo_ID.Tag = "MAGIC2"
    Me.valDry_Dispo_ID.Text = "0"
    '
    'valMagic_Dispo_ID
    '
    Me.valMagic_Dispo_ID.Location = New System.Drawing.Point(10, 113)
    Me.valMagic_Dispo_ID.Name = "valMagic_Dispo_ID"
    Me.valMagic_Dispo_ID.Size = New System.Drawing.Size(54, 20)
    Me.valMagic_Dispo_ID.TabIndex = 8
    Me.valMagic_Dispo_ID.Tag = "DRY2"
    Me.valMagic_Dispo_ID.Text = "0"
    '
    'valCombined
    '
    Me.valCombined.AutoSize = True
    Me.valCombined.Location = New System.Drawing.Point(10, 93)
    Me.valCombined.Name = "valCombined"
    Me.valCombined.Size = New System.Drawing.Size(15, 14)
    Me.valCombined.TabIndex = 7
    Me.valCombined.Tag = "BOTH"
    Me.valCombined.UseVisualStyleBackColor = True
    '
    'valAppt_Date
    '
    Me.valAppt_Date.Location = New System.Drawing.Point(10, 67)
    Me.valAppt_Date.Name = "valAppt_Date"
    Me.valAppt_Date.Size = New System.Drawing.Size(54, 20)
    Me.valAppt_Date.TabIndex = 6
    Me.valAppt_Date.Tag = "BOTH"
    '
    'valEstimator
    '
    Me.valEstimator.Location = New System.Drawing.Point(10, 41)
    Me.valEstimator.Name = "valEstimator"
    Me.valEstimator.Size = New System.Drawing.Size(54, 20)
    Me.valEstimator.TabIndex = 5
    Me.valEstimator.Tag = "BOTH"
    '
    'valAddress_ID
    '
    Me.valAddress_ID.Location = New System.Drawing.Point(10, 15)
    Me.valAddress_ID.Name = "valAddress_ID"
    Me.valAddress_ID.Size = New System.Drawing.Size(54, 20)
    Me.valAddress_ID.TabIndex = 4
    Me.valAddress_ID.Tag = "BOTH"
    '
    'timLock
    '
    Me.timLock.Enabled = True
    '
    'frmDispo
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(892, 627)
    Me.Controls.Add(Me.fraValues)
    Me.Controls.Add(Me.txtMagic)
    Me.Controls.Add(Me.txtDry)
    Me.Controls.Add(Me.cmdAddReset)
    Me.Controls.Add(Me.cmdPickUp)
    Me.Controls.Add(Me.fraAppointments)
    Me.Controls.Add(Me.lblEstimator)
    Me.Controls.Add(Me.cboEstimator)
    Me.Controls.Add(Me.tabDispo)
    Me.Name = "frmDispo"
    Me.Text = "Dispo"
    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    Me.tabDispo.ResumeLayout(False)
    Me.tpgDry.ResumeLayout(False)
    Me.tpgDry.PerformLayout()
    CType(Me.picDry, System.ComponentModel.ISupportInitialize).EndInit()
    Me.fraReset.ResumeLayout(False)
    Me.fraReset.PerformLayout()
    Me.fraDryStatus.ResumeLayout(False)
    Me.fraDryStatus.PerformLayout()
    Me.fraDryActions.ResumeLayout(False)
    Me.tpgMagic.ResumeLayout(False)
    Me.tpgMagic.PerformLayout()
    CType(Me.picMagicDone, System.ComponentModel.ISupportInitialize).EndInit()
    Me.fraMagicStatus.ResumeLayout(False)
    Me.fraMagicStatus.PerformLayout()
    Me.fraMagicActions.ResumeLayout(False)
    Me.fraValues.ResumeLayout(False)
    Me.fraValues.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents tabDispo As System.Windows.Forms.TabControl
  Friend WithEvents tpgDry As System.Windows.Forms.TabPage
  Friend WithEvents tpgMagic As System.Windows.Forms.TabPage
  Friend WithEvents cboEstimator As System.Windows.Forms.ComboBox
  Friend WithEvents lblEstimator As System.Windows.Forms.Label
  Friend WithEvents fraDryActions As System.Windows.Forms.GroupBox
  Friend WithEvents cmdDryNA As System.Windows.Forms.Button
  Friend WithEvents cmdDryDNQ As System.Windows.Forms.Button
  Friend WithEvents cmdDryReset As System.Windows.Forms.Button
  Friend WithEvents cmdDrySave As System.Windows.Forms.Button
  Friend WithEvents fraDryStatus As System.Windows.Forms.GroupBox
  Friend WithEvents optDryDNS As System.Windows.Forms.RadioButton
  Friend WithEvents optDrySold As System.Windows.Forms.RadioButton
  Friend WithEvents fraMagicStatus As System.Windows.Forms.GroupBox
  Friend WithEvents optMagicDNS As System.Windows.Forms.RadioButton
  Friend WithEvents optMagicSold As System.Windows.Forms.RadioButton
  Friend WithEvents fraMagicActions As System.Windows.Forms.GroupBox
  Friend WithEvents cmdMagicNA As System.Windows.Forms.Button
  Friend WithEvents cmdMagicDNQ As System.Windows.Forms.Button
  Friend WithEvents cmdMagicReset As System.Windows.Forms.Button
  Friend WithEvents cmdMagicSave As System.Windows.Forms.Button
  Friend WithEvents lblDryPrice As System.Windows.Forms.Label
  Friend WithEvents txtDryPrice As System.Windows.Forms.TextBox
  Friend WithEvents lblMagicPrice As System.Windows.Forms.Label
  Friend WithEvents txtMagicPrice As System.Windows.Forms.TextBox
  Friend WithEvents fraAppointments As System.Windows.Forms.GroupBox
  Friend WithEvents cmdPickUp As System.Windows.Forms.Button
  Friend WithEvents cmdAddReset As System.Windows.Forms.Button
  Friend WithEvents txtDry As System.Windows.Forms.TextBox
  Friend WithEvents txtMagic As System.Windows.Forms.TextBox
  Friend WithEvents fraValues As System.Windows.Forms.GroupBox
  Friend WithEvents valEstimator As System.Windows.Forms.TextBox
  Friend WithEvents valAddress_ID As System.Windows.Forms.TextBox
  Friend WithEvents valAppt_Date As System.Windows.Forms.TextBox
  Friend WithEvents valCombined As System.Windows.Forms.CheckBox
  Friend WithEvents valDry_Dispo_ID As System.Windows.Forms.TextBox
  Friend WithEvents valMagic_Dispo_ID As System.Windows.Forms.TextBox
  Friend WithEvents valSource As System.Windows.Forms.TextBox
  Friend WithEvents valMagic_New As System.Windows.Forms.CheckBox
  Friend WithEvents valDry_New As System.Windows.Forms.CheckBox
  Friend WithEvents valDNS As System.Windows.Forms.CheckBox
  Friend WithEvents valSold As System.Windows.Forms.CheckBox
  Friend WithEvents valPrice As System.Windows.Forms.TextBox
  Friend WithEvents valZip As System.Windows.Forms.TextBox
  Friend WithEvents fraReset As System.Windows.Forms.GroupBox
  Friend WithEvents cmdExit As System.Windows.Forms.Button
  Friend WithEvents cmdDNQSave As System.Windows.Forms.Button
  Friend WithEvents cmdResetSave As System.Windows.Forms.Button
  Friend WithEvents cmdSchedule As System.Windows.Forms.Button
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents dtpCall_Date As System.Windows.Forms.DateTimePicker
  Friend WithEvents lblReason As System.Windows.Forms.Label
  Friend WithEvents txtReason As System.Windows.Forms.TextBox
  Friend WithEvents picDry As System.Windows.Forms.PictureBox
  Friend WithEvents picMagicDone As System.Windows.Forms.PictureBox
  Friend WithEvents timLock As System.Windows.Forms.Timer
End Class
