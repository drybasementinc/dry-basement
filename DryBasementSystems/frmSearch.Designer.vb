﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearch
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.fraInfo1 = New System.Windows.Forms.GroupBox()
    Me.lblCompanyName = New System.Windows.Forms.Label()
    Me.txtCompany_Name = New System.Windows.Forms.TextBox()
    Me.cboCity = New System.Windows.Forms.ComboBox()
    Me.txtZip = New System.Windows.Forms.TextBox()
    Me.lblZip = New System.Windows.Forms.Label()
    Me.cboState = New System.Windows.Forms.ComboBox()
    Me.lblState = New System.Windows.Forms.Label()
    Me.lblCity = New System.Windows.Forms.Label()
    Me.cboAddress_Ending = New System.Windows.Forms.ComboBox()
    Me.cboAddress_Direction = New System.Windows.Forms.ComboBox()
    Me.txtAddress_Numbers = New System.Windows.Forms.TextBox()
    Me.txtAddress = New System.Windows.Forms.TextBox()
    Me.lblAddress = New System.Windows.Forms.Label()
    Me.lblLast_Name = New System.Windows.Forms.Label()
    Me.txtLast_Name = New System.Windows.Forms.TextBox()
    Me.lblFirst_Name = New System.Windows.Forms.Label()
    Me.txtFirst_Name = New System.Windows.Forms.TextBox()
    Me.txtPhone1EXT = New System.Windows.Forms.TextBox()
    Me.cboSource = New System.Windows.Forms.ComboBox()
    Me.lblSource = New System.Windows.Forms.Label()
    Me.cboFoundation = New System.Windows.Forms.ComboBox()
    Me.lblFoundation = New System.Windows.Forms.Label()
    Me.lblEmail = New System.Windows.Forms.Label()
    Me.txtEmail = New System.Windows.Forms.TextBox()
    Me.txtPhone1 = New System.Windows.Forms.TextBox()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.dtpDate2 = New System.Windows.Forms.DateTimePicker()
    Me.lblAppt_Date = New System.Windows.Forms.Label()
    Me.dtpDate1 = New System.Windows.Forms.DateTimePicker()
    Me.lblPhone = New System.Windows.Forms.Label()
    Me.cmdSearch = New System.Windows.Forms.Button()
    Me.cmdClear = New System.Windows.Forms.Button()
    Me.fraInfo1.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'fraInfo1
    '
    Me.fraInfo1.Controls.Add(Me.lblCompanyName)
    Me.fraInfo1.Controls.Add(Me.txtCompany_Name)
    Me.fraInfo1.Controls.Add(Me.cboCity)
    Me.fraInfo1.Controls.Add(Me.txtZip)
    Me.fraInfo1.Controls.Add(Me.lblZip)
    Me.fraInfo1.Controls.Add(Me.cboState)
    Me.fraInfo1.Controls.Add(Me.lblState)
    Me.fraInfo1.Controls.Add(Me.lblCity)
    Me.fraInfo1.Controls.Add(Me.cboAddress_Ending)
    Me.fraInfo1.Controls.Add(Me.cboAddress_Direction)
    Me.fraInfo1.Controls.Add(Me.txtAddress_Numbers)
    Me.fraInfo1.Controls.Add(Me.txtAddress)
    Me.fraInfo1.Controls.Add(Me.lblAddress)
    Me.fraInfo1.Controls.Add(Me.lblLast_Name)
    Me.fraInfo1.Controls.Add(Me.txtLast_Name)
    Me.fraInfo1.Controls.Add(Me.lblFirst_Name)
    Me.fraInfo1.Controls.Add(Me.txtFirst_Name)
    Me.fraInfo1.Location = New System.Drawing.Point(12, 24)
    Me.fraInfo1.Name = "fraInfo1"
    Me.fraInfo1.Size = New System.Drawing.Size(536, 159)
    Me.fraInfo1.TabIndex = 1
    Me.fraInfo1.TabStop = False
    Me.fraInfo1.Text = "Address Information"
    '
    'lblCompanyName
    '
    Me.lblCompanyName.AutoSize = True
    Me.lblCompanyName.Location = New System.Drawing.Point(9, 22)
    Me.lblCompanyName.Name = "lblCompanyName"
    Me.lblCompanyName.Size = New System.Drawing.Size(51, 13)
    Me.lblCompanyName.TabIndex = 5
    Me.lblCompanyName.Text = "Company"
    '
    'txtCompany_Name
    '
    Me.txtCompany_Name.Location = New System.Drawing.Point(83, 19)
    Me.txtCompany_Name.Name = "txtCompany_Name"
    Me.txtCompany_Name.Size = New System.Drawing.Size(438, 20)
    Me.txtCompany_Name.TabIndex = 0
    Me.txtCompany_Name.Tag = "SEARCH"
    '
    'cboCity
    '
    Me.cboCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboCity.FormattingEnabled = True
    Me.cboCity.Location = New System.Drawing.Point(83, 98)
    Me.cboCity.Name = "cboCity"
    Me.cboCity.Size = New System.Drawing.Size(209, 21)
    Me.cboCity.TabIndex = 10
    Me.cboCity.Tag = "SEARCH"
    '
    'txtZip
    '
    Me.txtZip.Location = New System.Drawing.Point(428, 97)
    Me.txtZip.Name = "txtZip"
    Me.txtZip.Size = New System.Drawing.Size(94, 20)
    Me.txtZip.TabIndex = 9
    Me.txtZip.Tag = "SEARCH"
    '
    'lblZip
    '
    Me.lblZip.AutoSize = True
    Me.lblZip.Location = New System.Drawing.Point(398, 100)
    Me.lblZip.Name = "lblZip"
    Me.lblZip.Size = New System.Drawing.Size(22, 13)
    Me.lblZip.TabIndex = 30
    Me.lblZip.Text = "Zip"
    '
    'cboState
    '
    Me.cboState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboState.FormattingEnabled = True
    Me.cboState.Items.AddRange(New Object() {"MO", "KS"})
    Me.cboState.Location = New System.Drawing.Point(342, 97)
    Me.cboState.Name = "cboState"
    Me.cboState.Size = New System.Drawing.Size(49, 21)
    Me.cboState.TabIndex = 11
    Me.cboState.Tag = "SEARCH"
    '
    'lblState
    '
    Me.lblState.AutoSize = True
    Me.lblState.Location = New System.Drawing.Point(304, 101)
    Me.lblState.Name = "lblState"
    Me.lblState.Size = New System.Drawing.Size(32, 13)
    Me.lblState.TabIndex = 28
    Me.lblState.Text = "State"
    '
    'lblCity
    '
    Me.lblCity.AutoSize = True
    Me.lblCity.Location = New System.Drawing.Point(9, 100)
    Me.lblCity.Name = "lblCity"
    Me.lblCity.Size = New System.Drawing.Size(24, 13)
    Me.lblCity.TabIndex = 26
    Me.lblCity.Text = "City"
    '
    'cboAddress_Ending
    '
    Me.cboAddress_Ending.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboAddress_Ending.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboAddress_Ending.FormattingEnabled = True
    Me.cboAddress_Ending.Location = New System.Drawing.Point(428, 70)
    Me.cboAddress_Ending.Name = "cboAddress_Ending"
    Me.cboAddress_Ending.Size = New System.Drawing.Size(94, 21)
    Me.cboAddress_Ending.TabIndex = 8
    Me.cboAddress_Ending.Tag = "SEARCH"
    '
    'cboAddress_Direction
    '
    Me.cboAddress_Direction.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboAddress_Direction.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboAddress_Direction.FormattingEnabled = True
    Me.cboAddress_Direction.Location = New System.Drawing.Point(159, 71)
    Me.cboAddress_Direction.Name = "cboAddress_Direction"
    Me.cboAddress_Direction.Size = New System.Drawing.Size(65, 21)
    Me.cboAddress_Direction.TabIndex = 6
    Me.cboAddress_Direction.Tag = "SEARCH"
    '
    'txtAddress_Numbers
    '
    Me.txtAddress_Numbers.Location = New System.Drawing.Point(83, 71)
    Me.txtAddress_Numbers.Name = "txtAddress_Numbers"
    Me.txtAddress_Numbers.Size = New System.Drawing.Size(70, 20)
    Me.txtAddress_Numbers.TabIndex = 5
    Me.txtAddress_Numbers.Tag = "SEARCH"
    '
    'txtAddress
    '
    Me.txtAddress.Location = New System.Drawing.Point(230, 71)
    Me.txtAddress.Name = "txtAddress"
    Me.txtAddress.Size = New System.Drawing.Size(192, 20)
    Me.txtAddress.TabIndex = 7
    Me.txtAddress.Tag = "SEARCH"
    '
    'lblAddress
    '
    Me.lblAddress.AutoSize = True
    Me.lblAddress.Location = New System.Drawing.Point(9, 74)
    Me.lblAddress.Name = "lblAddress"
    Me.lblAddress.Size = New System.Drawing.Size(45, 13)
    Me.lblAddress.TabIndex = 21
    Me.lblAddress.Text = "Address"
    '
    'lblLast_Name
    '
    Me.lblLast_Name.AutoSize = True
    Me.lblLast_Name.Location = New System.Drawing.Point(268, 48)
    Me.lblLast_Name.Name = "lblLast_Name"
    Me.lblLast_Name.Size = New System.Drawing.Size(58, 13)
    Me.lblLast_Name.TabIndex = 16
    Me.lblLast_Name.Text = "Last Name"
    '
    'txtLast_Name
    '
    Me.txtLast_Name.Location = New System.Drawing.Point(342, 45)
    Me.txtLast_Name.Name = "txtLast_Name"
    Me.txtLast_Name.Size = New System.Drawing.Size(180, 20)
    Me.txtLast_Name.TabIndex = 2
    Me.txtLast_Name.Tag = ""
    '
    'lblFirst_Name
    '
    Me.lblFirst_Name.AutoSize = True
    Me.lblFirst_Name.Location = New System.Drawing.Point(9, 48)
    Me.lblFirst_Name.Name = "lblFirst_Name"
    Me.lblFirst_Name.Size = New System.Drawing.Size(57, 13)
    Me.lblFirst_Name.TabIndex = 14
    Me.lblFirst_Name.Text = "First Name"
    '
    'txtFirst_Name
    '
    Me.txtFirst_Name.Location = New System.Drawing.Point(83, 45)
    Me.txtFirst_Name.Name = "txtFirst_Name"
    Me.txtFirst_Name.Size = New System.Drawing.Size(179, 20)
    Me.txtFirst_Name.TabIndex = 1
    Me.txtFirst_Name.Tag = ""
    '
    'txtPhone1EXT
    '
    Me.txtPhone1EXT.Location = New System.Drawing.Point(290, 18)
    Me.txtPhone1EXT.Name = "txtPhone1EXT"
    Me.txtPhone1EXT.Size = New System.Drawing.Size(58, 20)
    Me.txtPhone1EXT.TabIndex = 74
    Me.txtPhone1EXT.Tag = "SEARCH"
    '
    'cboSource
    '
    Me.cboSource.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboSource.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboSource.FormattingEnabled = True
    Me.cboSource.Location = New System.Drawing.Point(401, 19)
    Me.cboSource.Name = "cboSource"
    Me.cboSource.Size = New System.Drawing.Size(258, 21)
    Me.cboSource.TabIndex = 69
    Me.cboSource.Tag = "SEARCH"
    '
    'lblSource
    '
    Me.lblSource.AutoSize = True
    Me.lblSource.Location = New System.Drawing.Point(354, 21)
    Me.lblSource.Name = "lblSource"
    Me.lblSource.Size = New System.Drawing.Size(41, 13)
    Me.lblSource.TabIndex = 73
    Me.lblSource.Text = "Source"
    '
    'cboFoundation
    '
    Me.cboFoundation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
    Me.cboFoundation.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
    Me.cboFoundation.FormattingEnabled = True
    Me.cboFoundation.Location = New System.Drawing.Point(531, 44)
    Me.cboFoundation.Name = "cboFoundation"
    Me.cboFoundation.Size = New System.Drawing.Size(128, 21)
    Me.cboFoundation.TabIndex = 68
    Me.cboFoundation.Tag = "SEARCH"
    '
    'lblFoundation
    '
    Me.lblFoundation.AutoSize = True
    Me.lblFoundation.Location = New System.Drawing.Point(457, 47)
    Me.lblFoundation.Name = "lblFoundation"
    Me.lblFoundation.Size = New System.Drawing.Size(60, 13)
    Me.lblFoundation.TabIndex = 72
    Me.lblFoundation.Text = "Foundation"
    '
    'lblEmail
    '
    Me.lblEmail.AutoSize = True
    Me.lblEmail.Location = New System.Drawing.Point(9, 44)
    Me.lblEmail.Name = "lblEmail"
    Me.lblEmail.Size = New System.Drawing.Size(32, 13)
    Me.lblEmail.TabIndex = 71
    Me.lblEmail.Text = "Email"
    '
    'txtEmail
    '
    Me.txtEmail.Location = New System.Drawing.Point(79, 44)
    Me.txtEmail.Name = "txtEmail"
    Me.txtEmail.Size = New System.Drawing.Size(372, 20)
    Me.txtEmail.TabIndex = 67
    Me.txtEmail.Tag = "SEARCH"
    '
    'txtPhone1
    '
    Me.txtPhone1.Location = New System.Drawing.Point(79, 18)
    Me.txtPhone1.Name = "txtPhone1"
    Me.txtPhone1.Size = New System.Drawing.Size(205, 20)
    Me.txtPhone1.TabIndex = 64
    Me.txtPhone1.Tag = "SEARCH"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.dtpDate2)
    Me.GroupBox1.Controls.Add(Me.lblAppt_Date)
    Me.GroupBox1.Controls.Add(Me.dtpDate1)
    Me.GroupBox1.Controls.Add(Me.lblPhone)
    Me.GroupBox1.Controls.Add(Me.txtPhone1)
    Me.GroupBox1.Controls.Add(Me.txtPhone1EXT)
    Me.GroupBox1.Controls.Add(Me.txtEmail)
    Me.GroupBox1.Controls.Add(Me.lblEmail)
    Me.GroupBox1.Controls.Add(Me.cboSource)
    Me.GroupBox1.Controls.Add(Me.lblSource)
    Me.GroupBox1.Controls.Add(Me.cboFoundation)
    Me.GroupBox1.Controls.Add(Me.lblFoundation)
    Me.GroupBox1.Location = New System.Drawing.Point(12, 189)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(673, 170)
    Me.GroupBox1.TabIndex = 76
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'dtpDate2
    '
    Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpDate2.Location = New System.Drawing.Point(390, 70)
    Me.dtpDate2.Name = "dtpDate2"
    Me.dtpDate2.Size = New System.Drawing.Size(269, 20)
    Me.dtpDate2.TabIndex = 79
    '
    'lblAppt_Date
    '
    Me.lblAppt_Date.AutoSize = True
    Me.lblAppt_Date.Location = New System.Drawing.Point(9, 70)
    Me.lblAppt_Date.Name = "lblAppt_Date"
    Me.lblAppt_Date.Size = New System.Drawing.Size(39, 13)
    Me.lblAppt_Date.TabIndex = 78
    Me.lblAppt_Date.Text = "Date 1"
    '
    'dtpDate1
    '
    Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dtpDate1.Location = New System.Drawing.Point(79, 70)
    Me.dtpDate1.Name = "dtpDate1"
    Me.dtpDate1.Size = New System.Drawing.Size(269, 20)
    Me.dtpDate1.TabIndex = 77
    Me.dtpDate1.Value = New Date(2012, 5, 5, 0, 0, 0, 0)
    '
    'lblPhone
    '
    Me.lblPhone.AutoSize = True
    Me.lblPhone.Location = New System.Drawing.Point(9, 22)
    Me.lblPhone.Name = "lblPhone"
    Me.lblPhone.Size = New System.Drawing.Size(38, 13)
    Me.lblPhone.TabIndex = 76
    Me.lblPhone.Text = "Phone"
    '
    'cmdSearch
    '
    Me.cmdSearch.Location = New System.Drawing.Point(597, 37)
    Me.cmdSearch.Name = "cmdSearch"
    Me.cmdSearch.Size = New System.Drawing.Size(87, 32)
    Me.cmdSearch.TabIndex = 77
    Me.cmdSearch.Text = "Search"
    Me.cmdSearch.UseVisualStyleBackColor = True
    '
    'cmdClear
    '
    Me.cmdClear.Location = New System.Drawing.Point(597, 75)
    Me.cmdClear.Name = "cmdClear"
    Me.cmdClear.Size = New System.Drawing.Size(87, 32)
    Me.cmdClear.TabIndex = 78
    Me.cmdClear.Text = "Clear"
    Me.cmdClear.UseVisualStyleBackColor = True
    '
    'frmSearch
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(730, 512)
    Me.Controls.Add(Me.cmdClear)
    Me.Controls.Add(Me.cmdSearch)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.fraInfo1)
    Me.KeyPreview = True
    Me.Name = "frmSearch"
    Me.Text = "Search"
    Me.fraInfo1.ResumeLayout(False)
    Me.fraInfo1.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents fraInfo1 As System.Windows.Forms.GroupBox
  Friend WithEvents lblCompanyName As System.Windows.Forms.Label
  Friend WithEvents txtCompany_Name As System.Windows.Forms.TextBox
  Friend WithEvents cboCity As System.Windows.Forms.ComboBox
  Friend WithEvents txtZip As System.Windows.Forms.TextBox
  Friend WithEvents lblZip As System.Windows.Forms.Label
  Friend WithEvents cboState As System.Windows.Forms.ComboBox
  Friend WithEvents lblState As System.Windows.Forms.Label
  Friend WithEvents lblCity As System.Windows.Forms.Label
  Friend WithEvents cboAddress_Ending As System.Windows.Forms.ComboBox
  Friend WithEvents cboAddress_Direction As System.Windows.Forms.ComboBox
  Friend WithEvents txtAddress_Numbers As System.Windows.Forms.TextBox
  Friend WithEvents txtAddress As System.Windows.Forms.TextBox
  Friend WithEvents lblAddress As System.Windows.Forms.Label
  Friend WithEvents lblLast_Name As System.Windows.Forms.Label
  Friend WithEvents txtLast_Name As System.Windows.Forms.TextBox
  Friend WithEvents lblFirst_Name As System.Windows.Forms.Label
  Friend WithEvents txtFirst_Name As System.Windows.Forms.TextBox
  Friend WithEvents txtPhone1EXT As System.Windows.Forms.TextBox
  Friend WithEvents cboSource As System.Windows.Forms.ComboBox
  Friend WithEvents lblSource As System.Windows.Forms.Label
  Friend WithEvents cboFoundation As System.Windows.Forms.ComboBox
  Friend WithEvents lblFoundation As System.Windows.Forms.Label
  Friend WithEvents lblEmail As System.Windows.Forms.Label
  Friend WithEvents txtEmail As System.Windows.Forms.TextBox
  Friend WithEvents txtPhone1 As System.Windows.Forms.TextBox
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents lblPhone As System.Windows.Forms.Label
  Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
  Friend WithEvents lblAppt_Date As System.Windows.Forms.Label
  Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
  Friend WithEvents cmdSearch As System.Windows.Forms.Button
  Friend WithEvents cmdClear As System.Windows.Forms.Button
End Class
