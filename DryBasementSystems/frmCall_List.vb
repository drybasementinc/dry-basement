﻿Public Class frmCall_List
  'General Declarations
  Dim Address As New DataTable
  Dim Clients As New DataTable
  Dim General As New DataTable
  Dim DryDispo As New DataTable
  Dim MagicDispo As New DataTable
  Dim DryProduction As New DataTable
  Dim MagicProduction As New DataTable
  Dim Est_Schedule As New DataTable
  Dim CallLog As New DataTable

  Dim drGeneral As Double
  Dim drAddress As Double
  Dim drClients As Double
  Dim drDryDispo As Double
  Dim drMagicDispo As Double
  Dim drDryProduction As Double
  Dim drMagicProduction As Double
  Dim drEst_Schedule As Double
  Dim drCallLog As Double

  Dim i As Integer
  Dim r As Integer
  Dim c As Integer

  Private Sub frmCall_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Dim List As New DataTable
    Connect("Call_List", List, 0, , "Name")
    For intRow As Integer = 0 To List.Rows.Count - 1
      cboCallList.Items.Add(List.Rows(intRow).Item("Name"))
    Next

    Grid_Lock(Me, grdCallList, True, True)
  End Sub

  Private Sub cmdGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGo.Click
    Dim Call_List As New DataTable

    Connect("Call_List_Info", Call_List, 0, "Call_List = '" & cboCallList.Text & "' AND Active_Date <= " & FilterByDate(Now.Date))

    With grdCallList

      .ColumnCount = 7
      .RowCount = Call_List.Rows.Count
      .Columns(0).HeaderText = "NAME"
      .Columns(1).HeaderText = "ADDRESS"
      .Columns(2).HeaderText = "CITY"
      .Columns(3).HeaderText = "STATE"
      .Columns(4).HeaderText = "ZIP"
      .Columns(5).HeaderText = "PERSON"
      .Columns(6).HeaderText = "LAST CONTACT"

      For intRow As Integer = 0 To Call_List.Rows.Count - 1
        Connect("Addresses", Address, 0, "ID = " & Call_List.Rows(intRow).Item("Address_ID"))
        Connect("Clients", Clients, 0, "ID = " & Address.Rows(0).Item("Current_Client_ID"))

        Try
          .Item(0, intRow).Value = Clients.Rows(0).Item("First_Name") & " " & Clients.Rows(0).Item("Last_Name")
        Catch ex As Exception
          .Item(0, intRow).Value = "Unable to find Client Info"
        End Try
        .Item(0, intRow).Tag = Address.Rows(0).Item("ID")
        .Item(1, intRow).Value = Address.Rows(0).Item("Address_Numbers") & " " & Address.Rows(0).Item("Address_Direction") & " " & Address.Rows(0).Item("Address") & " " & Address.Rows(0).Item("Address_Ending")
        .Item(1, intRow).Tag = Address.Rows(0).Item("ID")
        .Item(2, intRow).Value = Address.Rows(0).Item("City")
        .Item(2, intRow).Tag = Address.Rows(0).Item("ID")
        .Item(3, intRow).Value = Address.Rows(0).Item("State")
        .Item(3, intRow).Tag = Address.Rows(0).Item("ID")
        .Item(4, intRow).Value = Address.Rows(0).Item("Zip")
        .Item(4, intRow).Tag = Address.Rows(0).Item("ID")
        '.Item(5, intRow).Value = Address.Rows(0).Item("Estimator")
        .Item(5, intRow).Tag = Address.Rows(0).Item("ID")
        .Item(6, intRow).Value = Call_List.Rows(intRow).Item("Last_Contact_Name")
        .Item(6, intRow).Tag = Address.Rows(0).Item("ID")
      Next
    End With
  End Sub

  Private Sub grdCallList_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCallList.CellDoubleClick
    dblAddressID = grdCallList.CurrentCell.Tag
    frmLead.ShowDialog(Me)
    cmdGo_Click(Me, e)
  End Sub

  Private Sub frmCall_List_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
    grdCallList.Width = Me.Width - grdCallList.Left - 30
    grdCallList.Height = Me.Height - grdCallList.Top - 30
  End Sub
End Class