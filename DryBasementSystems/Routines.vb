﻿Imports System.Net.Mail
Imports System.Drawing.Printing

Module Routines
  Public Data_Swap As New DataTable
  Public DS_Targert As String
  Public DS_Form As String

  Public Function LT(ByVal text As String, ByVal length As Integer)
    Dim txt As New TextBox
    With txt
      .Text = text
      If length >= Len(text) = True Then
        Return Nothing
        Exit Function
      End If
      .SelectionStart = length
      .SelectionLength = Len(.Text) - .SelectionStart
      Return .SelectedText
    End With
  End Function

  Public Function RT(ByVal text As String, ByVal length As Integer)
    Dim txt As New TextBox
    With txt
      .Text = text
      If length >= Len(text) = True Then
        Return Nothing
        Exit Function
      End If
      .SelectionStart = 0
      .SelectionLength = Len(.Text) - length
      Return .SelectedText
    End With
  End Function

  Public Sub Form_Loader(
    ByVal Source As System.Object,
    ByVal tag1 As String,
    ByVal Data1 As DataTable,
    ByVal Row1 As Double,
    Optional ByVal tag2 As String = "NONE",
    Optional ByVal Data2 As DataTable = Nothing,
    Optional ByVal Row2 As Double = 0,
    Optional ByVal tag3 As String = "NONE",
    Optional ByVal Data3 As DataTable = Nothing,
    Optional ByVal Row3 As Double = 0,
    Optional ByVal tag4 As String = "NONE",
    Optional ByVal Data4 As DataTable = Nothing,
    Optional ByVal Row4 As Double = 0,
    Optional ByVal tag5 As String = "NONE",
    Optional ByVal Data5 As DataTable = Nothing,
    Optional ByVal Row5 As Double = 0)

    Dim int1 As Integer
    Dim strControlName As String
    Dim DataSource As New DataTable
    Dim r As Integer = 0
    Dim c As Integer = 0

    Try
      For int1 = 0 To Source.Controls.Count - 1
        If Source.Controls(int1).Tag & "" = tag1 Then
          If Data1.Rows.Count > 0 Then
            'obtains the corresponding field name to be used later
            strControlName = LT(Source.Controls(int1).Name, 3)
            If TypeOf Source.controls(int1) Is CheckBox Or TypeOf Source.controls(int1) Is RadioButton Then
              Source.controls(int1).checked = Data1.Rows(Row1).Item(strControlName)
            ElseIf TypeOf Source.controls(int1) Is DataGridView Then
              For r = 0 To Source.controls(int1).rowcount - 1
                For c = 0 To Source.controls(int1).columncount - 1
                  Source.controls(int1).item(c, r).value = Data1.Rows(Row1 + r).Item(Source.controls(int1).columns(c).headercell.tag)
                Next
              Next

            Else
              If (Data1.Columns.Contains(strControlName)) Then
                If IsDBNull(Data1.Rows(Row1).Item(strControlName)) = False Then
                  Source.controls(int1).text = Data1.Rows(Row1).Item(strControlName)
                End If
              End If
            End If
          End If
        ElseIf Source.Controls(int1).Tag & "" = tag2 Then
          If Data2.Rows.Count > 0 Then
            'obtains the corresponding field name to be used later
            strControlName = LT(Source.Controls(int1).Name, 3)
            If TypeOf Source.controls(int1) Is CheckBox Or TypeOf Source.controls(int1) Is RadioButton Then
              Source.controls(int1).checked = Data2.Rows(Row2).Item(strControlName)
            ElseIf TypeOf Source.controls(int1) Is DataGridView Then
              For r = 0 To Source.controls(int1).rowcount - 1
                For c = 0 To Source.controls(int1).columncount - 1
                  Source.controls(int1).item(c, r).value = Data2.Rows(Row2 + r).Item(Source.controls(int1).columns(c).headercell.tag)
                Next
              Next
            Else
              If IsDBNull(Data2.Rows(Row2).Item(strControlName)) = False Then
                Source.controls(int1).text = Data2.Rows(Row2).Item(strControlName)
              End If
            End If
          End If
        ElseIf Source.Controls(int1).Tag & "" = tag3 Then
          If Data3.Rows.Count > 0 Then
            'obtains the corresponding field name to be used later
            strControlName = LT(Source.Controls(int1).Name, 3)
            If TypeOf Source.controls(int1) Is CheckBox Or TypeOf Source.controls(int1) Is RadioButton Then
              Source.controls(int1).checked = Data3.Rows(Row3).Item(strControlName)
            ElseIf TypeOf Source.controls(int1) Is DataGridView Then
              For r = 0 To Source.controls(int1).rowcount - 1
                For c = 0 To Source.controls(int1).columncount - 1
                  Source.controls(int1).item(c, r).value = Data3.Rows(Row3 + r).Item(Source.controls(int1).columns(c).headercell.tag)
                Next
              Next
            Else
              If IsDBNull(Data3.Rows(Row3).Item(strControlName)) = False Then
                Source.controls(int1).text = Data3.Rows(Row3).Item(strControlName)
              End If
            End If
          End If
        ElseIf Source.Controls(int1).Tag & "" = tag4 Then
          If Data4.Rows.Count > 0 Then
            'obtains the corresponding field name to be used later
            strControlName = LT(Source.Controls(int1).Name, 3)
            If TypeOf Source.controls(int1) Is CheckBox Or TypeOf Source.controls(int1) Is RadioButton Then
              Source.controls(int1).checked = Data4.Rows(Row4).Item(strControlName)
            ElseIf TypeOf Source.controls(int1) Is DataGridView Then
              For r = 0 To Source.controls(int1).rowcount - 1
                For c = 0 To Source.controls(int1).columncount - 1
                  Source.controls(int1).item(c, r).value = Data4.Rows(Row4 + r).Item(Source.controls(int1).columns(c).headercell.tag)
                Next
              Next
            Else
              If IsDBNull(Data4.Rows(Row4).Item(strControlName)) = False Then
                Source.controls(int1).text = Data4.Rows(Row4).Item(strControlName)
              End If
            End If
          End If
        ElseIf Source.Controls(int1).Tag & "" = tag5 Then
          If Data5.Rows.Count > 0 Then
            'obtains the corresponding field name to be used later
            strControlName = LT(Source.Controls(int1).Name, 3)
            If TypeOf Source.controls(int1) Is CheckBox Or TypeOf Source.controls(int1) Is RadioButton Then
              Source.controls(int1).checked = Data5.Rows(Row5).Item(strControlName)
            Else
              If IsDBNull(Data5.Rows(Row5).Item(strControlName)) = False Then
                Source.controls(int1).text = Data5.Rows(Row5).Item(strControlName)
              End If
            End If
          End If
        ElseIf Source.Controls(int1).HasChildren = True Then
          Form_Loader(Source.controls(int1), tag1, Data1, Row1, tag2, Data2, Row2, tag3, Data3, Row3, tag4, Data4, Row4, tag5, Data5, Row5)
        End If
      Next

    Catch ex As Exception
      MsgBox("Could not load information. Please make sure the correct information was provided.")
      LogException(ex)
    End Try
  End Sub

  Public Sub Combo_Check(ByVal sender As ComboBox, ByVal e As System.EventArgs)
    If sender.Text = "" Then Exit Sub
    If sender.Items.Contains(sender.Text) = False Then
      sender.Text = ""
      MsgBox("Please choose from the selection.")
      sender.Focus()
    End If
  End Sub

  Public Sub Add_Combo_Check(ByVal Source As System.Object)
    Dim cbo As ComboBox
    Dim intCount As Integer

    Try
      For intCount = 0 To Source.Controls.Count - 1
        If TypeOf (Source.Controls(intCount)) Is ComboBox Then
          cbo = Source.controls(intCount)
          cbo.AutoCompleteSource = AutoCompleteSource.ListItems
          cbo.AutoCompleteMode = AutoCompleteMode.SuggestAppend
          AddHandler cbo.LostFocus, AddressOf Combo_Check
        ElseIf Source.Controls(intCount).HasChildren = True Then
          Add_Combo_Check(Source.controls(intCount))
        End If
      Next

    Catch ext As Exception
      MsgBox("Could not add handler. Combo boxes will be unstable.")
    End Try
  End Sub


  Public Function String_to_Color(ByVal Color As String)
    Select Case UCase(Color)
      Case "0"
        Return Drawing.Color.Black
      Case "192"
        Return Drawing.Color.Red
      Case "32768"
        Return Drawing.Color.Green
      Case "16711680"
        Return Drawing.Color.Blue
      Case "65535"
        Return Drawing.Color.Yellow
      Case "8421504"
        Return Drawing.Color.LightSlateGray
      Case "-2147483643"
        Return Drawing.Color.White
      Case "BLACK"
        Return Drawing.Color.Black
      Case "RED"
        Return Drawing.Color.Red
      Case "GREEN"
        Return Drawing.Color.Green
      Case "BLUE"
        Return Drawing.Color.Blue
      Case "YELLOW"
        Return Drawing.Color.Yellow
      Case "GRAY"
        Return Drawing.Color.LightSlateGray
      Case "GREY"
        Return Drawing.Color.LightSlateGray
      Case "WHITE"
        Return Drawing.Color.White
      Case Else
        Return Drawing.Color.White
    End Select
  End Function

  Public Function Color_to_String(ByVal color As Drawing.Color)
    Select Case color
      Case Drawing.Color.Black
        Return "BLACK"
      Case Drawing.Color.Red
        Return "RED"
      Case Drawing.Color.Green
        Return "GREEN"
      Case Drawing.Color.Blue
        Return "BLUE"
      Case Drawing.Color.Yellow
        Return "YELLOW"
      Case Drawing.Color.LightSlateGray
        Return "GRAY"
      Case Drawing.Color.White
        Return "WHITE"
      Case Else
        Return Nothing
    End Select
  End Function

  Public Function Calendar_Date(ByVal sender As System.Object, ByVal Start_Date As Date)
    datCalendar = Start_Date
    frmCalendar.ShowDialog(sender)
    frmCalendar.Close()
    Return datCalendar
  End Function

  Public Sub Grid_Lock(ByVal sender As System.Object, ByVal Grid As DataGridView, Optional ByVal Sortable As Boolean = False, Optional ByVal Resizeable As Boolean = False)
    With Grid
      .AllowUserToAddRows = False
      .AllowUserToDeleteRows = False
      .AllowUserToOrderColumns = False
      .RowHeadersVisible = False
      .ReadOnly = True
      If Sortable = False Then
        For inttemp As Integer = 0 To .ColumnCount - 1
          .Columns(inttemp).SortMode = DataGridViewColumnSortMode.NotSortable
        Next
      End If
      If Resizeable = False Then
        For intr As Integer = 0 To Grid.RowCount - 1
          .Rows(intr).Resizable = DataGridViewTriState.False
        Next
        For intc As Integer = 0 To Grid.ColumnCount - 1
          .Columns(intc).Resizable = DataGridViewTriState.False
        Next
      End If
    End With
  End Sub

  Public Function Font_Change(ByVal style As String, ByVal Size As Single, ByVal Special As System.Drawing.FontStyle)
    Dim fs As Font

    fs = New System.Drawing.Font(style, Size, Special)
    Return fs
  End Function



  Public Sub SendMail(ByVal strFrom As String, ByVal strTo As String, ByVal strCC As String, ByVal strSubject As String, ByVal strBody As String, ByVal strAttachments As String)
    ''Dim smtpmail As SmtpClient = New SmtpClient("209.85.225.108", 587)
    ''Dim insMail As New MailMessage()
    ''smtpmail.EnableSsl = True
    ''smtpmail.Credentials = New Net.NetworkCredential("ray.mayberry@gmail.com", "free2007")
    ''insMail.To.Add(strTo)
    ''insMail.From = New MailAddress(strFrom)
    ''insMail.Subject = strSubject
    ''insMail.Body = strBody
    ''If strAttachments <> "" Then insMail.Attachments.Add(New Attachment(strAttachments))
    ''smtpmail.EnableSsl = True
    ''Try
    ''  smtpmail.Send(insMail)
    ''Catch ex As Exception
    ''  MsgBox("FUCK")
    ''End Try

    Exit Sub

    'Dim smtpmail As SmtpClient = New SmtpClient("smtp.gmail.com", 587)
    ''Dim smtpmail As SmtpClient = New SmtpClient("email.mxserver.us", 465)
    ''send the email 
    'Try
    '  Dim strTos() As String = strTo.Split(";")
    '  Dim insMail As New MailMessage()
    '  With insMail
    '    .From = New MailAddress(strFrom)
    '    Dim strFile As String

    '    For Each strFile In strTos
    '      .To.Add(strFile.Trim)
    '    Next
    '    '.To.Add(strTo)
    '    .Subject = strSubject
    '    .Body = strBody

    '    '.CC.Add(strCC)
    '    If Not strAttachments.Equals(String.Empty) Then
    '      'Dim strFile As String
    '      Dim strAttach() As String = strAttachments.Split(";")
    '      For Each strFile In strAttach
    '        .Attachments.Add(New Attachment(strFile.Trim()))
    '      Next
    '    End If
    '  End With


    '  'smtpmail.Credentials = New Net.NetworkCredential("info@drybasementsystems.com", "drybase")
    '  smtpmail.Credentials = New Net.NetworkCredential("ray.mayberry@gmail.com", "free2007")
    '  'smtpmail.EnableSsl = True
    '  'smtpmail.UseDefaultCredentials = True
    '  smtpmail.Send(insMail)
    '  insMail.Dispose()


    'Catch e As Exception
    '  MsgBox(e.Message)
    'End Try

  End Sub

  Public Sub Data_Export(ByVal Data_Source As DataTable, ByVal Target_Source As DataTable) ', ByVal Targert_Form As String)
    Target_Source = Data_Source
    'Data_Swap = Data_Source
    'DS_Targert = Target_Source
    'DS_Form = Targert_Form
  End Sub

  Public Sub Data_Import(ByVal Target_Source As DataTable, ByVal Sender As Form)
    If Sender.Name = DS_Form Then
      Target_Source = Data_Swap
    End If
  End Sub

  Public Sub Lead_Print(ByVal ID As Double, ByVal sender As Form, Optional ByVal PageNumber As Integer = 1, Optional ByVal EMAIL As Boolean = False)
    Dim flgClose As Boolean
    Dim intPage As Integer
    Dim r As Integer
    Dim c As Integer
    Dim ER As String
    Dim EC As String
    Dim cell As String
    Dim Cell2 As String
    Dim PrevText As String
    Dim strTemp As String
    Dim Addresses As New DataTable
    Dim drAddress As Double
    Dim Clients As New DataTable
    Dim drClients As Double
    Dim Info As New DataTable
    Dim drInfo As DataTable
    Dim DryProduction As New DataTable
    Dim drDryProd As Double
    Dim MagicProduction As New DataTable
    Dim drMagicProd As Double
    Dim DryDispo As New DataTable
    Dim drDryDispo As Double
    Dim MagicDispo As New DataTable
    Dim drMagicDispo As Double
    Dim Schedule As New DataTable
    Dim drSchedule As Double
    On Error Resume Next

    sender.UseWaitCursor = True

    Dim xlsheet As New excel
    xlsheet.Excel_Open_File("C:\Ray_Mayberry\Dry_Basement\NewLeads3.xls")

    Connect("Addresses", Addresses, 0, "ID = " & ID)
    If Addresses.Rows.Count <> 1 Then
      MsgBox("An unexpected number of records were returned. Unable to proceed with print job.")
      xlsheet.Excel_Close_File(False)
      sender.UseWaitCursor = False
      Exit Sub
    End If

    Connect("Clients", Clients, 0, "ID = " & Addresses.Rows(0).Item("Current_Client_ID"))
    If Clients.Rows.Count <> 1 Then
      MsgBox("An unexpected number of records were returned. Unable to proceed with print job.")
      xlsheet.Excel_Close_File(False)
      sender.UseWaitCursor = False
      Exit Sub
    End If

    Connect("Est_Info", Info, 0, "Address_ID = " & Addresses.Rows(0).Item("ID"))
    If Info.Rows.Count <> 1 Then
      MsgBox("An unexpected number of records were returned. Unable to proceed with print job.")
      xlsheet.Excel_Close_File(False)
      sender.UseWaitCursor = False
      Exit Sub
    End If

    Connect("Schedule", Schedule, 0, "Address_ID = " & Addresses.Rows(0).Item("ID") & " AND Estimator_Appt = 1", "Date DESC")
    Connect("Dry_Dispo", DryDispo, 0, "Address_ID = " & Addresses.Rows(0).Item("ID"), "Appt_Date")
    Connect("Magic_Dispo", MagicDispo, 0, "Address_ID = " & Addresses.Rows(0).Item("ID"), "Appt_Date")
    Connect("Dry_Production", DryProduction, 0, "Address_ID = " & Addresses.Rows(0).Item("ID"), "Start_Date")
    Connect("Magic_Production", MagicProduction, 0, "Address_ID = " & Addresses.Rows(0).Item("ID"), "Start_Date")


    With xlsheet
      .Cell("b39").value = ""
      drDryDispo = 0
      While drDryDispo < DryDispo.Rows.Count
        If DryDispo.Rows(drDryDispo).Item("Reset") = "True" Then
          If .Cell("b39").value <> "" Then
            .Cell("b39").value = .Cell("b39").value & (Chr(13) + Chr(10)) & DryDispo.Rows(drDryDispo).Item("Appt_Date") & " " & DryDispo.Rows(drDryDispo).Item("Reset_Reason")
          Else
            .Cell("b39").value = .Cell("b39").value & DryDispo.Rows(drDryDispo).Item("Appt_Date") & " " & DryDispo.Rows(drDryDispo).Item("Reset_Reason")
          End If
        Else
          If .Cell("b39").value <> "" Then
            .Cell("b39").value = .Cell("b39") & (Chr(13) + Chr(10)) & DryDispo.Rows(drDryDispo).Item("Appt_Date") & " " & DryDispo.Rows(drDryDispo).Item("Print_Offer")
          Else
            .Cell("b39").value = .Cell("b39") & DryDispo.Rows(drDryDispo).Item("Appt_Date") & " " & DryDispo.Rows(drDryDispo).Item("Print_Offer")
          End If
        End If
        drDryDispo += 1
      End While

      drMagicDispo = 0
      While drMagicDispo < MagicDispo.Rows.Count
        If MagicDispo.Rows(drMagicDispo).Item("Reset") = "True" Then
          If .Cell("b39").value <> "" Then
            .Cell("b39").value = .Cell("b39").value & (Chr(13) + Chr(10)) & MagicDispo.Rows(drMagicDispo).Item("Appt_Date") & " " & MagicDispo.Rows(drMagicDispo).Item("Reset_Reason")
          Else
            .Cell("b39").value = .Cell("b39").value & MagicDispo.Rows(drMagicDispo).Item("Appt_Date") & " " & MagicDispo.Rows(drMagicDispo).Item("Reset_Reason")
          End If
        Else
          If .Cell("b39").value <> "" Then
            .Cell("b39").value = .Cell("b39") & (Chr(13) + Chr(10)) & MagicDispo.Rows(drMagicDispo).Item("Appt_Date") & " " & MagicDispo.Rows(drMagicDispo).Item("Print_Offer")
          Else
            .Cell("b39").value = .Cell("b39") & MagicDispo.Rows(drMagicDispo).Item("Appt_Date") & " " & MagicDispo.Rows(drMagicDispo).Item("Print_Offer")
          End If
        End If
        drMagicDispo += 1
      End While

      .Cell("b47").value = ""
      drDryProd = 0
      While drDryProd < DryProduction.Rows.Count - 1
        If .Cell("b47").value <> "" Then
          .Cell("b47").value = .Cell("b47") & (Chr(13) + Chr(10)) & DryProduction.Rows(drDryProd).Item("Complete_Date") & " " & DryProduction.Rows(drDryProd).Item("Print_Installed")
        Else
          .Cell("b47").value = .Cell("b47") & DryProduction.Rows(drDryProd).Item("Complete_Date") & " " & DryProduction.Rows(drDryProd).Item("Print_Installed")
        End If
        If DryProduction.Rows(drDryProd).Item("Complete") = "True" Then MsgBox("WORK REPORT    " & Clients.Rows(0).Item("Last_Name") & "/" & Addresses.Rows(0).Item("Estimator"))
        drDryProd += 1
      End While
      intPage = intPage + 1

      .Cell("b2").value = Clients.Rows(0).Item("First_Name")
      .Cell("g2").value = Clients.Rows(0).Item("Last_Name")
      If IsDBNull(Clients.Rows(0).Item("First_Name2")) = False Then
        .Cell("b3").value = Clients.Rows(0).Item("First_Name2")
      End If
      If IsDBNull(Clients.Rows(0).Item("Last_Name2")) = False Then
        .Cell("g3").value = Clients.Rows(0).Item("Last_Name2")
      End If

      If IsDBNull(Schedule.Rows(0).Item("Bonus_Sig")) = False Then .Cell("i1").value = Schedule.Rows(0).Item("Bonus_Sig")

      strResponse = Addresses.Rows(0).Item("Address_Numbers")
      If Addresses.Rows(0).Item("Address_Direction") <> "" Then strResponse = strResponse & " " & Addresses.Rows(0).Item("Address_Direction")
      strResponse = strResponse & " " & Addresses.Rows(0).Item("Address")
      If Addresses.Rows(0).Item("Address_Ending") <> "" Then strResponse = strResponse & " " & Addresses.Rows(0).Item("Address_Ending")
      .Cell("b4").value = strResponse
      .Cell("b5").value = Addresses.Rows(0).Item("City") & " " & Addresses.Rows(0).Item("State") & " " & Addresses.Rows(0).Item("Zip")
      If IsDBNull(Addresses.Rows(0).Item("Zone")) = False And Addresses.Rows(0).Item("Zone") <> "" Then .Cell("b1").value = Addresses.Rows(0).Item("Zone")
      If IsDBNull(Schedule.Rows(0).Item("Set_Date")) = False Then .Cell("k1").value = Schedule.Rows(0).Item("Set_Date")
      .Cell("b6").value = Clients.Rows(0).Item("Phone1")
      .Cell("g6").value = Clients.Rows(0).Item("Phone2")
      .Cell("c7").value = Clients.Rows(0).Item("Email")
      .Cell("h9").value = ""
      If Addresses.Rows(0).Item("Assistance") = "True" Then
        .Cell("h9").value = Trim(Addresses.Rows(0).Item("Rep1") & " " & Addresses.Rows(0).Item("Rep2") & " " & Addresses.Rows(0).Item("Rep3"))
      End If
      .Cell("g8").value = Info.Rows(0).Item("Source") & "/" & Info.Rows(0).Item("Source2")
      If Addresses.Rows(0).Item("Dry_New") = "True" Then .Cell("g8").value = .Cell("g8").value & "/LEAD"
      .Cell("b8").value = Addresses.Rows(0).Item("Foundation")
      .Cell("b25").value = ""
      .Cell("b25").ShrinkToFit = True
      .Cell("b25").WrapText = True
      If IsDBNull(Addresses.Rows(0).Item("Comments")) = False Then .Cell("b25").value = Addresses.Rows(0).Item("Comments")
      .Cell("b25").value = Info.Rows(0).Item("Est_comments") & "" & (Chr(13) + Chr(10)) & .Cell("b25").value
      If Err.Number = 1004 Then
        Err.Clear()
        .Cell("b25").value = Replace(Addresses.Rows(0).Item("Comments"), (Chr(13) + Chr(10)), " <NEXT> ")
        While Not c = 0
          c = InStr(c, .Cell("b25").value, "<NEXT>")
          If c > 0 Then
            .Cell("b25").Characters(c, 6).Font.ColorIndex = 5
            .Cell("b25").Characters(c, 6).Font.Bold = True
          End If
          If c <> 0 Then c = c + 1
        End While
      End If
      .Cell("b9").value = Replace(Schedule.Rows(0).Item("Appt_Type"), "NORMAL", "")
      .Cell("b9").value = .Cell("b9") & Format(Schedule.Rows(0).Item("Start_Time"), "h:mm AMPM") & " " & Schedule.Rows(0).Item("Person") & " " & Schedule.Rows(0).Item("Date")
      For c = 10 To 24
        .Cell("b" & c).value = ""
        .Cell("b" & c).UnMerge()
      Next
      strResponse = Replace(Addresses.Rows(0).Item("Directions"), (Chr(13) + Chr(10)), "<NEXT>")
      .Cell("b10").value = strResponse
      c = 1
      While Not c = 0
        c = InStr(c, .Cell("b10").value, "<NEXT>")
        If c > 0 Then
          .Cell("b10").Characters(c, 6).Font.ColorIndex = 5
          .Cell("b10").Characters(c, 6).Font.Bold = True
        End If
        If c <> 0 Then c = c + 1
      End While
      .Cell("b10").Interior.ColorIndex = "xlAutomatic"
      For c = 10 To 24
        .Cell("b10", "k24").Merge()
      Next
      .FitToPage(1, 1)

      strResponse = ""
      If Info.Rows(0).Item("Prob_Water") = "True" Then strResponse += "WATER/"
      If Info.Rows(0).Item("Prob_Cracks") = "True" Then strResponse += "CRACKS/"
      If Info.Rows(0).Item("Prob_Settling") = "True" Then strResponse += "SETTLING/"
      If Info.Rows(0).Item("Prob_Bowed") = "True" Then strResponse += "BOWED/"
      If Info.Rows(0).Item("Prob_Crawl") = "True" Then strResponse += "CRAWL/"
      If Info.Rows(0).Item("Prob_Other") = "True" Then strResponse += "OTHER"
      .Cell("h5").value = strResponse
      If Info.Rows(0).Item("Prob_Other") = "True" Then .Cell("b25").value = Info.Rows(0).Item("Prob_Other_Text") & Chr(13) + Chr(10) & .Cell("b25").value

      If IsDBNull(Schedule.Rows(0).Item("Confirmer")) = False Then .Cell("g1").value = Schedule.Rows(0).Item("Confirmer")
      strResponse = vbYes

      If EMAIL = False Then
        .Excel_Show(False)
        Dim ps As New PrinterSettings()

        .Print(ps.PrinterName, True)
        .Excel_Close_File()
      Else
        .SaveAs("C:/EstimatorAppts/" & Addresses.Rows(0).Item("Estimator") & "/" & Format(Addresses.Rows(0).Item("Current_Appt_Date"), "MM-DD-YY") & "_Appt" & intPage & ".xls")
        .Excel_Close_File()
        SendMail("ray.mayberry@gmail.com", "ray.mayberry@gmail.com", "", "Lead", "", "C:/EstimatorAppts/" & Addresses.Rows(0).Item("Estimator") & "/" & Format(Addresses.Rows(0).Item("Current_Appt_Date"), "MM-DD-YY") & "_Appt" & intPage & ".xls")
      End If

    End With
    sender.UseWaitCursor = False
  End Sub
End Module
