

CREATE NONCLUSTERED INDEX [IX_Clients_First_Name] ON [dbo].[Clients] ( [First_Name] ASC )
GO

CREATE NONCLUSTERED INDEX [IX_Clients_Last_Name] ON [dbo].[Clients] ( [Last_Name] ASC )
GO

CREATE NONCLUSTERED INDEX [IX_Clients_Company_Name] ON [dbo].[Clients] ( [Company_Name] ASC )
GO


CREATE NONCLUSTERED INDEX [IX_Addresses_Address_Numbers] ON [dbo].[Addresses] ( [Address_Numbers] ASC )
GO

CREATE NONCLUSTERED INDEX [IX_Addresses_Address] ON [dbo].[Addresses] ( [Address] ASC )
GO

CREATE NONCLUSTERED INDEX [IX_Addresses_City] ON [dbo].[Addresses] ( [City] ASC )
GO

CREATE NONCLUSTERED INDEX [IX_Addresses_Zip] ON [dbo].[Addresses] ( [Zip] ASC )
GO


CREATE NONCLUSTERED INDEX [IX_Magic_Dispo_Address_ID] ON [dbo].[Magic_Dispo] ( [Address_ID] ASC )
GO


CREATE NONCLUSTERED INDEX [IX_Magic_Production_Address_ID] ON [dbo].[Magic_Production] ( [Address_ID] ASC )
GO

