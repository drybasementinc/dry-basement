



select
	 a.ID AddressId
	,c.ID ClientId
	,c.First_Name
	,c.Last_Name
	,c.First_Name2
	,c.Last_Name2
	,c.Company_Name
	,c.Phone1
	,c.Phone1_Ext
	,a.Address_Numbers
	,a.Address_Direction
	,a.[Address]
	,a.Address_Ending
	,a.City
	,a.[State]
	,a.Zip AddressZip

	,p.PumpWorks
	,p.PumpWorksPlus
	,p.PumpWorksHD
	,p.SuperSump
	,p.Ultra
	,p.TripleSafe
	
	,a.Next_Annual_Date
	,p.Complete_Date

from
dbo.Addresses a
join dbo.Clients c on a.Current_Client_ID = c.ID
join dbo.Dry_Production p on a.ID = p.Address_ID

where
a.Annual = 1

and a.Next_Annual_Date is null

and
(
(a.Next_Annual_Date >= '2013-10-01' and a.Next_Annual_Date < '2013-11-01')
or
(a.Next_Annual_Date is null and DATEADD(year, 1, p.Complete_Date) >= '2013-10-01' and DATEADD(year, 1, p.Complete_Date) < '2013-11-01')
)



