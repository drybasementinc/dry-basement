﻿/*
Create DryBasement SQL Database schema

Creates tables with identity keys

when merging data from Access, enable "Enable Identity Insert" on SSIS package

*/



/****** Object:  Table [dbo].[Access_Levels]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Access_Levels](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Access_Level_Title] [nvarchar](255) NULL,
	[Access_Level] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Address_Directions]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address_Directions](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Direction] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Address_Endings]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address_Endings](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Endings] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Addresses]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Addresses](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Old_ID] [int] NULL,
	[Address_Numbers] [nvarchar](50) NULL,
	[Address_Direction] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[Address_Ending] [nvarchar](50) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [int] NULL,
	[Current_Client_ID] [int] NULL,
	[Prior_Client1_ID] [int] NULL,
	[Last_Estimator_ID] [int] NULL,
	[Foundation] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NULL,
	[Source2] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Prob_Cracks] [bit] NULL,
	[Prob_Water] [bit] NULL,
	[Prob_Bowed] [bit] NULL,
	[Prob_Settling] [bit] NULL,
	[Prob_Crawl] [bit] NULL,
	[Prob_Other] [bit] NULL,
	[Prob_Other_Text] [nvarchar](255) NULL,
	[Installed] [bit] NULL,
	[House_Description] [nvarchar](255) NULL,
	[Dry_Dispo_Done] [bit] NULL,
	[Magic_Dispo_Done] [bit] NULL,
	[Dry_New] [bit] NULL,
	[Magic_New] [bit] NULL,
	[Dry_RESET] [bit] NULL,
	[RESET_Contacted] [bit] NULL,
	[RESET_Name] [nvarchar](255) NULL,
	[RESET_Delay] [datetime] NULL,
	[Magic_RESET] [bit] NULL,
	[RESET_Estimator] [nvarchar](50) NULL,
	[Current_Dry_Dispo] [int] NULL,
	[Current_Dry_Production] [int] NULL,
	[Current_Magic_Dispo] [int] NULL,
	[Current_Magic_Production] [int] NULL,
	[WaterProofing] [bit] NULL,
	[Piering] [bit] NULL,
	[Anchoring] [bit] NULL,
	[Cosmetic] [bit] NULL,
	[Miscellaneous] [bit] NULL,
	[Directions] [nvarchar](max) NULL,
	[Comments] [nvarchar](max) NULL,
	[Current_Appt_Date] [datetime] NULL,
	[Appt_Time] [datetime] NULL,
	[Estimator] [nvarchar](50) NULL,
	[Appt_Type] [nvarchar](255) NULL,
	[Assistance] [bit] NULL,
	[Rep1] [nvarchar](50) NULL,
	[Rep2] [nvarchar](50) NULL,
	[Rep3] [nvarchar](50) NULL,
	[Rep4] [nvarchar](50) NULL,
	[Rep5] [nvarchar](50) NULL,
	[Assistance_Details] [nvarchar](255) NULL,
	[Assistance_Status] [nvarchar](50) NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [nvarchar](50) NULL,
	[Active] [bit] NULL,
	[Bonus_Sig] [nvarchar](255) NULL,
	[Activity] [nvarchar](max) NULL,
	[Confirmed] [bit] NULL,
	[Confirmer] [nvarchar](255) NULL,
	[Set_Date] [datetime] NULL,
	[Real_Appt_Time] [datetime] NULL,
	[Zone] [nvarchar](50) NULL,
	[Reval] [bit] NULL,
	[Reval_Delay] [datetime] NULL,
	[Reval_Contacted] [nvarchar](255) NULL,
	[Reval_Caller] [nvarchar](255) NULL,
	[Reval_Comments] [nvarchar](max) NULL,
	[Reval_Add_Date] [datetime] NULL,
	[CallBack] [nvarchar](255) NULL,
	[Annual] [bit] NULL,
	[Annual_Date] [nvarchar](255) NULL,
	[Next_Annual_Date] [datetime] NULL,
	[Number_Pumps] [nvarchar](50) NULL,
	[Annual_Done] [bit] NULL,
	[Annual_Appt_Date] [datetime] NULL,
	[Annual_Person] [nvarchar](255) NULL,
	[Annual_Appt_Time] [datetime] NULL,
	[Annual_Dispo_Done] [bit] NULL,
	[Annual_ID] [int] NULL,
	[Annual_Active] [bit] NULL,
	[Annual_Contacted] [bit] NULL,
	[Annual_Vacant] [bit] NULL,
	[Annual_Remove] [bit] NULL,
	[Annual_Activity] [nvarchar](max) NULL,
	[Annual_Sent] [datetime] NULL,
	[Plumbing_Card] [bit] NULL,
	[Plumbing_Sent] [datetime] NULL,
	[TBF_Card] [bit] NULL,
	[TBF_Sent] [datetime] NULL,
	[CARD_VACANT] [bit] NULL,
	[CARD_BAD] [bit] NULL,
	[NO_ADVERTISE] [bit] NULL,
	[NO_ADVERTISE_DATE] [datetime] NULL,
	[Canceled] [bit] NULL,
	[Canceled_Date] [datetime] NULL,
	[Full_Source] [nvarchar](50) NULL,
	[File_Location] [nvarchar](50) NULL,
	[NO_SERVICE] [bit] NULL,
	[Follow_Up] [bit] NULL,
	[Reference] [bit] NULL,
	[Cancel_Delay] [datetime] NULL,
	[Cancel_Contacted] [nvarchar](50) NULL,
	[Reval_Letter] [bit] NULL,
	[Reval_Gift] [bit] NULL,
	[Reval_Sent_Date] [datetime] NULL,
	[Reval_Letter_Received] [bit] NULL,
	[Reval_Last_Call_Date] [datetime] NULL,
	[Job_Type] [nvarchar](50) NULL,
	[Otto_Status] [nvarchar](50) NULL,
	[Bad_Date] [bit] NULL,
	[DriveTime] [nvarchar](50) NULL,
	[Miles] [nvarchar](50) NULL,
	[Estimator_Comments] [nvarchar](max) NULL,
	[Prob_Remodel] [bit] NULL,
	[Prob_Mold] [bit] NULL,
	[Cancel_Call] [bit] NULL,
	[Cancel_Work] [bit] NULL,
	[Cancel_Last_Call_Date] [datetime] NULL,
	[Cancel_Comments] [nvarchar](max) NULL,
	[Cancel_Letter_Received] [bit] NULL,
	[Magic_Reval] [bit] NULL,
	[New_Product] [bit] NULL,
	[New_Product_Last_Call_Date] [datetime] NULL,
	[Pump] [bit] NULL,
	[Pump_Last_Call_Date] [datetime] NULL,
	[Pump_Contacted] [nvarchar](50) NULL,
	[Pump_Delay] [datetime] NULL,
	[Pump_Comments] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Admin_Password]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin_Password](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Password] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Advertising]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Advertising](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Source] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
	[Installed] [int] NULL,
	[Sold] [int] NULL,
	[Canceled] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Always_Info]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Always_Info](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Water_Heater] [bit] NULL,
	[Drain] [bit] NULL,
	[Toilet] [bit] NULL,
	[Faucet] [bit] NULL,
	[Disposal] [bit] NULL,
	[Lowes] [bit] NULL,
	[Lowes_Store] [nvarchar](50) NULL,
	[Additional] [bit] NULL,
	[Additional_Text] [nvarchar](255) NULL,
	[AP_Bonus] [nvarchar](50) NULL,
	[AP_Zone] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Announcements]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Announcements](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Expire] [datetime] NULL,
	[Announce] [nvarchar](255) NULL,
	[Date] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Annual]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Annual](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Offer] [nvarchar](max) NULL,
	[Print_Offer] [nvarchar](255) NULL,
	[Appt_Date] [datetime] NULL,
	[Price] [nvarchar](50) NULL,
	[Annual_Charge] [int] NULL,
	[Subfloor] [int] NULL,
	[WaterGuard] [int] NULL,
	[Combo] [int] NULL,
	[SuperSump] [int] NULL,
	[Ultra] [int] NULL,
	[TripleSafe] [int] NULL,
	[TrenchGrate] [int] NULL,
	[TrenchDrain] [int] NULL,
	[Lateral] [int] NULL,
	[SunHouse] [int] NULL,
	[WellDucts] [int] NULL,
	[SaniDry] [int] NULL,
	[SaniDryCSB] [int] NULL,
	[Flexispan] [int] NULL,
	[DryWell] [int] NULL,
	[BriteWall] [int] NULL,
	[SmartSump] [int] NULL,
	[CleanSpace] [int] NULL,
	[DryTrak] [int] NULL,
	[CrackFiller] [int] NULL,
	[SmartDrain] [int] NULL,
	[FloodRail] [int] NULL,
	[CondensationPump] [int] NULL,
	[HouseHolders] [int] NULL,
	[FloorHolders] [int] NULL,
	[GradeBeam] [int] NULL,
	[WallReplacement] [int] NULL,
	[DBAS] [int] NULL,
	[Straightening] [int] NULL,
	[DrainMatting] [int] NULL,
	[FloodcheckHoses] [int] NULL,
	[FloodRing] [int] NULL,
	[IntegratedHH] [int] NULL,
	[AdjustablePost] [int] NULL,
	[ThermalDryWall] [int] NULL,
	[TBF] [int] NULL,
	[SaniDryDucted] [int] NULL,
	[SaniDryCSBDucted] [int] NULL,
	[ZenWall] [int] NULL,
	[ThermalDryFlooring] [int] NULL,
	[ThermalDryMatting] [int] NULL,
	[CrawlOSphere] [int] NULL,
	[VentCovers] [int] NULL,
	[EverlastCrawlDoor] [int] NULL,
	[EverlastWindows] [int] NULL,
	[EgressWindows] [int] NULL,
	[LawnScape] [int] NULL,
	[Overflow] [int] NULL,
	[BasementFinish] [int] NULL,
	[PumpReplacement] [int] NULL,
	[PumpWarranty] [int] NULL,
	[UltraPump] [int] NULL,
	[Stand] [int] NULL,
	[AnnualFloodCheckHoses] [int] NULL,
	[AnnualFloodRing] [int] NULL,
	[RainChute] [int] NULL,
	[PumpLid] [int] NULL,
	[BrightWall] [int] NULL,
	[BrightWallCrack] [int] NULL,
	[RidgidBrightWall] [int] NULL,
	[BrightPost] [int] NULL,
	[AnnualLateral] [int] NULL,
	[IceGuard] [int] NULL,
	[Alarm] [int] NULL,
	[WindowWell] [int] NULL,
	[WellDuct] [int] NULL,
	[AnnualCombo] [int] NULL,
	[AnnualSaniDry] [int] NULL,
	[Dehumidifier] [int] NULL,
	[FlexispanNew] [int] NULL,
	[FlexispanExisting] [int] NULL,
	[Discharge] [int] NULL,
	[CheckValve] [int] NULL,
	[Fernco] [int] NULL,
	[DehumidifierHose] [int] NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Annual_Info]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Annual_Info](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Pump_Count] [int] NULL,
	[Comments] [nvarchar](max) NULL,
	[Last_Tech] [nvarchar](50) NULL,
	[Last_Annual] [datetime] NULL,
	[Next_Annual] [datetime] NULL,
	[Price] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Annual_Peeps]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Annual_Peeps](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Name] [nvarchar](255) NULL,
	[Working] [bit] NULL,
	[Senority] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Annual_Schedule]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Annual_Schedule](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Date] [datetime] NULL,
	[Start_Time] [datetime] NULL,
	[End_Time] [datetime] NULL,
	[Person] [nvarchar](255) NULL,
	[Position] [int] NULL,
	[Info] [nvarchar](255) NULL,
	[Operator] [nvarchar](255) NULL,
	[Initials] [nvarchar](50) NULL,
	[Pumps] [int] NULL,
	[BackColor] [int] NULL,
	[FontColor] [int] NULL,
	[Bold] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Appt_Types]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Appt_Types](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Appt_Type] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Billers]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Billers](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Biller] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Call_List]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Call_List](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Name] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Call_List_Info]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Call_List_Info](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Call_List] [nvarchar](255) NULL,
	[Active_Date] [datetime] NULL,
	[Last_Contact_Name] [nvarchar](50) NULL,
	[Last_Contact_Date] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Call_List_Numbers]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Call_List_Numbers](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Call_List] [nvarchar](50) NULL,
	[Caller] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
	[Calls] [int] NULL,
	[Contacts] [int] NULL,
	[Letter] [int] NULL,
	[Dead] [int] NULL,
	[Set] [int] NULL,
	[PRO_FOUNDATION] [int] NULL,
	[KC_WATERPROOFING] [int] NULL,
	[VANTANGE] [int] NULL,
	[OLSHAN] [int] NULL,
	[PIER_MASTER] [int] NULL,
	[OTHER] [int] NULL,
	[REFUSED] [int] NULL,
	[Work_Done] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Call_Log]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Call_Log](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Date] [datetime] NULL,
	[Type] [nvarchar](255) NULL,
	[Notes] [nvarchar](255) NULL,
	[Person] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cancel_Numbers]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cancel_Numbers](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Caller] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
	[Calls] [int] NULL,
	[Contacts] [int] NULL,
	[Letter] [int] NULL,
	[Dead] [int] NULL,
	[Set] [int] NULL,
	[PRO_FOUNDATION] [int] NULL,
	[KC_WATERPROOFING] [int] NULL,
	[VANTANGE] [int] NULL,
	[OLSHAN] [int] NULL,
	[PIER_MASTER] [int] NULL,
	[OTHER] [int] NULL,
	[REFUSED] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clients]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Company_Name] [nvarchar](50) NULL,
	[First_Name] [nvarchar](255) NULL,
	[Last_Name] [nvarchar](255) NULL,
	[First_Name2] [nvarchar](50) NULL,
	[Last_Name2] [nvarchar](50) NULL,
	[Phone_Type1] [nvarchar](50) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone_Type2] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Email] [nvarchar](255) NULL,
	[Source] [nvarchar](50) NULL,
	[Lit] [bit] NULL,
	[LitPrint] [bit] NULL,
	[Last_Address] [nvarchar](255) NULL,
	[Plumbing] [bit] NULL,
	[Survey] [bit] NULL,
	[Survey_Date] [datetime] NULL,
	[Survey_Needed] [bit] NULL,
	[Survey_Name] [nvarchar](255) NULL,
	[Survey_Address] [nvarchar](255) NULL,
	[Survey_Estimate_Date] [datetime] NULL,
	[Survey_Telephone] [nvarchar](255) NULL,
	[Survey_Estimator] [nvarchar](255) NULL,
	[Survey_1] [bit] NULL,
	[Survey_2] [bit] NULL,
	[Survey_3] [bit] NULL,
	[Survey_4] [bit] NULL,
	[Survey_5] [bit] NULL,
	[Survey_6] [bit] NULL,
	[Survey_7] [nvarchar](255) NULL,
	[Survey_8] [nvarchar](255) NULL,
	[Survey_9] [nvarchar](255) NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [nvarchar](50) NULL,
	[EmailOK] [bit] NULL,
	[Phone1_Ext] [nvarchar](50) NULL,
	[Phone2_Ext] [nvarchar](50) NULL,
	[Email_Provider] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Custom_Reports]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Custom_Reports](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Name] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[LEVEL] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Dispo_Categories]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dispo_Categories](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Category] [nvarchar](255) NULL,
	[Company] [nvarchar](50) NULL,
	[Placement] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Dispo_Measurements]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dispo_Measurements](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Measurement] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Dry_Dispo]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dry_Dispo](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Offer] [nvarchar](max) NULL,
	[Print_Offer] [nvarchar](255) NULL,
	[Estimator] [nvarchar](255) NULL,
	[Appt_Date] [datetime] NULL,
	[Sold] [bit] NULL,
	[DNS] [bit] NULL,
	[DNQ] [bit] NULL,
	[DNQ_Reason] [nvarchar](255) NULL,
	[RESET] [bit] NULL,
	[RESET_Reason] [nvarchar](255) NULL,
	[Appt_Reset] [bit] NULL,
	[PickUp] [bit] NULL,
	[Cancel] [bit] NULL,
	[Price] [nvarchar](50) NULL,
	[NoWater] [int] NULL,
	[PumpWorks] [int] NULL,
	[PumpWorksPlus] [int] NULL,
	[PumpWorksHD] [int] NULL,
	[WaterNaviGator] [int] NULL,
	[FreezeShield] [int] NULL,
	[WallShield_W] [int] NULL,
	[WallShield_G] [int] NULL,
	[DesertAir] [int] NULL,
	[DesertAirLP] [int] NULL,
	[WindowDrain] [int] NULL,
	[DryShield] [int] NULL,
	[DryShield_F] [int] NULL,
	[DryShield_W] [int] NULL,
	[LightWell] [int] NULL,
	[SurfaceDrain] [int] NULL,
	[Subfloor] [int] NULL,
	[WaterGuard] [int] NULL,
	[Combo] [int] NULL,
	[SuperSump] [int] NULL,
	[Ultra] [int] NULL,
	[TripleSafe] [int] NULL,
	[TrenchGrate] [int] NULL,
	[TrenchDrain] [int] NULL,
	[Lateral] [int] NULL,
	[SunHouse] [int] NULL,
	[WellDucts] [int] NULL,
	[SaniDry] [int] NULL,
	[SaniDryCSB] [int] NULL,
	[Flexispan] [int] NULL,
	[DryWell] [int] NULL,
	[BriteWall] [int] NULL,
	[SmartSump] [int] NULL,
	[CleanSpace] [int] NULL,
	[CleanFloor] [int] NULL,
	[CleanWalls] [int] NULL,
	[DryTrak] [int] NULL,
	[CrackFiller] [int] NULL,
	[SmartDrain] [int] NULL,
	[FloodRail] [int] NULL,
	[HouseHolders] [int] NULL,
	[FloorHolders] [int] NULL,
	[GradeBeam] [int] NULL,
	[WallReplacement] [int] NULL,
	[HHAS] [int] NULL,
	[Straightening] [int] NULL,
	[DrainMatting] [int] NULL,
	[FloodcheckHoses] [int] NULL,
	[FloodRing] [int] NULL,
	[IntegratedHH] [int] NULL,
	[AdjustablePost] [int] NULL,
	[ThermalDryWall] [int] NULL,
	[Anabec] [int] NULL,
	[AnabecFog] [int] NULL,
	[Discharge] [int] NULL,
	[NewFoundation] [int] NULL,
	[Perminator] [int] NULL,
	[RainChuteEZ] [int] NULL,
	[MetalGrate] [int] NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [datetime] NULL,
	[Combined] [bit] NULL,
	[Magic_Dispo_ID] [int] NULL,
	[Foam] [int] NULL,
	[Follow_Up] [bit] NULL,
	[Info_Needed] [bit] NULL,
	[Info_Due_Date] [datetime] NULL,
	[Info_Given] [bit] NULL,
	[Zip] [int] NULL,
	[Source] [nvarchar](50) NULL,
	[Dry_New] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Dry_Production]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dry_Production](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Dispo_ID] [int] NULL,
	[AddOn_ID] [int] NULL,
	[AddOn2_ID] [int] NULL,
	[AddOn3_ID] [int] NULL,
	[Price] [nvarchar](50) NULL,
	[Installed] [nvarchar](max) NULL,
	[Print_Installed] [nvarchar](255) NULL,
	[Estimator] [nvarchar](255) NULL,
	[Appt_Date] [datetime] NULL,
	[Pre-Job] [nvarchar](50) NULL,
	[Foreman] [nvarchar](255) NULL,
	[GLG] [bit] NULL,
	[GLG_Date] [datetime] NULL,
	[Start_Date] [datetime] NULL,
	[Complete_Date] [datetime] NULL,
	[Canceled] [bit] NULL,
	[Cancel_Reason] [nvarchar](255) NULL,
	[NoWater] [int] NULL,
	[PumpWorks] [int] NULL,
	[PumpWorksPlus] [int] NULL,
	[PumpWorksHD] [int] NULL,
	[WaterNaviGator] [int] NULL,
	[FreezeShield] [int] NULL,
	[WallShield_W] [int] NULL,
	[WallShield_G] [int] NULL,
	[DesertAir] [int] NULL,
	[DesertAirLP] [int] NULL,
	[WindowDrain] [int] NULL,
	[DryShield] [int] NULL,
	[DryShield_F] [int] NULL,
	[DryShield_W] [int] NULL,
	[LightWell] [int] NULL,
	[SurfaceDrain] [int] NULL,
	[SuperSump] [int] NULL,
	[Subfloor] [int] NULL,
	[WaterGuard] [int] NULL,
	[Combo] [int] NULL,
	[Ultra] [int] NULL,
	[TripleSafe] [int] NULL,
	[TrenchGrate] [int] NULL,
	[TrenchDrain] [int] NULL,
	[Lateral] [int] NULL,
	[SunHouse] [int] NULL,
	[WellDucts] [int] NULL,
	[SaniDry] [int] NULL,
	[SaniDryCSB] [int] NULL,
	[Flexispan] [int] NULL,
	[DryWell] [int] NULL,
	[BriteWall] [int] NULL,
	[SmartSump] [int] NULL,
	[CleanSpace] [int] NULL,
	[CleanWalls] [int] NULL,
	[CleanFloor] [int] NULL,
	[DryTrak] [int] NULL,
	[CrackFiller] [int] NULL,
	[ThermalDryWall] [int] NULL,
	[SmartDrain] [int] NULL,
	[FloodRail] [int] NULL,
	[HouseHolders] [int] NULL,
	[FloorHolders] [int] NULL,
	[GradeBeam] [int] NULL,
	[WallReplacement] [int] NULL,
	[HHAS] [int] NULL,
	[Straightening] [int] NULL,
	[DrainMatting] [int] NULL,
	[FloodcheckHoses] [int] NULL,
	[FloodRing] [int] NULL,
	[IntegratedHH] [int] NULL,
	[AdjustablePost] [int] NULL,
	[Anabec] [int] NULL,
	[AnabecFog] [int] NULL,
	[Discharge] [int] NULL,
	[NewFoundation] [int] NULL,
	[Perminator] [int] NULL,
	[RainChuteEZ] [int] NULL,
	[MetalGrate] [int] NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [nvarchar](50) NULL,
	[Combined] [bit] NULL,
	[Magic_Production_ID] [int] NULL,
	[Finance_ID] [int] NULL,
	[Complete] [bit] NULL,
	[Advertising_Done] [bit] NULL,
	[Foam] [int] NULL,
	[Zip] [int] NULL,
	[Source] [nvarchar](50) NULL,
	[Dry_New] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Dry_Source_Info]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dry_Source_Info](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Day] [datetime] NULL,
	[SBC] [int] NULL,
	[YELLOW] [int] NULL,
	[VM] [int] NULL,
	[RADIO] [int] NULL,
	[TV] [int] NULL,
	[HOME_SHOW] [int] NULL,
	[FALL_SHOW] [int] NULL,
	[REM_SHOW] [int] NULL,
	[NEWS_PAPER] [int] NULL,
	[WEB] [int] NULL,
	[DH] [int] NULL,
	[YRD_SIGN] [int] NULL,
	[AUTO] [int] NULL,
	[BLDG_SIGN] [int] NULL,
	[LYRIC_OPERA] [int] NULL,
	[REF] [int] NULL,
	[MW] [int] NULL,
	[ANGIES_LIST] [int] NULL,
	[RSVP] [int] NULL,
	[JSM] [int] NULL,
	[BBB] [int] NULL,
	[CSM] [int] NULL,
	[WALK_IN] [int] NULL,
	[OTHER] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employees]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Name] [nvarchar](50) NULL,
	[Display_Name] [nvarchar](50) NULL,
	[Initials] [nvarchar](50) NULL,
	[Group_Name] [nvarchar](50) NULL,
	[Position] [int] NULL,
	[Active] [bit] NULL,
	[Off_Day] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ERROR_LOG1]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ERROR_LOG1](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Error_Descr] [nvarchar](max) NULL,
	[Error_User] [nvarchar](255) NULL,
	[HANDLED] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Est_Info]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Est_Info](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Water] [bit] NULL,
	[Cracks] [bit] NULL,
	[Settling] [bit] NULL,
	[Bowed_Walls] [bit] NULL,
	[Crawl] [bit] NULL,
	[Mold] [bit] NULL,
	[Remodeling] [bit] NULL,
	[Other] [bit] NULL,
	[Other_Text] [nvarchar](255) NULL,
	[Zone] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NULL,
	[Source2] [nvarchar](50) NULL,
	[Estimator_Comments] [nvarchar](max) NULL,
	[Full_Source] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Estimator_Numbers]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estimator_Numbers](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Date] [datetime] NULL,
	[Estimator] [nvarchar](50) NULL,
	[Combined_Appts] [int] NULL,
	[Dry_Appts] [int] NULL,
	[Magic_Appts] [int] NULL,
	[Combined_Leads] [int] NULL,
	[Dry_Leads] [int] NULL,
	[Magic_Leads] [int] NULL,
	[Combined_Reset] [int] NULL,
	[Dry_Reset] [int] NULL,
	[Magic_Reset] [int] NULL,
	[Combined_DNQ] [int] NULL,
	[Dry_DNQ] [int] NULL,
	[Magic_DNQ] [int] NULL,
	[Combined_Sits] [int] NULL,
	[Dry_Sits] [int] NULL,
	[Magic_Sits] [int] NULL,
	[Combined_Sales] [int] NULL,
	[Dry_Sales] [int] NULL,
	[Magic_Sales] [int] NULL,
	[Combined_PickUps] [int] NULL,
	[Dry_PickUps] [int] NULL,
	[Magic_PickUps] [int] NULL,
	[Combined_Volume] [int] NULL,
	[Dry_Volume] [int] NULL,
	[Magic_Volume] [int] NULL,
	[Combined_Cancels] [int] NULL,
	[Dry_Cancels] [int] NULL,
	[Magic_Cancels] [int] NULL,
	[Combined_Cancel_Volume] [int] NULL,
	[Dry_Cancel_Volume] [int] NULL,
	[Magic_Cancel_Volume] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Estimators]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estimators](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Estimator] [nvarchar](255) NULL,
	[Phone] [nvarchar](50) NULL,
	[Working] [bit] NULL,
	[Alias] [nvarchar](50) NULL,
	[Initials] [nvarchar](3) NULL,
	[Senority] [int] NULL,
	[Reval_Letter] [int] NULL,
	[Reval_Gift] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Expenses]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expenses](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Date] [datetime] NULL,
	[Source] [nvarchar](50) NULL,
	[Biller] [nvarchar](50) NULL,
	[Amount] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Fargo]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fargo](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Name] [nvarchar](255) NULL,
	[Charge] [decimal](18, 3) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[File_Locations]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[File_Locations](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Location] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Finance_Types]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Finance_Types](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Type] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Financing]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Financing](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Dry_ID] [int] NULL,
	[Magic_ID] [int] NULL,
	[Main] [bit] NULL,
	[Date] [nvarchar](50) NULL,
	[Type] [nvarchar](255) NULL,
	[Detail] [nvarchar](255) NULL,
	[Additional] [nvarchar](255) NULL,
	[Amount] [int] NULL,
	[Total] [int] NULL,
	[Main_ID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Foreman]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Foreman](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Foreman] [nvarchar](255) NULL,
	[Phone] [nvarchar](50) NULL,
	[Working] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Foundation_Types]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Foundation_Types](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Type] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Inventory]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Item_Code] [nvarchar](255) NULL,
	[Item_Description] [nvarchar](255) NULL,
	[Measurement] [nvarchar](255) NULL,
	[Location] [nvarchar](50) NULL,
	[Quantity] [int] NULL,
	[Company] [nvarchar](50) NULL,
	[Active] [bit] NULL,
	[Price] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Job_Types]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job_Types](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Job_Type] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Magic_Dispo]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Magic_Dispo](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Price] [nvarchar](50) NULL,
	[Print_Offer] [nvarchar](255) NULL,
	[Offer] [nvarchar](max) NULL,
	[Address_ID] [int] NULL,
	[Estimator] [nvarchar](255) NULL,
	[Appt_Date] [datetime] NULL,
	[Sold] [bit] NULL,
	[DNS] [bit] NULL,
	[DNQ] [bit] NULL,
	[DNQ_Reason] [nvarchar](255) NULL,
	[PickUp] [bit] NULL,
	[RESET] [bit] NULL,
	[RESET_Reason] [nvarchar](255) NULL,
	[Appt_Reset] [bit] NULL,
	[Cancel] [bit] NULL,
	[TBF] [int] NULL,
	[BlackCeiling] [int] NULL,
	[Bath] [int] NULL,
	[SaniDryDucted] [int] NULL,
	[SaniDryCSBDucted] [int] NULL,
	[ZenWall] [int] NULL,
	[ThermalDryFlooring] [int] NULL,
	[ThermalDryMatting] [int] NULL,
	[ThermalDryTile] [int] NULL,
	[ThermalDryCarpet] [int] NULL,
	[CrawlOSphere] [int] NULL,
	[VentCovers] [int] NULL,
	[EverlastCrawlDoor] [int] NULL,
	[MagicSunHouse] [int] NULL,
	[EverlastWindows] [int] NULL,
	[EgressWindows] [int] NULL,
	[GlassBlock] [int] NULL,
	[ConcreteBlock] [int] NULL,
	[LawnScape] [int] NULL,
	[CurtainDrain] [int] NULL,
	[FrenchDrain] [int] NULL,
	[Overflow] [int] NULL,
	[BasementFinish] [int] NULL,
	[RR] [int] NULL,
	[ConcreteRR] [int] NULL,
	[MagicWallReplace] [int] NULL,
	[CatchBasin] [int] NULL,
	[MagicSuperSump] [int] NULL,
	[MagicUltra] [int] NULL,
	[MagicRegrade] [int] NULL,
	[DrPlus] [int] NULL,
	[ConPump] [int] NULL,
	[Other] [nvarchar](255) NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [datetime] NULL,
	[Combined] [bit] NULL,
	[Dry_Dispo_ID] [int] NULL,
	[MagicFoam] [int] NULL,
	[Zip] [int] NULL,
	[Source] [nvarchar](50) NULL,
	[Magic_New] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Magic_Production]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Magic_Production](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Dispo_ID] [int] NULL,
	[Price] [nvarchar](50) NULL,
	[Print_Installed] [nvarchar](255) NULL,
	[Installed] [nvarchar](max) NULL,
	[Estimator] [nvarchar](255) NULL,
	[Appt_Date] [datetime] NULL,
	[Pre-Job] [nvarchar](50) NULL,
	[Foreman] [nvarchar](50) NULL,
	[Start_Date] [datetime] NULL,
	[Complete_Date] [datetime] NULL,
	[Canceled] [bit] NULL,
	[Canceled_Reason] [nvarchar](255) NULL,
	[TBF] [int] NULL,
	[BlackCeiling] [int] NULL,
	[SaniDryDucted] [int] NULL,
	[SaniDryCSBDucted] [int] NULL,
	[ZenWall] [int] NULL,
	[ThermalDryFlooring] [int] NULL,
	[ThermalDryMatting] [int] NULL,
	[ThermalDryTile] [int] NULL,
	[ThermalDryCarpet] [int] NULL,
	[CrawlOSphere] [int] NULL,
	[VentCovers] [int] NULL,
	[EverlastCrawlDoor] [int] NULL,
	[MagicSunHouse] [int] NULL,
	[EverlastWindows] [int] NULL,
	[EgressWindows] [int] NULL,
	[GlassBlock] [int] NULL,
	[ConcreteBlock] [int] NULL,
	[LawnScape] [int] NULL,
	[CurtainDrain] [int] NULL,
	[FrenchDrain] [int] NULL,
	[Overflow] [int] NULL,
	[BasementFinish] [int] NULL,
	[RR] [int] NULL,
	[ConcreteRR] [int] NULL,
	[MagicWallReplace] [int] NULL,
	[CatchBasin] [int] NULL,
	[MagicSuperSump] [int] NULL,
	[MagicUltra] [int] NULL,
	[MagicRegrade] [int] NULL,
	[DrPlus] [int] NULL,
	[ConPump] [int] NULL,
	[Bath] [int] NULL,
	[Other] [nvarchar](255) NULL,
	[Last_Modified_On] [datetime] NULL,
	[Combined] [bit] NULL,
	[Dry_Production_ID] [int] NULL,
	[Finance_ID] [int] NULL,
	[Complete] [bit] NULL,
	[MagicFoam] [int] NULL,
	[Zip] [int] NULL,
	[Source] [nvarchar](50) NULL,
	[Magic_New] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Magic_Source_Info]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Magic_Source_Info](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Day] [datetime] NULL,
	[HMAG] [int] NULL,
	[REVAL] [int] NULL,
	[TBF_RS] [int] NULL,
	[TBF_VM] [int] NULL,
	[TBF_TV] [int] NULL,
	[TBF_WEB] [int] NULL,
	[TBF_RADIO] [int] NULL,
	[OTHER] [int] NULL,
	[HOME_SHOW] [int] NULL,
	[RS] [int] NULL,
	[VM] [int] NULL,
	[WEB] [int] NULL,
	[RADIO] [int] NULL,
	[TV] [int] NULL,
	[SETTLERS] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Maintenance_Recieve]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maintenance_Recieve](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Zip] [int] NULL,
	[Card] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[New_Zips]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[New_Zips](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](5) NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Address_Endings]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Address_Endings](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Endings] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Addresses]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Addresses](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Old_ID] [int] NULL,
	[Address_Numbers] [nvarchar](50) NULL,
	[Address_Direction] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[Address_Ending] [nvarchar](50) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [int] NULL,
	[Current_Client_ID] [int] NULL,
	[Prior_Client1_ID] [int] NULL,
	[Last_Estimator_ID] [int] NULL,
	[Foundation] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NULL,
	[Source2] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Prob_Cracks] [bit] NULL,
	[Prob_Water] [bit] NULL,
	[Prob_Bowed] [bit] NULL,
	[Prob_Settling] [bit] NULL,
	[Prob_Crawl] [bit] NULL,
	[Prob_Other] [bit] NULL,
	[Prob_Other_Text] [nvarchar](255) NULL,
	[Installed] [bit] NULL,
	[House_Description] [nvarchar](255) NULL,
	[Dry_Dispo_Done] [bit] NULL,
	[Magic_Dispo_Done] [bit] NULL,
	[Dry_New] [bit] NULL,
	[Magic_New] [bit] NULL,
	[Dry_RESET] [bit] NULL,
	[RESET_Contacted] [bit] NULL,
	[RESET_Name] [nvarchar](255) NULL,
	[RESET_Delay] [datetime] NULL,
	[Magic_RESET] [bit] NULL,
	[RESET_Estimator] [nvarchar](50) NULL,
	[Current_Dry_Dispo] [int] NULL,
	[Current_Dry_Production] [int] NULL,
	[Current_Magic_Dispo] [int] NULL,
	[Current_Magic_Production] [int] NULL,
	[WaterProofing] [bit] NULL,
	[Piering] [bit] NULL,
	[Anchoring] [bit] NULL,
	[Cosmetic] [bit] NULL,
	[Miscellaneous] [bit] NULL,
	[Directions] [nvarchar](max) NULL,
	[Comments] [nvarchar](max) NULL,
	[Current_Appt_Date] [datetime] NULL,
	[Appt_Time] [datetime] NULL,
	[Estimator] [nvarchar](50) NULL,
	[Appt_Type] [nvarchar](255) NULL,
	[Assistance] [bit] NULL,
	[Rep1] [nvarchar](50) NULL,
	[Rep2] [nvarchar](50) NULL,
	[Rep3] [nvarchar](50) NULL,
	[Rep4] [nvarchar](50) NULL,
	[Rep5] [nvarchar](50) NULL,
	[Assistance_Details] [nvarchar](255) NULL,
	[Assistance_Status] [nvarchar](50) NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [nvarchar](50) NULL,
	[Active] [bit] NULL,
	[Bonus_Sig] [nvarchar](255) NULL,
	[Activity] [nvarchar](max) NULL,
	[Confirmed] [bit] NULL,
	[Confirmer] [nvarchar](255) NULL,
	[Set_Date] [datetime] NULL,
	[Real_Appt_Time] [datetime] NULL,
	[Zone] [nvarchar](50) NULL,
	[Reval] [bit] NULL,
	[Reval_Delay] [datetime] NULL,
	[Reval_Contacted] [nvarchar](255) NULL,
	[Reval_Caller] [nvarchar](255) NULL,
	[Reval_Comments] [nvarchar](max) NULL,
	[Reval_Add_Date] [datetime] NULL,
	[CallBack] [nvarchar](255) NULL,
	[Annual] [bit] NULL,
	[Annual_Date] [nvarchar](255) NULL,
	[Next_Annual_Date] [datetime] NULL,
	[Number_Pumps] [nvarchar](50) NULL,
	[Annual_Done] [bit] NULL,
	[Annual_Appt_Date] [datetime] NULL,
	[Annual_Person] [nvarchar](255) NULL,
	[Annual_Appt_Time] [datetime] NULL,
	[Annual_Dispo_Done] [bit] NULL,
	[Annual_ID] [int] NULL,
	[Annual_Active] [bit] NULL,
	[Annual_Contacted] [bit] NULL,
	[Annual_Vacant] [bit] NULL,
	[Annual_Remove] [bit] NULL,
	[Annual_Activity] [nvarchar](max) NULL,
	[Annual_Sent] [datetime] NULL,
	[Plumbing_Card] [bit] NULL,
	[Plumbing_Sent] [datetime] NULL,
	[TBF_Card] [bit] NULL,
	[TBF_Sent] [datetime] NULL,
	[CARD_VACANT] [bit] NULL,
	[CARD_BAD] [bit] NULL,
	[NO_ADVERTISE] [bit] NULL,
	[NO_ADVERTISE_DATE] [datetime] NULL,
	[Canceled] [bit] NULL,
	[Canceled_Date] [datetime] NULL,
	[Full_Source] [nvarchar](50) NULL,
	[File_Location] [nvarchar](50) NULL,
	[NO_SERVICE] [bit] NULL,
	[Follow_Up] [bit] NULL,
	[Reference] [bit] NULL,
	[Cancel_Delay] [datetime] NULL,
	[Cancel_Contacted] [nvarchar](50) NULL,
	[Reval_Letter] [bit] NULL,
	[Reval_Gift] [bit] NULL,
	[Reval_Sent_Date] [datetime] NULL,
	[Reval_Letter_Received] [bit] NULL,
	[Reval_Last_Call_Date] [datetime] NULL,
	[Job_Type] [nvarchar](50) NULL,
	[Otto_Status] [nvarchar](50) NULL,
	[Bad_Date] [bit] NULL,
	[DriveTime] [nvarchar](50) NULL,
	[Miles] [nvarchar](50) NULL,
	[Estimator_Comments] [nvarchar](max) NULL,
	[Prob_Remodel] [bit] NULL,
	[Prob_Mold] [bit] NULL,
	[Cancel_Call] [bit] NULL,
	[Cancel_Work] [bit] NULL,
	[Cancel_Last_Call_Date] [datetime] NULL,
	[Cancel_Comments] [nvarchar](max) NULL,
	[Cancel_Letter_Received] [bit] NULL,
	[Magic_Reval] [bit] NULL,
	[New_Product] [bit] NULL,
	[New_Product_Last_Call_Date] [datetime] NULL,
	[Pump] [bit] NULL,
	[Pump_Last_Call_Date] [datetime] NULL,
	[Pump_Contacted] [nvarchar](50) NULL,
	[Pump_Delay] [datetime] NULL,
	[Pump_Comments] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Clients]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Clients](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Company_Name] [nvarchar](50) NULL,
	[First_Name] [nvarchar](255) NULL,
	[Last_Name] [nvarchar](255) NULL,
	[First_Name2] [nvarchar](50) NULL,
	[Last_Name2] [nvarchar](50) NULL,
	[Phone_Type1] [nvarchar](50) NULL,
	[Phone1] [nvarchar](50) NULL,
	[Phone_Type2] [nvarchar](50) NULL,
	[Phone2] [nvarchar](50) NULL,
	[Email] [nvarchar](255) NULL,
	[Source] [nvarchar](50) NULL,
	[Lit] [bit] NULL,
	[LitPrint] [bit] NULL,
	[Last_Address] [nvarchar](255) NULL,
	[Plumbing] [bit] NULL,
	[Survey] [bit] NULL,
	[Survey_Date] [datetime] NULL,
	[Survey_Needed] [bit] NULL,
	[Survey_Name] [nvarchar](255) NULL,
	[Survey_Address] [nvarchar](255) NULL,
	[Survey_Estimate_Date] [datetime] NULL,
	[Survey_Telephone] [nvarchar](255) NULL,
	[Survey_Estimator] [nvarchar](255) NULL,
	[Survey_1] [bit] NULL,
	[Survey_2] [bit] NULL,
	[Survey_3] [bit] NULL,
	[Survey_4] [bit] NULL,
	[Survey_5] [bit] NULL,
	[Survey_6] [bit] NULL,
	[Survey_7] [nvarchar](255) NULL,
	[Survey_8] [nvarchar](255) NULL,
	[Survey_9] [nvarchar](255) NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [nvarchar](50) NULL,
	[EmailOK] [bit] NULL,
	[Phone1_Ext] [nvarchar](50) NULL,
	[Phone2_Ext] [nvarchar](50) NULL,
	[Email_Provider] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Dry_Dispo]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Dry_Dispo](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Offer] [nvarchar](max) NULL,
	[Print_Offer] [nvarchar](255) NULL,
	[Estimator] [nvarchar](255) NULL,
	[Appt_Date] [datetime] NULL,
	[Sold] [bit] NULL,
	[DNS] [bit] NULL,
	[DNQ] [bit] NULL,
	[DNQ_Reason] [nvarchar](255) NULL,
	[RESET] [bit] NULL,
	[RESET_Reason] [nvarchar](255) NULL,
	[Appt_Reset] [bit] NULL,
	[PickUp] [bit] NULL,
	[Cancel] [bit] NULL,
	[Price] [nvarchar](50) NULL,
	[NoWater] [int] NULL,
	[PumpWorks] [int] NULL,
	[PumpWorksPlus] [int] NULL,
	[PumpWorksHD] [int] NULL,
	[WaterNaviGator] [int] NULL,
	[FreezeShield] [int] NULL,
	[WallShield_W] [int] NULL,
	[WallShield_G] [int] NULL,
	[DesertAir] [int] NULL,
	[DesertAirLP] [int] NULL,
	[WindowDrain] [int] NULL,
	[DryShield] [int] NULL,
	[DryShield_F] [int] NULL,
	[DryShield_W] [int] NULL,
	[LightWell] [int] NULL,
	[SurfaceDrain] [int] NULL,
	[Subfloor] [int] NULL,
	[WaterGuard] [int] NULL,
	[Combo] [int] NULL,
	[SuperSump] [int] NULL,
	[Ultra] [int] NULL,
	[TripleSafe] [int] NULL,
	[TrenchGrate] [int] NULL,
	[TrenchDrain] [int] NULL,
	[Lateral] [int] NULL,
	[SunHouse] [int] NULL,
	[WellDucts] [int] NULL,
	[SaniDry] [int] NULL,
	[SaniDryCSB] [int] NULL,
	[Flexispan] [int] NULL,
	[DryWell] [int] NULL,
	[BriteWall] [int] NULL,
	[SmartSump] [int] NULL,
	[CleanSpace] [int] NULL,
	[CleanFloor] [int] NULL,
	[CleanWalls] [int] NULL,
	[DryTrak] [int] NULL,
	[CrackFiller] [int] NULL,
	[SmartDrain] [int] NULL,
	[FloodRail] [int] NULL,
	[HouseHolders] [int] NULL,
	[FloorHolders] [int] NULL,
	[GradeBeam] [int] NULL,
	[WallReplacement] [int] NULL,
	[HHAS] [int] NULL,
	[Straightening] [int] NULL,
	[DrainMatting] [int] NULL,
	[FloodcheckHoses] [int] NULL,
	[FloodRing] [int] NULL,
	[IntegratedHH] [int] NULL,
	[AdjustablePost] [int] NULL,
	[ThermalDryWall] [int] NULL,
	[Anabec] [int] NULL,
	[AnabecFog] [int] NULL,
	[Discharge] [int] NULL,
	[NewFoundation] [int] NULL,
	[Perminator] [int] NULL,
	[RainChuteEZ] [int] NULL,
	[MetalGrate] [int] NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [datetime] NULL,
	[Combined] [bit] NULL,
	[Magic_Dispo_ID] [int] NULL,
	[Foam] [int] NULL,
	[Follow_Up] [bit] NULL,
	[Info_Needed] [bit] NULL,
	[Info_Due_Date] [datetime] NULL,
	[Info_Given] [bit] NULL,
	[Zip] [int] NULL,
	[Source] [nvarchar](50) NULL,
	[Dry_New] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Dry_Production]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Dry_Production](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Dispo_ID] [int] NULL,
	[AddOn_ID] [int] NULL,
	[AddOn2_ID] [int] NULL,
	[AddOn3_ID] [int] NULL,
	[Price] [nvarchar](50) NULL,
	[Installed] [nvarchar](max) NULL,
	[Print_Installed] [nvarchar](255) NULL,
	[Estimator] [nvarchar](255) NULL,
	[Appt_Date] [datetime] NULL,
	[Pre-Job] [nvarchar](50) NULL,
	[Foreman] [nvarchar](255) NULL,
	[GLG] [bit] NULL,
	[GLG_Date] [datetime] NULL,
	[Start_Date] [datetime] NULL,
	[Complete_Date] [datetime] NULL,
	[Canceled] [bit] NULL,
	[Cancel_Reason] [nvarchar](255) NULL,
	[NoWater] [int] NULL,
	[PumpWorks] [int] NULL,
	[PumpWorksPlus] [int] NULL,
	[PumpWorksHD] [int] NULL,
	[WaterNaviGator] [int] NULL,
	[FreezeShield] [int] NULL,
	[WallShield_W] [int] NULL,
	[WallShield_G] [int] NULL,
	[DesertAir] [int] NULL,
	[DesertAirLP] [int] NULL,
	[WindowDrain] [int] NULL,
	[DryShield] [int] NULL,
	[DryShield_F] [int] NULL,
	[DryShield_W] [int] NULL,
	[LightWell] [int] NULL,
	[SurfaceDrain] [int] NULL,
	[SuperSump] [int] NULL,
	[Subfloor] [int] NULL,
	[WaterGuard] [int] NULL,
	[Combo] [int] NULL,
	[Ultra] [int] NULL,
	[TripleSafe] [int] NULL,
	[TrenchGrate] [int] NULL,
	[TrenchDrain] [int] NULL,
	[Lateral] [int] NULL,
	[SunHouse] [int] NULL,
	[WellDucts] [int] NULL,
	[SaniDry] [int] NULL,
	[SaniDryCSB] [int] NULL,
	[Flexispan] [int] NULL,
	[DryWell] [int] NULL,
	[BriteWall] [int] NULL,
	[SmartSump] [int] NULL,
	[CleanSpace] [int] NULL,
	[CleanWalls] [int] NULL,
	[CleanFloor] [int] NULL,
	[DryTrak] [int] NULL,
	[CrackFiller] [int] NULL,
	[ThermalDryWall] [int] NULL,
	[SmartDrain] [int] NULL,
	[FloodRail] [int] NULL,
	[HouseHolders] [int] NULL,
	[FloorHolders] [int] NULL,
	[GradeBeam] [int] NULL,
	[WallReplacement] [int] NULL,
	[HHAS] [int] NULL,
	[Straightening] [int] NULL,
	[DrainMatting] [int] NULL,
	[FloodcheckHoses] [int] NULL,
	[FloodRing] [int] NULL,
	[IntegratedHH] [int] NULL,
	[AdjustablePost] [int] NULL,
	[Anabec] [int] NULL,
	[AnabecFog] [int] NULL,
	[Discharge] [int] NULL,
	[NewFoundation] [int] NULL,
	[Perminator] [int] NULL,
	[RainChuteEZ] [int] NULL,
	[MetalGrate] [int] NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [nvarchar](50) NULL,
	[Combined] [bit] NULL,
	[Magic_Production_ID] [int] NULL,
	[Finance_ID] [int] NULL,
	[Complete] [bit] NULL,
	[Advertising_Done] [bit] NULL,
	[Foam] [int] NULL,
	[Zip] [int] NULL,
	[Source] [nvarchar](50) NULL,
	[Dry_New] [bit] NULL,
	[Info_ID] [int] NULL,
	[Inventory_Path] [nvarchar](255) NULL,
	[Inventory_Complete] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Estimator_Numbers]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Estimator_Numbers](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Date] [datetime] NULL,
	[Estimator] [nvarchar](50) NULL,
	[Combined_Appts] [int] NULL,
	[Dry_Appts] [int] NULL,
	[Magic_Appts] [int] NULL,
	[Combined_Leads] [int] NULL,
	[Dry_Leads] [int] NULL,
	[Magic_Leads] [int] NULL,
	[Combined_Reset] [int] NULL,
	[Dry_Reset] [int] NULL,
	[Magic_Reset] [int] NULL,
	[Combined_DNQ] [int] NULL,
	[Dry_DNQ] [int] NULL,
	[Magic_DNQ] [int] NULL,
	[Combined_Sits] [int] NULL,
	[Dry_Sits] [int] NULL,
	[Magic_Sits] [int] NULL,
	[Combined_Sales] [int] NULL,
	[Dry_Sales] [int] NULL,
	[Magic_Sales] [int] NULL,
	[Combined_PickUps] [int] NULL,
	[Dry_PickUps] [int] NULL,
	[Magic_PickUps] [int] NULL,
	[Combined_Volume] [int] NULL,
	[Dry_Volume] [int] NULL,
	[Magic_Volume] [int] NULL,
	[Combined_Cancels] [int] NULL,
	[Dry_Cancels] [int] NULL,
	[Magic_Cancels] [int] NULL,
	[Combined_Cancel_Volume] [int] NULL,
	[Dry_Cancel_Volume] [int] NULL,
	[Magic_Cancel_Volume] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Estimators]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Estimators](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Estimator] [nvarchar](255) NULL,
	[Phone] [nvarchar](50) NULL,
	[Working] [bit] NULL,
	[Alias] [nvarchar](50) NULL,
	[Initials] [nvarchar](3) NULL,
	[Senority] [int] NULL,
	[Reval_Letter] [int] NULL,
	[Reval_Gift] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Estimators_Schedule]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Estimators_Schedule](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Date] [datetime] NULL,
	[Time] [datetime] NULL,
	[Person] [nvarchar](255) NULL,
	[Position] [int] NULL,
	[Info] [nvarchar](255) NULL,
	[Operator] [nvarchar](255) NULL,
	[Estimator_Appt] [bit] NULL,
	[Address_ID] [int] NULL,
	[BackColor] [int] NULL,
	[FontColor] [int] NULL,
	[Bold] [bit] NULL,
	[Appt_Type] [nvarchar](255) NULL,
	[Detail] [nvarchar](255) NULL,
	[Extra] [bit] NULL,
	[Extra_Info] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Foreman]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Foreman](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Foreman] [nvarchar](255) NULL,
	[Phone] [nvarchar](50) NULL,
	[Working] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Magic_Dispo]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Magic_Dispo](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Price] [nvarchar](50) NULL,
	[Print_Offer] [nvarchar](255) NULL,
	[Offer] [nvarchar](max) NULL,
	[Address_ID] [int] NULL,
	[Estimator] [nvarchar](255) NULL,
	[Appt_Date] [datetime] NULL,
	[Sold] [bit] NULL,
	[DNS] [bit] NULL,
	[DNQ] [bit] NULL,
	[DNQ_Reason] [nvarchar](255) NULL,
	[PickUp] [bit] NULL,
	[RESET] [bit] NULL,
	[RESET_Reason] [nvarchar](255) NULL,
	[Appt_Reset] [bit] NULL,
	[Cancel] [bit] NULL,
	[TBF] [int] NULL,
	[BlackCeiling] [int] NULL,
	[Bath] [int] NULL,
	[SaniDryDucted] [int] NULL,
	[SaniDryCSBDucted] [int] NULL,
	[ZenWall] [int] NULL,
	[ThermalDryFlooring] [int] NULL,
	[ThermalDryMatting] [int] NULL,
	[ThermalDryTile] [int] NULL,
	[ThermalDryCarpet] [int] NULL,
	[CrawlOSphere] [int] NULL,
	[VentCovers] [int] NULL,
	[EverlastCrawlDoor] [int] NULL,
	[MagicSunHouse] [int] NULL,
	[EverlastWindows] [int] NULL,
	[EgressWindows] [int] NULL,
	[GlassBlock] [int] NULL,
	[ConcreteBlock] [int] NULL,
	[LawnScape] [int] NULL,
	[CurtainDrain] [int] NULL,
	[FrenchDrain] [int] NULL,
	[Overflow] [int] NULL,
	[BasementFinish] [int] NULL,
	[RR] [int] NULL,
	[ConcreteRR] [int] NULL,
	[MagicWallReplace] [int] NULL,
	[CatchBasin] [int] NULL,
	[MagicSuperSump] [int] NULL,
	[MagicUltra] [int] NULL,
	[MagicRegrade] [int] NULL,
	[DrPlus] [int] NULL,
	[ConPump] [int] NULL,
	[Other] [nvarchar](255) NULL,
	[Last_Modified_By] [nvarchar](50) NULL,
	[Last_Modified_On] [datetime] NULL,
	[Combined] [bit] NULL,
	[Dry_Dispo_ID] [int] NULL,
	[MagicFoam] [int] NULL,
	[Zip] [int] NULL,
	[Source] [nvarchar](50) NULL,
	[Magic_New] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Magic_Production]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Magic_Production](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Address_ID] [int] NULL,
	[Dispo_ID] [int] NULL,
	[Price] [nvarchar](50) NULL,
	[Print_Installed] [nvarchar](255) NULL,
	[Installed] [nvarchar](max) NULL,
	[Estimator] [nvarchar](255) NULL,
	[Appt_Date] [datetime] NULL,
	[Pre-Job] [nvarchar](50) NULL,
	[Foreman] [nvarchar](50) NULL,
	[Start_Date] [datetime] NULL,
	[Complete_Date] [datetime] NULL,
	[Canceled] [bit] NULL,
	[Canceled_Reason] [nvarchar](255) NULL,
	[TBF] [int] NULL,
	[BlackCeiling] [int] NULL,
	[SaniDryDucted] [int] NULL,
	[SaniDryCSBDucted] [int] NULL,
	[ZenWall] [int] NULL,
	[ThermalDryFlooring] [int] NULL,
	[ThermalDryMatting] [int] NULL,
	[ThermalDryTile] [int] NULL,
	[ThermalDryCarpet] [int] NULL,
	[CrawlOSphere] [int] NULL,
	[VentCovers] [int] NULL,
	[EverlastCrawlDoor] [int] NULL,
	[MagicSunHouse] [int] NULL,
	[EverlastWindows] [int] NULL,
	[EgressWindows] [int] NULL,
	[GlassBlock] [int] NULL,
	[ConcreteBlock] [int] NULL,
	[LawnScape] [int] NULL,
	[CurtainDrain] [int] NULL,
	[FrenchDrain] [int] NULL,
	[Overflow] [int] NULL,
	[BasementFinish] [int] NULL,
	[RR] [int] NULL,
	[ConcreteRR] [int] NULL,
	[MagicWallReplace] [int] NULL,
	[CatchBasin] [int] NULL,
	[MagicSuperSump] [int] NULL,
	[MagicUltra] [int] NULL,
	[MagicRegrade] [int] NULL,
	[DrPlus] [int] NULL,
	[ConPump] [int] NULL,
	[Bath] [int] NULL,
	[Other] [nvarchar](255) NULL,
	[Last_Modified_On] [datetime] NULL,
	[Combined] [bit] NULL,
	[Dry_Production_ID] [int] NULL,
	[Finance_ID] [int] NULL,
	[Complete] [bit] NULL,
	[MagicFoam] [int] NULL,
	[Zip] [int] NULL,
	[Source] [nvarchar](50) NULL,
	[Magic_New] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_New_Zips]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_New_Zips](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](5) NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Operators]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Operators](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Log_In] [nvarchar](255) NULL,
	[Current] [bit] NULL,
	[Name] [nvarchar](255) NULL,
	[Level] [nvarchar](50) NULL,
	[Logged_On] [bit] NULL,
	[Search1] [nvarchar](50) NULL,
	[Search2] [nvarchar](50) NULL,
	[Search3] [nvarchar](50) NULL,
	[Search4] [nvarchar](50) NULL,
	[Search5] [nvarchar](50) NULL,
	[Search6] [nvarchar](50) NULL,
	[Search7] [nvarchar](50) NULL,
	[Search8] [nvarchar](50) NULL,
	[Form_Height] [nvarchar](50) NULL,
	[Form_Width] [nvarchar](50) NULL,
	[Printer] [nvarchar](50) NULL,
	[PING] [bit] NULL,
	[PING_Message] [nvarchar](255) NULL,
	[Shut_Out] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Schedule]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Schedule](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Date] [datetime] NULL,
	[Start_Time] [datetime] NULL,
	[End_Time] [datetime] NULL,
	[Person] [nvarchar](255) NULL,
	[Info] [nvarchar](255) NULL,
	[Operator] [nvarchar](255) NULL,
	[Estimator_Appt] [bit] NULL,
	[Address_ID] [int] NULL,
	[BackColor] [nvarchar](50) NULL,
	[FontColor] [nvarchar](50) NULL,
	[Bold] [bit] NULL,
	[Appt_Type] [nvarchar](255) NULL,
	[Detail] [nvarchar](255) NULL,
	[Extra] [bit] NULL,
	[Extra_Info] [nvarchar](255) NULL,
	[Bonus_Sig] [nvarchar](50) NULL,
	[Confirmer] [nvarchar](50) NULL,
	[Dispo_Done] [bit] NULL,
	[Confirmed] [bit] NULL,
	[Set_Date] [datetime] NULL,
	[Always_Appt] [bit] NULL,
	[Annual_Appt] [bit] NULL,
	[Canceled] [bit] NULL,
	[Source] [nvarchar](50) NULL,
	[Source2] [nvarchar](50) NULL,
	[Filler] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Source]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Source](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Source] [nvarchar](255) NULL,
	[Company] [nvarchar](255) NULL,
	[Biller1] [nvarchar](50) NULL,
	[Biller2] [nvarchar](50) NULL,
	[Biller3] [nvarchar](50) NULL,
	[Biller4] [nvarchar](50) NULL,
	[Biller5] [nvarchar](50) NULL,
	[Biller6] [nvarchar](50) NULL,
	[Biller7] [nvarchar](50) NULL,
	[Biller8] [nvarchar](50) NULL,
	[Biller9] [nvarchar](50) NULL,
	[Biller10] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Source2]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Source2](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Source] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Old_Zips]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Old_Zips](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](5) NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Operators]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operators](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Log_In] [nvarchar](255) NULL,
	[Current] [bit] NULL,
	[Name] [nvarchar](255) NULL,
	[Level] [nvarchar](50) NULL,
	[Logged_On] [bit] NULL,
	[Search1] [nvarchar](50) NULL,
	[Search2] [nvarchar](50) NULL,
	[Search3] [nvarchar](50) NULL,
	[Search4] [nvarchar](50) NULL,
	[Search5] [nvarchar](50) NULL,
	[Search6] [nvarchar](50) NULL,
	[Search7] [nvarchar](50) NULL,
	[Search8] [nvarchar](50) NULL,
	[Form_Height] [nvarchar](50) NULL,
	[Form_Width] [nvarchar](50) NULL,
	[Printer] [nvarchar](50) NULL,
	[PING] [bit] NULL,
	[PING_Message] [nvarchar](255) NULL,
	[Shut_Out] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Paste Errors]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Paste Errors](
	[Field0] [nvarchar](max) NULL,
	[Field1] [nvarchar](max) NULL,
	[Field2] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payments]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payments](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Production_ID] [int] NULL,
	[Payment] [int] NULL,
	[Date] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Problems]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Problems](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Problem] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product_Inventory_Cost]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Inventory_Cost](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Product_ID] [nvarchar](50) NULL,
	[Item1] [nvarchar](255) NULL,
	[Item1_Quanity] [int] NULL,
	[Item1_Measurement] [nvarchar](255) NULL,
	[Item2] [nvarchar](50) NULL,
	[Item2_Quanity] [int] NULL,
	[Item2_Measurement] [nvarchar](50) NULL,
	[Item3] [nvarchar](255) NULL,
	[Item3_Quanity] [int] NULL,
	[Item3_Measurement] [nvarchar](50) NULL,
	[Item4] [nvarchar](255) NULL,
	[Item4_Quanity] [int] NULL,
	[Item4_Measurement] [nvarchar](50) NULL,
	[Item5] [nvarchar](255) NULL,
	[Item5_Quanity] [int] NULL,
	[Item5_Measurement] [nvarchar](50) NULL,
	[Item6] [nvarchar](255) NULL,
	[Item6_Quanity] [int] NULL,
	[Item6_Measurement] [nvarchar](50) NULL,
	[Item7] [nvarchar](255) NULL,
	[Item7_Quanity] [int] NULL,
	[Item7_Measurement] [nvarchar](50) NULL,
	[Item8] [nvarchar](255) NULL,
	[Item8_Quanity] [int] NULL,
	[Item8_Measurement] [nvarchar](50) NULL,
	[Item9] [nvarchar](255) NULL,
	[Item9_Quanity] [int] NULL,
	[Item9_Measurement] [nvarchar](50) NULL,
	[Item10] [nvarchar](255) NULL,
	[Item10_Quanity] [int] NULL,
	[Item10_Measurement] [nvarchar](50) NULL,
	[Item11] [nvarchar](255) NULL,
	[Item11_Quanity] [int] NULL,
	[Item11_Measurement] [nvarchar](50) NULL,
	[Item12] [nvarchar](50) NULL,
	[Item12_Quanity] [int] NULL,
	[Item12_Measurement] [nvarchar](50) NULL,
	[Item13] [nvarchar](50) NULL,
	[Item13_Quanity] [int] NULL,
	[Item13_Measurement] [nvarchar](50) NULL,
	[Item14] [nvarchar](50) NULL,
	[Item14_Quanity] [int] NULL,
	[Item14_Measurement] [nvarchar](50) NULL,
	[Item15] [nvarchar](50) NULL,
	[Item15_Quanity] [int] NULL,
	[Item15_Measurement] [nvarchar](50) NULL,
	[Item16] [nvarchar](50) NULL,
	[Item16_Quanity] [int] NULL,
	[Item16_Measurement] [nvarchar](50) NULL,
	[Item17] [nvarchar](50) NULL,
	[Item17_Quanity] [int] NULL,
	[Item17_Measurement] [nvarchar](50) NULL,
	[Item18] [nvarchar](50) NULL,
	[Item18_Quanity] [int] NULL,
	[Item18_Measurement] [nvarchar](50) NULL,
	[Item19] [nvarchar](50) NULL,
	[Item19_Quanity] [int] NULL,
	[Item19_Measurement] [nvarchar](50) NULL,
	[Item20] [nvarchar](50) NULL,
	[Item20_Quanity] [int] NULL,
	[Item20_Measurement] [nvarchar](50) NULL,
	[Item21] [nvarchar](50) NULL,
	[Item21_Quanity] [int] NULL,
	[Item21_Measurement] [nvarchar](50) NULL,
	[Item22] [nvarchar](50) NULL,
	[Item22_Quanity] [int] NULL,
	[Item22_Measurement] [nvarchar](50) NULL,
	[Item23] [nvarchar](50) NULL,
	[Item23_Quanity] [int] NULL,
	[Item23_Measurement] [nvarchar](50) NULL,
	[Item24] [nvarchar](50) NULL,
	[Item24_Quanity] [int] NULL,
	[Item24_Measurement] [nvarchar](50) NULL,
	[Item25] [nvarchar](50) NULL,
	[Item25_Quanity] [int] NULL,
	[Item25_Measurement] [nvarchar](50) NULL,
	[Item26] [nvarchar](50) NULL,
	[Item26_Quanity] [int] NULL,
	[Item26_Measurement] [nvarchar](50) NULL,
	[Item27] [nvarchar](50) NULL,
	[Item27_Quanity] [int] NULL,
	[Item27_Measurement] [nvarchar](50) NULL,
	[Item28] [nvarchar](50) NULL,
	[Item28_Quanity] [int] NULL,
	[Item28_Measurement] [nvarchar](50) NULL,
	[Item29] [nvarchar](50) NULL,
	[Item29_Quanity] [int] NULL,
	[Item29_Measurement] [nvarchar](50) NULL,
	[Item30] [nvarchar](50) NULL,
	[Item30_Quanity] [int] NULL,
	[Item30_Measurement] [nvarchar](50) NULL,
	[Item31] [nvarchar](50) NULL,
	[Item31_Quanity] [int] NULL,
	[Item31_Measurement] [nvarchar](50) NULL,
	[Item32] [nvarchar](50) NULL,
	[Item32_Quanity] [int] NULL,
	[Item32_Measurement] [nvarchar](50) NULL,
	[Item33] [nvarchar](50) NULL,
	[Item33_Quanity] [int] NULL,
	[Item33_Measurement] [nvarchar](50) NULL,
	[Item34] [nvarchar](50) NULL,
	[Item34_Quanity] [int] NULL,
	[Item34_Measurement] [nvarchar](50) NULL,
	[Item35] [nvarchar](50) NULL,
	[Item35_Quanity] [int] NULL,
	[Item35_Measurement] [nvarchar](50) NULL,
	[Item36] [nvarchar](50) NULL,
	[Item36_Quanity] [int] NULL,
	[Item36_Measurement] [nvarchar](50) NULL,
	[Item37] [nvarchar](50) NULL,
	[Item37_Quanity] [int] NULL,
	[Item37_Measurement] [nvarchar](50) NULL,
	[Item38] [nvarchar](50) NULL,
	[Item38_Quanity] [int] NULL,
	[Item38_Measurement] [nvarchar](50) NULL,
	[Item39] [nvarchar](50) NULL,
	[Item39_Quanity] [int] NULL,
	[Item39_Measurement] [nvarchar](50) NULL,
	[Item40] [nvarchar](50) NULL,
	[Item40_Quanity] [int] NULL,
	[Item40_Measurement] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Production_Info]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Production_Info](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Production_ID] [int] NULL,
	[PreJob] [bit] NULL,
	[PreJobPerson] [nvarchar](50) NULL,
	[DigRite] [bit] NULL,
	[DigRitePerson] [nvarchar](50) NULL,
	[Cert] [bit] NULL,
	[CertPerson] [nvarchar](50) NULL,
	[Finance] [bit] NULL,
	[FinancePerson] [nvarchar](50) NULL,
	[Permits] [bit] NULL,
	[PermitsPerson] [nvarchar](50) NULL,
	[WaterWarranty] [bit] NULL,
	[WaterPerson] [nvarchar](50) NULL,
	[AnchorWarranty] [bit] NULL,
	[AnchorPerson] [nvarchar](50) NULL,
	[HHWarranty] [bit] NULL,
	[HHPerson] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Product] [nvarchar](255) NULL,
	[Product_Field] [nvarchar](255) NULL,
	[Company] [nvarchar](255) NULL,
	[Abbr] [nvarchar](50) NULL,
	[Measurement] [nvarchar](50) NULL,
	[Price] [int] NULL,
	[Category] [nvarchar](50) NULL,
	[Active] [bit] NULL,
	[Placement] [int] NULL,
	[Inventory_ID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Pump_Numbers]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pump_Numbers](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Caller] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
	[Calls] [int] NULL,
	[Contacts] [int] NULL,
	[Letter] [int] NULL,
	[Dead] [int] NULL,
	[Set] [int] NULL,
	[PRO_FOUNDATION] [int] NULL,
	[KC_WATERPROOFING] [int] NULL,
	[VANTANGE] [int] NULL,
	[OLSHAN] [int] NULL,
	[PIER_MASTER] [int] NULL,
	[OTHER] [int] NULL,
	[REFUSED] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Report_Items]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Report_Items](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Field_Name] [nvarchar](255) NULL,
	[Table] [nvarchar](255) NULL,
	[Data_Type] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Report_Names]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Report_Names](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Report_Name] [nvarchar](255) NULL,
	[Report_Filter] [nvarchar](max) NULL,
	[Active] [bit] NULL,
	[Position] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Reval_Numbers]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reval_Numbers](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Caller] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
	[Calls] [int] NULL,
	[Contacts] [int] NULL,
	[Letter] [int] NULL,
	[Dead] [int] NULL,
	[Set] [int] NULL,
	[PRO_FOUNDATION] [int] NULL,
	[KC_WATERPROOFING] [int] NULL,
	[VANTANGE] [int] NULL,
	[OLSHAN] [int] NULL,
	[PIER_MASTER] [int] NULL,
	[OTHER] [int] NULL,
	[REFUSED] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sales_Goal]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sales_Goal](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Month] [nvarchar](50) NULL,
	[Year] [int] NULL,
	[Goal] [int] NULL,
	[OFF1] [datetime] NULL,
	[OFF2] [datetime] NULL,
	[OFF3] [datetime] NULL,
	[OFF4] [datetime] NULL,
	[OFF5] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Schedule]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Date] [datetime] NULL,
	[Start_Time] [datetime] NULL,
	[End_Time] [datetime] NULL,
	[Person] [nvarchar](255) NULL,
	[Info] [nvarchar](255) NULL,
	[Operator] [nvarchar](255) NULL,
	[Estimator_Appt] [bit] NULL,
	[Address_ID] [int] NULL,
	[BackColor] [int] NULL,
	[FontColor] [int] NULL,
	[Bold] [bit] NULL,
	[Appt_Type] [nvarchar](255) NULL,
	[Detail] [nvarchar](255) NULL,
	[Extra] [bit] NULL,
	[Extra_Info] [nvarchar](255) NULL,
	[Bonus_Sig] [nvarchar](50) NULL,
	[Confirmer] [nvarchar](50) NULL,
	[Dispo_Done] [bit] NULL,
	[Confirmed] [bit] NULL,
	[Set_Date] [datetime] NULL,
	[Always_Appt] [bit] NULL,
	[Annual_Appt] [bit] NULL,
	[Canceled] [bit] NULL,
	[Source] [nvarchar](50) NULL,
	[Source2] [nvarchar](50) NULL,
	[Filler] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Schedule_Flags]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule_Flags](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Estimator_Busy] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Schedule_Groups]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule_Groups](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Group] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Schedule_Rules]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule_Rules](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Date_Operator1] [nvarchar](255) NULL,
	[Date1] [datetime] NULL,
	[Date_Operator2] [nvarchar](255) NULL,
	[Date2] [datetime] NULL,
	[Person] [nvarchar](255) NULL,
	[Time_Operator1] [nvarchar](255) NULL,
	[Time1] [datetime] NULL,
	[Time_Operator2] [nvarchar](255) NULL,
	[Time2] [datetime] NULL,
	[Appt_Type_Operator] [nvarchar](255) NULL,
	[Appt_Type] [nvarchar](255) NULL,
	[Message] [nvarchar](255) NULL,
	[Delete_Date] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Source]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Source](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Source] [nvarchar](255) NULL,
	[Company] [nvarchar](255) NULL,
	[Biller1] [nvarchar](50) NULL,
	[Biller2] [nvarchar](50) NULL,
	[Biller3] [nvarchar](50) NULL,
	[Biller4] [nvarchar](50) NULL,
	[Biller5] [nvarchar](50) NULL,
	[Biller6] [nvarchar](50) NULL,
	[Biller7] [nvarchar](50) NULL,
	[Biller8] [nvarchar](50) NULL,
	[Biller9] [nvarchar](50) NULL,
	[Biller10] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Source2]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Source2](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[Source] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[UserName] [nvarchar](255) NULL,
	[Password] [nvarchar](50) NULL,
	[Current] [bit] NULL,
	[Name] [nvarchar](255) NULL,
	[Level] [nvarchar](50) NULL,
	[Logged_On] [bit] NULL,
	[Search1] [nvarchar](50) NULL,
	[Search2] [nvarchar](50) NULL,
	[Search3] [nvarchar](50) NULL,
	[Search4] [nvarchar](50) NULL,
	[Search5] [nvarchar](50) NULL,
	[Search6] [nvarchar](50) NULL,
	[Search7] [nvarchar](50) NULL,
	[Search8] [nvarchar](50) NULL,
	[Form_Height] [nvarchar](50) NULL,
	[Form_Width] [nvarchar](50) NULL,
	[Printer] [nvarchar](50) NULL,
	[PING] [bit] NULL,
	[PING_Message] [nvarchar](255) NULL,
	[Shut_Out] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Zips]    Script Date: 9/27/2013 3:46:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Zips](
	[ID] [int] IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](5) NULL,
	[Default] [bit] NOT NULL
) ON [PRIMARY]



GO
/****** Object:  View [dbo].[Search]    Script Date: 9/26/2013 4:38:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Search]
AS
SELECT
 c.ID
,c.Company_Name
,c.First_Name
,c.Last_Name
,c.First_Name2
,c.Last_Name2
,c.Phone_Type1
,c.Phone1
,c.Phone1_Ext
,c.Email
,c.Source
,a.ID AS AddressId
,a.Address_Numbers
,a.Address_Direction
,a.Address
,a.Address_Ending
,a.City
,a.State
,a.Zip
,a.Foundation

FROM
dbo.Addresses a
INNER JOIN dbo.Clients c ON a.Current_Client_ID = c.ID

GO


CREATE VIEW [dbo].[InventoryReportMagicDispo]
AS
select
	 a.ID AddressId
	,c.ID ClientId
	,d.ID MagicDispoId
	,c.First_Name
	,c.Last_Name
	,c.First_Name2
	,c.Last_Name2
	,c.Company_Name
	,c.Phone1
	,c.Phone1_Ext
	,a.Address_Numbers
	,a.Address_Direction
	,a.[Address]
	,a.Address_Ending
	,a.City
	,a.[State]
	,a.Zip AddressZip
	
	,d.[Price]
	,d.[Print_Offer]
	,d.[Offer]
	,d.[Estimator]
	,d.[Appt_Date]
	,d.[Sold]
	,d.[DNS]
	,d.[DNQ]
	,d.[DNQ_Reason]
	,d.[PickUp]
	,d.[RESET]
	,d.[RESET_Reason]
	,d.[Appt_Reset]
	,d.[Cancel]
	,d.[TBF]
	,d.[BlackCeiling]
	,d.[Bath]
	,d.[SaniDryDucted]
	,d.[SaniDryCSBDucted]
	,d.[ZenWall]
	,d.[ThermalDryFlooring]
	,d.[ThermalDryMatting]
	,d.[ThermalDryTile]
	,d.[ThermalDryCarpet]
	,d.[CrawlOSphere]
	,d.[VentCovers]
	,d.[EverlastCrawlDoor]
	,d.[MagicSunHouse]
	,d.[EverlastWindows]
	,d.[EgressWindows]
	,d.[GlassBlock]
	,d.[ConcreteBlock]
	,d.[LawnScape]
	,d.[CurtainDrain]
	,d.[FrenchDrain]
	,d.[Overflow]
	,d.[BasementFinish]
	,d.[RR]
	,d.[ConcreteRR]
	,d.[MagicWallReplace]
	,d.[CatchBasin]
	,d.[MagicSuperSump]
	,d.[MagicUltra]
	,d.[MagicRegrade]
	,d.[DrPlus]
	,d.[ConPump]
	,d.[Other]
	,d.[Last_Modified_By]
	,d.[Last_Modified_On]
	,d.[Combined]
	,d.[Dry_Dispo_ID]
	,d.[MagicFoam]
	,d.[Zip] DispoZip
	,d.[Source]
	,d.[Magic_New]

from
	dbo.Addresses a
	join dbo.Clients c on a.Current_Client_ID = c.ID
	join dbo.Magic_Dispo d on a.ID = d.Address_ID

GO



CREATE VIEW [dbo].[InventoryReportMagicProduction]
AS
select
	 a.ID AddressId
	,c.ID ClientId
	,p.ID MagicProductionId
	,c.First_Name
	,c.Last_Name
	,c.First_Name2
	,c.Last_Name2
	,c.Company_Name
	,c.Phone1
	,c.Phone1_Ext
	,a.Address_Numbers
	,a.Address_Direction
	,a.[Address]
	,a.Address_Ending
	,a.City
	,a.[State]
	,a.Zip AddressZip
	
	,p.[Dispo_ID]
	,p.[Price]
	,p.[Print_Installed]
	,p.[Installed]
	,p.[Estimator]
	,p.[Appt_Date]
	,p.[Pre-Job]
	,p.[Foreman]
	,p.[Start_Date]
	,p.[Complete_Date]
	,p.[Canceled]
	,p.[Canceled_Reason]
	,p.[TBF]
	,p.[BlackCeiling]
	,p.[SaniDryDucted]
	,p.[SaniDryCSBDucted]
	,p.[ZenWall]
	,p.[ThermalDryFlooring]
	,p.[ThermalDryMatting]
	,p.[ThermalDryTile]
	,p.[ThermalDryCarpet]
	,p.[CrawlOSphere]
	,p.[VentCovers]
	,p.[EverlastCrawlDoor]
	,p.[MagicSunHouse]
	,p.[EverlastWindows]
	,p.[EgressWindows]
	,p.[GlassBlock]
	,p.[ConcreteBlock]
	,p.[LawnScape]
	,p.[CurtainDrain]
	,p.[FrenchDrain]
	,p.[Overflow]
	,p.[BasementFinish]
	,p.[RR]
	,p.[ConcreteRR]
	,p.[MagicWallReplace]
	,p.[CatchBasin]
	,p.[MagicSuperSump]
	,p.[MagicUltra]
	,p.[MagicRegrade]
	,p.[DrPlus]
	,p.[ConPump]
	,p.[Bath]
	,p.[Other]
	,p.[Last_Modified_On]
	,p.[Combined]
	,p.[Dry_Production_ID]
	,p.[Finance_ID]
	,p.[Complete]
	,p.[MagicFoam]
	,p.[Zip] ProductionZip
	,p.[Source]
	,p.[Magic_New]

from
	dbo.Addresses a
	join dbo.Clients c on a.Current_Client_ID = c.ID
	join dbo.Magic_Production p on a.ID = p.Address_ID

GO
